package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Akbar
 */

public class ZendeskPojo implements Serializable
{
    /*"errNum":21,
"errFlag":"0",
"errMsg":"Got the details!",
"response":{

    "results":[]
    }*/
    private String errFlag;
    private String errMsg;

    public String getErrNum() {
        return errNum;
    }

    private String errNum;

    public ResponseZendesk getResponse() {
        return response;
    }

    private ResponseZendesk response;

    public String getErrMsg() {
        return errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }
}
