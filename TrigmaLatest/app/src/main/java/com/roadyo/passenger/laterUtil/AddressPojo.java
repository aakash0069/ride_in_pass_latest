package com.roadyo.passenger.laterUtil;

import java.io.Serializable;

/**
 * Created by embed on 17/10/16.
 */
public class AddressPojo implements Serializable {

    private String PLACE_ID ;
    private String LATITUDE ;
    private String LONGITUDE ;
    private String FORMATED_ADDRESS ;
    private String ADDRESS_TYPE;
    private String USER_ID ;
    private String FLAT_NO_OR_HOUSE_NO ;
    private String LANDMARK ;

    public String getPLACE_ID() {
        return PLACE_ID;
    }

    public void setPLACE_ID(String PLACE_ID) {
        this.PLACE_ID = PLACE_ID;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getFORMATED_ADDRESS() {
        return FORMATED_ADDRESS;
    }

    public void setFORMATED_ADDRESS(String FORMATED_ADDRESS) {
        this.FORMATED_ADDRESS = FORMATED_ADDRESS;
    }

    public String getADDRESS_TYPE() {
        return ADDRESS_TYPE;
    }

    public void setADDRESS_TYPE(String ADDRESS_TYPE) {
        this.ADDRESS_TYPE = ADDRESS_TYPE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getFLAT_NO_OR_HOUSE_NO() {
        return FLAT_NO_OR_HOUSE_NO;
    }

    public void setFLAT_NO_OR_HOUSE_NO(String FLAT_NO_OR_HOUSE_NO) {
        this.FLAT_NO_OR_HOUSE_NO = FLAT_NO_OR_HOUSE_NO;
    }

    public String getLANDMARK() {
        return LANDMARK;
    }

    public void setLANDMARK(String LANDMARK) {
        this.LANDMARK = LANDMARK;
    }
}
