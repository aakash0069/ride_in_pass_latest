package com.roadyo.passenger.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.bloomingdalelimousine.ridein.R;
import com.roadyo.passenger.main.MainActivity;
import com.roadyo.passenger.pojo.LiveBookingResponse;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by embed on 25/8/16.
 *
 */
public class Cancel_Adapter extends RecyclerView.Adapter
{
    private Context mcontext;
    private ArrayList<String> cancelReasonsList;
    private ProgressDialog dialogL;
    private String getCancelResponse;
    private SessionManager sessionManager;
    private ArrayList<ImageView> buttons;

    public Cancel_Adapter(Context mcontext, ArrayList<String> cancelReasonsList)
    {
        this.mcontext = mcontext;
        this.cancelReasonsList = cancelReasonsList;
        sessionManager=new SessionManager(mcontext);
        buttons= new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.cancel_row,parent,false);
        return new ViewHldr(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof  ViewHldr)
        {
            final ViewHldr hldr = (ViewHldr) holder;
            hldr.mText.setText(cancelReasonsList.get(position));

            buttons.add(hldr.arrowImage);

            hldr.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetAll();
                    hldr.arrowImage.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void resetAll() {
        for (int i=0;i<buttons.size();i++)
        {
            buttons.get(i).setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        Utility.printLog("size of list in adapetr "+cancelReasonsList.size());
        return cancelReasonsList.size();
    }

    private class ViewHldr extends RecyclerView.ViewHolder {

        ImageView arrowImage;
        TextView mText;
        RelativeLayout linearLayout;

        ViewHldr(View inflate) {
            super(inflate);
            mText = (TextView) inflate.findViewById(R.id.cancel_reason_text);
            arrowImage = (ImageView) inflate.findViewById(R.id.arrowImage);
            linearLayout= (RelativeLayout) inflate.findViewById(R.id.reason_layout);

            if(sessionManager.getLanguageCode().equals("en"))
            {
                Utility.setTypefaceMuliRegular(mcontext,mText);
            }
        }
    }


    public void submitOnclick()
    {
        String reason="";
        for(int i=0;i<buttons.size();i++)
        {
            if(buttons.get(i).getVisibility()==View.VISIBLE)
            {
                Utility.printLog("buttons size "+buttons.get(i)+" cancel reason list size "+cancelReasonsList.get(i));
                reason=cancelReasonsList.get(i);
            }
        }
        if(!reason.equals(""))
        {
            CancelAppointment(reason);
        }
        else
        {
            Utility.showToast(mcontext,mcontext.getResources().getString(R.string.select_reason));
        }
    }
    private void CancelAppointment(String reason)
    {
        dialogL= Utility.GetProcessDialogNew((Activity) mcontext, mcontext.getResources().getString(R.string.cancelling_trip));
        dialogL.setCancelable(false);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {

            //aakash change
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",sessionManager.getSessionToken());
            jsonObject.put("ent_dev_id", sessionManager.getDeviceId());
            jsonObject.put("ent_booking_id", sessionManager.getBookingId());
            jsonObject.put("ent_mid",/*"123"*/ sessionManager.getMid());
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_reason", reason);
            Utility.printLog("params to cancel "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(mcontext.getString(R.string.BASE_URL)+"cancelAppointment", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String response)
            {
                getCancelResponse = response;
                Utility.printLog("response for the cancel "+response);
                getCancelInfo();
            }

            @Override
            public void onError(String error) {
                if (dialogL != null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Utility.showToast(mcontext,mcontext.getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void getCancelInfo()
    {
        Gson gson = new Gson();
        LiveBookingResponse cancelBooking = gson.fromJson(getCancelResponse, LiveBookingResponse.class);
        if(cancelBooking !=null)
        {
            if(cancelBooking.getErrFlag().equals("0"))
            {
                sessionManager.setBookingReponseFromPubnub("");
                sessionManager.setPubnubResponse(sessionManager.getPubnubResponseForNonbooking());
                sessionManager.setDriverOnWay(false);
                sessionManager.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                sessionManager.setDriverArrived(false);
                sessionManager.setTripBegin(false);
                sessionManager.setInvoiceRaised(false);
                sessionManager.storeAptDate(null);
                Utility.showToast(mcontext,cancelBooking.getErrMsg());
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(mcontext, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mcontext.startActivity(i);
                ((Activity)mcontext).overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);

            }

            else if(cancelBooking.getErrFlag().equals("1"))
            {
                sessionManager.setBookingReponseFromPubnub("");
                sessionManager.setPubnubResponse(sessionManager.getPubnubResponseForNonbooking());
                sessionManager.setDriverOnWay(false);
                sessionManager.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                sessionManager.setDriverArrived(false);
                sessionManager.setTripBegin(false);
                sessionManager.setInvoiceRaised(false);
                sessionManager.storeAptDate(null);
                Utility.ShowAlert(cancelBooking.getErrMsg(),mcontext);
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(mcontext, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mcontext.startActivity(i);
                ((Activity)mcontext).overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);

            }
        }
    }

}
