package com.roadyo.passenger.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import org.json.JSONException;
import org.json.JSONObject;

public class PasswordUpdateActivity extends AppCompatActivity {

    private Button proceed;
    private TextInputLayout password_til;
    private TextInputLayout con_password_til;
    private EditText password_et;
    private EditText con_password_et;
    private ProgressDialog dialogL;
    private JSONObject jsonObject;
    private String ResetData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ResetData="";

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("ResetData")){
            ResetData = bundle.getString("ResetData");
        }

        password_til = (TextInputLayout) findViewById(R.id.password_til);
        con_password_til = (TextInputLayout) findViewById(R.id.con_password_til);

        password_et = (EditText) findViewById(R.id.password_et);
        con_password_et = (EditText) findViewById(R.id.con_password_et);

        proceed = (Button) findViewById(R.id.proceed);

        password_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                password_til.setError(null);
            }
        });

        con_password_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                con_password_til.setError(null);
            }
        });


        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Log.d("OTP_OBTAINED"," OTP "+OTP+" USER ADDED "+temp_pass_et.getText().toString());

                if(password_et.getText().toString().isEmpty()){
                    password_til.setError(getString(R.string.this_above_field_can_not_be_empty));
                    return;
                }

                if(con_password_et.getText().toString().isEmpty()){
                    password_til.setError(getString(R.string.this_above_field_can_not_be_empty));
                    return;
                }

                if(!password_et.getText().toString().equals(con_password_et.getText().toString())){
                    password_til.setError(getString(R.string.confirm_password_donot_match));
                    return;
                }

                updatePassword(password_et.getText().toString());

            }
        });

    }



    /**
     * <h>BackgroundFrgtPwd</h>
     * For sending the varification code for forgot password.
     * The value returned is as void.
     * <pre>
     *    Used for sending the varification code to mobile number during the registration by doing the Okhttp call for an api called ForgotPasswordWithOtp.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void updatePassword(final String password) {


        dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(PasswordUpdateActivity.this,getResources().getString(R.string.please_wait));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        jsonObject=new JSONObject();
        try
        {
            SessionManager sessionManager=new SessionManager(this);
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_resetdata", ResetData);
            jsonObject.put("ent_password", password);
            jsonObject.put("ent_user_type", "2");
            Utility.printLog("paras to otp "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"updatePassword", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog( "success for updated the language " + result);
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                try
                {
                    if(result!=null)
                    {
                        jsonObject=new JSONObject(result);
                        String errFlag=jsonObject.getString("errFlag");
                        String errMsg=jsonObject.getString("errMsg");

                        Utility.printLog( "success for updated the language errFlag: " + errFlag);

                        if(errFlag.equals("0"))
                        {
//							resopnse_tv.setText(errMsg);
                            alert(errFlag,errMsg);
                        }
                        else
                        {
//                            resopnse_tv.setText(errMsg);
//							phone_layout.setError(errMsg);
                        }
                    }
                    else
                    {
                        Utility.showToast(PasswordUpdateActivity.this,getResources().getString(R.string.network_connection_fail));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error)
            {
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                Utility.showToast(PasswordUpdateActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    public void alert(final String errFlag, String msg)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,5);
        // set title
        alertDialogBuilder.setTitle(getString(R.string.note));
        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {
                        //closing the application
                        dialog.dismiss();
                        if(errFlag.equals("0")) {
                            finish();
                            overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                        } else {

                        }
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
