package com.roadyo.passenger.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roadyo.passenger.pubnu.pojo.PubnubCarTypes;
import com.threembed.utilities.Utility;
import com.bloomingdalelimousine.ridein.R;

public class CarFareDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_fare_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        TextView base_fair_amount_tv = (TextView) findViewById(R.id.base_fair_amount_tv);
        TextView booking_fee_amount_tv = (TextView) findViewById(R.id.booking_fee_amount_tv);
        TextView min_fair_amount_tv = (TextView) findViewById(R.id.min_fair_amount_tv);
        TextView per_kilometer_amount_tv = (TextView) findViewById(R.id.per_kilometer_amount_tv);
        TextView per_minute_amount_tv = (TextView) findViewById(R.id.per_minute_amount_tv);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){

            PubnubCarTypes pubnubCarTypes = (PubnubCarTypes) bundle.getSerializable("PubnubCarTypes");
            base_fair_amount_tv.setText(getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(pubnubCarTypes.getBasefare().trim())));
            booking_fee_amount_tv.setText(getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(pubnubCarTypes.getMin_fare().trim())));
            min_fair_amount_tv.setText(getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(pubnubCarTypes.getMin_fare().trim())));
            per_kilometer_amount_tv.setText(getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(pubnubCarTypes.getPrice_per_km().trim())));
            per_minute_amount_tv.setText(getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(pubnubCarTypes.getPrice_per_min().trim())));

        }
    }

    private String round(double value)
    {
        @SuppressLint("DefaultLocale") String s = String.format("%.2f", value);
        Utility.printLog("rounded value=" + s);
        return s;
    }

}
