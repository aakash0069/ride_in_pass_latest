package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * Created by rahul on 22/11/16.
 */

public class ZendeskCommentsPojo implements Serializable
{
    /*    "errNum":21,
    "errFlag":"0",
    "errMsg":"Got the details!",
    "response":{}*/
    private String errFlag;
    private String errMsg;

    public String getErrNum() {
        return errNum;
    }

    private String errNum;
    private ZendeskCommentsResponse response;

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ZendeskCommentsResponse getResponse() {
        return response;
    }
}
