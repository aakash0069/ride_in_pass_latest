package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Akbar
 */

public class GetKeysPojo implements Serializable
{
    private String errFlag,errMsg;
    private String pub,sub;
    private ArrayList<String> GoogleKeysArray;

    public ArrayList<String> getGoogleKeysArray() {
        return GoogleKeysArray;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getPub() {
        return pub;
    }

    public String getSub() {
        return sub;
    }
}
