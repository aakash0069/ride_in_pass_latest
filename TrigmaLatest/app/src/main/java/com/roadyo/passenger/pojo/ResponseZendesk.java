package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Akbar
 */
public class ResponseZendesk implements Serializable
{
    /* "results":[]*/
    private ArrayList<ResultsForZendesk> results;

    public ArrayList<ResultsForZendesk> getResults() {
        return results;
    }
}
