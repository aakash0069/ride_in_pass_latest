package com.roadyo.passenger.main;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.Support_new_pojo;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;
import org.json.JSONException;
import org.json.JSONObject;

public class SupportFragment extends Fragment {
	private RelativeLayout progress_bar_layout;
	private LinearLayout mainllout,child_layout;
	private ArrayList<ImageView> rightArrowImages;
	private int previousView=0,totalCount=0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view=inflater.inflate(R.layout.support_new,container,false);
		initializeVariables(view);
		return view;
	}

	private void initializeVariables(View view)
	{
		MainActivity activity = (MainActivity) getActivity();
		activity.fragment_title.setText(getResources().getString(R.string.support));

		progress_bar_layout = (RelativeLayout) view.findViewById(R.id.progress_bar_layout);
		mainllout = (LinearLayout)view.findViewById(R.id.mainllout);
		child_layout = (LinearLayout)view.findViewById(R.id.child_layout);
		rightArrowImages=new ArrayList<>();

		activity.action_bar.setVisibility(View.VISIBLE);
		activity.fragment_title_logo.setVisibility(View.VISIBLE);
		activity.fragment_title.setVisibility(View.GONE);
		activity.edit_profile.setVisibility(View.GONE);

		if(Utility.isNetworkAvailable(getActivity()))
		{
			getSupportService();
		}
		else
		{
			Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
		}
	}
	private void getSupportService()
	{
		final SessionManager sessionManager=new SessionManager(getActivity());
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_sid",sessionManager.getSid());
			jsonObject.put("ent_lan","0");
			Utility.printLog("params to supprt "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		OkHttpRequestObject.postRequest(getActivity().getString(R.string.BASE_URL)+"support", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				Utility.printLog("result for support API "+result);
				if(result!=null) {
					Gson gson = new Gson();
					final Support_new_pojo support_pojo = gson.fromJson(result, Support_new_pojo.class);
					if(isAdded())
					{
						if (support_pojo.getErrFlag().equals("0")) {
							progress_bar_layout.setVisibility(View.GONE);
							final LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
							for(int i=0;i<support_pojo.getSupport().size();i++)
							{
								final View row = layoutInflater.inflate(R.layout.support_list_row, mainllout, false);
								TextView list_row_text= (TextView) row.findViewById(R.id.list_row_text);
								RelativeLayout list_row_layout= (RelativeLayout) row.findViewById(R.id.list_row_layout);
								final ImageView right_arrow= (ImageView) row.findViewById(R.id.right_arrow);
								if(sessionManager.getLanguageCode().equals("en"))
									Utility.setTypefaceMuliRegular(getActivity(),list_row_text);
								rightArrowImages.add(right_arrow);

								if(sessionManager.getLanguageCode().equals("ar"))
								{
									right_arrow.setScaleX(-1);
								}

								list_row_text.setText(support_pojo.getSupport().get(i).getTag());
								if(support_pojo.getSupport().get(i).getChilds().isEmpty())
								{
//									right_arrow.setVisibility(View.INVISIBLE);
									right_arrow.setImageDrawable(getResources().getDrawable(R.drawable.icon));
								} else {
									right_arrow.setImageDrawable(getResources().getDrawable(R.drawable.plus_icon));
								}
								mainllout.addView(row);

								final int finalI = i;
								list_row_layout.setOnClickListener(new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										if(!support_pojo.getSupport().get(finalI).getChilds().isEmpty())
										{
											if(previousView!=finalI+1)
											{
												if(previousView!=0)
													mainllout.removeViews(previousView,totalCount);
//												Utility.resetAll(rightArrowImages,getActivity(),finalI,true);
												right_arrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.minus_icon));
												previousView=finalI+1;
												totalCount=support_pojo.getSupport().get(finalI).getChilds().size();
												for(int i=0;i<support_pojo.getSupport().get(finalI).getChilds().size();i++)
												{
													View child_row = layoutInflater.inflate(R.layout.child_row_layout, child_layout, false);
													Utility.printLog("tags for need help "+support_pojo.getSupport().get(i).getTag());
													TextView list_row_text= (TextView) child_row.findViewById(R.id.list_row_text);
													RelativeLayout list_row_layout= (RelativeLayout) child_row.findViewById(R.id.list_row_layout);

													if(sessionManager.getLanguageCode().equals("en"))
														Utility.setTypefaceMuliRegular(getActivity(),list_row_text);

													final int finalI1 = i;
													list_row_layout.setOnClickListener(new View.OnClickListener() {
														@Override
														public void onClick(View v) {
															Intent intent=new Intent(getActivity(),WebViewActivity.class);
															intent.putExtra("URL",support_pojo.getSupport().get(finalI).getChilds().get(finalI1).getLink());
															intent.putExtra("TITLE", getResources().getString(R.string.tell_us_more));
															startActivity(intent);
															getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
														}
													});
													list_row_text.setText(support_pojo.getSupport().get(finalI).getChilds().get(i).getTag());
													mainllout.addView(child_row,finalI+1);
												}
											}
											else
											{
//												Utility.resetAll(rightArrowImages,getActivity(),finalI,false);
												mainllout.removeViews(previousView,totalCount);
												right_arrow.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.plus_icon));
												previousView=0;
											}
										}
										else
										{
											if(!support_pojo.getSupport().get(finalI).getLink().equals(""))
											{
												Intent intent=new Intent(getActivity(),WebViewActivity.class);
												intent.putExtra("URL",support_pojo.getSupport().get(finalI).getLink());
												intent.putExtra("TITLE", getResources().getString(R.string.tell_us_more));
												startActivity(intent);
												getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
											}
										}
									}
								});
							}

						} else {
							if(isAdded())
							{
								progress_bar_layout.setVisibility(View.GONE);
								Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
							}
						}
					}
				}
			}
			@Override
			public void onError(String error)
			{
				if(isAdded())
				{
					progress_bar_layout.setVisibility(View.GONE);
					Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
				}
			}
		});
	}
}