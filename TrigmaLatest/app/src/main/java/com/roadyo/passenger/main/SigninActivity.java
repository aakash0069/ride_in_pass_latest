
package com.roadyo.passenger.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.LoginResponse;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.LocationUtil;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;
import static com.threembed.utilities.Utility.getResId;

public class SigninActivity extends AppCompatActivity implements OnClickListener, GoogleApiClient.OnConnectionFailedListener
		,LocationUtil.GetLocationListener{
	private static final int FACEBOOK_OR_EMAIL = 99;
	private EditText password;
	private AutoCompleteTextView username;
	private TextView login;
	private SessionManager session;
	private String strServerResponse;
	private LocationUtil networkUtil;
	//Retrieve the distance matrix keys saved in shared preference and remove the one used before
	String[] emailIdArray ;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);

		session = new SessionManager(SigninActivity.this);
		initialize();
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		Utility.printLog("result from google login "+connectionResult.getErrorCode()+" messag "+connectionResult.getErrorMessage());
	}

	private void initialize()
	{
		if(!session.getEmailId().equals(""))
		{
			Gson gson=new Gson();
			emailIdArray = gson.fromJson(session.getEmailId(), String[].class);
		}

		/**
		 * to set language support
		 */
		Utility.setLanguageSupport(this);
		/**
		 * to set the status bar color
		 */
		Utility.setStatusBarColor(this);
		Utility.printLog("device id in signin "+session.getDeviceId()+" regid "+session.getRegistrationId());

		username= (AutoCompleteTextView) findViewById(R.id.user_name);
		password=(EditText)findViewById(R.id.password);
		TextView forgot_password = (TextView) findViewById(R.id.forgot_password);
		TextView title = (TextView) findViewById(R.id.title);

		ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
//		countryCodeTv = (TextView) findViewById(R.id.code);
		login=(TextView) findViewById(R.id.login_btn);
//		flag= (ImageView) findViewById(R.id.flag);

		if(!session.getEmailId().equals(""))
		{
			ArrayAdapter adapter = new
					ArrayAdapter(this,android.R.layout.simple_list_item_1,emailIdArray);
			username.setAdapter(adapter);
			username.setThreshold(1);
		}


		RelativeLayout RL_Signin = (RelativeLayout) findViewById(R.id.rl_signin);
//		RelativeLayout countryPicker = (RelativeLayout) findViewById(R.id.countryPicker);
		if (title != null) {
			title.setText(getResources().getString(R.string.login));
		}
		username.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				username.setError(null);
			}
		});
		password.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				password.setError(null);
			}
		});



		/**
		 * to set typeface
		 */
		if(session.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(this,username);
			Utility.setTypefaceMuliRegular(this,password);
//			Utility.setTypefaceMuliRegular(this,countryCodeTv);
			Utility.setTypefaceMuliRegular(this,login);
			Utility.setTypefaceMuliRegular(this,forgot_password);
			Utility.setTypefaceMuliRegular(this,title);
		}


		login.setOnClickListener(this);

		if (forgot_password != null) {
			forgot_password.setOnClickListener(this);
		}

		if (RL_Signin != null) {
			RL_Signin.setOnClickListener(this);
		}

//		if (countryPicker != null) {
//			countryPicker.setOnClickListener(this);
//		}

		if (back_btn != null) {
			back_btn.setOnClickListener(this);
		}

		/**
		 * to flip the arrow
		 */
		if(session.getLanguageCode().equals("ar"))
		{
			if (back_btn != null) {
				back_btn.setScaleX(-1);
			}
		}
		Utility.printLog("language in signiin "+session.getLanguageCode());
	}



	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.rl_signin:
			{
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
				break;
			}
			case R.id.back_btn:
			{
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
				break;
			}
			case R.id.forgot_password:
			{
				Intent intent=new Intent(SigninActivity.this,ForgotPwdActivity.class);
				intent.putExtra("FROM","SIGNIN");
				startActivity(intent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
				break;
			}

			case R.id.login_btn:
			{
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(login.getWindowToken(),InputMethodManager.RESULT_UNCHANGED_SHOWN);
				if(validateFields()){

					if(Utility.isNetworkAvailable(SigninActivity.this))
					{
						UserLogin();
						Utility.printLog("numeric or character "+android.text.TextUtils.isDigitsOnly( username.getText().toString()));
					}
					else
					{
						Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
					}
				}
				break;
			}
		}
	}


	@Override
	protected void onResume() {
		super.onResume();

		/**
		 * to get the current location
		 */
		getCurrentLocation();
	}

	private void getCurrentLocation()
	{
		if (networkUtil == null)
		{
			networkUtil = new LocationUtil(SigninActivity.this, this);
		}
		else
		{
			networkUtil.checkLocationSettings();
		}
	}

	private void UserLogin()
	{
		final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SigninActivity.this,getResources().getString(R.string.loggingIn));
		dialogL.setCancelable(false);
		dialogL.show();

		JSONObject jsonObject=new JSONObject();
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();

//			jsonObject.put("ent_email",countryCodeTv.getText().toString()+username.getText().toString());
			jsonObject.put("ent_email",username.getText().toString());
			jsonObject.put("ent_password", password.getText().toString());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_push_token", session.getRegistrationId());
			jsonObject.put("ent_latitude", session.getCurrentLat());
			jsonObject.put("ent_longitude", session.getCurrentLong());
			jsonObject.put("ent_date_time", curenttime);
			jsonObject.put("ent_device_type","2");
			jsonObject.put("ent_login_type","1");
			jsonObject.put("ent_dev_model", Build.MODEL);
			jsonObject.put("ent_dev_os",Build.VERSION.RELEASE);
			jsonObject.put("ent_app_version",Utility.currentVersion(this));
			jsonObject.put("ent_manf",Build.MANUFACTURER);
			jsonObject.put("ent_language", session.getLanguage());
			Utility.printLog("params to login "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"slaveLogin", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				if (dialogL.isShowing())
					dialogL.dismiss();
				strServerResponse = result;
				Utility.printLog("Login Response success"+strServerResponse);
				getUserLoginInfo(dialogL);
			}

			@Override
			public void onError(String error)
			{
				if (dialogL.isShowing())
					dialogL.dismiss();
				Utility.printLog("on error for the login "+error);
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
			}
		});
	}

	private void getUserLoginInfo(ProgressDialog dialogL)
	{
		dialogL.dismiss();
		if(strServerResponse!=null && !strServerResponse.equals(""))
		{
			Gson gson = new Gson();
			LoginResponse response = gson.fromJson(strServerResponse, LoginResponse.class);
			Utility.printLog("Login Response "+strServerResponse);
			if(response.getErrFlag().equals("0"))
			{
				VariableConstants.COUNTER=0;
				DatabasePickupHandler dbPickup=new DatabasePickupHandler(SigninActivity.this);
				if(response.getAddresses().size()>0)
				{
					for(int i=0;i<response.getAddresses().size();i++)
					{
						dbPickup.addPickupLocation("",response.getAddresses().get(i).getAddressID(),
								response.getAddresses().get(i).getAddress1(),
								""+response.getAddresses().get(i).getLat()
								,""+response.getAddresses().get(i).getLng(),response.getAddresses().get(i).getLocationType(),"0");
					}
				}
				session.setCardToken("");
				session.setLast4Digits("");
				session.setBookingReponseFromPubnub("");
				session.setPubnubResponseForNonBooking("");
				session.setPubnubResponse("");
				session.storeSessionToken(response.getToken());
				session.setPhoneNumber(response.getEmail());
				session.setIsLogin(true);
				session.storeLoginId(username.getText().toString());
				session.storeServerChannel(response.getServerChn());
				session.storeChannelName(response.getChn());
				session.setPresenceChn(response.getPresenseChn());
				session.setStripeKey(response.getStipeKey());
				session.setPublishKey(response.getPub());
				session.setSubscribeKey(response.getSub());
				session.setSid(response.getSid());
				session.setUserName(response.getName());
				session.setPaymentUrl(response.getPaymentUrl());
				session.setProfileImage(response.getProfilePic());

				/**
				 * store all configured data from backen in SharedPReference
				 */
				session.setPubnubIntervalForHome(Long.parseLong(response.getConfigData().getPubnubIntervalHome()));
				session.setEtaIntervalForTracking(Long.parseLong(response.getConfigData().getDistanceMatrixInterval()));

				/**
				 * to set the google matrix keys from backend and
				 * set them in shared preference
				 */
				List<String> googleMatrixKeys = new ArrayList<>();
				googleMatrixKeys.addAll(response.getConfigData().getGoogleKeysArray());
				String jsonText = gson.toJson(googleMatrixKeys);
				session.setGoogleMatrixData(jsonText);

				/**
				 * to set the server key in shared reference
				 * from 0th index from backend
				 */
				if(!response.getConfigData().getGoogleKeysArray().isEmpty())
					session.setServerKey(response.getConfigData().getGoogleKeysArray().get(0));

				Intent intent=new Intent(SigninActivity.this,MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
			}
			else
			{
				Utility.showToast(getApplicationContext(),response.getErrMsg());
			}
		}
		else
		{
			Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
		}
	}

	//to get device id
	@SuppressLint("HardwareIds")
	public  String getDeviceId()
	{
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}
	private boolean validateFields() {
		if(username.getText().toString().isEmpty())
		{
			username.setError(getResources().getString(R.string.email_empty));
			return false;
		}
		if(password.getText().toString().isEmpty())
		{
			password.setError(getResources().getString(R.string.password_empty));
			return false;
		}

		//aakash change
		if(!android.util.Patterns.EMAIL_ADDRESS.matcher(username.getText().toString()).matches()){
			username.setError(getResources().getString(R.string.invalid_email));
			return false;
		}

		return true;
	}
	@Override
	public void onBackPressed()
	{
		finish();
		overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
	}

	@Override
	protected void onPause() {
		super.onPause();
		networkUtil.stop_Location_Update();
	}

	@Override
	public void updateLocation(Location location) {
		Utility.printLog("current latlongs in SigninActivity "+location.getLatitude()+" "+location.getLongitude());
		/**
		 * to store the current lalongs in shared preference and global variable
		 */
		session.setCurrentLat((float) location.getLatitude());
		session.setCurrentLong((float) location.getLongitude());
	}

	@Override
	public void location_Error(String error) {

	}
}