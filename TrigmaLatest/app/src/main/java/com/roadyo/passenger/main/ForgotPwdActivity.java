package com.roadyo.passenger.main;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import static com.threembed.utilities.Utility.getResId;

public class ForgotPwdActivity extends AppCompatActivity implements OnClickListener
{
	private EditText email;
	private  JSONObject jsonObject;
	ProgressDialog dialogL;
	private TextView countryCode;
	private ImageView flag;
	private TextInputLayout phone_layout;
	private String calledFrom;
	private TextView response_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_password);
		intialize();
	}

	/**
	 * For initialising the all view componets.
	 * The value returned is as void.
	 * <pre>
	 *    Used for initialising the all view componets
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */
	private void intialize()
	{
		/**
		 * to set language support
		 */
		Utility.setLanguageSupport(this);
		/**
		 * to set the status bar color
		 */
		Utility.setStatusBarColor(this);

		email=(EditText)findViewById(R.id.etPass);
		RelativeLayout back = (RelativeLayout) findViewById(R.id.rl_signin);
		ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
		Button submit = (Button) findViewById(R.id.submit);
		TextView tvDescription = (TextView) findViewById(R.id.tvDescription);
		TextView title = (TextView) findViewById(R.id.title);
		TextView got_back_to_login = (TextView) findViewById(R.id.got_back_to_login);
		RelativeLayout countryPicker= (RelativeLayout) findViewById(R.id.countryPicker);
		response_tv= (TextView) findViewById(R.id.response_tv);
		countryCode= (TextView) findViewById(R.id.code);
		phone_layout= (TextInputLayout) findViewById(R.id.phone_layout);
		flag= (ImageView) findViewById(R.id.flag);
		if (title != null) {
			title.setText(getResources().getString(R.string.change_pass));
		}
		email.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			public void onFocusChange(View v, boolean hasFocus)
			{
				Utility.printLog("setOnFocusChangeListener hasFocus= " + hasFocus);
				if (!hasFocus) {
					if (email.getText().toString().trim().isEmpty())
					{
						email.setError(getResources().getString(R.string.email_empty));
					} else
					{
						if (!validateEmail(email.getText().toString().trim()))
						{
							email.setError(getResources().getString(R.string.enter_valid_email));
						}
					}
				}
			}
		});

		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				phone_layout.setError(null);
				phone_layout.setErrorEnabled(false);
			}
		});
		/**
		 * to get the default country code
		 */
//		getCountryDialCode();

		if (back != null) {
			back.setOnClickListener(this);
		}
		if (back_btn != null) {
			back_btn.setOnClickListener(this);
		}
		if (got_back_to_login != null) {
			got_back_to_login.setOnClickListener(this);
		}
		if (submit != null) {
			submit.setOnClickListener(this);
		}
		if (countryPicker != null) {
			countryPicker.setOnClickListener(this);
		}

		/**
		 * to set the typeface
		 */
		SessionManager sessionManager=new SessionManager(this);
		if(sessionManager.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(this,tvDescription);
			Utility.setTypefaceMuliRegular(this,email);
			Utility.setTypefaceMuliRegular(this,submit);
			Utility.setTypefaceMuliRegular(this,title);
			Utility.setTypefaceMuliRegular(this,countryCode);
		}
		/**
		 * to flip the arrow
		 */
		if(sessionManager.getLanguageCode().equals("ar"))
		{
			if (back_btn != null) {
				back_btn.setScaleX(-1);
			}
		}

		/**
		 * to get the variable from where this activity is called
		 * to add the authentiaction
		 * if from signin otp is sent to API or from profile then session token is sent
		 */
		calledFrom=getIntent().getStringExtra("FROM");
	}

	/**
	 * <h>validateEmail</h>
	 * For Validating the email enterd by the user.
	 * The value returned is as boolean.
	 * <pre>
	 *    Used for Validating the email enterd by the user.
	 *    if the input email matches the expression returns the true value, or else ruturms false
	 * </pre>
	 *
	 * @param email
	 * @return boolean
	 * @since 1.0
	 */

	public boolean validateEmail(String email) {
		boolean isValid = false;
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
		{
			isValid = true;
		}
		return isValid;
	}

	/**
	 * <h>getCountryDialCode</h>
	 * Used to get default countrycode.
	 * The value returned is as void.
	 * <pre>
	 *    Used for get default countrycode.
	 * </pre>
	 *
	 * @param
	 * @return contryDialCode as String.
	 * @since 1.0
	 */
	public String getCountryDialCode(){
		String contryId;
		String contryDialCode = null;
		TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		contryId = telephonyMngr.getNetworkCountryIso().toUpperCase();
		String[] arrContryCode=ForgotPwdActivity.this.getResources().getStringArray(R.array.DialingCountryCode);
		for (String anArrContryCode : arrContryCode) {
			String[] arrDial = anArrContryCode.split(",");
			if (arrDial[1].trim().equals(contryId.trim())) {
				contryDialCode = arrDial[0];
				countryCode.setText(contryDialCode);
				String drawableName = "flag_"
						+ contryId.toLowerCase(Locale.ENGLISH);
				flag.setBackgroundResource(getResId(drawableName));
				Utility.printLog("country code " + contryDialCode);
				break;
			}
		}
		return contryDialCode;
	}

	@Override
	public void onClick(View v)
	{
		if(v.getId()==R.id.rl_signin)
		{
			finish();
			overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
			return;
		}
		if(v.getId()==R.id.back_btn)
		{
			finish();
			overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
			return;
		}

		if(v.getId()==R.id.got_back_to_login)
		{
			finish();
			overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
			return;
		}

		if(v.getId()==R.id.submit)
		{
			if(!email.getText().toString().isEmpty())
			{
				if (!validateEmail(email.getText().toString().trim()))
				{
					phone_layout.setErrorEnabled(false);
					phone_layout.setError(getResources().getString(R.string.enter_valid_email));
				} else {
					if (Utility.isNetworkAvailable(ForgotPwdActivity.this))
						BackgroundFrgtPwd();
					else
						Utility.showToast(ForgotPwdActivity.this, getResources().getString(R.string.network_connection_fail));
				}
			}
			else
			{
				//aakash change
				phone_layout.setError(getResources().getString(R.string.enter_valid_email));
			}
		}
		if(v.getId()==R.id.countryPicker)
		{
//			showDialoagforcountrypicker();
		}
	}

	/**
	 * <h>showDialoagforcountrypicker</h>
	 * For displaying the country code of all the country.
	 * The value returned is as void.
	 * <pre>
	 *    Used for displaying the country code of all the country.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */
	void showDialoagforcountrypicker(){
		final CountryPicker picker = CountryPicker.newInstance(getResources().getString(R.string.selectCountry));
		picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
		picker.setListener(new CountryPickerListener()
		{
			@Override
			public void onSelectCountry(String name, String code, String dialCode) {
				countryCode.setText(dialCode);
				String drawableName = "flag_"
						+ code.toLowerCase(Locale.ENGLISH);
				flag.setBackgroundResource(getResId(drawableName));
				picker.dismiss();
			}
		});
	}

	/**
	 * <h>BackgroundFrgtPwd</h>
	 * For sending the varification code for forgot password.
	 * The value returned is as void.
	 * <pre>
	 *    Used for sending the varification code to mobile number during the registration by doing the Okhttp call for an api called ForgotPasswordWithOtp.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */
	private void BackgroundFrgtPwd()
	{
		response_tv.setText("");
		dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(ForgotPwdActivity.this,getResources().getString(R.string.please_wait));
		dialogL.setCancelable(true);
		if (dialogL!=null)
		{
			dialogL.show();
		}
		jsonObject=new JSONObject();
		try
		{
			SessionManager sessionManager=new SessionManager(this);
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_date_time", curenttime);
//			jsonObject.put("ent_mobile", countryCode.getText().toString()+email.getText().toString());
			jsonObject.put("ent_email", email.getText().toString());
			jsonObject.put("ent_user_type", "2");
			jsonObject.put("ent_language", sessionManager.getLanguage());
			Utility.printLog("paras to otp "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"forgotPassword", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				Utility.printLog( "success for updated the language " + result);
				if (dialogL!=null)
				{
					dialogL.dismiss();
				}
				try
				{
					if(result!=null)
					{
						jsonObject=new JSONObject(result);
						String errFlag=jsonObject.getString("errFlag");
						String errMsg=jsonObject.getString("errMsg");

						Utility.printLog( "success for updated the language errFlag: " + errFlag);

						if(errFlag.equals("0"))
						{
							String OTP=jsonObject.getString("OTP");
							String ResetData=jsonObject.getString("ResetData");
							alert(errFlag,errMsg,OTP,ResetData);
						}
						else
						{
							response_tv.setText(errMsg);
						}
					}
					else
					{
						Utility.showToast(ForgotPwdActivity.this,getResources().getString(R.string.network_connection_fail));
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
			@Override
			public void onError(String error)
			{
				if (dialogL!=null)
				{
					dialogL.dismiss();
				}
				Utility.showToast(ForgotPwdActivity.this,getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	public void alert(final String errFlag, String msg, final String OTP, final String ResetData)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this,5);
		// set title
		alertDialogBuilder.setTitle(getString(R.string.note));
		// set dialog message
		alertDialogBuilder
				.setMessage(msg)
				.setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog,int id)
					{
						//closing the application
						dialog.dismiss();
						if(errFlag.equals("0")) {
							Intent intent = new Intent(ForgotPwdActivity.this, PasswordValidationActivity.class);
							intent.putExtra("OTP",OTP);
							intent.putExtra("ResetData",ResetData);
							startActivity(intent);
							finish();
							overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
						} else {

						}
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	@Override
	public void onBackPressed()
	{
		finish();
		overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
	}
}
