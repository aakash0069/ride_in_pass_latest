package com.roadyo.passenger.laterUtil;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by embed on 21/9/16.
 */
public class LocationUpdating implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private Context context;
    private boolean isRepeatable;
    MyLocationListener myLocationListener;
    private LocationManager locationManager;

    private static final int MIN_TIME = 2;
    private static final int MIN_DISTANCE = 1;
    private static final int REQUEST_LOCATION = 23;
    LocationRequest mLocationRequest_Once = new LocationRequest().setNumUpdates(1).setInterval(MIN_DISTANCE * 1000).setSmallestDisplacement(MIN_DISTANCE);
    LocationRequest mLocationRequest_Always = new LocationRequest().setInterval(MIN_DISTANCE * 1000).setSmallestDisplacement(MIN_DISTANCE);
    LocationRequest mLocationRequest;
    public LocationUpdating(Context context) {
        this.context = context;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void startLocationUpdate(boolean repeat, MyLocationListener myLocationListener) {
        isRepeatable = repeat;
        this.myLocationListener = myLocationListener;
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("wraper_location", "inside wrap " + location.toString());
        myLocationListener.getLoaction(location);
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(!isRepeatable){
            mLocationRequest=mLocationRequest_Once;
        } else {
            mLocationRequest=mLocationRequest_Always;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    public void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }
}
