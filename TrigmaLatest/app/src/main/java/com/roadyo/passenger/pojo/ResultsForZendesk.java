package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * @author Akbar
 */
public class ResultsForZendesk  implements Serializable
{
    /*    "url":"https:\/\/3embedsoft.zendesk.com\/api\/v2\/tickets\/19.json",
    "id":19,
    "external_id":null,
    "via":{},
    "created_at":"2016-11-19T15:24:20Z",
    "updated_at":"2016-11-19T15:24:20Z",
    "type":null,
    "subject":"ashish test",
    "raw_subject":"ashish test",
    "description":"testing",
    "priority":null,
    "status":"open",
    "recipient":null,
    "requester_id":7379248369,
    "submitter_id":7379248369,
    "assignee_id":7372709769,
    "organization_id":null,
    "group_id":26229729,
    "collaborator_ids":[
    ],
    "forum_topic_id":null,
    "problem_id":null,
    "has_incidents":false,
    "is_public":true,
    "due_at":null,
    "tags":[
    ],
    "custom_fields":[
    ],
    "satisfaction_rating":null,
    "sharing_agreement_ids":[
    ],
    "fields":[
    ],
    "brand_id":1512809,
    "allow_channelback":false,
    "result_type":"ticket"*/
    private String description;
    private String status;

    public String getSubmitter_id() {
        return submitter_id;
    }

    private String submitter_id;

    public String getCreated_at() {
        return created_at;
    }

    private String created_at;

    public String getId() {
        return id;
    }

    private String id;

    public String getUpdated_at() {
        return updated_at;
    }

    private String updated_at;

    public String getSubject() {
        return subject;
    }

    private String subject;

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }
}
