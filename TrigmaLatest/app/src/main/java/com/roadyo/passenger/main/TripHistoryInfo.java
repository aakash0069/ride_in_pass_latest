package com.roadyo.passenger.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.squareup.picasso.Picasso;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import java.util.Locale;

import static android.view.View.VISIBLE;


/**
 * @author Akbar
 */
public class TripHistoryInfo extends Activity implements View.OnClickListener
{
    ImageView ivdriver;

    TextView drivername;
    RelativeLayout wallet_layout,rl_TermsandCond;
    Context mcontext;
    SessionManager manager;
    String email,apptdate,currencysymbol,driverProfileImage,rating,totalAmount;
    TextView airportValue,invoice_discount_amount,invoice_total_amount,distanceValue,timeValue,bookingId,distanceText,timeText,walletAmtTV,invoice_discount_txt,meterFee,titleTextView;
    String baseFare,airportFee,lastDigits="",payType,added_to_wallet,cashCollected,walletAmt,discountVal,completedDate,bookingID,distanceFee,fName,dropAddr,title,pickAddr,timeFee,distance,time,code;
    ImageButton back_btn;
    private TextView completed_at;
    private String waitingFee,waitingTime,previousDue,duePayment,subtotal,card_deduct;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.triphistoryinfo);
        manager = new SessionManager(this);
        /**
         * to set language support
         */
        Utility.setLanguageSupport(this);
        /**
         * to set the status bar color
         */
        Utility.setStatusBarColor(this);

        if(getIntent().getExtras()!=null)
        {
            email = getIntent().getStringExtra("EMAIL");
            apptdate = getIntent().getStringExtra("APPTDATE");
            driverProfileImage = getIntent().getStringExtra("DRIVER_IMAGE");
            rating = getIntent().getStringExtra("RATING");
            totalAmount = getIntent().getStringExtra("TOTAL_AMOUNT");
            baseFare = getIntent().getStringExtra("BASE_FARE");
            airportFee = getIntent().getStringExtra("AIRPORT_FEE");
            discountVal = getIntent().getStringExtra("DISCOUNT");
            distanceFee = getIntent().getStringExtra("DISTANCE_FEE");
            timeFee = getIntent().getStringExtra("TIME_FEE");
            distance = getIntent().getStringExtra("DISTANCE");
            time = getIntent().getStringExtra("TIME");
            code = getIntent().getStringExtra("CODE");
            fName = getIntent().getStringExtra("FIRST_NAME");
            title = getIntent().getStringExtra("TITLE");
            pickAddr = getIntent().getStringExtra("PICK_ADD");
            dropAddr = getIntent().getStringExtra("DROP_ADDR");
            walletAmt = getIntent().getStringExtra("WALLET_AMT");
            bookingID = getIntent().getStringExtra("BOOKING_ID");
            cashCollected = getIntent().getStringExtra("CASH_COLLECTED");
            added_to_wallet = getIntent().getStringExtra("ADDED_TO_WALLET");
            payType = getIntent().getStringExtra("PAY_TYPE");
            completedDate = getIntent().getStringExtra("COMPLETE_TIME");
            lastDigits = getIntent().getStringExtra("LAST_DIGITS");
            waitingFee = getIntent().getStringExtra("WAITING_FEE");
            waitingTime = getIntent().getStringExtra("WAITING_TIME");
            previousDue = getIntent().getStringExtra("PREVIOUS_DUE");
            duePayment = getIntent().getStringExtra("DUE_PAYMENT");
            subtotal = getIntent().getStringExtra("SUBTOTAL");
            card_deduct = getIntent().getStringExtra("CARD_DEDUCT");
            Utility.printLog("title and drop date "+title +" "+cashCollected+" "+rating+" "+bookingID);
        }
        initialize();
        mcontext = this;
        currencysymbol = getResources().getString(R.string.currencuSymbol);
        typeface();
    }

    private void initialize()
    {
        ivdriver = (ImageView) findViewById(R.id.ivdriver);
        drivername = (TextView) findViewById(R.id.drivername);
        airportValue = (TextView) findViewById(R.id.airportValue);
        invoice_discount_amount = (TextView) findViewById(R.id.invoice_discount_amount);
        invoice_total_amount = (TextView) findViewById(R.id.invoice_total_amount);
        distanceValue = (TextView) findViewById(R.id.distanceValue);
        timeValue = (TextView) findViewById(R.id.timeValue);
        distanceText = (TextView) findViewById(R.id.distanceText);
        timeText = (TextView) findViewById(R.id.timeText);
        meterFee = (TextView) findViewById(R.id.meterFee);
        titleTextView = (TextView) findViewById(R.id.title);
        bookingId = (TextView) findViewById(R.id.bookingId);
        back_btn = (ImageButton) findViewById(R.id.back_btn);
        invoice_discount_txt = (TextView) findViewById(R.id.invoice_discount_txt);
        walletAmtTV = (TextView) findViewById(R.id.wallet_amt);
        completed_at = (TextView) findViewById(R.id.completed_at);
        TextView  cash_collected_amt = (TextView) findViewById(R.id.cash_collected_amt);
        TextView  cash_collected_txt = (TextView) findViewById(R.id.cash_collected_txt);
        rl_TermsandCond = (RelativeLayout) findViewById(R.id.rl_signin);
        wallet_layout = (RelativeLayout) findViewById(R.id.wallet_layout);
        RelativeLayout airportLayout = (RelativeLayout) findViewById(R.id.airportLayout);
        RelativeLayout invoice_discount_layout = (RelativeLayout) findViewById(R.id.invoice_discount_layout);
        TextView added_or_removed_wallet_txt= (TextView) findViewById(R.id.added_or_removed_wallet_txt);
        TextView added_or_removed_wallet_amt= (TextView) findViewById(R.id.added_or_removed_wallet_amt);
        RelativeLayout added_or_removed_wallet= (RelativeLayout) findViewById(R.id.added_or_removed_wallet);
        TextView waiting_time= (TextView) findViewById(R.id.waiting_time);
        TextView waiting_time_text= (TextView) findViewById(R.id.waiting_time_text);
        TextView previous_due_txt= (TextView) findViewById(R.id.previous_due_txt);
        TextView previous_due_amount= (TextView)findViewById(R.id.previous_due_amount);
        TextView due_payment_text= (TextView) findViewById(R.id.due_payment_text);
        TextView due_payment_amount= (TextView) findViewById(R.id.due_payment_amount);
        TextView payment_method_text= (TextView) findViewById(R.id.payment_method_text);
        RelativeLayout waiting_time_layout= (RelativeLayout) findViewById(R.id.waiting_time_layout);
        RelativeLayout previous_due_layout= (RelativeLayout) findViewById(R.id.previous_due_layout);
        RelativeLayout due_payment_layout= (RelativeLayout) findViewById(R.id.due_payment_layout);
        RelativeLayout action_bar_layout= (RelativeLayout) findViewById(R.id.action_bar_layout);
        RelativeLayout cash_collected_layout= (RelativeLayout) findViewById(R.id.cash_collected_layout);

        if(manager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(this,cash_collected_amt);
            Utility.setTypefaceMuliRegular(this,cash_collected_txt);
            Utility.setTypefaceMuliRegular(this,added_or_removed_wallet_txt);
            Utility.setTypefaceMuliRegular(this,added_or_removed_wallet_amt);
            Utility.setTypefaceMuliRegular(this,waiting_time);
            Utility.setTypefaceMuliRegular(this,waiting_time_text);
            Utility.setTypefaceMuliRegular(this,previous_due_amount);
            Utility.setTypefaceMuliRegular(this,previous_due_txt);
            Utility.setTypefaceMuliRegular(this,due_payment_text);
            Utility.setTypefaceMuliRegular(this,due_payment_amount);
            Utility.setTypefaceMuliRegular(this,payment_method_text);
        }

        titleTextView.setText(title);
        completed_at.setText(getResources().getString(R.string.comppleted_at)+" "+completedDate);
//        action_bar_layout.setBackgroundColor(getResources().getColor(R.color.background));

        if(driverProfileImage!=null)
        {
            Picasso.with(this).load(driverProfileImage)
                    .transform(new CircleTransform())
                    .resize(getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumWidth(),
                            getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumHeight())
                    .into(ivdriver);
        }
        bookingId.setText(getResources().getString(R.string.booking_id)+" "+bookingID);
        if(!baseFare.equals(""))
        {
            meterFee.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(baseFare)));
        }
        if(!airportFee.equals("") || airportFee!=null)
        {
            if(!airportFee.equals(""))
            {
                if(Double.parseDouble(airportFee)>0)
                {
                    airportValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(airportFee)));
                }
                else
                {
                    airportLayout.setVisibility(View.GONE);
                }
            }
        }
        else
        {
            airportLayout.setVisibility(View.GONE);
        }
        if(added_to_wallet!=null && !added_to_wallet.equals("0") && !added_to_wallet.equals(""))
        {
            added_or_removed_wallet.setVisibility(View.VISIBLE);
            added_or_removed_wallet_amt.setText(round(Double.parseDouble(added_to_wallet))+" "+getResources().getString(R.string.currencuSymbol));
        }
        if(!discountVal.equals(""))
        {
            if(Double.parseDouble(discountVal)>0)
            {
                invoice_discount_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(discountVal)));
            }else
            {
                invoice_discount_layout.setVisibility(View.GONE);
            }
        }
        else
        {
            invoice_discount_layout.setVisibility(View.GONE);
        }
        if(!totalAmount.equals(""))
            invoice_total_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(totalAmount)));
        if(!distanceFee.equals(""))
            distanceValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(distanceFee)));
        if(!timeFee.equals(""))
            timeValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(timeFee)));
        if(payType.equals("2") )
        {
            if(!cashCollected.equals(""))
            {
                if(Double.parseDouble(cashCollected)!=0)
                    cash_collected_amt.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(cashCollected)));
                else
                    cash_collected_layout.setVisibility(View.GONE);
            }
            else
            {
                cash_collected_layout.setVisibility(View.GONE);
            }
        }
        else
        {
            Utility.printLog("last digist "+lastDigits);
            if(!lastDigits.equals(""))
            {
                cash_collected_txt.setText(getResources().getString(R.string.paid_by_card)+" "+lastDigits);
                cash_collected_amt.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(card_deduct)));
            }
            else
            {
                cash_collected_layout.setVisibility(View.GONE);
            }
        }

        if(!walletAmt.equals("") && !walletAmt.equals("0") )
        {
            wallet_layout.setVisibility(VISIBLE);
            walletAmtTV.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(walletAmt)));
        }
        if(!distance.equals(""))
            distanceText.setText(getResources().getString(R.string.distnceFee) + "(" +distance+" "+getResources().getString(R.string.distanceUnit)+")");
        if(!time.equals(""))
            timeText.setText(getResources().getString(R.string.timeFare) + "(" + getDurationString(Integer.parseInt(time))+")");
        if(!code.equals(""))
            invoice_discount_txt.setText(getResources().getString(R.string.discunt) + "(" + code+")");
        drivername.setText(fName);
        rl_TermsandCond.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        Utility.printLog("map image url rating "+rating);

        if(!waitingFee.equals(""))
        {
            if(Double.parseDouble(waitingFee)!=0)
            {
                waiting_time.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(waitingFee)));
            }
            else
            {
                waiting_time_layout.setVisibility(View.GONE);
            }
            if(!waitingTime.equals(""))
            {
                waiting_time_text.setText(getResources().getString(R.string.waiting_time)+"("+getDurationString(Integer.parseInt(waitingTime))+")");
            }
        }
        else
        {
            waiting_time_layout.setVisibility(View.GONE);
        }
        if(!previousDue.equals(""))
        {
            if(Double.parseDouble(previousDue)>0)
            {
                previous_due_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(previousDue)));
            }
            else
            {
                previous_due_layout.setVisibility(View.GONE);
            }
        }
        else
        {
            previous_due_layout.setVisibility(View.GONE);
        }
        if(!duePayment.equals(""))
        {
            if(Double.parseDouble(duePayment)>0)
            {
                due_payment_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(duePayment)));
            }
            else
            {
                due_payment_layout.setVisibility(View.GONE);
            }
        }
        else
        {
            due_payment_layout.setVisibility(View.GONE);
        }
        if(!subtotal.equals(""))
            invoice_total_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(subtotal)));

        SessionManager sessionManager=new SessionManager(this);
        /**
         * to flip the arrow
         */
        if(sessionManager.getLanguageCode().equals("ar"))
        {
            back_btn.setScaleX(-1);
        }
    }

    private String getDurationString(int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        return twoDigitString(hours)+"H" + ":" + twoDigitString(minutes)+"M" + ":" + twoDigitString(seconds)+"S";
    }
    private String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    private void typeface()
    {
        TextView invoice_subtotal_txt= (TextView) findViewById(R.id.invoice_subtotal_txt);
        TextView distanceText= (TextView) findViewById(R.id.distanceText);
        TextView timeText= (TextView) findViewById(R.id.timeText);
        TextView airportText= (TextView) findViewById(R.id.airportText);
        TextView invoice_discount_txt= (TextView) findViewById(R.id.invoice_discount_txt);
        TextView invoice_total_txt= (TextView) findViewById(R.id.invoice_total_txt);
        TextView wallet_txt = (TextView) findViewById(R.id.wallet_txt);

        SessionManager sessionManager=new SessionManager(this);
        if(sessionManager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(this,titleTextView);
            Utility.setTypefaceMuliRegular(this,wallet_txt);
            Utility.setTypefaceMuliRegular(this,meterFee);
            Utility.setTypefaceMuliRegular(this,invoice_discount_txt);
            Utility.setTypefaceMuliRegular(this,walletAmtTV);
            Utility.setTypefaceMuliRegular(this,timeText);
            Utility.setTypefaceMuliRegular(this,distanceText);
            Utility.setTypefaceMuliRegular(this,wallet_txt);
            Utility.setTypefaceMuliRegular(this,timeValue);
            Utility.setTypefaceMuliRegular(this,distanceValue);
            Utility.setTypefaceMuliRegular(this,invoice_discount_amount);
            Utility.setTypefaceMuliRegular(this,bookingId);
            Utility.setTypefaceMuliRegular(this,invoice_subtotal_txt);
            Utility.setTypefaceMuliRegular(this,invoice_discount_txt);
            Utility.setTypefaceMuliRegular(this,invoice_subtotal_txt);
            Utility.setTypefaceMuliRegular(this,drivername);
            Utility.setTypefaceMuliRegular(this,completed_at);

            Utility.setTypefaceMuliRegular(this,completed_at);
            Utility.setTypefaceMuliRegular(this,airportValue);
            Utility.setTypefaceMuliRegular(this,distanceText);
            Utility.setTypefaceMuliRegular(this,timeText);
            Utility.setTypefaceMuliRegular(this,airportText);
            Utility.setTypefaceMuliBold(this,invoice_total_txt);
            Utility.setTypefaceMuliBold(this,invoice_total_amount);
        }

    }

    private String round(double value)
    {
        String s = String.format(Locale.ENGLISH,"%.2f", value);
        Utility.printLog("rounded value="+s);
        return s;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rl_signin:
            {
                finish();
                break;
            }
            case R.id.back_btn:
            {
                finish();
                break;
            }
        }
    }
}
