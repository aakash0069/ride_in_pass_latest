package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rahul on 22/11/16.
 */
public class ZendeskCommentsResponse implements Serializable
{
    private ArrayList<Comments> comments;

    public ArrayList<Comments> getComments() {
        return comments;
    }
}
