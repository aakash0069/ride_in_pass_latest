package com.roadyo.passenger.main;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.LinkAddress;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.CardDetails;
import com.roadyo.passenger.pojo.FareCalData;
import com.roadyo.passenger.pojo.FareCalculation;
import com.roadyo.passenger.pojo.GetCarDetails;
import com.roadyo.passenger.pubnu.pojo.PubnubCarTypes;
import com.roadyo.passenger.pubnu.pojo.PubnubResponseNew;
import com.squareup.picasso.Picasso;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import static android.view.View.VISIBLE;

public class LaterBookingActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {

    private static final int PAYMENT_REQUEST = 199;
    private ImageView pickup_location_search_iv;
    private ImageView dropoff_loation_search_iv;
    private ImageView car_type_image_iv;
    private TextView pickup_location_address_tv;
    private TextView dropoff_location_address_tv;
    private TextView later_booking_date_value_tv;
    private TextView later_booking_time_value_tv;
    private TextView car_type_name_tv;
    private TextView change_car_type_tv;
    private TextView fare_estimated_amount_tv;
    private LinearLayout select_payment_type_ll;
    private LinearLayout fare_layout_ll;
    private TextView car_passenger_capcity_tv;
    private Button schedule_later_booking_btn;
    private double from_latitude,from_longitude;
    private double to_latitude,to_longitude;
    private String mPICKUP_ADDRESS;
    private String mDROP_ADDRESS;
    private String payment_type_selected="2";
    private TextView cash_dollar_symbol_tv;
    private ImageView selected_card_image_iv;
    private String selected_car_type_id;// = "9";
    private TextView card_or_cash_selected_tv;
    private CardDetails cardDetails;
    private SessionManager session;
    private String promo_code_applied="";
    private PubnubResponseNew filterResponse;
    private int cartype_len;
    private LinearLayout car_type_choice_layout_ll;
    private CardView car_select_card_view_cv;
    private PubnubCarTypes car_detail_selected;
    private LinearLayout temp_car_type_ll;
    private int selected_index;
    private String later_booking_date;
    private String laterBookingDate;

    private JSONArray all_car_types;
    private ArrayList<FareCalData> fare_for_all_cartypes;
    private String calculated_fare;
    private String ent_fare_estimate="";
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_later_booking2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new SessionManager(this);

        pickup_location_search_iv = (ImageView) findViewById(R.id.pickup_location_search_iv);
        dropoff_loation_search_iv = (ImageView) findViewById(R.id.dropoff_loation_search_iv);

        pickup_location_address_tv = (TextView) findViewById(R.id.pickup_location_address_tv);
        dropoff_location_address_tv = (TextView) findViewById(R.id.dropoff_location_address_tv);

        later_booking_date_value_tv = (TextView) findViewById(R.id.later_booking_date_value_tv);
        later_booking_time_value_tv = (TextView) findViewById(R.id.later_booking_time_value_tv);

        car_type_image_iv = (ImageView) findViewById(R.id.car_type_image_iv);
        car_type_name_tv = (TextView) findViewById(R.id.car_type_name_tv);
        change_car_type_tv = (TextView) findViewById(R.id.change_car_type_tv);
        change_car_type_tv.setOnClickListener(this);

        car_type_choice_layout_ll = (LinearLayout) findViewById(R.id.car_type_choice_layout_ll);
        car_select_card_view_cv = (CardView) findViewById(R.id.car_select_card_view_cv);

        fare_estimated_amount_tv = (TextView) findViewById(R.id.fare_estimated_amount_tv);

        car_passenger_capcity_tv = (TextView) findViewById(R.id.car_passenger_capcity_tv);

        select_payment_type_ll = (LinearLayout) findViewById(R.id.select_payment_type_ll);
        fare_layout_ll = (LinearLayout) findViewById(R.id.fare_layout_ll);

        cash_dollar_symbol_tv = (TextView) findViewById(R.id.cash_dollar_symbol_tv);
        selected_card_image_iv = (ImageView) findViewById(R.id.selected_card_image_iv);
        card_or_cash_selected_tv = (TextView) findViewById(R.id.card_or_cash_selected_tv);

        schedule_later_booking_btn = (Button) findViewById(R.id.schedule_later_booking_btn);


        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        pickup_location_search_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VariableConstants.TYPE="";
                VariableConstants.SEARCH_CALLED=true;
                Intent addressIntent=new Intent(LaterBookingActivity.this, SearchAddressGooglePlacesActivity.class);
                addressIntent.putExtra("chooser", getResources().getString(R.string.pickup_location));
                startActivityForResult(addressIntent, 18);
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
            }
        });
        dropoff_loation_search_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addressIntent=new Intent(LaterBookingActivity.this, SearchAddressGooglePlacesActivity.class);
                addressIntent.putExtra("chooser", getResources().getString(R.string.drop_location));
                if(!pickup_location_address_tv.getText().toString().isEmpty()) {
                    addressIntent.putExtra("PICKUP_ADDRESS", pickup_location_address_tv.getText().toString());
                }
                startActivityForResult(addressIntent, 30);
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
            }
        });

        select_payment_type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaterBookingActivity.this,SelectPaymentTypeActivity.class);
                intent.putExtra("from_latitude",from_latitude);
                intent.putExtra("from_longitude",from_longitude);
                startActivityForResult(intent,PAYMENT_REQUEST);
            }
        });


        if(!session.getPubnubResponse().equals(""))
        {
            setCarsLayoutForChoice(session.getPubnubResponse());
        }

        later_booking_date_value_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpLaterDate();
            }
        });

        later_booking_time_value_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(later_booking_date!=null) {
                    setUpLaterTime();
                } else
                {
                    setUpLaterDate();
                }
            }
        });

        schedule_later_booking_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateData()){
                    sendLaterBooking();
                }
            }
        });

    }

    private boolean validateData() {

        if(pickup_location_address_tv.getText().toString().isEmpty()){
            Toast.makeText(LaterBookingActivity.this, ""+getString(R.string.please_provide_pickup), Toast.LENGTH_SHORT).show();
            return false;
        }

        if(dropoff_location_address_tv.getText().toString().isEmpty()){
            Toast.makeText(LaterBookingActivity.this, ""+getString(R.string.please_provide_drop_off), Toast.LENGTH_SHORT).show();
            return false;
        }

        if(later_booking_date_value_tv.getText().toString().isEmpty()){
            Toast.makeText(LaterBookingActivity.this, ""+getString(R.string.please_select_date), Toast.LENGTH_SHORT).show();
            return false;
        }

        if(later_booking_time_value_tv.getText().toString().isEmpty()){
            Toast.makeText(LaterBookingActivity.this, ""+getString(R.string.please_select_time), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void setCarsLayoutForChoice(String message){


        Utility.printLog("RK_ETA_OBTAINED current_master_response "+ session.getPubnubResponse());
        Utility.printLog("current_master_response "+ session.getPubnubResponse() +" "+ VariableConstants.CONFIRMATION_CALLED
//                    +" type id "+current_master_type_id
        );

        String status;
        try {
            JSONObject jsonObject = new JSONObject(message);
            status = jsonObject.getString("a");
            switch (status) {
                case "2":

                    Gson gson = new Gson();
                    filterResponse = gson.fromJson(message, PubnubResponseNew.class);

                    int temp_num_of_type=filterResponse.getTypes().size();
                    String temp_car_type_id="";

//                    if(temp_num_of_type!=cartype_len){
//                        setUpCarTypeViews(filterResponse);
//                    }
                    all_car_types = new JSONArray();

                    if(temp_num_of_type>0) {

                        car_detail_selected = filterResponse.getTypes().get(0);
                        car_passenger_capcity_tv.setText("1 - "+car_detail_selected.getMax_size().trim());

                        final LayoutInflater layoutInflater = LayoutInflater.from(LaterBookingActivity.this);
                        for(int i=0;i<temp_num_of_type;i++) {

                            Utility.printLog("currentTypeIdslist " +filterResponse.getTypes().get(i).getType_id());
                            all_car_types.put(i,filterResponse.getTypes().get(i).getType_id());


                            final View row = layoutInflater.inflate(R.layout.later_booking_car_type_single_view_layout, car_type_choice_layout_ll, false);

                            ImageView car_image_iv = (ImageView) row.findViewById(R.id.car_image_iv);
                            TextView car_name_tv = (TextView) row.findViewById(R.id.car_name_tv);
                            final LinearLayout car_type_ll = (LinearLayout) row.findViewById(R.id.car_type_ll);

                            String img_id = filterResponse.getTypes().get(i).getVehicle_img_off();
                            if(img_id!=null)
                                Picasso.with(LaterBookingActivity.this)
                                        .load(filterResponse.getTypes().get(i).getVehicle_img_off())
                                        .transform(new CircleTransform()).into(car_image_iv);

                            car_name_tv.setText(filterResponse.getTypes().get(i).getType_name());
                            if(i==0){
                                car_type_ll.setSelected(true);
                                temp_car_type_ll = car_type_ll;
                                Picasso.with(LaterBookingActivity.this)
                                        .load(filterResponse.getTypes().get(0).getVehicle_img_off())
                                        .transform(new CircleTransform()).into(car_type_image_iv);
                                car_type_name_tv.setText(filterResponse.getTypes().get(0).getType_name());

//                                car_select_card_view_cv.setVisibility(View.GONE);
                            }

                            final int temp_selected_index = i;
                            car_type_ll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    temp_car_type_ll.setSelected(false);
                                    car_detail_selected=filterResponse.getTypes().get(temp_selected_index);
                                    car_type_ll.setSelected(true);
                                    Picasso.with(LaterBookingActivity.this)
                                            .load(filterResponse.getTypes().get(temp_selected_index).getVehicle_img_off())
                                            .transform(new CircleTransform()).into(car_type_image_iv);
                                    car_type_name_tv.setText(filterResponse.getTypes().get(temp_selected_index).getType_name());
                                    temp_car_type_ll = car_type_ll;
//                                    car_type_choice_layout_ll.setVisibility(View.GONE);
                                    car_select_card_view_cv.setVisibility(View.GONE);
                                    if(fare_for_all_cartypes!=null){
                                        if(fare_for_all_cartypes.size()>0){
                                            for (int i=0;i<fare_for_all_cartypes.size();i++) {
                                                if(car_detail_selected.getType_id().equals(fare_for_all_cartypes.get(i).getType_id())){
                                                    fare_estimated_amount_tv.setText(getResources().getString(R.string.currencuSymbol)+" "+fare_for_all_cartypes.get(i).getFare());
                                                }
                                            }
                                        }
                                    }

                                    car_passenger_capcity_tv.setText("1 - "+car_detail_selected.getMax_size().trim());
                                }
                            });
                            car_type_choice_layout_ll.addView(row);
                        }
                    }
                    break;
            }
        } catch (JSONException e){

        }
    }


    public void setUpLaterDate() {
        String fulldate = Utility.getDate();
        String[] fulldatesplited = fulldate.split("-");
        int year = Integer.parseInt(fulldatesplited[0]);
        int month = Integer.parseInt(fulldatesplited[1]);
        int date = Integer.parseInt(fulldatesplited[2]);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this, (DatePickerDialog.OnDateSetListener) this, year, (month-1), date);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
        datePickerDialog.show();
    }

    public void setUpLaterTime() {
        String fulldate = Utility.getTime();
        String[] fulldatesplited = fulldate.split(":");
        int hh = Integer.parseInt(fulldatesplited[0]);
        int mm = Integer.parseInt(fulldatesplited[1]);
//		int date = Integer.parseInt(fulldatesplited[2]);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,(TimePickerDialog.OnTimeSetListener) this,hh,mm,false);
        timePickerDialog.show();

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear+1;
        Log.d("selected_date", "year " + year + " monthOfYear " + month + " dayOfMonth " + dayOfMonth);
        later_booking_date = year + "-" + oneToTwoDigitConvertor(month) + "-" + oneToTwoDigitConvertor(dayOfMonth);
        later_booking_date_value_tv.setText(Utility.getFormatedDate(later_booking_date));
        setUpLaterTime();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.d("selected_date_time", "hourOfDay " + hourOfDay + " minute " + minute );

        Calendar c = Calendar.getInstance();
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int min = c.get(Calendar.MINUTE);

        if(later_booking_date.equals(Utility.getDate())) {

            //aakash change time

          /*  laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
            //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
            Utility.printLog("laterBookingDate=" + laterBookingDate);
            later_booking_time_value_tv.setText(Utility.getTimeFormated(laterBookingDate));*/

            if (hourOfDay == hour+1 && minute >= min) {

                laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
                //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
                Utility.printLog("laterBookingDate=" + laterBookingDate);
                later_booking_time_value_tv.setText(Utility.getTimeFormated(laterBookingDate));

            } else if (hourOfDay > hour+1) {

                laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
                //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
                Utility.printLog("laterBookingDate=" + laterBookingDate);
                later_booking_time_value_tv.setText(Utility.getTimeFormated(laterBookingDate));
            }

            else {
                Toast.makeText(this, "Please select at least 1 hour later time from your current time!", Toast.LENGTH_SHORT).show();
            }
        } else {
            laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
            //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
            Utility.printLog("laterBookingDate=" + laterBookingDate);
            later_booking_time_value_tv.setText(Utility.getTimeFormated(laterBookingDate));
        }
    }

    private String oneToTwoDigitConvertor(int num){
        if(num<10) {
            return "0"+num;
        } else {
            return ""+num;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==18)
        {
            if (data != null) {
                String latitudeString = data.getStringExtra("LATITUDE_SEARCH");
                String logitudeString = data.getStringExtra("LONGITUDE_SEARCH");
                mPICKUP_ADDRESS = data.getStringExtra("SearchAddress");
                String description = data.getStringExtra("DESCRIPTION");
                Utility.printLog("id from google " + data.getIntExtra("ID", 0));
                Utility.printLog("description from ggogle " + description);
//                databaseId = data.getStringExtra("ID");
                if (mPICKUP_ADDRESS != null) {
                    pickup_location_address_tv.setText(mPICKUP_ADDRESS);
                }

                double temp_from_latitude =Double.parseDouble(latitudeString);
                double temp_from_longitude =Double.parseDouble(logitudeString);

                if(from_latitude==0 && from_longitude==0) {
                    from_latitude = Double.parseDouble(latitudeString);
                    from_longitude = Double.parseDouble(logitudeString);
                    if(to_latitude!=0 && to_longitude!=0) {
                        getFareEstimate();
                    }
                } else {
                    if(temp_from_latitude!=from_latitude || temp_from_longitude!=from_longitude){
                        from_latitude = Double.parseDouble(latitudeString);
                        from_longitude = Double.parseDouble(logitudeString);
                        if(to_latitude!=0 && to_longitude!=0) {
                            getFareEstimate();
                        }
                    }
                }
            }
        }

        if(requestCode==30)
        {
            if (data != null) {
                String latitudeString = data.getStringExtra("LATITUDE_SEARCH");
                String logitudeString = data.getStringExtra("LONGITUDE_SEARCH");
                mDROP_ADDRESS = data.getStringExtra("SearchAddress");
                String description = data.getStringExtra("DESCRIPTION");
                Utility.printLog("id from google " + data.getIntExtra("ID", 0));
                Utility.printLog("description from ggogle " + description);
//                databaseId = data.getStringExtra("ID");
                if (mDROP_ADDRESS != null) {
                    dropoff_location_address_tv.setText(mDROP_ADDRESS);
                }

                double temp_to_latitude = Double.parseDouble(latitudeString);
                double temp_to_longitude = Double.parseDouble(logitudeString);


                if(to_latitude==0 && to_longitude==0) {
                    to_latitude = Double.parseDouble(latitudeString);
                    to_longitude = Double.parseDouble(logitudeString);
                    if(from_latitude!=0 && from_longitude!=0) {
                        getFareEstimate();
                    }
                } else {
                    if(temp_to_latitude!=to_latitude || temp_to_longitude!=to_longitude){
                        to_latitude = Double.parseDouble(latitudeString);
                        to_longitude = Double.parseDouble(logitudeString);
                        if(from_latitude!=0 && from_longitude!=0) {
                            getFareEstimate();
                        }
                    }
                }
            }
        }

        if(requestCode==PAYMENT_REQUEST){

            if(resultCode== Activity.RESULT_OK) {

                Bundle bundle = data.getExtras();
                if (bundle.containsKey("PAYMENT_TYPE")) {
                    payment_type_selected = bundle.getString("PAYMENT_TYPE");
                    Log.d("PAYMENT_TYPE",payment_type_selected);
                    if (payment_type_selected.equals("1")) {

                        cash_dollar_symbol_tv.setVisibility(View.GONE);
                        selected_card_image_iv.setVisibility(View.VISIBLE);

                        cardDetails = (CardDetails) bundle.getSerializable("CardDetails");
                        final int drawable = Utility.setCreditCardLogo(cardDetails.getType());
                        selected_card_image_iv.setImageResource(drawable);
                        card_or_cash_selected_tv.setText("**** **** **** "+getLast4Digits(cardDetails.getLast4()));
                        selected_card_image_iv.setImageResource(drawable);
                        session.setPaymentType("1");

                    } else {
                        cash_dollar_symbol_tv.setVisibility(View.VISIBLE);
                        selected_card_image_iv.setVisibility(View.GONE);
                        card_or_cash_selected_tv.setText(getResources().getString(R.string.cash));
                        session.setPaymentType("2");
                    }

                    if (bundle.containsKey("Promo_Code")) {
                        promo_code_applied = bundle.getString("Promo_Code");
                    }

                } else {
                    Toast.makeText(LaterBookingActivity.this, "" + getString(R.string.please_select_your_payment), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private String getLast4Digits(String name) {
        return name.substring(name.length() - 4);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.change_car_type_tv) {
            car_select_card_view_cv.setVisibility(View.VISIBLE);
        }
    }



    /**
     * to ge the fare estimate from pick to drop
     */
    private void getFareEstimate()
    {
        final ProgressDialog dialogL;
        dialogL=com.threembed.utilities.Utility.GetProcessDialog(LaterBookingActivity.this);

        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_type_id",session.getCarTypeId());
            if(all_car_types!=null)
                jsonObject.put("ent_type_Ids",all_car_types);
//            jsonObject.put("ent_curr_lat", String.valueOf(currentLatitude));
//            jsonObject.put("ent_curr_long", String.valueOf(currentLongitude));
            jsonObject.put("ent_curr_lat", String.valueOf(from_latitude));
            jsonObject.put("ent_curr_long", String.valueOf(from_longitude));

            //if driver is not in booking then send the locations from homepage in idle state

            jsonObject.put("ent_from_lat", from_latitude);
            jsonObject.put("ent_from_long", from_longitude);
            jsonObject.put("ent_to_lat", to_latitude);
            jsonObject.put("ent_to_long", to_longitude);

            jsonObject.put("ent_date_time", curenttime);
            Utility.printLog("fare estimate params "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
//        OkHttpRequestObject.postRequest(VariableConstants.BASE_URL+"fareCalculator", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"fareCalculatorforTypes", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                dialogL.dismiss();
                if(result!=null)
                {
                    Gson gson=new Gson();
                    FareCalculation fare_resp = gson.fromJson(result, FareCalculation.class);
                    if(fare_resp.getErrFlag().equals("0"))
                    {
                        fare_layout_ll.setVisibility(View.VISIBLE);

                        calculated_fare=fare_resp.getFare();
                        fare_for_all_cartypes = fare_resp.getData();
                        if(fare_for_all_cartypes.size()>0){
                            for (int i=0;i<fare_for_all_cartypes.size();i++){
                                if(car_detail_selected.getType_id().equals(fare_for_all_cartypes.get(i).getType_id())){
                                    fare_estimated_amount_tv.setText(getResources().getString(R.string.currencuSymbol)+" "+fare_for_all_cartypes.get(i).getFare());
                                }
                            }
                        }
                    }
                    else
                    {
                        Utility.showToast(LaterBookingActivity.this,fare_resp.getErrMsg());
                    }
                }
                else
                {
                    Utility.showToast(LaterBookingActivity.this,getResources().getString(R.string.network_connection_fail));
                }
                Utility.printLog("fare estimate in home page "+result);
            }

            @Override
            public void onError(String error)
            {
                if (dialogL != null) {
                    dialogL.dismiss();
                }
                Utility.showToast(LaterBookingActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void sendLaterBooking() {

        final ProgressDialog dialogL= Utility.GetProcessDialogNew(LaterBookingActivity.this, getResources().getString(R.string.sendingRequest));
        if (dialogL!=null)
        {
            dialogL.show();
        }

        SessionManager session=new SessionManager(LaterBookingActivity.this);
        Utility utility=new Utility();
        String curenttime=utility.getCurrentGmtTime();

//        Utility.printLog("sendLaterBooking Car_Type_Id = " + Car_Type_Id);

        jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id",session.getDeviceId());
            jsonObject.put("ent_wrk_type",car_detail_selected.getType_id());
            jsonObject.put("ent_addr_line1",pickup_location_address_tv.getText().toString());
            jsonObject.put("ent_lat",from_latitude);
            jsonObject.put("ent_long",from_longitude);

            jsonObject.put("ent_drop_addr_line1",dropoff_location_address_tv.getText().toString());
            jsonObject.put("ent_drop_addr_line2","");
            jsonObject.put("ent_drop_lat",to_latitude);
            jsonObject.put("ent_drop_long",to_longitude);

            jsonObject.put("ent_payment_type",payment_type_selected);
            jsonObject.put("ent_zipcode","560024");
            jsonObject.put("ent_later_dt",laterBookingDate);

            if(fare_for_all_cartypes.size()>0){
                for (int i=0;i<fare_for_all_cartypes.size();i++){
                    if(car_detail_selected.getType_id().equals(fare_for_all_cartypes.get(i).getType_id())){
                        ent_fare_estimate = fare_for_all_cartypes.get(i).getFare();
                    }
                }
            }

            jsonObject.put("ent_fare_estimate", ent_fare_estimate);
            jsonObject.put("ent_date_time",curenttime);

            if (!session.getPromocode().equals("")) {
                jsonObject.put("ent_coupon", session.getPromocode().toLowerCase());
            }

            if(payment_type_selected.equals("1") && cardDetails!=null)
            jsonObject.put("ent_token", cardDetails.getId());

            Utility.printLog("The value of kvPair later booking "+jsonObject);
        }
        catch (JSONException e)
        {

            if (dialogL!=null)
            {
                dialogL.dismiss();
            }

            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"liveBooking", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog( "keys from pubnub   " + result);
                if(result!=null)
                {
                    if (dialogL!=null)
                    {
                        dialogL.dismiss();

                    }
                    Utility.printLog("Success of getting later booking response=" + result);
//                        mPromoCode = null;
                    parseSendLaterBooking(result);

                }
                else
                {
                    Utility.showToast(LaterBookingActivity.this,getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                Utility.printLog("Success of getting later booking error=" + error);
                Utility.showToast(LaterBookingActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });


    }

    private void parseSendLaterBooking(String liveBookingStatus)
    {
        GetCarDetails response = new GetCarDetails();
        Gson gson = new Gson();
        response=gson.fromJson(liveBookingStatus,GetCarDetails.class);

        if(response!=null)
        {
            if(response.getErrFlag().equals("0"))
            {
                showAlert(response.getErrMsg(), response.getErrFlag());
            }
            else
            {
                Utility.printLog("inside RESULT_OK true");
                showAlert(response.getErrMsg(),response.getErrFlag() );
            }
        }
        else
        {
            Utility.printLog("inside RESULT_OK true");
        }
    }

    public void showAlert(String msg, final String flag)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LaterBookingActivity.this,5);
        // set title
        alertDialogBuilder.setTitle(getResources().getString(R.string.note));
        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {
                        //closing the application
                        dialog.dismiss();
                        if(flag.equals("0")){
                            finish();
                            overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                        }
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

}
