package com.roadyo.passenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.bloomingdalelimousine.ridein.R;
import com.roadyo.passenger.adapter.History_Adapter;
import com.roadyo.passenger.pojo.AppointmentList;
import com.roadyo.passenger.pojo.AppointmentResponse;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * @author  Akbar
 */
public class HistoryFragment extends Fragment
{
    private TextView emptyscreen;
    private History_Adapter mAdapter;
    private SessionManager manager;
    private String appointmentMonth,appointmentYear;
    private ArrayList<AppointmentList> rowItems = new ArrayList<>();
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;
    public static RelativeLayout progress_bar;
    private RecyclerView recyclerView;
    private String lastCount="0";
    RelativeLayout container;
    ImageView expanded_image;
    private ProgressBar imageProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view= inflater.inflate(R.layout.history_fragment,container,false);
        SetDate();
        initializeVariables(view);
        return view;
    }
    private void SetDate()
    {
        Utility utility=new Utility();
        String curenttime=utility.getCurrentGmtTime();
        Utility.printLog("MasterJobStarted curenttime="+curenttime);
        String[] date_time = curenttime.split(" ");
        String[] date = date_time[0].split("-");
        String year = date[0];
        String month = date[1];
        appointmentYear = year;
        appointmentMonth = month;
    }

    private void initializeVariables(View view)
    {

        Utility.printLog("Fragment_Called HistoryFragment onCreateView Called");
        manager = new SessionManager(getActivity());

        container= (RelativeLayout) view.findViewById(R.id.container);
        expanded_image = (ImageView) view.findViewById(R.id.expanded_image);
        imageProgressBar= (ProgressBar) view.findViewById(R.id.imageProgressBar);

        recyclerView = (RecyclerView) view.findViewById(R.id.lvhome);
        emptyscreen = (TextView) view.findViewById(R.id.emptyscreen);

        progress_bar= (RelativeLayout) view.findViewById(R.id.progress_bar);

        MainActivity activity = (MainActivity) getActivity();
        activity.fragment_title.setText(getResources().getString(R.string.bookings_menu));
        activity.action_bar.setVisibility(View.VISIBLE);
        activity.fragment_title_logo.setVisibility(View.VISIBLE);
        activity.fragment_title.setVisibility(View.GONE);
        activity.edit_profile.setVisibility(View.GONE);


    }

    @Override
    public void onStart() {
        super.onStart();
        Utility.printLog("Fragment_Called HistoryFragment onStart Called");

        VariableConstants.COUNTER=0;
        mAdapter = new History_Adapter(getActivity(),rowItems,container,expanded_image,imageProgressBar);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        SessionManager sessionManager=new SessionManager(getActivity());
        /**
         * to set the typeface
         */
        if(sessionManager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(getActivity(),emptyscreen);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            VariableConstants.COUNTER++;
                            if(lastCount.equals("0"))
                            {
                                if(Utility.isNetworkAvailable(getActivity()))
                                {
                                    BookingHistoryData();
                                }
                                else
                                {
                                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                                }
                            }
                        }
                    }
                }
            }
        });

        expanded_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageProgressBar.setVisibility(View.GONE);
            }
        });


        if(lastCount.equals("0"))
        {
            if(Utility.isNetworkAvailable(getActivity()))
            {
                BookingHistoryData();
            }
            else
            {
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        }
    }

    private void BookingHistoryData()
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",manager.getSessionToken());
            jsonObject.put("ent_dev_id", manager.getDeviceId());
            jsonObject.put("ent_page_index", VariableConstants.COUNTER+"");
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_appnt_dt", appointmentYear+"-"+appointmentMonth);
            Utility.printLog("params to slave appitnme "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL) + "getSlaveAppointments", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result) {
                fetchData(result);
            }

            @Override
            public void onError(String error) {
            }
        });
    }
    private void fetchData(String result)
    {
        progress_bar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        Utility.printLog("booking respose  "+result);
        Gson gson = new Gson();
        AppointmentResponse response = gson.fromJson(result, AppointmentResponse.class);
        if(response !=null)
        {
            lastCount=response.getLastcount();
            if(response.getErrFlag().equals("0") && isAdded())
            {
                if(response.getAppointments().isEmpty())
                {
                    emptyscreen.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                rowItems.addAll(response.getAppointments());
            }
            else
            {
                emptyscreen.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            if(response.getErrNum().equals("6") || response.getErrNum().equals("7") ||
                    response.getErrNum().equals("94") || response.getErrNum().equals("96"))
            {
                Utility.showToast(getActivity(),response.getErrMsg());
                Intent i = new Intent(getActivity(), SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getActivity().startActivity(i);
                getActivity().overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }						else if(isAdded())
                mAdapter.notifyDataSetChanged();
        }
        else if(isAdded())
        {
            Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
        }
    }
}
