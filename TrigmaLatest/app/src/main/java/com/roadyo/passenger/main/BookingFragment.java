package com.roadyo.passenger.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.threembed.utilities.Utility;
import com.bloomingdalelimousine.ridein.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class BookingFragment extends Fragment {

    private ViewPager viewPager;
    private Button upcoming_trips_btn,past_trips_btn;
    private MainActivity activity;
    private MyPageAdapter pageAdapter;
    private int position;
    List<Fragment> fragments;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking, container, false);
        initializeView(view);
        return view;
    }

    private void initializeView(final View view) {
//        activity = (MainActivity) getActivity();
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        upcoming_trips_btn = (Button) view.findViewById(R.id.upcoming_trips_btn);
        past_trips_btn = (Button) view.findViewById(R.id.past_trips_btn);
        upcoming_trips_btn.setSelected(true);
        past_trips_btn.setSelected(false);
        fragments = new ArrayList<>();

        activity = (MainActivity) getActivity();
        activity.fragment_title.setText(getResources().getString(R.string.bookings_menu));
        activity.action_bar.setVisibility(View.VISIBLE);
        activity.fragment_title_logo.setVisibility(View.VISIBLE);
        activity.fragment_title.setVisibility(View.GONE);
        activity.edit_profile.setVisibility(View.GONE);

//        fragments = getFragments();
        fragments.clear();
//        fragments.add(new UpcommingTripsFragment());
        fragments.add(new UpcommingTripsFragment());
        fragments.add(new HistoryFragment());

//        pageAdapter = new MyPageAdapter(activity.getSupportFragmentManager(), fragments);

        pageAdapter = new MyPageAdapter(activity.getSupportFragmentManager());
        viewPager.setAdapter(pageAdapter);
        pageAdapter.notifyDataSetChanged();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position1) {
                position=position1;
                if(position1==0){
                    upcoming_trips_btn.setSelected(true);
                    past_trips_btn.setSelected(false);
                } else if(position1==1){
                    upcoming_trips_btn.setSelected(false);
                    past_trips_btn.setSelected(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        upcoming_trips_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==1) {
                    upcoming_trips_btn.setSelected(true);
                    past_trips_btn.setSelected(false);
                    viewPager.setCurrentItem(0);
                }
            }
        });

        past_trips_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==0) {
                    upcoming_trips_btn.setSelected(false);
                    past_trips_btn.setSelected(true);
                    viewPager.setCurrentItem(1);
                }
            }
        });

        Utility.printLog("Fragment_Called BookingFragment onCreateView Called");

    }

    @Override
    public void onStart() {
        super.onStart();
        Utility.printLog("Fragment_Called BookingFragment onCreateView Called");
    }

    class MyPageAdapter extends FragmentStatePagerAdapter {
//        private List<Fragment> fragments;
//        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
        public MyPageAdapter(FragmentManager fm) {
            super(fm);
//            this.fragments = fragments;
        }
        @Override
        public Fragment getItem(int position) {
//            return this.fragments.get(position);
            return fragments.get(position);
        }
        @Override
        public int getCount() {
//            return this.fragments.size();
            return fragments.size();
        }
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();
        fList.add(new UpcommingTripsFragment());
        fList.add(new HistoryFragment());
        return fList;
    }
}
