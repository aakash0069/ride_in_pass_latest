package com.roadyo.passenger.main;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;


public class AboutFragment extends Fragment implements android.view.View.OnClickListener
{
	@Override
	public android.view.View onCreateView(android.view.LayoutInflater inflater,
										  android.view.ViewGroup container,
										  android.os.Bundle savedInstanceState) {
		@SuppressLint("InflateParams") View view  = inflater.inflate(R.layout.passenger_about_us, null);
		initLayoutId(view);

		if(!Utility.isNetworkAvailable(getActivity()))
		{
			Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.internetStatus");
			homeIntent.putExtra("STATUS", "0");
			getActivity().sendBroadcast(homeIntent);
		}
		return view;
	}
	private void initLayoutId(View view)
	{
		LinearLayout ll_rate_app = (LinearLayout) view.findViewById(R.id.ll_rate_app);
		LinearLayout ll_legal = (LinearLayout) view.findViewById(R.id.ll_legal);

		ll_rate_app.setOnClickListener(this);
		ll_legal.setOnClickListener(this);

		MainActivity activity = (MainActivity) getActivity();
		activity.fragment_title.setText(getResources().getString(R.string.about));
		activity.action_bar.setVisibility(View.VISIBLE);
		activity.fragment_title_logo.setVisibility(View.VISIBLE);
		activity.fragment_title.setVisibility(View.GONE);
		activity.edit_profile.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.ll_rate_app:
			{
				if (Utility.isNetworkAvailable(getActivity()))
				{
					Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
					Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
					try {
						startActivity(goToMarket);
					} catch (ActivityNotFoundException e)
					{
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
					}
				}
				else
				{
					Utility.ShowAlert(getResources().getString(R.string.network_connection_fail), getActivity());
				}
				break;
			}
			case R.id.ll_legal:
			{
				statrtncview();
				break;
			}
		}
	}
	private void statrtncview()
	{
		Intent intent=new Intent(getActivity(), TermsActivity.class);
		startActivity(intent);
		getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
	}
}
