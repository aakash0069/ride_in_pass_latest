package com.roadyo.passenger.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roadyo.passenger.pubnu.pojo.PubnubCarTypes;
import com.squareup.picasso.Picasso;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

public class CarDescriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_description);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        TextView title_tv = (TextView) findViewById(R.id.title_tv);
        TextView car_passenger_capcity_tv = (TextView) findViewById(R.id.car_passenger_capcity_tv);
        TextView car_description_tv = (TextView) findViewById(R.id.car_description_tv);
        ImageView car_type_image_iv = (ImageView) findViewById(R.id.car_type_image_iv);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            PubnubCarTypes pubnubCarTypes = (PubnubCarTypes) bundle.getSerializable("PubnubCarTypes");
            title_tv.setText(pubnubCarTypes.getType_name().trim());
            car_description_tv.setText(pubnubCarTypes.getType_desc().trim());
            car_passenger_capcity_tv.setText("1 - "+pubnubCarTypes.getMax_size().trim());

            Picasso.with(CarDescriptionActivity.this).load(pubnubCarTypes.getVehicle_img_off())
                    .transform(new CircleTransform()).into(car_type_image_iv);

        }

    }

}
