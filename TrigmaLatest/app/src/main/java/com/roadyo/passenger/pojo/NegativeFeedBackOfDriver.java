package com.roadyo.passenger.pojo;

/**
 * Created by embed on 10/5/17.
 */
public class NegativeFeedBackOfDriver {
    private String reason="";
    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
