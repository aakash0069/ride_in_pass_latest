package com.roadyo.passenger.modelclass;

import java.util.ArrayList;


public class GeometryData {

	private LocationData distance;
	private LocationData duration;

	public LocationData getDistance() {
		return distance;
	}

	public void setDistance(LocationData distance) {
		this.distance = distance;
	}

	public LocationData getDuration() {
		return duration;
	}

	public void setDuration(LocationData duration) {
		this.duration = duration;
	}

}
