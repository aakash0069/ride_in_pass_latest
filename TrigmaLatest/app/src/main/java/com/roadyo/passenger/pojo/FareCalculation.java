package com.roadyo.passenger.pojo;


/*{
    "errNum":"21",
    "errFlag":"0",
    "errMsg":"Got the details!",
    "dis":"6.6 km",
    "fare":"119.448",
    "curDis":"1 m",
    "t":{},
    "t1":{},
    "ar":{}
}*/

import java.util.ArrayList;

public class FareCalculation
{
//	String errNum;
//	String errFlag;
//	String errMsg;
//	String dis;
	String fare;
//	String curDis;
//	public String getErrNum() {
//		return errNum;
//	}
//	public String getErrFlag() {
//		return errFlag;
//	}
//	public String getErrMsg() {
//		return errMsg;
//	}
//	public String getDis() {
//		return dis;
//	}
	public String getFare() {
		return fare;
	}
//	public String getCurDis() {
//		return curDis;
//	}
//	public void setErrNum(String errNum) {
//		this.errNum = errNum;
//	}
//	public void setErrFlag(String errFlag) {
//		this.errFlag = errFlag;
//	}
//	public void setErrMsg(String errMsg) {
//		this.errMsg = errMsg;
//	}

	private String surg_price;

	private String errNum;

	private String dis;

	private String errMsg;

	private ArrayList<FareCalData> data;

	private String curDis;

	private String errFlag;

	public String getSurg_price ()
	{
		return surg_price;
	}

	public void setSurg_price (String surg_price)
	{
		this.surg_price = surg_price;
	}

	public String getErrNum ()
	{
		return errNum;
	}

	public void setErrNum (String errNum)
	{
		this.errNum = errNum;
	}

	public String getDis ()
	{
		return dis;
	}

	public void setDis (String dis)
	{
		this.dis = dis;
	}

	public String getErrMsg ()
	{
		return errMsg;
	}

	public void setErrMsg (String errMsg)
	{
		this.errMsg = errMsg;
	}

	public ArrayList<FareCalData> getData ()
	{
		return data;
	}

	public void setData (ArrayList<FareCalData> data)
	{
		this.data = data;
	}

	public String getCurDis ()
	{
		return curDis;
	}

	public void setCurDis (String curDis)
	{
		this.curDis = curDis;
	}

	public String getErrFlag ()
	{
		return errFlag;
	}

	public void setErrFlag (String errFlag)
	{
		this.errFlag = errFlag;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [surg_price = "+surg_price+", errNum = "+errNum+", dis = "+dis+", errMsg = "+errMsg+", data = "+data+", curDis = "+curDis+", errFlag = "+errFlag+"]";
	}

}

