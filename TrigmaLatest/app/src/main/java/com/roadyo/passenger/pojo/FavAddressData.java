package com.roadyo.passenger.pojo;

public class
FavAddressData
{
	private String place_id;

	private String addressType_number;

	private String zipCode;

	private String locationType;

	private String isFavourite;

	private String address1;

	private String lng;

	private String address2;

	private String addressType;

	private String addressID;

	private String lat;

	public String getPlace_id ()
	{
		return place_id;
	}

	public void setPlace_id (String place_id)
	{
		this.place_id = place_id;
	}

	public String getAddressType_number ()
	{
		return addressType_number;
	}

	public void setAddressType_number (String addressType_number)
	{
		this.addressType_number = addressType_number;
	}

	public String getZipCode ()
	{
		return zipCode;
	}

	public void setZipCode (String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getLocationType ()
	{
		return locationType;
	}

	public void setLocationType (String locationType)
	{
		this.locationType = locationType;
	}

	public String getIsFavourite ()
	{
		return isFavourite;
	}

	public void setIsFavourite (String isFavourite)
	{
		this.isFavourite = isFavourite;
	}

	public String getAddress1 ()
	{
		return address1;
	}

	public void setAddress1 (String address1)
	{
		this.address1 = address1;
	}

	public String getLng ()
	{
		return lng;
	}

	public void setLng (String lng)
	{
		this.lng = lng;
	}

	public String getAddress2 ()
	{
		return address2;
	}

	public void setAddress2 (String address2)
	{
		this.address2 = address2;
	}

	public String getAddressType ()
	{
		return addressType;
	}

	public void setAddressType (String addressType)
	{
		this.addressType = addressType;
	}

	public String getAddressID ()
	{
		return addressID;
	}

	public void setAddressID (String addressID)
	{
		this.addressID = addressID;
	}

	public String getLat ()
	{
		return lat;
	}

	public void setLat (String lat)
	{
		this.lat = lat;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [place_id = "+place_id+", addressType_number = "+addressType_number+", zipCode = "+zipCode+", locationType = "+locationType+", isFavourite = "+isFavourite+", address1 = "+address1+", lng = "+lng+", address2 = "+address2+", addressType = "+addressType+", addressID = "+addressID+", lat = "+lat+"]";
	}
}
