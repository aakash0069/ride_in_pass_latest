package com.roadyo.passenger.pojo;

public class StatusData {
	/*  "pickLat":13.028775090149,
        "pickLong":77.588610872626,
        "addr1":"14, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
        "desLat":0,
        "desLong":0,
        "dropAddr1":"",
        "apptDt":"2016-09-29 01:55:15",
        "email":"rahuljain@mobifyi.com",
        "bid":"57ec2724861e5335568b4569",
        "chn":"qdl_63DBF26F-56C3-4526-A34B-32D962202BAF",
        "model":"",
        "plateNo":"",
        "carMapImage":"",
        "fName":"rahuljain@mobifyi.com",
        "lName":"rahuljain@mobifyi.com",
        "tipPercent":0,
        "status":2,
        "rateStatus":0*/
	private String pickLat;
	private String pickLong;
	private String addr1;
	private String desLat;
	private String desLong;
	private String dropAddr1;
	private String apptDt;
	private String email;
	private String bid;
	private String chn;
	private String model;
	private String plateNo;
	private String carMapImage;
	private String fName;
	private String lName;
	private String rateStatus;
	private String mobile;
	private String carImage;
	private String r;
	private String pPic;
	private String share;
	private String make;
	private String driverLat;
	private String paymentType="";

	public String getFareEstimate() {
		return fareEstimate;
	}

	private String fareEstimate;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getMls_issued_ptc_driver_licence() {
		return mls_issued_ptc_driver_licence;
	}

	public void setMls_issued_ptc_driver_licence(String mls_issued_ptc_driver_licence) {
		this.mls_issued_ptc_driver_licence = mls_issued_ptc_driver_licence;
	}

	private String mls_issued_ptc_driver_licence;

	public String getDriverLong() {
		return driverLong;
	}

	public String getDriverLat() {
		return driverLat;
	}

	private String driverLong;

	public String getColor() {
		return color;
	}

	public String getMake() {
		return make;
	}

	private String color;

	public String getTypeId() {
		return typeId;
	}

	private String typeId;

	public String getUpdateReview() {
		return UpdateReview;
	}

	private String UpdateReview;

	public String getMid() {
		return mid;
	}

	private String mid;
	private InvoicePojo Invoice;

	public InvoicePojo getInvoice() {
		return Invoice;
	}

	public String getStatus() {
		return status;
	}

	private String status;

	public String getShare() {
		return share;
	}
	public String getPickLat() {
		return pickLat;
	}
	public String getPickLong() {
		return pickLong;
	}
	public String getAddr1() {
		return addr1;
	}
	public String getDesLat() {
		return desLat;
	}
	public String getDesLong() {
		return desLong;
	}
	public String getDropAddr1() {
		return dropAddr1;
	}
	public String getApptDt() {
		return apptDt;
	}
	public String getEmail() {
		return email;
	}
	public String getBid() {
		return bid;
	}
	public String getChn() {
		return chn;
	}
	public String getModel() {
		return model;
	}
	public String getPlateNo() {
		return plateNo;
	}
	public String getCarMapImage() {
		return carMapImage;
	}
	public String getfName() {
		return fName;
	}
	public String getlName() {
		return lName;
	}
	public String getRateStatus() {
		return rateStatus;
	}
	public String getMobile() {
		return mobile;
	}
	public String getCarImage() {
		return carImage;
	}
	public String getR() {
		return r;
	}
	public String getpPic() {
		return pPic;
	}
}
