package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * @author Akbar
 */
public class LanguagesClass implements Serializable
{
    private String id;
    private String code;

    public String getName() {
        return name;
    }

    private String name;
    private MandatoryVersion versions;

    public MandatoryVersion getVersions() {
        return versions;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }
}
