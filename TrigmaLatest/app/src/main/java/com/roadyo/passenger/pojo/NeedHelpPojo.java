package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rahul on 8/10/16.
 */

public class NeedHelpPojo implements Serializable
{
    /*    "errNum":33,
    "errFlag":"0",
    "errMsg":"Got the details!",
    "HelpedTextArray":[]*/
    private String errFlag,errMsg;
    private ArrayList<HelpedTextArrayClass> HelpedTextArray;

    public ArrayList<HelpedTextArrayClass> getHelpedTextArray() {
        return HelpedTextArray;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

}
