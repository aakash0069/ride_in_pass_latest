package com.roadyo.passenger.pubnu.pojo;

import java.util.ArrayList;

public class PubnubResponseNew 
{
	/*masArr":[
	{ 
	"tid":1,
	"mas":[
	{
	"d":22.398394726144,
	"chn":"qd_352136062663873",
	"e":"kishore@gmail.com",
	"lt":13.02884224,
	"lg":77.58958957
	},
	{
	"d":4269.0117993985,
	"chn":"qd_6EDB16F1-7F9F-41FB-8540-4DEFD6070407",
	"e":"deva@yahoo.com",
	"lt":12.990862846375,
	"lg":77.593780517578
	}
	]
	}
	*/
	
	/*{

	    "n":"Dharma Krish",
	    "chn":"qd_A1000037CE3BC3",
	    "lt":13.028831558474515,
	    "e_id":"dharma@mobifyi.com",
	    "lg":77.58959056411352,
	    "tp":"1",
	    "a":4

	}*/

	/*    "masArr":[],
    "st":3,
    "flag":0,
    "types":[],
    "a":2,
    "tp":-1*/
	
	String a;
	String flag;
	String tp;
	String n;
	String chn;
	String lt;
	String lg;
	String bid;
	String d;
	private String bookingStatus;
	private String r;
	private ConfigurationData configData;

	public String getBookingAmountAllowUpto() {
		return bookingAmountAllowUpto;
	}

	public ConfigurationData getConfigData() {
		return configData;
	}

	String bookingAmountAllowUpto;

	public String getExpiredTime() {
		return ExpiredTime;
	}

	String ExpiredTime;

	public String getEta() {
		return eta;
	}

	String eta;

	public String getR() {
		return r;
	}

	ArrayList<AvailableDriversNew> masArr;

	public String getBookingStatus() {
		return bookingStatus;
	}

	ArrayList<PubnubCarTypes> types;
	
	public ArrayList<PubnubCarTypes> getTypes() {
		return types;
	}

	public void setTypes(ArrayList<PubnubCarTypes> types) {
		this.types = types;
	}

	public String getLt() {
		return lt;
	}

	public String getLg() {
		return lg;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getFlag() {
		return flag;
	}

	public ArrayList<AvailableDriversNew> getMasArr() {
		return masArr;
	}

	public void setMasArr(ArrayList<AvailableDriversNew> masArr) {
		this.masArr = masArr;
	}

	public String getBid() {
		return bid;
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}


}
