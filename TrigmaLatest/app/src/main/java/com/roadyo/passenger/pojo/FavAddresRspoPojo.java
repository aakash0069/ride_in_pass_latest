package com.roadyo.passenger.pojo;

import java.util.ArrayList;

public class FavAddresRspoPojo
{
	private String errNum;

	private String errMsg;

	private ArrayList<FavAddressData> data;

	private String errFlag;

	public String getErrNum ()
	{
		return errNum;
	}

	public void setErrNum (String errNum)
	{
		this.errNum = errNum;
	}

	public String getErrMsg ()
	{
		return errMsg;
	}

	public void setErrMsg (String errMsg)
	{
		this.errMsg = errMsg;
	}

	public ArrayList<FavAddressData> getData ()
	{
		return data;
	}

	public void setData (ArrayList<FavAddressData> data)
	{
		this.data = data;
	}

	public String getErrFlag ()
	{
		return errFlag;
	}

	public void setErrFlag (String errFlag)
	{
		this.errFlag = errFlag;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [errNum = "+errNum+", errMsg = "+errMsg+", data = "+data+", errFlag = "+errFlag+"]";
	}
}
