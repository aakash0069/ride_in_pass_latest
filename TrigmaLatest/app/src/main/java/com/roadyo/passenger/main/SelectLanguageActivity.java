package com.roadyo.passenger.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.roadyo.passenger.adapter.AdapterLanguages;
import com.roadyo.passenger.pojo.LanguagesClass;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import java.util.ArrayList;

public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);

        initializations();
    }

    private void initializations()
    {
        /**
         * to set language support
         */
        Utility.setLanguageSupport(this);
        /**
         * to set the status bar color
         */
        Utility.setStatusBarColor(this);

        ArrayList<LanguagesClass> mData= (ArrayList<LanguagesClass>) getIntent().getSerializableExtra("LANGUAGES_LIST");
        ImageButton ivBackbtn= (ImageButton) findViewById(R.id.back_btn);
        RelativeLayout rl_signin= (RelativeLayout) findViewById(R.id.rl_signin);
        TextView signup_payment_skip= (TextView) findViewById(R.id.signup_payment_skip);
        TextView title= (TextView) findViewById(R.id.title);

        RecyclerView listview = (RecyclerView) findViewById(R.id.listview);
        AdapterLanguages adapter = new AdapterLanguages(mData, SelectLanguageActivity.this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        if (listview != null) {
            listview.setLayoutManager(llm);
        }
        if (listview != null) {
            listview.setAdapter(adapter);
        }

        if (title != null) {
            title.setText(getString(R.string.select_lang));
        }
        if (signup_payment_skip != null) {
            signup_payment_skip.setOnClickListener(this);
            signup_payment_skip.setVisibility(View.VISIBLE);
            signup_payment_skip.setText(getString(R.string.done));
        }
        if (rl_signin != null) {
            rl_signin.setOnClickListener(this);
        }
        if (ivBackbtn != null) {
            ivBackbtn.setOnClickListener(this);
        }
        SessionManager sessionManager=new SessionManager(this);
        /**
         * to flip the arrow
         */
        if(sessionManager.getLanguageCode().equals("ar"))
        {
            if (ivBackbtn != null) {
                ivBackbtn.setScaleX(-1);
            }
        }
        /**
         * to set typeface
         */
        if(sessionManager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(this,signup_payment_skip);
            Utility.setTypefaceMuliRegular(this,title);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup_payment_skip:
            {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
            case R.id.back_btn:
            {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
            case R.id.rl_signin:
            {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
        }
    }
}
