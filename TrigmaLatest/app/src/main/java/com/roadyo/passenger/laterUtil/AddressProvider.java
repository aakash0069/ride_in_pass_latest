package com.roadyo.passenger.laterUtil;


/**
 * Created by Raghav on 8/12/2016.
 */
public interface AddressProvider {
    public void onAddress(LocationPlaces places);
    public void onError(String error);
}
