package com.roadyo.passenger.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.roadyo.passenger.pojo.CardDetails;
import com.roadyo.passenger.pojo.FareCalData;
import com.roadyo.passenger.pubnu.pojo.PubnubCarTypes;
import com.roadyo.passenger.pubnu.pojo.PubnubResponseNew;
import com.squareup.picasso.Picasso;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.HttpConnection;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.PathJSONParser;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RouteAndPaymentSelectActivity extends AppCompatActivity {

    private static final int PAYMENT_REQUEST = 101;
    private LayoutInflater inflater;
    private SessionManager session;
    private static PubnubResponseNew filterResponse;
    private static ArrayList<PubnubCarTypes> car_types;
    private PubNub pubnub;
    private Timer pollingTimer = new Timer();
    private Timer myTimer_publish;
    private int selectedPos=0;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments representing
     * each object in a collection. We use a {@link FragmentStatePagerAdapter}
     * derivative, which will destroy and re-create fragments as needed, saving and restoring their
     * state in the process. This is important to conserve memory and is a best practice when
     * allowing navigation between objects in a potentially large collection.
     */
    CarTypeCollectionPagerAdapter mCarTypeCollectionPagerAdapter;

    /**
     * The {@link ViewPager} that will display the object collection.
     */
    ViewPager mViewPager;
    View pickup_marker_view;
    View dropoff_marker_view;
    private double from_latitude,from_longitude,to_latitude,to_longitude;
    private String pickup_address,dropoff_address;

    private String driver_lat,driver_long;
    String car_type_name="";
    private TextView car_passenger_capcity_tv;
    //    public static TextView single_Eta_tv;
    private Button request_car_btn;
    private LinearLayout select_payment_type_ll;
    private String payment_type_selected;
    private CardDetails cardDetails;
    private String promo_code_applied;
    private GoogleMap googleMap;
    private Polyline polylineFinal;
    Bundle bundle1;
    private static ArrayList<FareCalData> fare_for_all_cartypes;
    private TextView cash_dollar_symbol_tv;
    private ImageView selected_card_image_iv;
    private String selected_car_type_id;// = "9";
    private String ent_fare_estimate="";// = "9";
    private TextView card_or_cash_selected_tv;

    static Call call;
    public static OkHttpClient client;// = new OkHttpClient();
    private String eta="",eta_one_line="";
    //    private ProgressBar eta_prgress_bar;
    private TextView eta_tv;
    private static Marker pickup_marker;
    private LatLng pickup_latlng;
    private LatLng dropoff_latlng;
    private static Marker dropoff_marker;

    /**
     * Internal reference to the {@link Toast} object that will be displayed.
     */
    private Toast no_cars_toast;
    private ArrayList<String> nearestDrivers;
    private TimerTask myTimerTask_publish;
    private TextView no_drivers_tv;
    private Handler handler;
    private boolean isFirstTime;
    private ProgressDialog dialogL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_and_payment_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        isFirstTime = true;
        mCarTypeCollectionPagerAdapter = null;
        dialogL=Utility.GetProcessDialogNew(RouteAndPaymentSelectActivity.this,getResources().getString(R.string.loggingIn));
        dialogL.setCancelable(false);
        dialogL.show();
        client = new OkHttpClient();
        pickup_marker=null;
        fare_for_all_cartypes=null;
        eta="";
        car_type_name="";
        nearestDrivers = new ArrayList<>();
        no_cars_toast = Toast.makeText(this, "" + getString(R.string.no_drivers_found1), Toast.LENGTH_SHORT);

        mViewPager = (ViewPager) findViewById(R.id.pager);

        session = new SessionManager(this);
        session.setPaymentType("2");
        inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        pickup_marker_view = inflater.inflate(R.layout.pickup_marker_layout, null);
        dropoff_marker_view = inflater.inflate(R.layout.dropoff_marker_layout, null);
        eta_tv = (TextView) pickup_marker_view.findViewById(R.id.eta_tv);
        bundle1 = getIntent().getExtras();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_for_bounds);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap1) {
                googleMap=googleMap1;
                Bundle bundle = getIntent().getExtras();
                if(bundle!=null && bundle.containsKey("PICK_DROP_INFO")) {
                    from_latitude = 13.0247282;
                    from_longitude = 77.5910052;
                    pickup_address = bundle.getString("ent_addr_line1");
                    from_latitude = Double.parseDouble(bundle.getString("from_latitude"));
                    from_longitude = Double.parseDouble(bundle.getString("from_longitude"));
                    pickup_latlng = new LatLng(from_latitude,from_longitude);
                    to_latitude = 12.9947034;
                    to_longitude = 77.5345258;
                    dropoff_address = bundle.getString("ent_drop_addr_line1");
                    to_latitude = Double.parseDouble(bundle.getString("to_latitude"));
                    to_longitude = Double.parseDouble(bundle.getString("to_longitude"));

                    Utility.printLog(
                            "FromLatitude b "+from_latitude +" FromLongitude b "+from_longitude+
                                    " to_latitude b "+to_latitude +" to_longitude b "+to_longitude
                    );
                    dropoff_latlng = new LatLng(to_latitude, to_longitude);
                    pickup_marker = googleMap.addMarker(new MarkerOptions()
                            .position(pickup_latlng)
                            .flat(false)
                            .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(pickup_marker_view))));
                    dropoff_marker = googleMap.addMarker(new MarkerOptions()
                            .position(dropoff_latlng)
                            .flat(false)
                            .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(dropoff_marker_view))));
                    fare_for_all_cartypes = (ArrayList<FareCalData>) bundle.getSerializable("fare_for_all_cartypes");

                    if(!session.getPubnubResponse().equals(""))
                    {
                        methodAfterGettingResponse(session.getPubnubResponse());
                    }

                    String url = getMapsApiDirectionsFromTourl(from_latitude,from_longitude,to_latitude,to_longitude);
                    Utility.printLog("getMapsApiDirectionsFromTourl ="
                            + url);
                    if(!url.equals(""))
                    {
                        ReadTask downloadTask = new ReadTask();
                        downloadTask.execute(url);
                    }
                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                    boundsBuilder.include(pickup_latlng);
                    boundsBuilder.include(dropoff_latlng);

                    //aakash change
                   // googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(),140));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(),50));
                    startPublishingWithTimer();
                }
            }
        });

        fare_for_all_cartypes = (ArrayList<FareCalData>) bundle1.getSerializable("fare_for_all_cartypes");
        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(myTimer_publish!=null)
                    myTimer_publish.cancel();
                if(myTimerTask_publish!=null)
                    myTimerTask_publish.cancel();
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(myTimer_publish!=null)
                    myTimer_publish.cancel();
                if(myTimerTask_publish!=null)
                    myTimerTask_publish.cancel();
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        select_payment_type_ll = (LinearLayout) findViewById(R.id.select_payment_type_ll);
        cash_dollar_symbol_tv = (TextView) findViewById(R.id.cash_dollar_symbol_tv);
        selected_card_image_iv = (ImageView) findViewById(R.id.selected_card_image_iv);
        card_or_cash_selected_tv = (TextView) findViewById(R.id.card_or_cash_selected_tv);
        car_passenger_capcity_tv = (TextView) findViewById(R.id.car_passenger_capcity_tv);
        request_car_btn = (Button) findViewById(R.id.request_car_btn);
        no_drivers_tv = (TextView) findViewById(R.id.no_drivers_tv);

        select_payment_type_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RouteAndPaymentSelectActivity.this,SelectPaymentTypeActivity.class);
                intent.putExtra("from_latitude",from_latitude);
                intent.putExtra("from_longitude",from_longitude);
                startActivityForResult(intent,PAYMENT_REQUEST);
            }
        });
        handler = new Handler();
        customToaster();
        request_car_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(card_or_cash_selected_tv.getText().toString().isEmpty()) {
                    Toast.makeText(RouteAndPaymentSelectActivity.this, "" + getString(R.string.please_select_your_payment), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(nearestDrivers.size()>0) {
                                        if(myTimer_publish!=null)
                        myTimer_publish.cancel();
                    if(myTimerTask_publish!=null)
                        myTimerTask_publish.cancel();

                    for(int i=0;i<fare_for_all_cartypes.size();i++){
                        if(selected_car_type_id.equals(fare_for_all_cartypes.get(i).getType_id())) {
                            ent_fare_estimate = fare_for_all_cartypes.get(i).getFare();
                        }
                    }
                    String subTypes = null;
                    for (int j=selectedPos+1;j<filterResponse.getTypes().size();j++)
                    {
                        if(j==selectedPos+1)
                            subTypes=filterResponse.getTypes().get(j).getType_id();
                        else
                            subTypes=","+filterResponse.getTypes().get(j).getType_id();
                    }

                    Intent intent = new Intent(RouteAndPaymentSelectActivity.this, ConfirmPickupForBookingActivity.class);
                    intent.putExtra("from_latitude", from_latitude);
                    intent.putExtra("from_longitude", from_longitude);
                    intent.putExtra("PICKUP_ADDRESS", pickup_address);
                    intent.putExtra("FromLatitude", from_latitude);
                    intent.putExtra("FromLongitude", from_longitude);
                    Utility.printLog(
                            "FromLatitude " + from_latitude
                                    + "FromLongitude " + from_longitude
                    );
                    intent.putExtra("DROPOFF_ADDRESS", dropoff_address);
                    intent.putExtra("ToLatitude", ""+to_latitude);
                    intent.putExtra("ToLongitude", ""+to_longitude);
                    intent.putExtra("my_drivers", nearestDrivers);
                    intent.putExtra("Car_Type", selected_car_type_id);
                    intent.putExtra("ent_fare_estimate", ent_fare_estimate);
                    intent.putExtra("CURRENT_MASTER_TYPE_ID", selected_car_type_id);
                    intent.putExtra("SUB_TYPES", subTypes);
                    Utility.printLog("surge in request positions  notes in home 1" + session.getDriverCooments());
                    session.storePickuplat("" + from_latitude);
                    session.storePickuplng("" + from_longitude);
                    session.storeDropofflat("" + to_latitude);
                    session.storeDropofflng("" + to_longitude);
                    startActivity(intent);
                } else {
                    no_drivers_tv.setVisibility(View.VISIBLE);

                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            no_drivers_tv.setVisibility(View.GONE);
                        }
                    }, 5000);
                }
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                nearestDrivers.clear();
                String p_num = filterResponse.getTypes().get(position).getMax_size().trim();
                String car_type_name = filterResponse.getTypes().get(position).getType_name().trim();
                selected_car_type_id = filterResponse.getTypes().get(position).getType_id();
                session.setCarTypeId(selected_car_type_id);
                Log.d("p_num",p_num);

                car_passenger_capcity_tv.setText("1 - "+p_num);
                request_car_btn.setText(getString(R.string.request)+" "+car_type_name.trim().toUpperCase());
                publishMessage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    public void customToaster(){
        String text = "" + getString(R.string.no_drivers_found1);
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);
        // Initialize a new ClickableSpan to display red background
        ClickableSpan redClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Utility.printLog("clicked at this position s ");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+ getString(R.string.customer_care_num)));
                startActivity(callIntent);
            }
        };
        ssBuilder.setSpan(
                redClickableSpan, // Span to add
                text.indexOf(getString(R.string.customer_care_num)), // Start of the span (inclusive)
                text.indexOf(getString(R.string.customer_care_num)) + String.valueOf(getString(R.string.customer_care_num)).length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
        );
        // Display the spannable text to TextView
        no_drivers_tv.setText(ssBuilder);
        // Specify the TextView movement method
        no_drivers_tv.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private final Runnable sendData = new Runnable(){
        public void run(){
            try {
                //prepare and send the data here..
                handler.postDelayed(this, 5000);
                no_drivers_tv.setVisibility(View.GONE);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    // Convert a view to bitmap
    public Bitmap createDrawableFromView(View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void startPublishingWithTimer()
    {

        /**
         * initialize pubnub
         */
        PNConfiguration pnConfiguration = new PNConfiguration();

        /**
         * initializing the pubnub with keys from shared preferance
         */
        pnConfiguration.setUuid("s_"+session.getSid());

        if(!session.getPublishKey().equals("") && !session.getSubscribeKey().equals(""))
        {
            Utility.printLog("pub publish key 1"+session.getPublishKey());
            Utility.printLog("sub publish key 1"+session.getSubscribeKey());
            pnConfiguration.setSubscribeKey(session.getSubscribeKey());
            pnConfiguration.setPublishKey(session.getPublishKey());
        }

        pubnub = new PubNub(pnConfiguration);
        new BackgroundSubscribeMyChannel().execute();
        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
                Utility.printLog("status for th epubnub "+status.getStatusCode());
                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // internet got lost, do some magic and call reconnect when ready
                    Utility.printLog("successCallback internet got lost PNUnexpectedDisconnectCategory");
                    //pubnub.reconnect();
                } else if (status.getCategory() == PNStatusCategory.PNTimeoutCategory) {
                    // do some magic and call reconnect when ready
                    Utility.printLog("successCallback internet got lost PNTimeoutCategory");
                    //pubnub.reconnect();
                } else {
                    Utility.printLog("successCallback internet got lost "+status);
                }
            }

            @Override
            public void message(PubNub pubnub, final PNMessageResult message) {
                try
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run()
                        {
                            Utility.printLog(" successCallback message from pubnub 1 bug is 0 ");
                            methodAfterGettingResponse(message.getMessage().toString());
                        }
                    });
                } catch (Exception e) {
                    Utility.printLog("exception in success " + e);
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });


        Utility.printLog("CONTROL INSIDE startPublishingWithTimer");

        if(myTimer_publish!= null)
        {
            Utility.printLog("Timer already started");
            return;
        }

        myTimer_publish = new Timer();
        myTimerTask_publish = new TimerTask() {
            @Override
            public void run() {
                publishMessage();
                Utility.printLog("##################################################### RouteAndPaymentSelectActivity ");
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, session.getPubnubIntervalForHome()*1000,
                session.getPubnubIntervalForHome()*1000);
    }


    /**
     *subscribing to my server channel to listen all available drivers around you
     */
    class BackgroundSubscribeMyChannel extends AsyncTask<String,Void,String>
    {
        @Override
        protected String doInBackground(String... params)
        {

            Utility.printLog("session.getChannelName() ************* "+session.getChannelName());
            /**
             * listening to passenger channel
             */
            List<String> subscribed_channels = pubnub.getSubscribedChannels();
            if(!subscribed_channels.contains(session.getChannelName()))
            {
                pubnub.subscribe()
                        .channels(Arrays.asList(session.getChannelName())) // subscribe to channel groups
                        .withPresence()// also subscribe to related presence information
                        .execute();
            }
            return null;
        }
    }

    private void publishMessage() {
        if (from_latitude == 0.0 || from_longitude == 0.0) {
            Utility.printLog("startPublishingWithTimer getServerChannel no location");
        } else
        {
            if(Utility.isNetworkAvailable(this)){
                getPubnubDataInAPI();
            }
            else
            {
                Utility.showToast(this,getString(R.string.network_connection_fail));
            }
        }
    }

    private void getPubnubDataInAPI()
    {
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("a",1);
            jsonObject.put("pid",session.getLoginId());
            jsonObject.put("lt", String.valueOf(from_latitude));
            jsonObject.put("lg", String.valueOf(from_longitude));
            jsonObject.put("chn",session.getChannelName());
            jsonObject.put("st","3");
            jsonObject.put("tp","1");
            jsonObject.put("ent_splash","0");
            jsonObject.put("sid",session.getSid());
            Utility.printLog("params to check session "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.GETTING_PUBNUB_URL1), jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(final String result)
            {
                Utility.printLog( "success from get pubnub   " + result);
            }
            @Override
            public void onError(String error)
            {
                Utility.printLog( "success from get pubnub  error " + error);
                Utility.showToast(RouteAndPaymentSelectActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void methodAfterGettingResponse(String message) {

        Utility.printLog("RK_ETA_OBTAINED current_master_response "+ session.getPubnubResponse());
        Utility.printLog("current_master_response "+ session.getPubnubResponse() +" "+ VariableConstants.CONFIRMATION_CALLED
        );

        String status;
        try {
            JSONObject jsonObject = new JSONObject(message);
            status = jsonObject.getString("a");
            switch (status) {
                case "2":
                    Gson gson = new Gson();
                    filterResponse = gson.fromJson(message, PubnubResponseNew.class);
                    /**
                     * resetng all booking flags..
                     */
                    session.setDriverOnWay(false);
                    session.setDriverArrived(false);
                    session.setTripBegin(false);
                    session.setInvoiceRaised(false);
                    int temp_num_of_type=filterResponse.getTypes().size();
                    String temp_car_type_id="";
                    if(temp_num_of_type>0){
                        car_types = filterResponse.getTypes();
                        if(mCarTypeCollectionPagerAdapter==null){
                            mCarTypeCollectionPagerAdapter = new CarTypeCollectionPagerAdapter(getSupportFragmentManager(),car_types);
                            mViewPager.setAdapter(mCarTypeCollectionPagerAdapter);
                        } else {
                            mCarTypeCollectionPagerAdapter.updateCartypes(car_types);
                        }
                    }
                    if(temp_num_of_type>0) {
                        if(selected_car_type_id==null){
                            selected_car_type_id = filterResponse.getTypes().get(0).getType_id();
                            session.setCarTypeId(selected_car_type_id);
                            request_car_btn.setText(getString(R.string.request)+" "+filterResponse.getTypes().get(0).getType_name().trim()
                                    .toUpperCase());
                        }
                        for (int i = 0; i < temp_num_of_type; i++) {
                            temp_car_type_id = filterResponse.getMasArr().get(i).getTid();
                            Utility.printLog("RK_ETA_OBTAINED selected_car_type_id "+selected_car_type_id);
                            if (selected_car_type_id.equals(temp_car_type_id)) {
                                Utility.printLog("RK_ETA_OBTAINED selected_car_type_id b "+selected_car_type_id);
                                nearestDrivers.clear();
                                selectedPos=i;
                                int num_of_cars_available = filterResponse.getMasArr().get(i).getMas().size();
                                if (num_of_cars_available > 0) {
                                    for (int j = 0; j < filterResponse.getMasArr().get(i).getMas().size(); j++) {
                                        nearestDrivers.add(filterResponse.getMasArr().get(i).getMas().get(j).getMid());
                                    }
                                    if(no_cars_toast!=null){
                                        no_cars_toast.cancel();
                                    }
                                    new GET_ETA_AND_DIST().execute(car_types);
                                } else {
                                    if(dialogL!=null){
                                        dialogL.dismiss();
                                    }
                                    eta_tv.setText(getString(R.string.no_camel) + "\n" + getString(R.string.cars));
                                    if (googleMap != null) {
                                        if(pickup_marker!=null && !RouteAndPaymentSelectActivity.this.isFinishing()){
                                            pickup_marker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(pickup_marker_view)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        } catch (JSONException e){

        }
    }

    public class GET_ETA_AND_DIST extends AsyncTask<ArrayList<PubnubCarTypes>, Void, ArrayList<PubnubCarTypes>> {
        @Override
        protected void onPreExecute() {
        }
        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected ArrayList<PubnubCarTypes> doInBackground(ArrayList<PubnubCarTypes>... params) {
            ArrayList<PubnubCarTypes> types = params[0];
            eta=filterResponse.getMasArr().get(selectedPos).getMas().get(0).getT()+"\n"+"Min";
            eta_one_line=filterResponse.getMasArr().get(selectedPos).getMas().get(0).getT()+" Min";
            return types;
        }
        @Override
        protected void onPostExecute(ArrayList<PubnubCarTypes> result) {
            if(dialogL!=null){
                dialogL.dismiss();
            }
            try {
                if (eta != null) {
                    Utility.printLog("RK_ETA_OBTAINED b " + eta);
                    if (googleMap != null) {
                        Utility.printLog("RK_ETA_OBTAINED c " + eta);
                        Log.d("PickUpActivity1", "ARG_POSITION " + eta_one_line + " " + selected_car_type_id);
                        eta_tv.setText("" + eta);
                        if (filterResponse != null && result != null && result.size() > 0) {
                            boolean t1 = false;
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i).getType_id().equals(selected_car_type_id)) {
                                    t1 = true;
                                    Log.d("PickUpActivity1", "ARG_POSITION " + eta_one_line + " inside ");
                                    result.get(i).setEta("" + eta_one_line);
                                } else {
                                    result.get(i).setEta(" ...");
                                }
                            }
                            if (!isFinishing()) {
                                if (t1 && mCarTypeCollectionPagerAdapter != null) {
                                    if (mViewPager != null && mCarTypeCollectionPagerAdapter != null) {
                                        car_types = result;
                                        mCarTypeCollectionPagerAdapter.updateCartypes(car_types);
                                    }
                                }
                            }
                        }
                        if (pickup_marker != null && !RouteAndPaymentSelectActivity.this.isFinishing()) {
                            pickup_marker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(pickup_marker_view)));
                        }
                    }
                }
            } catch (Exception e){

            }
        }

    }

    public static String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Log.d("response_obt","\t 1");
            Response response = null;// = call.execute();
            if(call!=null) {
                Log.d("response_obt","\t 2");
                if (call.isExecuted()) {
                    Log.d("response_obt","\t 3");
                    call = client.newCall(request);
                    response=call.execute();
                    Log.d("response_obt","\t 4");
                }else {
                    Log.d("response_obt","\t 5");
                    if(!call.isCanceled()) {
                        Log.d("response_obt","\t 6");
                        call.cancel();
                        Log.d("response_obt","\t 7");
                        call = client.newCall(request);
                        response = call.execute();
                        Log.d("response_obt","\t 8");
                    }
                }
            }else {
                Log.d("response_obt","\t 9");
                call = client.newCall(request);
                response=call.execute();
                Log.d("response_obt","\t 10");
            }
            return response.body().string();
        }catch (Exception e){
            Log.d("response_obt",e.getMessage());
            return "";
        }
    }

    @Override
    protected void onDestroy() {
        mViewPager.setAdapter(null);
        super.onDestroy();
    }

    /**
     * A {@link FragmentStatePagerAdapter} that returns a fragment
     * representing an object in the collection.
     */
    public class CarTypeCollectionPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<PubnubCarTypes> carTypes;
        CarTypeCollectionPagerAdapter(FragmentManager fm, ArrayList<PubnubCarTypes> carTypes) {
            super(fm);
            this.carTypes = carTypes;
        }
        @Override
        public Fragment getItem(int i) {
            CarTypeObjectFragment fragment = new CarTypeObjectFragment();
            fragment.Fragment_type = filterResponse.getTypes().get(i).getType_id();
            Bundle args = new Bundle();
            args.putSerializable(CarTypeObjectFragment.ARG_CARTYPE, carTypes.get(i)); // Our object is just an integer :-P
            args.putSerializable(CarTypeObjectFragment.ARG_CAR_FARE, fare_for_all_cartypes.get(i)); // Our object is just an integer :-P
            fragment.setArguments(args);
            return fragment;
        }
        @Override
        public int getCount() {
            return carTypes.size();
        }
        void updateCartypes(ArrayList<PubnubCarTypes> carTypes1) {
            if (carTypes1 != null && CarTypeCollectionPagerAdapter.this != null){
                carTypes = carTypes1;
                notifyDataSetChanged();
            }
        }
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return filterResponse.getTypes().get(position).getType_name();
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class CarTypeObjectFragment extends Fragment
//            implements Observer
    {
        public static final String ARG_CARTYPE = "CARTYPE";
        public static final String ARG_CAR_FARE = "ARG_CAR_FARE";
        PubnubCarTypes pubnubCarTypes;
        FareCalData fareCalData;
        TextView eta_in_infragment_tv;
        public String Fragment_type;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.single_view_for_car_type_list, container, false);
            Bundle args = getArguments();
            pubnubCarTypes = (PubnubCarTypes) args.getSerializable(ARG_CARTYPE);
            fareCalData = (FareCalData) args.getSerializable(ARG_CAR_FARE);
            initializeview(rootView);
            return rootView;
        }

        private void initializeview(View view){
            ((TextView) view.findViewById(R.id.estimated_fare_amount_tv)).setText(getString(R.string.currencuSymbol)+" "+fareCalData.getFare().trim());
            eta_in_infragment_tv = (TextView) view.findViewById(R.id.eta_in_infragment_tv);
            eta_in_infragment_tv.setText(getString(R.string.estimated_arrival_time)+" "+pubnubCarTypes.getEta());
            Picasso.with(getActivity()).load(pubnubCarTypes.getVehicle_img_off())
                    .transform(new CircleTransform()).into(((ImageView) view.findViewById(R.id.car_type_image_iv)));
            ((ImageView) view.findViewById(R.id.car_type_image_iv)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(),CarDescriptionActivity.class);
                    intent.putExtra("PubnubCarTypes",pubnubCarTypes);
                    startActivity(intent);
                }
            });
            ((ImageView) view.findViewById(R.id.car_info_iv)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(),CarFareDetailsActivity.class);
                    intent.putExtra("PubnubCarTypes",pubnubCarTypes);
                    startActivity(intent);
                }
            });
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PAYMENT_REQUEST){
            if(resultCode== Activity.RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("PAYMENT_TYPE")) {
                    payment_type_selected = bundle.getString("PAYMENT_TYPE");
                    Log.d("PAYMENT_TYPE",payment_type_selected);
                    if (payment_type_selected.equals("1")) {
                        cash_dollar_symbol_tv.setVisibility(View.GONE);
                        selected_card_image_iv.setVisibility(View.VISIBLE);
                        cardDetails = (CardDetails) bundle.getSerializable("CardDetails");
                        final int drawable = Utility.setCreditCardLogo(cardDetails.getType());
                        selected_card_image_iv.setImageResource(drawable);
                        card_or_cash_selected_tv.setText("**** **** **** "+getLast4Digits(cardDetails.getLast4()));
                        selected_card_image_iv.setImageResource(drawable);
                        session.setPaymentType("1");
                    } else {
                        cash_dollar_symbol_tv.setVisibility(View.VISIBLE);
                        selected_card_image_iv.setVisibility(View.GONE);
                        card_or_cash_selected_tv.setText(getResources().getString(R.string.cash));
                        session.setPaymentType("2");
                    }
                    if (bundle.containsKey("Promo_Code")) {
                        promo_code_applied = bundle.getString("Promo_Code");
                    }
                } else {
                    Toast.makeText(RouteAndPaymentSelectActivity.this, "" + getString(R.string.please_select_your_payment), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private String getLast4Digits(String name) {
        return name.substring(name.length() - 4);
    }
    /**
     * To get the directions url between pickup location and drop location
     */
    private String getMapsApiDirectionsFromTourl(double srcLat,double srcLong,double destLat,double destLong) {
        Utility.printLog("getMapsApiDirectionsFromTourl HomePageFragment Driver reached  ");
        Utility.printLog("getDropofflat=" + session.getDropofflat()
                + " getDropofflng=" + session.getDropofflng());
        String url="";;
        if(googleMap!=null)
        {
                /*
                 * String waypoints = "waypoints=optimize:true|" +
                 * session.getPickuplat() + "," +session.getPickuplng() + "|" + "|" +
                 * session.getDropofflat() + "," + session.getDropofflng();
                 */
            String waypoints = "origin=" + destLat + ","
                    + destLong + "&" + "destination="
                    + srcLat + "," +  srcLong;
            String sensor = "sensor=false";
            String params = waypoints + "&" + sensor;
            String output = "json";
            url = "https://maps.googleapis.com/maps/api/directions/"
                    + output + "?" + params;
        }
        return url;
    }

    /**
     * plotting the directions in google map
     */
    class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            Utility.printLog("HomePageFragment Driver on the way onPostExecute");
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;
            // traversing through routes
            if(routes!=null)
            {
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    polyLineOptions.addAll(points);
                    polyLineOptions.width(8);
                    polyLineOptions.geodesic(true);
                    polyLineOptions.color(getResources().getColor(R.color.black));
                }
                if (polyLineOptions != null)
                    polylineFinal=googleMap.addPolyline(polyLineOptions);
            }
        }
    }

    //plotting the line for the tracking
    class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
            }
            return data;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }
}
