package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * @author Akbar
 */
public class InvoiceClass implements Serializable
{
    /*    "apntDt":"2016-10-04 18:02:03",
    "pPic":"DP_201610314157.jpg",
    "email":"ashish@mobifyi.com",
    "status":"Completed",
    "bid":24,
    "fname":"Ashish",
    "r":"2.5",
    "drop_dt":"2016-10-04 12:02:27",
    "apntDate":"2016-10-04",
    "addrLine1":"54, RBI Colony, Hebbal, Bengaluru, Karnataka 560024, India",
    "dropLine1":"Creative Villa Apartment, 44 RBI Colony, Vishveshvaraiah Nagar, Ganga Nagar, Bengaluru, Karnataka 560024, India",
    "routeImg":"",
    "invoice":[],
    "amount":390,
    "statCode":9*/
    private String amount;
    private String TripDistanceFee;
    private String TripTimeFee;
    private String airportFee;
    private String discountVal;
    private String subtotal;
    private String tip;
    private String payType;
    private String tipPercent,code;
    private String TripTime;
    private String min_fare;
    private String baseFare;
    private String TripDistance;
    private String walletDeducted;
    private String cancel_dt;
    private String Favouritepickup;
    private String type_name;


    public String getCardDeduct() {
        return cardDeduct;
    }

    private String cardDeduct;

    public String getFavouritedrop() {
        return Favouritedrop;
    }

    public String getFavouritepickup() {
        return Favouritepickup;
    }

    private String Favouritedrop;

    public String getCancelTime() {
        return cancelTime;
    }

    private String cancelTime;

    public String getCancel_dt() {
        return cancel_dt;
    }

    public String getCancelTimeApplied() {
        return cancelTimeApplied;
    }
    private String cancelTimeApplied;
    public String getDuePayment() {
        return duePayment;
    }
    private String duePayment;
    public String getSumOfLastDue() {
        return sumOfLastDue;
    }
    private String sumOfLastDue;
    public String getWaitingTime() {
        return waitingTime;
    }
    private String waitingTime;
    public String getStateCode() {
        return stateCode;
    }
    private String stateCode;
    public String getWaitingFee() {
        return waitingFee;
    }
    private String waitingFee;
    public String getCashCollected() {
        return cashCollected;
    }
    private String cashCollected;
    public String getCardlastDigit() {
        return cardlastDigit;
    }
    private String cardlastDigit;
    public String getWalletDebitCredit() {
        return walletDebitCredit;
    }
    private String walletDebitCredit;
    public String getTripTime() {
        return TripTime;
    }
    public String getTripDistance() {
        return TripDistance;
    }
    public String getCode() {
        return code;
    }
    public String getWalletDeducted() {
        return walletDeducted;
    }
    public String getTripDistanceFee() {
        return TripDistanceFee;
    }
    public String getTripTimeFee() {
        return TripTimeFee;
    }
    public String getAirportFee() {
        return airportFee;
    }
    public String getDiscountVal() {
        return discountVal;
    }
    public String getSubtotal() {
        return subtotal;
    }
    public String getTip() {
        return tip;
    }
    public String getPayType() {
        return payType;
    }
    public String getTipPercent() {
        return tipPercent;
    }
    public String getMin_fare() {
        return min_fare;
    }

    public String getType_name() {
        return type_name;
    }

    public String getBaseFare() {
        return baseFare;
    }
    public String getAmount() {
        return amount;
    }
}
