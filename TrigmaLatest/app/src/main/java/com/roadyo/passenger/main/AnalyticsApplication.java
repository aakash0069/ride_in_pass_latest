/*
 * Copyright Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.roadyo.passenger.main;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;
import com.flurry.android.FlurryAgent;

/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as.
 */
public class AnalyticsApplication extends Application {
    ActivityLifecycleCallbacks lifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//            Log.d("ActivityLifecycleCallRK","onActivityCreated");
        }

        @Override
        public void onActivityStarted(Activity activity) {
//            Log.d("ActivityLifecycleCallRK","onActivityStarted");
            FlurryAgent.onStartSession(activity, "2TFG4VCCJN2DDNS6ZPPC");
        }

        @Override
        public void onActivityResumed(Activity activity) {
//            Log.d("ActivityLifecycleCallRK","onActivityResumed");
        }

        @Override
        public void onActivityPaused(Activity activity) {
//            Log.d("ActivityLifecycleCallRK","onActivityPaused");
        }

        @Override
        public void onActivityStopped(Activity activity) {
//            Log.d("ActivityLifecycleCallRK","onActivityStopped");
            FlurryAgent.onEndSession(activity);
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
//            Log.d("ActivityLifecycleCallRK","onActivityDestroyed");
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreate() {
        super.onCreate();

        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withContinueSessionMillis(10)
                .build(this, "2TFG4VCCJN2DDNS6ZPPC");

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        AppLinkData.fetchDeferredAppLinkData(this,
                new AppLinkData.CompletionHandler() {
                    @Override
                    public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                        // Process app link data
                    }
                }
        );
        registerActivityLifecycleCallbacks(lifecycleCallbacks);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
