package com.roadyo.passenger.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.google.gson.Gson;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.roadyo.passenger.pojo.LiveBookingResponse;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Slider;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.threembed.utilities.WaveDrawable;
import static com.fcm.MyFirebaseMessagingService.isApplicationSentToBackground;

/************************************************************
 * RequestPickup activity is used for sending request to drivers for live booking.
 * If no driver will accept your request simply this activity will finish.
 *
 * Here i'm getting all data from HomeFragment.
 *
 ************************************************************/

public class RequestPickup extends Activity {
	private String from_latitude;
	private String from_longitude;
	private String to_latitude;
	private String to_longitude;
	private String pickup_address;
	private String dropoff_address;
	private String getBookingResponse;
	private String car_Id;
	private String laterBookingDate;
	private ArrayList<String> nearestDrivers = new ArrayList<>();
	private String surge = "",walletAmount="",wallet_tag="",ent_fare_estimate="";
	private SessionManager session;
	private SubscribeCallback subscribeCallback;
	CountDownTimer countDownTimer;
	private int expiryForBooking;
	public  PubNub pubnub;
	private int progressStatus = 0;
	private  SeekBar seekBar1;
	private String fav_pick,fav_drop,ent_sub_type;
	private String current_master_type_id;
	private BroadcastReceiver receiver;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.request_pickup);
		getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

		Utility.printLog("current_master_type_id " +current_master_type_id);
		Utility.printLog("RequestPickup Livebooking nearestDrivers size=" + nearestDrivers.size());
		subscribeCallback = new SubscribeCallback() {
			@Override
			public void status(PubNub pubnub, PNStatus status) {
			}
			@Override
			public void message(final PubNub pubnub, final PNMessageResult message)
			{
				try {
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							String pubnubA = null;
							Utility.printLog("response from tiggge message  " + message.getMessage().toString());
							try
							{
								JSONObject jsonObject=new JSONObject(message.getMessage().toString());
								pubnubA=jsonObject.getString("a");
								Utility.printLog("a tag in reposne "+pubnubA);
								switch (pubnubA)
								{
									case "4":   // when user rejects or accepts
									{
										try
										{
											switch (jsonObject.getString("status"))
											{
												case "6": //when the booking is accepted by driver  and he is on the way
												{
													//store the data and make the necessary variables true
													session.setBookingReponseFromPubnub(message.getMessage().toString());
													session.setDriverOnWay(true);
													Utility.printLog("response from sesssions RequestPickup"+message.getMessage().toString());
													session.setDriverArrived(false);
													session.setTripBegin(false);
													session.setInvoiceRaised(false);

													//unsubscrieb the channel and move back to home page for tracking
													List<String> subscribed_channels = pubnub.getSubscribedChannels();
													if(subscribed_channels.contains(session.getChannelName()))
													{
														pubnub.unsubscribe()
																.channels(Arrays.asList(session.getChannelName()))
																.execute();
													}
													pubnub.removeListener(subscribeCallback);
													countDownTimer.cancel();
													//to unregister the receiver for the push
													try {
														if (receiver != null) {
															RequestPickup.this.unregisterReceiver(receiver);
														}
													} catch (IllegalArgumentException e) {
														Utility.printLog("reciver is already unregistered");
														receiver = null;
													}
													Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
													returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
													startActivity(returnIntent);
													finish();
													overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
													break;
												}
											}
										} catch (JSONException e) {
											Utility.printLog("exception in success " + e);
										}
										break;
									}
									case "10":
									{
										//is the app is in background save the message and show on homepage
										if(isApplicationSentToBackground(RequestPickup.this))
										{
											try {
												if(!session.getAllBusyPushMessage().equals(""))
													session.setAllBusyPushMessage(jsonObject.getString("msg"));
											} catch (JSONException e) {
												e.printStackTrace();
											}
										}
										//if the status =10 then no drivers accepted booking so will show the popup and return to homepage
										List<String> subscribed_channels = pubnub.getSubscribedChannels();
										if(subscribed_channels.contains(session.getChannelName()))
										{
											pubnub.unsubscribe()
													.channels(Arrays.asList(session.getChannelName()))
													.execute();
										}
										//to unregister the receiver for the push
										try {
											if (receiver != null) {
												RequestPickup.this.unregisterReceiver(receiver);
											}
										} catch (IllegalArgumentException e) {
											Utility.printLog("reciver is already unregistered");
											receiver = null;
										}
										pubnub.removeListener(subscribeCallback);
										countDownTimer.cancel();
										session.setBookingReponseFromPubnub("");
										Utility.printLog("response from sesssions RequestPickup2 "+session.getBookingReponseFromPubnub());
										Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
										returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
										returnIntent.putExtra("MESSAGE",""+getString(R.string.no_drivers_found1));
										startActivity(returnIntent);
										finish();
										overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
										break;
									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
								Utility.printLog("a tag in exception "+e);
							}
						}
					});
				} catch (Exception e)
				{
					Utility.printLog("exception in success " + e);
				}
			}
			@Override
			public void presence(PubNub pubnub, PNPresenceEventResult presence) {
			}
		};

		initialize();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		/**
		 * to subscribe passenger channel to get the data for types get all available drivers
		 */
		List<String> subscribed_channels = pubnub.getSubscribedChannels();
		if(!subscribed_channels.contains(session.getChannelName()))
		{
			pubnub.subscribe()
					.channels(Arrays.asList(session.getChannelName())) // subscribe to channel groups
					.withPresence()// also subscribe to related presence information
					.execute();
		}
		//add listeneer of pubnub to handle the data gained from pubnub
		pubnub.addListener(subscribeCallback);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		/**
		 * if the app goes background then unsubscribe all channels subscribed
		 */
		List<String> subscribed_channels = pubnub.getSubscribedChannels();
		if(subscribed_channels.contains(session.getChannelName()))
		{
			pubnub.unsubscribe()
					.channels(Arrays.asList(session.getChannelName()))
					.execute();
		}
		pubnub.removeListener(subscribeCallback);
		//to unregister the receiver for the push
		try {
			if (receiver != null) {
				this.unregisterReceiver(receiver);
			}
		} catch (IllegalArgumentException e) {
			Utility.printLog("reciver is already unregistered");
			receiver = null;
		}
	}

	/**
	 * Initializing all variables in my view
	 */
	private void initialize() {
		session=new SessionManager(this);
		//to listen the push action
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.bloomingdalelimousine.ridein.push");
		receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				Utility.printLog("response from push in DISPATCH "+intent.getStringExtra("ACTION"));
				if(intent.getStringExtra("ACTION") !=null)
				{
					if(intent.getStringExtra("ACTION") .equals("421"))
					{
						//if push with action=421 then close the dispatch screen and return to homepage
						List<String> subscribed_channels = pubnub.getSubscribedChannels();
						if(subscribed_channels.contains(session.getChannelName()))
						{
							pubnub.unsubscribe()
									.channels(Arrays.asList(session.getChannelName()))
									.execute();
						}
						//to unregister the receiver for the push
						try {
							if (receiver != null) {
								RequestPickup.this.unregisterReceiver(receiver);
							}
						} catch (IllegalArgumentException e) {
							Utility.printLog("reciver is already unregistered");
							receiver = null;
						}
						pubnub.removeListener(subscribeCallback);
						countDownTimer.cancel();
						session.setBookingReponseFromPubnub("");
						Utility.printLog("response from sesssions RequestPickup3 "+session.getBookingReponseFromPubnub());
						Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
						returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						returnIntent.putExtra("MESSAGE",""+getString(R.string.no_drivers_found1));
						startActivity(returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
				}
			}
		};
		//to register the reciver
		registerReceiver(receiver, filter);
		RelativeLayout rrwaveout = (RelativeLayout) findViewById(R.id.rrwaveout);
		TextView requesting_text = (TextView) findViewById(R.id.requesting_text);
		WaveDrawable waveDrawable = new WaveDrawable(ContextCompat.getColor(this,  R.color.background), 450);
		seekBar1= (SeekBar) findViewById(R.id.seekBar1);
		Slider cancel_seek_bar = (Slider) findViewById(R.id.cancel_seek_bar);
		seekBar1.setEnabled(false);

		/**
		 * to get the params from home which are neccessory for booking request..
		 */
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			if(extras.getBoolean("ALREADY_SENT_BOOKING"))
			{
				expiryForBooking= Integer.parseInt(extras.getString("EXPIRED_TIME"));
				// start timer for booking for the peak time configured from backend
				triggerBoookingTimer();
			}
			else
			{
				from_latitude = extras.getString("FromLatitude");
				from_longitude = extras.getString("FromLongitude");
				to_latitude = extras.getString("ToLatitude");
				to_longitude = extras.getString("ToLongitude");
				pickup_address = extras.getString("PICKUP_ADDRESS");
				dropoff_address = extras.getString("DROPOFF_ADDRESS");
				nearestDrivers = (ArrayList<String>) extras.getSerializable("my_drivers");
				ent_fare_estimate = extras.getString("ent_fare_estimate");
				car_Id = extras.getString("Car_Type");
				String payment_type = extras.getString("PAYMENT_TYPE");
				laterBookingDate = extras.getString("Later_Booking_Date");
				surge = extras.getString("SURGE");
				current_master_type_id = extras.getString("CURRENT_MASTER_TYPE_ID");
				walletAmount = extras.getString("WALLET_BAL");
				wallet_tag = extras.getString("WALLET");
				fav_pick = extras.getString("FAV_PICK");
				fav_drop = extras.getString("FAV_DROP");
				ent_sub_type = extras.getString("SUB_TYPE");
				if(Utility.isNetworkAvailable(RequestPickup.this))
				{
					NewBackGroundLiveBooking();
				}
				else
				{
					//to unregister the receiver for the push
					try {
						if (receiver != null) {
							RequestPickup.this.unregisterReceiver(receiver);
						}
					} catch (IllegalArgumentException e) {
						Utility.printLog("reciver is already unregistered");
						receiver = null;
					}
					Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
					returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					Utility.showToast(RequestPickup.this,getString(R.string.network_connection_fail));
					startActivity(returnIntent);
					finish();
					overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
				}
			}
		}
		/**
		 * to set language support
		 */
		Utility.setLanguageSupport(this);

		/**
		 * if seek bar has progress > 65 progress then cancel the booking by calling the API
		 */
		cancel_seek_bar.setSliderProgressCallback(new Slider.SliderProgressCallback() {
			@Override
			public void onSliderProgressChanged(int progress) {
				if (progress > 65)
				{
					if(Utility.isNetworkAvailable(RequestPickup.this))
						cancelRequestPickup();
					else
						Utility.showToast(RequestPickup.this,getString(R.string.network_connection_fail));
				}
			}
		});
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
		{
			rrwaveout.setBackgroundDrawable(waveDrawable);
		}
		else
		{
			rrwaveout.setBackground(waveDrawable);
		}
		Interpolator interpolator = new LinearInterpolator();
		waveDrawable.setWaveInterpolator(interpolator);
		waveDrawable.startAnimation();

		PNConfiguration pnConfiguration = new PNConfiguration();
		pubnub = new PubNub(pnConfiguration);

		/**
		 * initializing the pubnub with keys from shared preferance
		 */
		pnConfiguration.setUuid("s_"+session.getSid());
		if(!session.getPublishKey().equals("") && !session.getSubscribeKey().equals(""))
		{
			Utility.printLog("pub publish key "+session.getPublishKey());
			Utility.printLog("sub publish key "+session.getSubscribeKey());
			pnConfiguration.setSubscribeKey(session.getSubscribeKey());
			pnConfiguration.setPublishKey(session.getPublishKey());
		}
		session = new SessionManager(RequestPickup.this);
		if(session.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(this,requesting_text);
		}
	}

	private void cancelRequestPickup() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("ent_sess_token", session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_booking_id", session.getBookingId());
			Utility.printLog("success for cancelling the request   " + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+ "cancelAppointmentRequest", jsonObject,
				new OkHttpRequestObject.JsonRequestCallback() {
			@Override
			public void onSuccess(String result) {
				Utility.printLog("success for cancelling the request   " + result);
				Utility.printLog("RequestPickup Livebooking  CancelRequestPickup status=" );
				/**
				 * if user cancels the booking then dismiss the timer running and unsubscribe the subscribed channels
				 */
				getBookingResponse = result;
				if(countDownTimer!=null)
					countDownTimer.cancel();
				getCancelInfo();
			}
			@Override
			public void onError(String error) {
				Utility.showToast(RequestPickup.this,getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	/**
	 * Handling cancel response
	 */
	private void getCancelInfo() {
		Gson gson = new Gson();
		LiveBookingResponse liveBookingResponse = gson.fromJson(getBookingResponse, LiveBookingResponse.class);
		if (liveBookingResponse != null)
		{
			//cancel the booking activity if user cancels
			if (liveBookingResponse.getErrFlag().equals("0")) {
				session.setBookingCancelled(true);
				session.storeBookingId("0");
				session.storeAptDate(null);
				List<String> subscribed_channels = pubnub.getSubscribedChannels();
				if(subscribed_channels.contains(session.getChannelName()))
				{
					pubnub.unsubscribe()
							.channels(Arrays.asList(session.getChannelName()))
							.execute();
				}
				pubnub.removeListener(subscribeCallback);
				session.setBookingReponseFromPubnub("");
				Utility.printLog("response from sesssions RequestPickup4 "+session.getBookingReponseFromPubnub());
				Intent returnIntent = new Intent();
				returnIntent.putExtra("MESSAGE",liveBookingResponse.getErrMsg());
				setResult(RESULT_CANCELED, returnIntent);
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
			}
			else
			{
				Utility.showToast(RequestPickup.this,liveBookingResponse.getErrMsg());
			}
		}
	}

	/**
	 * calling an API for booking driver to ge tthe booking Id and timer for booking along with the timer per driver
	 * @INPUT Session token, device id, car work type, pickup address, pickup latitude, pickup longitude
	 * payment type, zipcode, drop location, current date time
	 */
	private void NewBackGroundLiveBooking()
	{
		int i = 1;

		final JSONObject[] jsonObject = {new JSONObject()};
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject[0].put("ent_sess_token", session.getSessionToken());
			jsonObject[0].put("ent_dev_id", session.getDeviceId());
			jsonObject[0].put("ent_wrk_type", car_Id);
			jsonObject[0].put("ent_addr_line1", pickup_address);
			jsonObject[0].put("ent_lat", from_latitude);
			jsonObject[0].put("ent_long", from_longitude);
			if (dropoff_address != null) {
				jsonObject[0].put("ent_drop_addr_line1", dropoff_address);
				jsonObject[0].put("ent_drop_lat", to_latitude);
				jsonObject[0].put("ent_drop_long", to_longitude);
			}
			if (!session.getPromocode().equals("")) {
				jsonObject[0].put("ent_coupon", session.getPromocode());
			}
			jsonObject[0].put("ent_payment_type", session.getPaymentType());
			if(session.getPaymentType().equals("1")){
				jsonObject[0].put("ent_token", session.getCardToken());
			}
			jsonObject[0].put("ent_extra_notes", " ");
			jsonObject[0].put("ent_card_id", " ");
			jsonObject[0].put("ent_later_dt", laterBookingDate);
			jsonObject[0].put("ent_date_time", curenttime);
			jsonObject[0].put("ent_surge", surge);
			jsonObject[0].put("ent_wallet_bal", walletAmount);
			jsonObject[0].put("ent_wallet", wallet_tag);
			jsonObject[0].put("ent_fare_estimate", ent_fare_estimate);
			jsonObject[0].put("ent_payment_type", session.getPaymentType());
			if(session.getPaymentType().equals("1")){
				jsonObject[0].put("ent_token", session.getCardToken());
			}

			if(fav_pick!=null)
				jsonObject[0].put("ent_fav_pickup", fav_pick);
			if(fav_drop!=null)
				jsonObject[0].put("ent_fav_drop", fav_drop);

			jsonObject[0].put("ent_additional_info",session.getDriverCooments());
			jsonObject[0].put("ent_sub_type",ent_sub_type);
			Utility.printLog("params to live booking " + jsonObject[0]);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"liveBooking", jsonObject[0], new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				Utility.printLog("RequestPickup live booking response "+result);
				try {
					JSONObject jsonObject = new JSONObject(result);
					String errFlag = jsonObject.getString("errFlag");
					String errNum = jsonObject.getString("errNum");
					String errMsg = jsonObject.getString("errMsg");

					if (errFlag.equals("0")) {
						expiryForBooking = jsonObject.getInt("ExpiryForBooking");
						Utility.printLog("Livebooking Response: expiry  "+expiryForBooking+" "+ jsonObject.getString("cancellation_peak_time"));
						try {
							session.storeBookingId(jsonObject.getString("BookingId"));
							// start timer for booking for the peak time configured from backend
							triggerBoookingTimer();
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					//if the session is expired then logout the user and send to splashactivity
					else if(errNum.equals("6") || errNum.equals("7") ||
							errNum.equals("94") || errNum.equals("96"))
					{
						Utility.showToast(RequestPickup.this,errMsg);
						Intent i = new Intent(RequestPickup.this, SplashActivity.class);
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(i);
						overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
					}
					//if error while calling the API then send the message and return to homepage
					else
					{
						Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
						returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
						returnIntent.putExtra("MESSAGE",errMsg);
						startActivity(returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Utility.printLog("RequestPickup catch reason "+e.getMessage());
				}
			}

			@Override
			public void onError(String error)
			{
				Utility.printLog("on error for the login "+error);
				Utility.showToast(RequestPickup.this,getString(R.string.network_connection_fail));
			}
		});
	}

	@Override
	public void onBackPressed()
	{

	}

	private void triggerBoookingTimer()
	{
		seekBar1.setMax(expiryForBooking);
		Utility.printLog("max time for seek "+expiryForBooking);
		countDownTimer = new CountDownTimer(expiryForBooking*1000, 1000)
		{
			public void onTick(long millisUntilFinished)
			{
				//to set the seekbar with the progress
				Utility.printLog("max time for response from tiggge seconds remaining: " + millisUntilFinished / 1000);
				seekBar1.setProgress(progressStatus++);
			}
			public void onFinish()
			{
				//if timer expires then show the popup and return to homepage
				List<String> subscribed_channels = pubnub.getSubscribedChannels();
				if(subscribed_channels.contains(session.getChannelName()))
				{
					pubnub.unsubscribe()
							.channels(Arrays.asList(session.getChannelName()))
							.execute();
				}
				pubnub.removeListener(subscribeCallback);
				countDownTimer.cancel();
				session.setBookingReponseFromPubnub("");
				//to unregister the receiver for the push
				try {
					if (receiver != null) {
						RequestPickup.this.unregisterReceiver(receiver);
					}
				} catch (IllegalArgumentException e) {
					Utility.printLog("reciver is already unregistered");
					receiver = null;
				}
				Utility.printLog("response from sesssions RequestPickup5 "+session.getBookingReponseFromPubnub());
				Intent returnIntent = new Intent(RequestPickup.this, MainActivity.class);
				returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				returnIntent.putExtra("MESSAGE",""+getString(R.string.no_drivers_found1));
				startActivity(returnIntent);
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
			}
		}.start();
	}
}
