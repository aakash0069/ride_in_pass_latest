package com.roadyo.passenger.pojo;

public class SlaveAppointments {
	/*{
    "errNum":"21",
    "errFlag":"0",
    "errMsg":"Got the details!",
    "cc_fee":"0",
    "cancel_status":"",
    "cancelAmt":"0",
    "code":"",
    "discount":"0",
    "tip":"0",
    "tipPercent":"0",
    "waitTime":"0",
    "statCode":"6",
    "distanceFee":"0",
    "tollFee":"0",
    "airportFee":"0",
    "parkingFee":"0",
    "fName":"M",
    "lName":"bnmn",
    "mobile":"944508569",
    "addr1":"16, 1st Main Rd, SBM Colony, Anandnagar, Hebbal",
    "addr2":"",
    "dropAddr1":"",
    "dropAddr2":"",
    "amount":"10.00",
    "pPic":"aa_default_profile_pic.gif",
    "dis":"0",
    "dur":"",
    "fare":"0",
    "pickLat":"13.0311",
    "pickLong":"77.5883",
    "ltg":"13.0288383,77.5896556",
    "dropLat":"0",
    "dropLong":"0",
    "apptDt":"2016-05-08 10:48:58",
    "pickupDt":"",
    "dropDt":"",
    "email":"m@gmail.com",
    "dt":"20160508104858",
    "bid":"98",
    "apptType":"1",
    "chn":"qd_867634021585144",
    "plateNo":"638653",
    "model":"Clio3",
    "payStatus":"1",
    "reportMsg":"",
    "payType":"2",
    "avgSpeed":"0",
    "share":"http:\/\/www.roadyo.in\/roadyo1.0\/admin\/track.php?id=98",
    "carImage":"http:\/\/www.roadyo.in\/roadyo1.0\/pics\/",
    "r":"3.9"

	}*/
	private String errFlag,errMsg;
	private InvoicePojo Invoice;

	public String getErrFlag() {
		return errFlag;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public InvoicePojo getInvoice() {
		return Invoice;
	}
}
