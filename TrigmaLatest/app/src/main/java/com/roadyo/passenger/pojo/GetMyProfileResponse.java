package com.roadyo.passenger.pojo;

public class GetMyProfileResponse {
/*	
    "errNum": "33",
    "errFlag": "0",
    "errMsg": "Got the details!",
    "fName": "sourabh1",
    "lName": "t",
    "email": "sorabh321@gmail.com",
    "phone": "23456",
    "pPic": "aa_default_profile_pic.gif"*/

	String errNum;
	String errFlag;
	String errMsg;
	private String fName;

	public String getlName() {
		return lName;
	}

	private String lName;
	private String email;
	private String phone;
	private String pPic;
	private String gender;

	public String getDob() {
		return dob;
	}

	public String getGender() {
		return gender;
	}

	private String dob;

	public String getErrNum() {
		return errNum;
	}

	public void setErrNum(String errNum) {
		this.errNum = errNum;
	}

	public String getErrFlag() {
		return errFlag;
	}

	public void setErrFlag(String errFlag) {
		this.errFlag = errFlag;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getfName() {
		return fName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getpPic() {
		return pPic;
	}
}
