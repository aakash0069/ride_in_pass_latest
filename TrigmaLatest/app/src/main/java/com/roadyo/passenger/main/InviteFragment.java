package com.roadyo.passenger.main;

import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bloomingdalelimousine.ridein.R;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class InviteFragment extends Fragment implements OnClickListener
{
    private  View view;
    private SessionManager session;
    private ShareDialog shareDialog;
    private LinearLayout referal_details;
    private RelativeLayout progress_bar_layout;
    private TextView coupon_code,share_text;
    private String shareMessage="";
    private TextView title_msg_tv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null)
        {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        view = inflater.inflate(R.layout.new_share_ayout, container, false);
        initialize();
        if(!Utility.isNetworkAvailable(getActivity()))
        {
            Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.internetStatus");
            homeIntent.putExtra("STATUS", "0");
            getActivity().sendBroadcast(homeIntent);
        }
        return view;
    }

    private void getReferalcodePageDetails()
    {
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id",session.getDeviceId());
            jsonObject.put("ent_dev_type","2");
            Utility.printLog("params to referal "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getReferralCode", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog("result for referal details  "+result);
                if(result!=null) {
                    referal_details.setVisibility(View.VISIBLE);
                    progress_bar_layout.setVisibility(View.GONE);
                    try {
                        JSONObject jsnResponse = new JSONObject(result);
                        String errFlag = jsnResponse.getString("errFlg");
                        String mErrNum = jsnResponse.getString("errNo");
                        String errMsg=jsnResponse.getString("errMsg");
                        if(mErrNum.equals("6") || mErrNum.equals("7") ||
                                mErrNum.equals("94") || mErrNum.equals("96"))
                        {
                            Utility.showToast(getActivity(),errMsg);
                            Intent i = new Intent(getActivity(), SplashActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(i);
                            getActivity().overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        }
                        else if(errFlag.equals("0"))
                        {
                            coupon_code.setText(jsnResponse.getString("referralCode"));
//                            share_text.setText(jsnResponse.getString("referralTitle"));
                            title_msg_tv.setText(jsnResponse.getString("referralTitle"));
                            shareMessage=jsnResponse.getString("referralBody");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utility.printLog("catch reason "+e);
                    }
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                progress_bar_layout.setVisibility(View.GONE);
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }
    private void initialize()
    {

//		ImageView facebook_share = (ImageView) view.findViewById(R.id.facebook_share);
//		ImageView twitter_share = (ImageView) view.findViewById(R.id.twitter_share);
//		ImageView whatsapp_share = (ImageView) view.findViewById(R.id.whatsapp_share);
//		ImageView email_share = (ImageView) view.findViewById(R.id.email_share);

		coupon_code = (TextView) view.findViewById(R.id.share_code);
        title_msg_tv = (TextView) view.findViewById(R.id.title_msg_tv);

//		referal_details = (RelativeLayout) view.findViewById(R.id.referal_details);
		referal_details = (LinearLayout) view.findViewById(R.id.referal_details);
		progress_bar_layout = (RelativeLayout) view.findViewById(R.id.progress_bar_layout);
//		share_text = (TextView)view.findViewById(R.id.share_text);
//		TextView txt_share_code = (TextView)view.findViewById(R.id.txt_share_code);
		session = new SessionManager(getActivity());

		if(Utility.isNetworkAvailable(getActivity()))
		{
			getReferalcodePageDetails();
		}
		else
		{
			Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
		}

        MainActivity activity = (MainActivity) getActivity();
        activity.fragment_title.setText(getResources().getString(R.string.refer_a_friend));
        activity.action_bar.setVisibility(View.VISIBLE);
        activity.fragment_title_logo.setVisibility(View.VISIBLE);
        activity.fragment_title.setVisibility(View.GONE);
        activity.edit_profile.setVisibility(View.GONE);

//		/**
//		 * to set the typeface
//		 */
//		if(session.getLanguageCode().equals("en"))
//		{
//			Utility.setTypefaceMuliRegular(getActivity(),txt_share_code);
//			Utility.setTypefaceMuliLight(getActivity(),share_text);
//			Utility.setTypefaceMuliLight(getActivity(),coupon_code);
//		}

        FacebookSdk.sdkInitialize(getActivity());
        shareDialog = new ShareDialog(this);

        LinearLayout facebook_share_ll = (LinearLayout) view.findViewById(R.id.facebook_share_ll);
        LinearLayout twitter_share_ll = (LinearLayout) view.findViewById(R.id.twitter_share_ll);
        LinearLayout message_share_ll = (LinearLayout) view.findViewById(R.id.message_share_ll);
        LinearLayout email_share_ll = (LinearLayout) view.findViewById(R.id.email_share_ll);

        facebook_share_ll.setOnClickListener(this);
        twitter_share_ll.setOnClickListener(this);
        message_share_ll.setOnClickListener(this);
        email_share_ll.setOnClickListener(this);

//		facebook_share.setOnClickListener(this);
//		twitter_share.setOnClickListener(this);
//		whatsapp_share.setOnClickListener(this);
//		email_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.facebook_share_ll:
            {
                if (ShareDialog.canShow(ShareLinkContent.class))
                {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(getResources().getString(R.string.app_name))
                            .setContentDescription("Use My referal code "+coupon_code.getText().toString()+" to signup")
                            .setImageUrl(Uri.parse(getString(R.string.FB_IMAGE)))
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.bloomingdalelimousine.ridein"))
                            .build();

                    shareDialog.show(linkContent);
                }
                break;
            }
            case R.id.twitter_share_ll:
            {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter"))
                    {
                        intent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }
                if(facebookAppFound)
                {
                    startActivity(intent);
                }
                else
                {
                    if (Utility.isNetworkAvailable(getActivity()))
                    {
                        Uri uri = Uri.parse("market://details?id=" + "com.twitter.android");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e)
                        {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android&hl=en")));
                        }
                    }
                    else
                    {
                        Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                    }
                }
                break;
            }
            case R.id.message_share_ll:
            {

               /* Intent intentsms = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:"));
                intentsms.putExtra( "sms_body", shareMessage);
                startActivity( intentsms );*/

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
                {
                    String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getActivity()); //Need to change the build to API 19

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);

                    if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
                    {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    getActivity().startActivity(sendIntent);

                }
                else //For early versions, do what worked for you before.
                {
                    Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                    sendIntent.setData(Uri.parse("sms:"));
                    sendIntent.putExtra("sms_body", shareMessage);
                    getActivity().startActivity(sendIntent);
                }

                break;
            }
            case R.id.email_share_ll:
            {
                Intent email = new Intent(Intent.ACTION_SENDTO);
                email.putExtra(Intent.EXTRA_SUBJECT,("Register On")+" " +getResources().getString(R.string.app_name));
                email.putExtra(Intent.EXTRA_TEXT, shareMessage);
                email.setType("text/plain");
                email.setType("message/rfc822");
                email.setData(Uri.parse("mailto:" + " "));
                startActivity(Intent.createChooser(email, getString(R.string.Choose_an_Email_client)));

                break;
            }
        }
    }
    public void onClickWhatsApp() {
        PackageManager pm=getActivity().getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Utility.showToast(getActivity(),getResources().getString(R.string.not_installed));
        }
    }
}
