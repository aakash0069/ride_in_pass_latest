package com.roadyo.passenger.main;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.roadyo.passenger.pojo.CardDetails;
import com.roadyo.passenger.pojo.GetCardResponse;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.view.View.VISIBLE;

public class SelectPaymentTypeActivity extends AppCompatActivity {

    private LinearLayout cash_layout_ll;
    private LinearLayout card_layout_ll;
    private LinearLayout add_card_layout_ll;
    private LinearLayout card_entry_layout_ll;
    private LinearLayout cards_list_ll;
    private ImageView open_cards_list_iv;
    private ImageView add_card_view_open_or_close_indicator_iv;
    private EditText _1st_four_digit_et,_2nd_four_digit_et,_3rd_four_digit_et,_4th_four_digit_et,month_et,yyyy_et,cvv_et,promo_code_et;
    private int startyy,stopyy;
    private GetCardResponse response;
    SessionManager sessionManager;

    private RecyclerView recyclerView;
    private Cards_Adapter cards_adapter;
    private ArrayList<CardDetails> rowItems;
    private TextView selected_card_number_tv;
    private ImageView selected_card_image_iv;
    private CardDetails selected_card_info;

    private String card_number;
    private Button save_card_details_btn;
    private boolean isProperMonth;
    private boolean isProperYear;
    private String access_token;
    private ProgressDialogFragment progressFragment;
    private ProgressDialog dialogL;
    private String month,year,cvv;
    private double currentLatitude;
    private double currentLongitude;
    private Button apply_promo_code_btn;
    private ScrollView scrollView;
    private View only_for_scroll_focusing_view;
    private ImageView clear_promo_code_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment_type);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            currentLatitude = bundle.getDouble("from_latitude");
            currentLongitude = bundle.getDouble("from_longitude");
        }

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        card_number = "";
        isProperMonth = false;
        isProperYear = false;
        sessionManager = new SessionManager(this);
        startyy=getYear();
        stopyy=startyy+20;

        progressFragment = ProgressDialogFragment.newInstance(R.string.progressMessage);

        cash_layout_ll = (LinearLayout) findViewById(R.id.cash_layout_ll);
        card_layout_ll = (LinearLayout) findViewById(R.id.card_layout_ll);
        add_card_layout_ll = (LinearLayout) findViewById(R.id.add_card_layout_ll);
        card_entry_layout_ll = (LinearLayout) findViewById(R.id.card_entry_layout_ll);
        cards_list_ll = (LinearLayout) findViewById(R.id.cards_list_ll);
        selected_card_number_tv = (TextView) findViewById(R.id.selected_card_number_tv);
        selected_card_image_iv = (ImageView) findViewById(R.id.selected_card_image_iv);
        open_cards_list_iv = (ImageView) findViewById(R.id.open_cards_list_iv);
        add_card_view_open_or_close_indicator_iv = (ImageView) findViewById(R.id.add_card_view_open_or_close_indicator_iv);
        save_card_details_btn = (Button) findViewById(R.id.save_card_details_btn);
        only_for_scroll_focusing_view = (View) findViewById(R.id.only_for_scroll_focusing_view);

        _1st_four_digit_et = (EditText) findViewById(R.id._1st_four_digit_et);
        _2nd_four_digit_et = (EditText) findViewById(R.id._2nd_four_digit_et);
        _3rd_four_digit_et = (EditText) findViewById(R.id._3rd_four_digit_et);
        _4th_four_digit_et = (EditText) findViewById(R.id._4th_four_digit_et);

        month_et = (EditText) findViewById(R.id.month_et);
        yyyy_et = (EditText) findViewById(R.id.yyyy_et);
        cvv_et = (EditText) findViewById(R.id.cvv_et);

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        addTextChangeListToEditText();

        cvv_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Utility.scrollToView(scrollView,only_for_scroll_focusing_view);
            }
        });


        cash_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setPaymentType("2");
                Intent intent = new Intent();
                intent.putExtra("PAYMENT_TYPE","2");
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        card_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_card_info!=null) {
                    sessionManager.setPaymentType("2");
                    sessionManager.setCardToken(selected_card_info.getId());
                    Intent intent = new Intent();
                    intent.putExtra("PAYMENT_TYPE", "1");
                    intent.putExtra("CardDetails", selected_card_info);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });

        open_cards_list_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(open_cards_list_iv.getRotation()==90) {
                    open_cards_list_iv.setRotation(270);
                    cards_list_ll.setVisibility(View.VISIBLE);
                } else {
                    open_cards_list_iv.setRotation(90);
                    cards_list_ll.setVisibility(View.GONE);
                }
            }
        });

        add_card_layout_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(add_card_view_open_or_close_indicator_iv.getRotation()==90) {
                    add_card_view_open_or_close_indicator_iv.setRotation(270);
                    card_entry_layout_ll.setVisibility(View.VISIBLE);

                } else {
                    add_card_view_open_or_close_indicator_iv.setRotation(90);
                    card_entry_layout_ll.setVisibility(View.GONE);
                }
            }
        });

        save_card_details_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _1st=_1st_four_digit_et.getText().toString(),
                        _2nd=_2nd_four_digit_et.getText().toString(),
                        _3rd=_3rd_four_digit_et.getText().toString(),
                        _4th=_4th_four_digit_et.getText().toString();

                if(_1st.isEmpty()){
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.invalid_card), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(_2nd.isEmpty()){
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.invalid_card), Toast.LENGTH_SHORT).show();
                    return;
                }
                card_number = _1st+_2nd;

                if(!_3rd.isEmpty()) {
                    card_number=_1st+_2nd+_3rd;
                    if(!_4th.isEmpty()){
                        card_number=_1st+_2nd+_3rd+_4th;
                    }
                }

                String mm = month_et.getText().toString();
                String yy = yyyy_et.getText().toString();
                String cvv1 = cvv_et.getText().toString();

                if(mm.isEmpty()){
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.please_enter_the_expiry_month), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if(isProperMonth) {
                        Toast.makeText(SelectPaymentTypeActivity.this, "" + getResources().getString(R.string.invalid_month), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if(yy.isEmpty()){
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.please_enter_the_expiry_year), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if(isProperYear) {
                        Toast.makeText(SelectPaymentTypeActivity.this, "" + getResources().getString(R.string.invalid_year), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if(cvv1.isEmpty()){
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.please_enter_the_cvv), Toast.LENGTH_SHORT).show();
                    return;
                }

                month = mm;
                year = yy;
                cvv = cvv1;

                saveCreditCard(card_number,month,year,cvv);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.cards_list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(SelectPaymentTypeActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        BackgroundGetCards();

        promo_code_et = (EditText) findViewById(R.id.promo_code_et);
        clear_promo_code_iv = (ImageView) findViewById(R.id.clear_promo_code_iv);
        apply_promo_code_btn = (Button) findViewById(R.id.apply_promo_code_btn);
        apply_promo_code_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aakash change
                if(promo_code_et.getText().toString().trim().length()>0){
                    ValidatePromoCode(promo_code_et.getText().toString());
                } else {
                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.pls_enter_promo_code), Toast.LENGTH_SHORT).show();
                }
            }
        });
        clear_promo_code_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                promo_code_et.setText("");
                sessionManager.setPromocode("");
                apply_promo_code_btn.setVisibility(View.VISIBLE);
                clear_promo_code_iv.setVisibility(View.GONE);
                promo_code_et.setEnabled(true);
//                if(promo_code_et.getText().toString().length()>0){
//                    ValidatePromoCode(promo_code_et.getText().toString());
//                } else {
//                    Toast.makeText(SelectPaymentTypeActivity.this, ""+getResources().getString(R.string.pls_enter_promo_code), Toast.LENGTH_SHORT).show();
//                }
            }
        });

        if(sessionManager.getPromocode()!=null && !sessionManager.getPromocode().equals("")){
            promo_code_et.setText("Promo Code \""+sessionManager.getPromocode()+"\" is Applied.");
            promo_code_et.setEnabled(false);
            clear_promo_code_iv.setVisibility(View.VISIBLE);
            apply_promo_code_btn.setVisibility(View.GONE);
        }

    }

    private void addTextChangeListToEditText() {
        _1st_four_digit_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==4){
                    _2nd_four_digit_et.requestFocus();
                }
            }
        });

        _2nd_four_digit_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==4){
                    _3rd_four_digit_et.requestFocus();
                }
            }
        });

        _3rd_four_digit_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==4){
                    _4th_four_digit_et.requestFocus();
                }
            }
        });

        _4th_four_digit_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length()==4){
                    month_et.requestFocus();
                }
            }
        });


        month_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                month_et.setTextColor(getResources().getColor(R.color.black));

                Log.d("month_et_setText",s.toString());
                if(!s.toString().equals("")){
                    if(s.length()==1){
                        if(Integer.parseInt(s.toString())>1){
                            month_et.setText("");
                            month_et.append("0"+s.toString());
                            yyyy_et.requestFocus();
                            isProperMonth = false;
                        }
                    } else if(s.length()==2){
                        if(Integer.parseInt(s.toString())>12){
//                                month_et.setText("0"+s.toString());
                            isProperMonth = true;
                            month_et.setTextColor(getResources().getColor(R.color.red_color));
                        } else {
                            isProperMonth = false;
                            yyyy_et.requestFocus();
                        }
                    }
                }

            }
        });

        yyyy_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                yyyy_et.setTextColor(getResources().getColor(R.color.black));

                if(!s.toString().equals("")) {
                    if(s.length()==4){
                        if(Integer.parseInt(s.toString())>stopyy || Integer.parseInt(s.toString())<startyy){
                            yyyy_et.setTextColor(getResources().getColor(R.color.red_color));
                            isProperYear = true;
                        } else {
                            isProperYear = false;
                            cvv_et.requestFocus();
                        }
                    } else {
                        isProperYear = true;
                    }
                }

            }
        });

        cvv_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>2){
                    Utility.scrollToView(scrollView,only_for_scroll_focusing_view);
                }
            }
        });
    }


    private int getYear() {
        Calendar c = Calendar.getInstance();
//        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy");
        String datetime = dateformat.format(c.getTime());
        Log.d("year_of_date",datetime);
        System.out.println(datetime);
        return Integer.parseInt(datetime);
    }


    public void BackgroundGetCards() {

        final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialog(this);
        dialogL.setCancelable(false);
        dialogL.show();
        JSONObject jsonObject = new JSONObject();
        try {
            SessionManager session = new SessionManager(SelectPaymentTypeActivity.this);
            Utility utility = new Utility();
            String curenttime = utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token", session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_date_time", curenttime);
            Utility.printLog("params to getCArds " + jsonObject);
        } catch (JSONException e) {
            dialogL.cancel();
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL) + "getCards", jsonObject, new OkHttpRequestObject.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                if (result != null ) {
                    dialogL.cancel();
                    Utility.printLog("get cards response" + result);
                    Gson gson = new Gson();
                    response = gson.fromJson(result, GetCardResponse.class);
                    Utility.printLog("prasms in on post exectute " + result + " wall bal " + response.getWalletBal());
                    if (response != null) {
                        if (response.getErrFlag().equals("0")) {

                            sessionManager.setWalletBal(response.getWalletBal());

                            if (response.getErrFlag().equals("0")) {
                                if (response.getCards().size() > 0) {
                                    rowItems = new ArrayList<>();
                                    cards_adapter = new Cards_Adapter(SelectPaymentTypeActivity.this, rowItems);
                                    recyclerView.setAdapter(cards_adapter);
                                    rowItems.addAll(response.getCards());
                                    if(!sessionManager.getCardToken().equals("")) {
                                        for (int i = 0; i<rowItems.size();i++) {
                                            if(sessionManager.getCardToken().equals(rowItems.get(i).getId())) {
                                                final int drawable = Utility.setCreditCardLogo(rowItems.get(i).getType());
                                                selected_card_number_tv.setText("**** **** **** " + getLast4Digits(rowItems.get(i).getLast4()));
                                                selected_card_image_iv.setImageResource(drawable);
                                                selected_card_info = rowItems.get(i);
                                            }
                                        }
                                    } else {
                                        sessionManager.setCardToken(rowItems.get(0).getId());
                                        final int drawable = Utility.setCreditCardLogo(rowItems.get(0).getType());
                                        selected_card_number_tv.setText("**** **** **** " + getLast4Digits(rowItems.get(0).getLast4()));
                                        selected_card_image_iv.setImageResource(drawable);
                                        selected_card_info = rowItems.get(0);
                                    }
                                    Utility.printLog("card list size " + rowItems.size());
                                    cards_adapter.notifyDataSetChanged();
                                    recyclerView.setVisibility(View.VISIBLE);
                                } else {
                                    sessionManager.setCardToken("");
                                }
                            }
                            if (response.getErrNum().equals("6") || response.getErrNum().equals("7") ||
                                    response.getErrNum().equals("94") || response.getErrNum().equals("96")) {
                                Utility.showToast(SelectPaymentTypeActivity.this,response.getErrMsg());
                                Intent i = new Intent(SelectPaymentTypeActivity.this, SplashActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                SelectPaymentTypeActivity.this.startActivity(i);
                                SelectPaymentTypeActivity.this.overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                            }
                        } else {
                            sessionManager.setCardToken("");
                            sessionManager.setWalletBal(response.getWalletBal());
                        }
                    } else {
                        Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
                    }
                } else {

                    Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.requestTimeout));
                }
            }

            @Override
            public void onError(String error) {
                dialogL.cancel();
                Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }


    public class Cards_Adapter extends RecyclerView.Adapter {
        private Context mcontext;
        private ArrayList<CardDetails> card_info_list;
        private SessionManager sessionManager;
        private ArrayList<ImageView> buttons;

        Cards_Adapter(Context mcontext, ArrayList<CardDetails> card_info_list) {
            this.mcontext = mcontext;
            this.card_info_list = card_info_list;
            sessionManager = new SessionManager(mcontext);
            buttons = new ArrayList<>();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mcontext).inflate(R.layout.card_single_layout, parent, false);
            return new Cards_Adapter.ViewHldr(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof Cards_Adapter.ViewHldr) {
                Utility.printLog("card list i adapter " + card_info_list.size());
                final Cards_Adapter.ViewHldr hldr = (Cards_Adapter.ViewHldr) holder;
                hldr.card_number.setText(getLast4Digits(card_info_list.get(position).getLast4()));

                final int drawable = Utility.setCreditCardLogo(card_info_list.get(position).getType());
                hldr.card_type_image.setImageResource(drawable);

                buttons.add(hldr.right_arrow3);

                if(sessionManager.getLanguageCode().equals("en"))
                {
                    Utility.setTypefaceMuliRegular(SelectPaymentTypeActivity.this,hldr.ends_txt);
                    Utility.setTypefaceMuliRegular(SelectPaymentTypeActivity.this,hldr.card_number);
                }

                if (!sessionManager.getCardToken().equals("")) {
                    if (card_info_list.get(position).getId().equals(sessionManager.getCardToken())) {
                        hldr.right_arrow3.setVisibility(View.VISIBLE);
                        selected_card_number_tv.setText("**** **** **** "+getLast4Digits(card_info_list.get(position).getLast4()));
                        selected_card_image_iv.setImageResource(drawable);
                        selected_card_info = card_info_list.get(position);
                    }
                }
                if(card_info_list.size()==1)
                {
                    resetAll();
                    selected_card_number_tv.setText("**** **** **** "+getLast4Digits(card_info_list.get(0).getLast4()));
                    selected_card_image_iv.setImageResource(drawable);
                    selected_card_info = card_info_list.get(0);
                    hldr.right_arrow3.setVisibility(View.VISIBLE);
                    sessionManager.setLast4Digits(getLast4Digits(card_info_list.get(0).getLast4()));
                    sessionManager.setCardToken(card_info_list.get(0).getId());
                }

                hldr.card_type_info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetAll();
                        selected_card_number_tv.setText("**** **** **** "+getLast4Digits(card_info_list.get(position).getLast4()));
                        selected_card_image_iv.setImageResource(drawable);
                        selected_card_info = card_info_list.get(position);
                        hldr.right_arrow3.setVisibility(View.VISIBLE);
                        sessionManager.setLast4Digits(getLast4Digits(card_info_list.get(position).getLast4()));
                        sessionManager.setCardToken(card_info_list.get(position).getId());
                    }
                });
                hldr.card_type_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetAll();
                        selected_card_number_tv.setText("**** **** **** "+getLast4Digits(card_info_list.get(position).getLast4()));
                        selected_card_image_iv.setImageResource(drawable);
                        selected_card_info = card_info_list.get(position);
                        hldr.right_arrow3.setVisibility(View.VISIBLE);
                        sessionManager.setLast4Digits(getLast4Digits(card_info_list.get(position).getLast4()));
                        sessionManager.setCardToken(card_info_list.get(position).getId());
                    }
                });

                hldr.delete_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Utility.isNetworkAvailable(mcontext)) {
                            final Dialog dialog = Utility.showPopupWithTwoButtons(mcontext);
                            TextView title_popup = (TextView) dialog.findViewById(R.id.title_popup);
                            TextView text_for_popup = (TextView) dialog.findViewById(R.id.text_for_popup);
                            TextView no_button = (TextView) dialog.findViewById(R.id.no_button);
                            TextView yes_button = (TextView) dialog.findViewById(R.id.yes_button);
                            title_popup.setText(mcontext.getResources().getString(R.string.alert));
                            text_for_popup.setText(mcontext.getResources().getString(R.string.delet_card));
                            yes_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    BackgroundDeleteCard(position);
                                }
                            });
                            no_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
                        }
                    }

                });
            }
        }

        private void resetAll() {
            for (int i = 0; i < buttons.size(); i++) {
                buttons.get(i).setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return card_info_list.size();
        }

        private class ViewHldr extends RecyclerView.ViewHolder {
            TextView card_number, ends_txt;
            ImageView right_arrow3, card_type_image;
            LinearLayout card_type_info;
            RelativeLayout delete_button;

            ViewHldr(View inflate) {
                super(inflate);
                card_number = (TextView) inflate.findViewById(R.id.card_number);
                ends_txt = (TextView) inflate.findViewById(R.id.ends_txt);
                card_type_image = (ImageView) inflate.findViewById(R.id.card_type_image);
                right_arrow3 = (ImageView) inflate.findViewById(R.id.right_arrow3);
                card_type_info = (LinearLayout) inflate.findViewById(R.id.card_type_info);
                delete_button = (RelativeLayout) inflate.findViewById(R.id.delete_button);
            }
        }

        private void BackgroundDeleteCard(final int position) {
            final ProgressDialog dialogL = com.threembed.utilities.Utility.GetProcessDialogNew((Activity) mcontext, mcontext.getResources().getString(R.string.deleting_card));
            if (dialogL != null) {
                dialogL.show();
            }
            JSONObject jsonObject = new JSONObject();
            try {
                Utility utility = new Utility();
                String curenttime = utility.getCurrentGmtTime();
                jsonObject.put("ent_sess_token", sessionManager.getSessionToken());
                jsonObject.put("ent_dev_id", sessionManager.getDeviceId());
                jsonObject.put("ent_cc_id", card_info_list.get(position).getId());
                jsonObject.put("ent_date_time", curenttime);
                Utility.printLog("params to delete " + jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OkHttpRequestObject.postRequest(getString(R.string.BASE_URL) + "removeCard", jsonObject, new OkHttpRequestObject.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    Utility.printLog("delete card response " + result);
                    if (result != null) {
                        String errMsg, errFlag;
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            errFlag = jsonObject.getString("errFlag");
                            errMsg = jsonObject.getString("errMsg");

                            if (dialogL != null) {
                                dialogL.dismiss();
                            }
                            if (errFlag.equals("0")) {
                                Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.card_removed));
                                card_info_list.remove(position);
                                cards_adapter.notifyDataSetChanged();
                                BackgroundGetCards();
                            } else {
                                Utility.showToast(SelectPaymentTypeActivity.this,errMsg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utility.ShowAlert(mcontext.getString(R.string.network_connection_fail), mcontext);
                    }
                }
                @Override
                public void onError(String error) {
                    if (dialogL != null) {
                        dialogL.dismiss();
                    }
                    Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
                }
            });
        }
    }


    /**
     * <h>saveCreditCard</h>
     * 	For generating the token and and saving the card on the server.
     * The value returned is as void.
     * <pre>
     *    Used for generating the token and and saving the card on the server.
     *    here it creates the card instance using the all required parameters for generating the card. parameters
     *    supplied to this methods are String cardNo,String expiryMonth,String expiryYear, and String cvv.
     *    using the above parameters inside the card instance along with the stripe key and TokenCallback() interface
     *    it generates token for the perticular enterd card info which is also refferd as access_token for perticular
     *    card.
     * </pre>
     *
     * @param cardNo String
     * @param expiryMonth String
     * @param expiryYear String
     * @param cvv String
     * @return void
     * @since 1.0
     */

    public void saveCreditCard(String cardNo,String expiryMonth,String expiryYear,String cvv)
    {
        Card card = new Card(cardNo,
                Integer.parseInt(expiryMonth),
                Integer.parseInt(expiryYear),
                cvv);

        boolean validation = card.validateCard();
        if(validation)
        {
            startProgress();
            new Stripe().createToken(
                    card,
                    sessionManager.getStripeKey(),
//					"",
                    new TokenCallback() {
                        public void onSuccess(Token token)
                        {
                            // getTokenList().addToList(token);
                            access_token=token.getId();
//							Utility.printLog("params to add card key "+sessionManager.getStripeKey());
                            finishProgress();
                            if(access_token!=null)
                            if(Utility.isNetworkAvailable(SelectPaymentTypeActivity.this))
                            {
                                BackGroundAddCard();
                            }
                            else Utility.ShowAlert(getResources().getString(R.string.network_connection_fail), SelectPaymentTypeActivity.this);
                            else Utility.ShowAlert(getResources().getString(R.string.network_connection_fail), SelectPaymentTypeActivity.this);

                        }
                        public void onError(Exception error) {
                            handleError(error.getLocalizedMessage());
                            finishProgress();
                        }
                    });
        }
        else
        {
            Utility.ShowAlert(getResources().getString(R.string.You_did_not_enter_valid_card), SelectPaymentTypeActivity.this);
        }
    }


    /**
     * <h>startProgress</h>
     * 	For showing the progress bar.
     * The value returned is as void.
     * <pre>
     *    Used for showing the progress bar.there by calling the getSupportFragmentManager()
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void startProgress()
    {
        progressFragment.show(getSupportFragmentManager(), "progress");
    }


    /**
     * <h>finishProgress</h>
     * 	For dismissing the progress bar.
     * The value returned is as void.
     * <pre>
     *    Used for dismissing the progress bar.there by calling the progressFragment.dismiss();
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */
    private void finishProgress()
    {
        progressFragment.dismiss();
    }

    private void handleError(String error) {

        Toast.makeText(SelectPaymentTypeActivity.this, error, Toast.LENGTH_SHORT).show();

//		DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.validationErrors, error);
//		fragment.show(getSupportFragmentManager(), "error");
    }


    /**
     * <h>BackGroundAddCard</h>
     * 	For adding the card in the background.
     * 	The value returned is as void.
     * <pre>
     *    Used for adding the card in the background. it called the server api request for an addCard.
     * </pre>
     *
     * @return void
     * @since 1.0
     */
    private void BackGroundAddCard()
    {
        dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SelectPaymentTypeActivity.this,getResources().getString(R.string.please_wait));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {

            SessionManager session=new SessionManager(this);
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();

            jsonObject.put("ent_sess_token",session.getSessionToken() );
            jsonObject.put("ent_dev_id",session.getDeviceId());
            jsonObject.put("ent_token",access_token);
            jsonObject.put("ent_date_time",curenttime);

            Utility.printLog("paras to otp "+jsonObject);
        }
        catch (JSONException e)
        {
            if (dialogL!=null)
            {
                dialogL.dismiss();
            }
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"addCard", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog( "success for updated the addCard   " + result);
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                try
                {
                    if(result!=null)
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        String errFlag=jsonObject.getString("errFlag");
                        String errMsg=jsonObject.getString("errMsg");

                        if(errFlag.equals("0"))
                        {
                            BackgroundGetCards();

//                            Intent intent=new Intent(SelectPaymentTypeActivity.this,MainActivity.class);
//                            startActivity(intent);
//                            finish();
//                            overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);

                            Toast.makeText(SelectPaymentTypeActivity.this, "Card Added Successfully!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Utility.ShowAlert(errMsg,SelectPaymentTypeActivity.this);
                        }
                    }
                    else
                    {
                        Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error)
            {
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private String getLast4Digits(String name) {
        return name.substring(name.length() - 4);
    }

    private void ValidatePromoCode(final String promoCode) {

        dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SelectPaymentTypeActivity.this,getResources().getString(R.string.please_wait));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.show();
        }

        JSONObject jsonObject = new JSONObject();
        try {
            Utility utility = new Utility();
            String curenttime = utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token", sessionManager.getSessionToken());
            jsonObject.put("ent_dev_id", sessionManager.getDeviceId());
            jsonObject.put("ent_coupon", promoCode);
            jsonObject.put("ent_lat", currentLatitude);
            jsonObject.put("ent_long", currentLongitude);
            jsonObject.put("ent_date_time", curenttime);
            Utility.printLog("params to validate promo "+jsonObject);
        } catch (JSONException e) {
            if (dialogL!=null)
            {
                dialogL.dismiss();
            }
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL) + "checkCoupon", jsonObject, new OkHttpRequestObject.JsonRequestCallback() {
            @Override
            public void onSuccess(String response) {
                Utility.printLog("success for updated the language   " + response);
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                if(response!=null)
                {
                    JSONObject jsnResponse;
                    try {
                        jsnResponse = new JSONObject(response);
                        String mErrNum = jsnResponse.getString("errFlag");
                        String errFlag = jsnResponse.getString("errFlag");
                        Utility.printLog("error for the promo " + mErrNum + " flag " + errFlag);
                        if (mErrNum.equals("0")) {
                            sessionManager.setPromocode(promoCode);
                            promo_code_et.setText("Promo Code \""+sessionManager.getPromocode()+"\" is Applied.");
                            promo_code_et.setEnabled(false);
                            clear_promo_code_iv.setVisibility(View.VISIBLE);
                            apply_promo_code_btn.setVisibility(View.GONE);
                            Utility.showToast(SelectPaymentTypeActivity.this,jsnResponse.getString("errMsg"));
                        } else {
                            Utility.showToast(SelectPaymentTypeActivity.this,jsnResponse.getString("errMsg"));
                            sessionManager.setPromocode("");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                Utility.showToast(SelectPaymentTypeActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }


}
