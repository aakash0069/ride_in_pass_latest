package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by rahul on 20/10/16.
 */

public class Cancel_Pojo implements Serializable
{
    /*    "errNum":21,
    "errFlag":"0",
    "errMsg":"Got the details!",
    "data":[]*/
    private String errNum,errFlag,errMsg;
    private ArrayList<String> getdata;

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }


    public String getErrNum() {

        return errNum;
    }

    public ArrayList<String> getGetdata() {
        return getdata;
    }
}
