package com.roadyo.passenger.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.FavAddresRspoPojo;
import com.roadyo.passenger.pojo.FavAddressData;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.AppLocationService;
import com.threembed.utilities.DBLocations;
import com.threembed.utilities.DB_Fav_Locations;
import com.threembed.utilities.DatabaseDropHandler;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.PlaceDetailsJSONParser;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;

public class SearchAddressGooglePlacesActivity extends Activity
{
	private static final int PLACE_AUTOCOMPLETE_REQUEST_WORK = 111;
	private static final int PLACE_AUTOCOMPLETE_REQUEST_HOME = 112;
	AutoCompleteTextView atv_pickup_places;
	AutoCompleteTextView atv_drop_places;
	PlacesTask placesTask;
	ParserTask parserTask;


	//	private ListView search_saved_address_listview;
	double currentLatitude,currentLongitude;
	private String HintText;
	private ArrayList<String> reference_id_list = new ArrayList<>();
	private ArrayList<String> address_list = new ArrayList<>();
	ParserTask placeDetailsParserTask,placesParserTask;
	final int PLACES=0;
	final int PLACES_DETAILS=1;
	DownloadTask placeDetailsDownloadTask;
	AddressAdapterForFav adapterForFav;
	private DatabasePickupHandler dbPickup;
	private DatabaseDropHandler dbDrop;
	private List<DBLocations>  dbLocations;
	private List<DB_Fav_Locations>  dbFavLocations;
	//	TextView title_recent_loc;
	private RelativeLayout saved_addr_layout;
	private List<String> resultDataList = new ArrayList<>();
	private SessionManager sessionManager;
	private LinearLayout add_work_ll;
	private LinearLayout add_home_ll;
	private LinearLayout set_pin_location_ll;

	boolean canUserTypeForSearch;
	private TextView add_home_tv;
	private TextView add_work_tv;
	private ImageView add_work_iv;
	private ImageView add_home_iv;
	private FavAddresRspoPojo favAddresRspoPojo;
	private FavAddressData favAddressData_work,favAddressData_home;

	//	private ListView searchAddressListview;
	private RecyclerView search_address_recycler_view;
	private RecyclerView.Adapter mAdapter;
	private RecyclerView.LayoutManager mLayoutManager;

	private SearchAddressGooglePlacesActivity activity;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main_search);
		activity=this;
		canUserTypeForSearch = false;
		sessionManager=new SessionManager(SearchAddressGooglePlacesActivity.this);

		/**
		 * to set language support
		 */
		Utility.setLanguageSupport(this);
		/**
		 * to set the status bar color
		 */
		Utility.setStatusBarColor(this);

		atv_pickup_places = (AutoCompleteTextView) findViewById(R.id.atv_pickup_places);
		atv_pickup_places.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
		atv_drop_places = (AutoCompleteTextView) findViewById(R.id.atv_drop_places);
		atv_drop_places.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);

		add_work_ll=(LinearLayout) findViewById(R.id.add_work_ll);
		add_home_ll=(LinearLayout) findViewById(R.id.add_home_ll);
		set_pin_location_ll=(LinearLayout) findViewById(R.id.set_pin_location_ll);

		add_work_iv = (ImageView) findViewById(R.id.add_work_iv);
		add_home_iv = (ImageView) findViewById(R.id.add_home_iv);
		add_work_tv = (TextView) findViewById(R.id.add_work_tv);
		add_home_tv = (TextView) findViewById(R.id.add_home_tv);
		search_address_recycler_view = (RecyclerView) findViewById(R.id.search_address_recycler_view);
		mLayoutManager = new LinearLayoutManager(this);
		search_address_recycler_view.setLayoutManager(mLayoutManager);
		RelativeLayout rl_change_card=(RelativeLayout) findViewById(R.id.rl_signin);
		ImageButton back_btn=(ImageButton) findViewById(R.id.back_btn);
		TextView title=(TextView) findViewById(R.id.title);
		final TextView title_saved_loc=(TextView) findViewById(R.id.title_saved_loc);
		saved_addr_layout=(RelativeLayout) findViewById(R.id.saved_addr_layout);
		SessionManager sessionManager=new SessionManager(this);
		/**
		 * to flip the arrow
		 */
		if(sessionManager.getLanguageCode().equals("ar"))
		{
			back_btn.setScaleX(-1);
		}

		if(sessionManager.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,title);
			Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,atv_pickup_places);
			Utility.setTypefaceMuliBold(SearchAddressGooglePlacesActivity.this,title_saved_loc);
		}
		title.setText(getResources().getString(R.string.search));

		rl_change_card.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
			}
		});
		back_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
			}
		});

		atv_pickup_places.setThreshold(0);
		atv_drop_places.setThreshold(0);

		Intent intent = getIntent();
		final Bundle extras = intent.getExtras();

		if(extras!=null)
		{
			HintText = extras.getString("chooser");
		}


		atv_pickup_places.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if(!canUserTypeForSearch){
					if(HintText.equals(getResources().getString(R.string.pickup_location))){

						placesTask = new PlacesTask();
						placesTask.execute(s.toString());
						if(s.toString().length()>0)
						{
							saved_addr_layout.setVisibility(View.GONE);
							title_saved_loc.setText(getResources().getString(R.string.search_result));
						}
						else
						{
							saved_addr_layout.setVisibility(View.VISIBLE);
							title_saved_loc.setText(getResources().getString(R.string.recent_loc));
							dbFavLocations = dbPickup.getFavAddress();
							if(dbFavLocations.size()>0)
							{
								Utility.printLog("ParserTask dbLocations saved size = "+dbFavLocations.size());
								adapterForFav=new AddressAdapterForFav(SearchAddressGooglePlacesActivity.this, dbFavLocations);
							}
						}
					}
				}


			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		atv_drop_places.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{

				if(!canUserTypeForSearch){
					placesTask = new PlacesTask();
					placesTask.execute(s.toString());
					if(s.toString().length()>0)
					{
						saved_addr_layout.setVisibility(View.GONE);
						title_saved_loc.setText(getResources().getString(R.string.search_result));
					}
					else
					{
						saved_addr_layout.setVisibility(View.VISIBLE);
						title_saved_loc.setText(getResources().getString(R.string.recent_loc));
						dbFavLocations = dbPickup.getFavAddress();
						if(dbFavLocations.size()>0)
						{
							Utility.printLog("ParserTask dbLocations saved size = "+dbFavLocations.size());
							adapterForFav=new AddressAdapterForFav(SearchAddressGooglePlacesActivity.this, dbFavLocations);
						}
					}
				}
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});




		if(HintText.equals(getResources().getString(R.string.pickup_location)))
		{
			atv_pickup_places.setEnabled(true);
			atv_drop_places.setEnabled(false);
			atv_pickup_places.setHint(HintText);
			atv_pickup_places.setThreshold(1);

			atv_pickup_places.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					canUserTypeForSearch =false;
					return false;
				}
			});

			dbPickup = new DatabasePickupHandler(this);
			dbLocations = dbPickup.getNonfavAddress();

			dbFavLocations = dbPickup.getFavAddress();

			for(int i=0;i<dbLocations.size() && i<7;i++)
			{
				resultDataList.add(dbLocations.get(i).getAddressName());
			}

			if(dbFavLocations.size()>0)
			{
				Utility.printLog("ParserTask dbLocations saved size = "+dbFavLocations.size());
				adapterForFav=new AddressAdapterForFav(SearchAddressGooglePlacesActivity.this, dbFavLocations);
			}
		}
		else if(HintText.equals(getResources().getString(R.string.drop_location)))
		{

			if(extras!=null)
			{
				String pickup_addr = extras.getString("PICKUP_ADDRESS");
				atv_pickup_places.setText(pickup_addr);
			}

			atv_drop_places.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					canUserTypeForSearch =false;
					return false;
				}
			});

			atv_pickup_places.setEnabled(false);
			atv_drop_places.setEnabled(true);
			atv_drop_places.setHint(HintText);
			atv_drop_places.setThreshold(1);
			dbDrop = new DatabaseDropHandler(this);
			dbPickup = new DatabasePickupHandler(this);
			dbLocations = dbDrop.getNonFavAddressForDrop();

			for(int i=0;i<dbLocations.size();i++)
			{
				Utility.printLog("ParserTask dbLocations size = "+dbLocations.size()+" elements "+dbLocations.get(i).getFormattedAddress());
				resultDataList.add(dbLocations.get(i).getFormattedAddress());
			}

			dbFavLocations = dbPickup.getFavAddress();

			if(dbFavLocations.size()>0)
			{
				Utility.printLog("ParserTask dbLocations saved size = "+dbFavLocations.size());
				adapterForFav = new AddressAdapterForFav(SearchAddressGooglePlacesActivity.this, dbFavLocations);
			}

		}

		if(resultDataList.size()>0)
		{
			Utility.printLog("ParserTask dbLocations size = "+dbLocations.size());
			mAdapter = new AddressAdapter(activity, resultDataList);
			search_address_recycler_view.setAdapter(mAdapter);
		}
		else
		{
			title_saved_loc.setVisibility(View.GONE);
		}
		AppLocationService appLocationService = new AppLocationService(SearchAddressGooglePlacesActivity.this);
		Location gpsLocation = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
		if(gpsLocation != null)
		{
			currentLatitude = gpsLocation.getLatitude();
			currentLongitude = gpsLocation.getLongitude();
		}
		else
		{
			Location nwLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);

			if(nwLocation != null)
			{
				currentLatitude = nwLocation.getLatitude();
				currentLongitude = nwLocation.getLongitude();
			}
		}

		add_work_ll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(favAddresRspoPojo!=null){
					// aakash change
					/*getWindow().setSoftInputMode(
							WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/

					/*InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);*/

					try {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
					} catch (Exception e) {
						e.printStackTrace();
					}


					if(add_work_tv.getText().toString().isEmpty()){
						try {
							Intent intent =
									new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
											.build(SearchAddressGooglePlacesActivity.this);
							startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_WORK);
						} catch (GooglePlayServicesRepairableException e) {
							// TODO: Handle the error.
						} catch (GooglePlayServicesNotAvailableException e) {
							// TODO: Handle the error.
						}
					} else {
						Intent returnIntent = new Intent();
						returnIntent.putExtra("LATITUDE_SEARCH",favAddressData_work.getLat());
						returnIntent.putExtra("LONGITUDE_SEARCH",favAddressData_work.getLng());
						returnIntent.putExtra("SearchAddress",favAddressData_work.getAddress1());
						returnIntent.putExtra("ADDRESS_NAME",favAddressData_work.getLocationType());
						setResult(RESULT_OK,returnIntent);
						finish();
					}

				} else {
					getFavAddress();
				}
			}
		});

		add_home_ll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(favAddresRspoPojo!=null){
					if(add_home_tv.getText().toString().isEmpty()){
						try {
							Intent intent =
									new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
											.build(SearchAddressGooglePlacesActivity.this);
							startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_HOME);
						} catch (GooglePlayServicesRepairableException e) {
							// TODO: Handle the error.
						} catch (GooglePlayServicesNotAvailableException e) {
							// TODO: Handle the error.
						}
					} else {
						Intent returnIntent = new Intent();
						returnIntent.putExtra("LATITUDE_SEARCH",favAddressData_home.getLat());
						returnIntent.putExtra("LONGITUDE_SEARCH",favAddressData_home.getLng());
						returnIntent.putExtra("SearchAddress",favAddressData_home.getAddress1());
						returnIntent.putExtra("ADDRESS_NAME",favAddressData_home.getLocationType());
						setResult(RESULT_OK,returnIntent);
						finish();
					}

				} else {
					getFavAddress();
				}
			}
		});

		set_pin_location_ll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(SearchAddressGooglePlacesActivity.this,ManualDropoffActivity.class);
				intent.putExtra("HintText",HintText);
				startActivityForResult(intent, 7861);
				Log.d("set_pin_location_ll",HintText);
				overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
			}
		});

		add_work_iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent intent =
							new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
									.build(SearchAddressGooglePlacesActivity.this);
					startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_WORK);
				} catch (GooglePlayServicesRepairableException e) {
					// TODO: Handle the error.
				} catch (GooglePlayServicesNotAvailableException e) {
					// TODO: Handle the error.
				}
			}
		});

		add_home_iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent intent =
							new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
									.build(SearchAddressGooglePlacesActivity.this);
					startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_HOME);
				} catch (GooglePlayServicesRepairableException e) {
					// TODO: Handle the error.
				} catch (GooglePlayServicesNotAvailableException e) {
					// TODO: Handle the error.
				}
			}
		});
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String>{

		private int downloadType=0;

		// Constructor
		DownloadTask(int type){
			this.downloadType = type;
		}

		@Override
		protected String doInBackground(String... url) {
			// For storing data from web service
			String data = "";
			try{
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			}catch(Exception e){
				Log.d("Background Task",e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			switch(downloadType){
				case PLACES:
					// Creating ParserTask for parsing Google Places
					placesParserTask = new ParserTask(PLACES);
					// Start parsing google places json data
					// This causes to execute doInBackground() of ParserTask class
					placesParserTask.execute(result);
					break;
				case PLACES_DETAILS :
					// Creating ParserTask for parsing Google Places
					placeDetailsParserTask = new ParserTask(PLACES_DETAILS);
					// Starting Parsing the JSON string
					// This causes to execute doInBackground() of ParserTask class
					placeDetailsParserTask.execute(result);
			}
		}
	}

	private String getPlaceDetailsUrl(String ref){

		// Obtain browser key from https://code.google.com/apis/console
		String key = "key="+sessionManager.getServerKey();
		// reference of place
		String reference = "reference="+ref;
		// Sensor enabled
		String sensor = "sensor=false";
		// Building the parameters to the web service
		String parameters = reference+"&"+sensor+"&"+key;
		// Output format
		String output = "json";
		// Building the url to the web service
		return "https://maps.googleapis.com/maps/api/place/details/"+output+"?"+parameters;
	}

	public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

		List<String> objects;
		Activity activity;

		// Provide a suitable constructor (depends on the kind of dataset)
		public AddressAdapter(Activity activity, List<String> objects) {
			this.objects=objects;
			this.activity=activity;
		}


		// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
		public class ViewHolder extends RecyclerView.ViewHolder {
			// each data item is just a string in this case

			public TextView locationName;
			public TextView addressTextview;
			public LinearLayout layout;

			public ViewHolder(View v) {
				super(v);
				locationName=(TextView) v.findViewById(R.id.location_name);
				addressTextview=(TextView) v.findViewById(R.id.address_textview);
				layout=(LinearLayout) v.findViewById(R.id.layout);

				/**
				 * to set typeface
				 */
				if(sessionManager.getLanguageCode().equals("en"))
				{
					Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,locationName);
					Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,addressTextview);
				}
			}
		}


		// Create new views (invoked by the layout manager)
		@Override
		public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			// create a new view
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_item, parent, false);
			// set the view's size, margins, paddings and layout parameters
			ViewHolder vh = new ViewHolder(v);
			return vh;
		}

		// Replace the contents of a view (invoked by the layout manager)
		@Override
		public void onBindViewHolder(final ViewHolder holder, final int position)
		{
			try {
				String[] total_addressStrings = objects.get(position).split(",");
				if(total_addressStrings.length>0)
				{
					String first_name = total_addressStrings[0];
					String last_name="";
					for(int i=1;i<total_addressStrings.length;i++)
					{
						last_name= last_name+total_addressStrings[i];
					}
					holder.locationName.setText(first_name);
					holder.addressTextview.setText(last_name);
				}

			}catch (Exception e){

			}

			holder.layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// Creating a DownloadTask to download Places details of the selected place
					placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);
					if(reference_id_list.size()>0 && reference_id_list.get(position)!=null)
					{
						// Getting url to the Google Places details api
						String url = getPlaceDetailsUrl(reference_id_list.get(position));
						Utility.printLog("ParserTask url="+url);

						// Start downloading Google Place Details
						// This causes to execute doInBackground() of DownloadTask class
						placeDetailsDownloadTask.execute(url);
						return;
					}
					if(dbLocations.size()>0)
					{
						Utility.printLog("ParserTask inside PLACES_DETAILS dbLocations.size()>0");
						Utility.printLog("inside listResult size < 0");
						Utility.printLog("inside dbLocations PICKUP index="+position +"  "+dbLocations.get(position).getAddressName());
						Intent returnIntent = new Intent();
						returnIntent.putExtra("LATITUDE_SEARCH",dbLocations.get(position).getLatitude());
						returnIntent.putExtra("LONGITUDE_SEARCH",dbLocations.get(position).getLongitude());
						returnIntent.putExtra("SearchAddress",dbLocations.get(position).getFormattedAddress());
						returnIntent.putExtra("ADDRESS_NAME",dbLocations.get(position).getAddressName());
						setResult(RESULT_OK,returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
				}
			});
		}

		@Override
		public int getItemCount() {
			Log.d("FoursquareApi_pa"," yes "+objects.size());
			return objects.size();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==786) {
			if (resultCode == Activity.RESULT_OK) {
				String type=data.getStringExtra("TYPE");
				Utility.printLog("on activity result fav type "+type);
				if(type.equals("1"))
				{
					dbFavLocations = dbPickup.getFavAddress();

					if(dbFavLocations.size()>0)
					{
						Utility.printLog("ParserTask dbLocations saved size = "+dbFavLocations.size());
						adapterForFav=new AddressAdapterForFav(SearchAddressGooglePlacesActivity.this, dbFavLocations);
					}
				}
			}
		}

		if(requestCode==7861) {
			if (resultCode == Activity.RESULT_OK) {
				Utility.printLog("lat from manual drop "+data.getStringExtra("LATITUDE_SEARCH"));
				Utility.printLog("long from manual drop "+data.getStringExtra("LONGITUDE_SEARCH"));
				Utility.printLog("addrress from manual drop "+data.getStringExtra("SearchAddress"));

				Intent returnIntent = new Intent();
				returnIntent.putExtra("LATITUDE_SEARCH",data.getStringExtra("LATITUDE_SEARCH"));
				returnIntent.putExtra("LONGITUDE_SEARCH",data.getStringExtra("LONGITUDE_SEARCH"));
				returnIntent.putExtra("SearchAddress",data.getStringExtra("SearchAddress"));
				setResult(RESULT_OK,returnIntent);
				finish();
				overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
			}
		}

		if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_WORK)
		{
			if(resultCode == RESULT_OK)
			{

				Place place = PlaceAutocomplete.getPlace(this, data);
				Log.i("TAG123", "Place: " + place.getName()
						+"\n"+place.getAddress()
				);

				addFavAddress(place.getAddress().toString(),place.getLatLng().latitude,place.getLatLng().longitude,getString(R.string.work),"1");


			} else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
				Status status = PlaceAutocomplete.getStatus(this, data);
				// TODO: Handle the error.
				Log.i("TAG123", status.getStatusMessage());

			} else if (resultCode == RESULT_CANCELED) {
				// The user canceled the operation.
			}
		}

		if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_HOME)
		{
			if(resultCode == RESULT_OK)
			{

				Place place = PlaceAutocomplete.getPlace(this, data);
				Log.i("TAG123", "Place: " + place.getName()
						+"\n"+place.getAddress()
				);

				addFavAddress(place.getAddress().toString(),place.getLatLng().latitude,place.getLatLng().longitude,getString(R.string.home),"2");

			} else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
				Status status = PlaceAutocomplete.getStatus(this, data);
				// TODO: Handle the error.
				Log.i("TAG123", status.getStatusMessage());

			} else if (resultCode == RESULT_CANCELED) {
				// The user canceled the operation.
			}
		}
	}

	private class AddressAdapterForFav extends ArrayAdapter<DB_Fav_Locations>
	{
		List<DB_Fav_Locations> objects;
		Activity activity;
		AddressAdapterForFav(Activity activity, List<DB_Fav_Locations> objects) {
			super(activity, R.layout.activity_main_search, objects);
			this.objects=objects;
			this.activity=activity;
		}
		@NonNull
		@Override
		public View getView(final int position, View convertView, @NonNull ViewGroup parent)
		{
			if(convertView==null)
			{
				convertView=SearchAddressGooglePlacesActivity.this.getLayoutInflater().inflate(R.layout.address_item,null);
			}
			TextView locationName=(TextView) convertView.findViewById(R.id.location_name);
			TextView addressTextview=(TextView) convertView.findViewById(R.id.address_textview);
			LinearLayout layout=(LinearLayout) convertView.findViewById(R.id.layout);
			locationName.setText(objects.get(position).get_formattedAddress());
			addressTextview.setText("  "+objects.get(position).get_description());

			if(sessionManager.getLanguageCode().equals("en"))
			{
				Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,locationName);
				Utility.setTypefaceMuliRegular(SearchAddressGooglePlacesActivity.this,addressTextview);
			}

			layout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(dbFavLocations.size()>0)
					{
						Utility.printLog("ParserTask inside PLACES_DETAILS dbLocations.size()>0");
						Utility.printLog("inside listResult size < 0");
						Intent returnIntent = new Intent();
						returnIntent.putExtra("LATITUDE_SEARCH",dbFavLocations.get(position).get_latitude());
						returnIntent.putExtra("LONGITUDE_SEARCH",dbFavLocations.get(position).getLongitude());
						returnIntent.putExtra("SearchAddress",dbFavLocations.get(position).getFormattedAddress());
						returnIntent.putExtra("ADDRESS_NAME",dbFavLocations.get(position).getAddressName());
						returnIntent.putExtra("DESCRIPTION",dbFavLocations.get(position).get_description());
						returnIntent.putExtra("ID",dbFavLocations.get(position).getID());
						setResult(RESULT_OK,returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
				}
			});
			return convertView;
		}
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException{
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try{
			URL url = new URL(strUrl);
			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();
			// Connecting to url
			urlConnection.connect();
			// Reading data from url
			iStream = urlConnection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
			StringBuffer sb  = new StringBuffer();
			String line = "";
			while( ( line = br.readLine())  != null){
				sb.append(line);
			}
			data = sb.toString();
			br.close();
		}catch(Exception ignored){
		}finally{
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}
	// Fetches all places from GooglePlaces AutoComplete Web Service
	private class PlacesTask extends AsyncTask<String, Void, String>
	{
		@Override
		protected String doInBackground(String... place) {
			// For storing data from web service
			String data = "";
			// Obtain browser key from https://code.google.com/apis/console
			String key = "key="+sessionManager.getServerKey();
			String input="";
			try {
				input = "input=" + URLEncoder.encode(place[0], "utf-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			String types = "establishment|geocode&location="+currentLatitude+","+currentLongitude+"&radius=500&amplanguage=en";
			// Sensor enabled
			//String sensor = "sensor=false";
			// Building the parameters to the web service
			String parameters = input+"&"+types+"&"+key;
			// Output format
			String output = "json";
			// Building the url to the web service
			String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
			Utility.printLog("SearchAddressGooglePlacesActivity url = "+url);
			try{
				// Fetching the data from web service in background
				data = downloadUrl(url);
			}catch(Exception e){
				Log.d("Background Task",e.toString());
			}
			return data;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// Creating ParserTask
			parserTask = new ParserTask(PLACES);
			// Starting Parsing the JSON string returned by Web Service
			parserTask.execute(result);
		}
	}
	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>
	{
		JSONObject jObject;
		int parserType = 0;
		ParserTask(int type){
			this.parserType = type;
		}
		@Override
		protected List<HashMap<String, String>> doInBackground(String... jsonData)
		{
			List<HashMap<String, String>> places = null;
			try {
				if(jsonData[0]!=null)
				{
					jObject = new JSONObject(jsonData[0]);
					switch(parserType){
						case PLACES :
							PlaceJSONParser placeJsonParser = new PlaceJSONParser();
							// Getting the parsed data as a List construct
							places = placeJsonParser.parse(jObject);
							break;
						case PLACES_DETAILS :
							PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
							// Getting the parsed data as a List construct
							places = placeDetailsJsonParser.parse(jObject);
					}
				}
			} catch (JSONException e) {
				Utility.printLog("ParserTask Exception: "+e.toString());
			}
			return places;
		}

		@Override
		protected void onPostExecute(List<HashMap<String, String>> result)
		{
			switch(parserType)
			{
				case PLACES :
					if(result!=null)
					{
						reference_id_list.clear();
						address_list.clear();
						for(int i=0;i<result.size();i++)
						{
							address_list.add(result.get(i).get("description"));
							reference_id_list.add(result.get(i).get("reference"));
						}
						mAdapter = new AddressAdapter(activity, address_list);
						search_address_recycler_view.setAdapter(mAdapter);
					}

					break;
				case PLACES_DETAILS :
					Utility.printLog("ParserTask inside PLACES_DETAILS");
					int clicked_index = 0;
					if(result!=null && result.size()>0)
					{
						Utility.printLog("ParserTask inside PLACES_DETAILS result.size()>0");
						double latitude = Double.parseDouble(result.get(0).get("lat"));
						double longitude = Double.parseDouble(result.get(0).get("lng"));
						if(HintText.equals(getResources().getString(R.string.pickup_location)))
						{
							dbLocations = dbPickup.getNonfavAddress();
							for(int i=0;i<dbLocations.size();i++)
							{
								if(address_list.get(clicked_index).equals(dbLocations.get(i).getFormattedAddress()))
								{
									resultDataList.add(dbLocations.get(i).getFormattedAddress());
								}
							}
							if(!resultDataList.contains(address_list.get(clicked_index)))
							{
								Utility.printLog("inside not contains ");
								dbPickup.addPickupLocation("","",
										address_list.get(clicked_index),
										""+latitude
										,""+longitude,"","1");
							}
						}
						else if(HintText.equals(getResources().getString(R.string.drop_location)))
						{
							dbLocations = dbDrop.getNonFavAddressForDrop();
							for(int i=0;i<dbLocations.size();i++)
							{
								if(address_list.get(clicked_index).equals(dbLocations.get(i).getFormattedAddress()))
								{
									resultDataList.add(dbLocations.get(i).getFormattedAddress());
								}
							}
							Utility.printLog("inside onItemClick DROP");
							if(!resultDataList.contains(address_list.get(clicked_index)))
							{
								dbDrop.addDropLocation("",
										address_list.get(clicked_index),
										""+latitude
										,""+longitude,"","1");
							}
						}
						Intent returnIntent = new Intent();
						returnIntent.putExtra("SearchAddress",address_list.get(clicked_index));
						returnIntent.putExtra("ADDRESS_NAME","");
						returnIntent.putExtra("LATITUDE_SEARCH",""+latitude);
						returnIntent.putExtra("LONGITUDE_SEARCH",""+longitude);
						setResult(RESULT_OK,returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
					else if(dbLocations.size()>0)
					{
						Utility.printLog("ParserTask inside PLACES_DETAILS dbLocations.size()>0");
						Utility.printLog("inside listResult size < 0");
						Utility.printLog("inside dbLocations PICKUP index="+ clicked_index +"  "+dbLocations.get(clicked_index).getAddressName());
						Intent returnIntent = new Intent();
						returnIntent.putExtra("LATITUDE_SEARCH",dbLocations.get(clicked_index).getLatitude());
						returnIntent.putExtra("LONGITUDE_SEARCH",dbLocations.get(clicked_index).getLongitude());
						returnIntent.putExtra("SearchAddress",dbLocations.get(clicked_index).getFormattedAddress());
						returnIntent.putExtra("ADDRESS_NAME",dbLocations.get(clicked_index).getAddressName());
						setResult(RESULT_OK,returnIntent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
					}
					break;
			}
		}
	}


	private void addFavAddress(final String address_selected,final double latitude,final double longitude,final String type,final String type_code)
	{
		SessionManager sessionManager=new SessionManager(this);
		final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialog(this);
		dialogL.setCancelable(false);
		dialogL.show();
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("ent_sess_token", sessionManager.getSessionToken());
			jsonObject.put("ent_dev_id", sessionManager.getDeviceId());
			jsonObject.put("ent_action", "1");
			jsonObject.put("ent_address1", address_selected);
			if(type.equals(getString(R.string.work))) {
				jsonObject.put("ent_addr_type", "1");
			} else {
				jsonObject.put("ent_addr_type", "2");
			}
			jsonObject.put("ent_address2", "");
			jsonObject.put("ent_lat", latitude);
			jsonObject.put("ent_long", longitude);
			jsonObject.put("ent_title", type);
			jsonObject.put("ent_sid", sessionManager.getSid());
			Utility.printLog("params to add fav address  "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"favourateAdresss", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				dialogL.cancel();
				if(result!=null)
				{
					Utility.printLog("resposne from fav address "+result);
					try {
						JSONObject jsonObject =new JSONObject(result);
						String errFlag=jsonObject.getString("errFlag");
						String errMsg=jsonObject.getString("errMsg");
						if(errFlag.equals("0"))
						{

							currentLatitude = latitude;
							currentLongitude = longitude;

							String data=jsonObject.getString("data");
							dbPickup.addPickupLocation("",data,
									address_selected,
									""+currentLatitude
									,""+currentLongitude,type,"0");

							canUserTypeForSearch = true;
							if(type_code.equals("2")){
								add_home_tv.setText(address_selected);
							} else {
								add_work_tv.setText(address_selected);
							}


							if(HintText.equals(getString(R.string.pickup_location))){
//								atv_pickup_places.setText("");
								atv_pickup_places.setText(address_selected);
							} else {
								atv_drop_places.setText("");
								atv_drop_places.setText(address_selected);
							}
							getFavAddress();

						}
						else
						{
							Dialog dialog=Utility.showPopupWithOneButton(SearchAddressGooglePlacesActivity.this) ;
							TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
							TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
							title_popup.setVisibility(View.GONE);
							text_for_popup.setText(errMsg);
							dialog.show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else
				{
					Utility.showToast(SearchAddressGooglePlacesActivity.this, getResources().getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				dialogL.cancel();
				Utility.printLog("on error for the login "+error);
				Utility.showToast(SearchAddressGooglePlacesActivity.this, getResources().getString(R.string.network_connection_fail));
			}
		});
	}


	private void getFavAddress()
	{
		SessionManager sessionManager=new SessionManager(this);
		final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialog(this);
		dialogL.setCancelable(false);
		dialogL.show();
		JSONObject jsonObject=new JSONObject();
		try {
			jsonObject.put("ent_sess_token", sessionManager.getSessionToken());
			jsonObject.put("ent_dev_id", sessionManager.getDeviceId());
			jsonObject.put("ent_sid", sessionManager.getSid());
			Utility.printLog("params to add fav address  "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getfavourateAdresss", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				dialogL.cancel();
				if(result!=null)
				{
					Utility.printLog("resposne from fav address "+result);
					try {
						JSONObject jsonObject =new JSONObject(result);
						String errFlag=jsonObject.getString("errFlag");
						String errMsg=jsonObject.getString("errMsg");
						if(errFlag.equals("0"))
						{
							Gson gson = new Gson();
							favAddresRspoPojo = gson.fromJson(result,FavAddresRspoPojo.class);
							if(favAddresRspoPojo.getData().size()>0){
								for(int i = 0; i<favAddresRspoPojo.getData().size(); i++) {
									if(favAddresRspoPojo.getData().get(i).getAddressType_number().equals("1")) {
										favAddressData_work = favAddresRspoPojo.getData().get(i);
										add_work_tv.setText(favAddresRspoPojo.getData().get(i).getAddress1().trim());
									} else if(favAddresRspoPojo.getData().get(i).getAddressType_number().equals("2")){
										favAddressData_home = favAddresRspoPojo.getData().get(i);
										add_home_tv.setText(favAddresRspoPojo.getData().get(i).getAddress1().trim());
									}
								}
							}
						}
						else
						{
							Dialog dialog=Utility.showPopupWithOneButton(SearchAddressGooglePlacesActivity.this) ;
							TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
							TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
							title_popup.setVisibility(View.GONE);
							text_for_popup.setText(errMsg);
							dialog.show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else
				{
					Utility.showToast(SearchAddressGooglePlacesActivity.this, getResources().getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				dialogL.cancel();
				Utility.printLog("on error for the login "+error);
				Utility.showToast(SearchAddressGooglePlacesActivity.this, getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
	}


	@Override
	protected void onStart() {
		super.onStart();
		getFavAddress();
	}
}