package com.roadyo.passenger.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.bloomingdalelimousine.ridein.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.threembed.utilities.InternalStorageContentProvider;
import com.threembed.utilities.LocationUtil;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import eu.janmuller.android.simplecropimage.CropImage;
import static com.threembed.utilities.Utility.getResId;

@SuppressWarnings("ALL")
public class SignupActivity extends AppCompatActivity implements OnClickListener
        ,LocationUtil.GetLocationListener
{
    private static final String TAG = "SignupActivity";
    private EditText firstName,lastName,email,mobileNo,password,referral_code,confirm_signup_password;
    private TextView countryCode;
    private CheckBox chkBox;
    private ImageView profile_pic;
    private final int REQUEST_CODE_GALLERY      = 0x1;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    private final int REQUEST_CODE_CROP_IMAGE   = 0x3;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static File mFileTemp;
    private Context context;
    private SessionManager session;
    LinearLayout countryPicker;
    private boolean isEmailVaild = false;
    String fbPic="";
    private boolean picSelected=false;
    JSONObject jsonObject;
    ImageView flag;
    ProgressBar progress_bar;
    private String type="1";
    ProgressDialog dialogL;
    String errFlag,errMsg;
    private TextInputLayout first_name_layout;
    private TextView title_popup,text_for_popup,yes_button;
    private RadioButton radioSexButton;
    private Dialog popupWithOneButton;
    private String dob="",gender;
    private LocationUtil networkUtil;
    private LinearLayout profile_first_last_name_ll;
    private TextView title_tv;
    private TextInputLayout last_name_layout;
    private Button next;
    TextInputLayout til_confirm_signup_password,til_signup_password;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        session = new SessionManager(SignupActivity.this);
        initialize();

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),VariableConstants.TEMP_PHOTO_FILE_NAME);
        }
    }

    /**
     * For initialising the all view componets.
     * The value returned is as void.
     * <pre>
     *    Used for initialising the all view componets
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */
    private void initialize()
    {
        /**
         * to set language support
         */
        Utility.setLanguageSupport(this);
        /**
         * to set the status bar color
         */
        Utility.setStatusBarColor(this);

        /**
         * to get the current location
         */
        getCurrentLocation();

        firstName=(EditText)findViewById(R.id.first_name);
        lastName=(EditText)findViewById(R.id.last_name);
        email=(EditText)findViewById(R.id.signup_email);
        mobileNo=(EditText)findViewById(R.id.signup_phone);
        referral_code=(EditText)findViewById(R.id.signup_referal_code);
        password=(EditText)findViewById(R.id.signup_password);
        confirm_signup_password=(EditText)findViewById(R.id.confirm_signup_password);
        chkBox=(CheckBox)findViewById(R.id.chkbox_TandC);

        til_confirm_signup_password=(TextInputLayout)findViewById(R.id.til_confirm_signup_password);
        til_signup_password=(TextInputLayout)findViewById(R.id.til_signup_password);




        profile_pic=(ImageView)findViewById(R.id.profile_pic);
        next = (Button) findViewById(R.id.signup_next);
        TextView terms_Cond = (TextView) findViewById(R.id.txt_TandC);
        title_tv = (TextView) findViewById(R.id.title_tv);
        TextView terms_plain_text = (TextView) findViewById(R.id.terms_plain_text);
        TextView txt_TandC = (TextView) findViewById(R.id.txt_TandC);
        profile_first_last_name_ll= (LinearLayout) findViewById(R.id.profile_first_last_name_ll);
        countryPicker= (LinearLayout) findViewById(R.id.countryPicker);
        RelativeLayout rl_signin= (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn= (ImageButton) findViewById(R.id.back_btn);
        countryCode= (TextView) findViewById(R.id.countryCode);
        TextView title= (TextView) findViewById(R.id.title);
        flag= (ImageView) findViewById(R.id.flag);
        progress_bar= (ProgressBar) findViewById(R.id.progress_bar);
        first_name_layout= (TextInputLayout) findViewById(R.id.first_name_layout);
        last_name_layout= (TextInputLayout) findViewById(R.id.last_name_layout);
        popupWithOneButton= Utility.showPopupWithOneButton(SignupActivity.this);
        title_popup= (TextView) popupWithOneButton.findViewById(R.id.title_popup);
        text_for_popup= (TextView) popupWithOneButton.findViewById(R.id.text_for_popup);
        yes_button= (TextView) popupWithOneButton.findViewById(R.id.yes_button);
        title_popup.setText(getResources().getString(R.string.alert));
        yes_button.setText(getResources().getString(R.string.ok));

        /**
         * to set the typeface
         */
        if(session.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(SignupActivity.this,firstName);
            Utility.setTypefaceMuliRegular(SignupActivity.this,lastName);
            Utility.setTypefaceMuliRegular(SignupActivity.this,email);
            Utility.setTypefaceMuliRegular(SignupActivity.this,title);
            Utility.setTypefaceMuliRegular(SignupActivity.this,mobileNo);
            Utility.setTypefaceMuliRegular(SignupActivity.this,referral_code);
            Utility.setTypefaceMuliRegular(SignupActivity.this,password);
            Utility.setTypefaceMuliRegular(SignupActivity.this,countryCode);
            Utility.setTypefaceMuliRegular(SignupActivity.this,txt_TandC);
            Utility.setTypefaceMuliRegular(SignupActivity.this,terms_plain_text);
            Utility.setTypefaceMuliRegular(SignupActivity.this,next);
            Utility.setTypefaceMuliRegular(SignupActivity.this,confirm_signup_password);
        }


        next.setOnClickListener(this);
        profile_pic.setOnClickListener(this);
        terms_Cond.setOnClickListener(this);
        countryPicker.setOnClickListener(this);
        rl_signin.setOnClickListener(this);
        //.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        /**
         * to get the default country code
         */
        getCountryDialCode();

        firstName.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            public void onFocusChange(View v, boolean hasFocus)
            {
                Utility.printLog("setOnFocusChangeListener hasFocus= "+hasFocus);
                if(!hasFocus)
                {
                    if(firstName.getText().toString().trim().isEmpty())
                    {
                        firstName.setError(getResources().getString(R.string.first_name_empty));
                    }
                    else  if(!firstName.getText().toString().matches("[a-zA-Z\u0600-\u06FF ]+"))
                    {
                        firstName.setError(getResources().getString(R.string.full_name_validation));
                    }
                }
            }
        });
        firstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                first_name_layout.setError(null);
            }
        });

        lastName.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            public void onFocusChange(View v, boolean hasFocus)
            {
                Utility.printLog("setOnFocusChangeListener hasFocus= "+hasFocus);
                if(!hasFocus)
                {
                    if(lastName.getText().toString().trim().isEmpty())
                    {
                        lastName.setError(getResources().getString(R.string.last_name_empty));
                    }
                    else  if(!lastName.getText().toString().matches("[a-zA-Z\u0600-\u06FF ]+"))
                    {
                        lastName.setError(getResources().getString(R.string.full_name_validation));
                    }
                }
            }
        });
        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                last_name_layout.setError(null);
            }
        });
        password.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                Utility.printLog("setOnFocusChangeListener hasFocus= " + hasFocus);
                if (!hasFocus) {
                    if (password.getText().toString().trim().isEmpty()) {
                        password.setError(getResources().getString(R.string.password_empty));
                    }
                }
            }
        });
        mobileNo.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            public void onFocusChange(View v, boolean hasFocus)
            {
                Utility.printLog("setOnFocusChangeListener hasFocus= "+hasFocus);
                if(!hasFocus)
                {
                    if(mobileNo.getText().toString().trim().isEmpty())
                    {
                        mobileNo.setError(getResources().getString(R.string.mobile_empty));
                    }
                    else
                    {
                        BackgroundValidateMobileNo();
                    }
                }
            }
        });
        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                mobileNo.setError(null);
            }
        });
        email.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            public void onFocusChange(View v, boolean hasFocus)
            {
                Utility.printLog("setOnFocusChangeListener hasFocus= " + hasFocus);
                if (!hasFocus) {
                    if (email.getText().toString().trim().isEmpty())
                    {
                        email.setError(getResources().getString(R.string.email_empty));
                    } else
                    {
                        if (!validateEmail(email.getText().toString().trim()))
                        {
                            email.setError(getResources().getString(R.string.enter_valid_email));
                        }
                    }
                }
            }
        });
        email.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable)
            {
                email.setError(null);
            }
        });

        /**
         * to flip the arrow
         */
        if(session.getLanguageCode().equals("ar"))
        {
            back_btn.setScaleX(-1);
        }

        title_tv.setVisibility(View.VISIBLE);
        Bundle bundle=getIntent().getExtras();

        if(bundle!=null && bundle.containsKey("FROM_ACTIVITY") && (bundle.getString("FROM_ACTIVITY").equals("1"))){
            title_tv.setText(getString(R.string.login_to_your_acc));
        } else {
            title_tv.setText(getString(R.string.create_account));
        }

        setUpBundleData(bundle);

    }

    private void setUpBundleData(Bundle bundle) {
        if(bundle!=null)
        {
            email.setText(bundle.getString("EMAIL"));
            email.setError(null);
            password.setText(bundle.getString("PASSWORD"));
            gender=bundle.getString("GENDER");
            confirm_signup_password.setText(bundle.getString("PASSWORD"));
            firstName.setText(bundle.getString("FIRST_NAME"));
            lastName.setText(""+bundle.getString("LAST_NAME"));
            next.setText(getString(R.string.proceed));
            firstName.setError(null);
            Utility.printLog("user pic "+bundle.getString("USER_PIC"));
            if(!bundle.getString("USER_PIC").equals(null))
            {
                fbPic=bundle.getString("USER_PIC");
            }
            type=bundle.getString("TYPE");
            profile_first_last_name_ll.setVisibility(View.GONE);

            email.setVisibility(View.GONE);
            password.setVisibility(View.GONE);
            Utility.printLog("fb pic "+gender);
            confirm_signup_password.setVisibility(View.GONE);

            til_confirm_signup_password.setVisibility(View.GONE);
            til_signup_password.setVisibility(View.GONE);



            if(!fbPic.equals(""))
            {
                /**
                 * to set the image into image view and
                 * add the write the image in the file
                 */
                profile_pic.setTag(setTarget(progress_bar));
                Picasso.with(SignupActivity.this).load(fbPic).into((Target) profile_pic.getTag()) ;

                Picasso.with(SignupActivity.this)
                        .load(fbPic)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            picSelected=true;
                                            Log.i("TAG", "i am inside try for file conversion" + mFileTemp);
                                            FileOutputStream ostream = new FileOutputStream(mFileTemp);
                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                                            ostream.close();
                                        } catch (Exception e) {
                                            Log.i("", "exception in bitmap" + e);
                                        }
                                    }
                                }).start();
                            }
                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }
                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                Log.i("", "bitmap failed while prepareload" + placeHolderDrawable);
                            }
                        });
            }
            else
            {
                picSelected=false;
            }
        }
    }

    /**
     * <h>getCurrentLocation</h>
     * For fetching the CurrentLocation.
     * The value returned is as void.
     * <pre>
     *    Used for fetching the CurrentLocation by creating the LocationUtil instance hich ihterns require the current activity and implimented class.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */
    private void getCurrentLocation()
    {
        if (networkUtil == null)
        {
            networkUtil = new LocationUtil(SignupActivity.this, this);
        }
        else
        {
            networkUtil.checkLocationSettings();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
    }

    /**
     * <h>getCountryDialCode</h>
     * Used to get default countrycode.
     * The value returned is as void.
     * <pre>
     *    Used for get default countrycode.
     * </pre>
     *
     * @param
     * @return contryDialCode as String.
     * @since 1.0
     */
    public String getCountryDialCode(){
        String contryId;
        String contryDialCode = null;
        TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        contryId = telephonyMngr.getNetworkCountryIso().toUpperCase();
        String[] arrContryCode=SignupActivity.this.getResources().getStringArray(R.array.DialingCountryCode);
        for (String anArrContryCode : arrContryCode) {
            String[] arrDial = anArrContryCode.split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                countryCode.setText(contryDialCode);
                String drawableName = "flag_"
                        + contryId.toLowerCase(Locale.ENGLISH);
                flag.setBackgroundResource(getResId(drawableName));
                Utility.printLog("country code " + contryDialCode);
                break;
            }
        }
        return contryDialCode;
    }


    /**
     * <h>checkPlayServices</h>
     * Used for checking the playServices Availablity.
     * The value returned is as boolean.
     * <pre>
     *    Used for checking the playServices Availablity. If the resultCode = ConnectionResult.SUCCESS then return the boolean as true else returns boolean as false.
     * </pre>
     *
     * @param
     * @return boolean.
     * @since 1.0
     */
    private boolean checkPlayServices()
    {
        Log.d(TAG, "onCreate checkPlayServices ");
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
                Utility.printLog( "This device is supported.");
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else
            {
                Utility.printLog( "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * <h>getDeviceId</h>
     * For Fetching the Device ID from the mobile.
     * The value returned is as a String.
     * <pre>
     *    Used for fetching device Id
     * </pre>
     *
     * @param
     * @return String as a device Id
     * @since 1.0
     */
    public  String getDeviceId(Context context)
    {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        Utility.printLog("country in signup "+telephonyManager.getNetworkCountryIso());
        return telephonyManager.getDeviceId();
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.rl_signin:
            {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
            case R.id.back_btn:
            {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
            case R.id.signup_next:
            {
                if(!email.getText().toString().isEmpty())
                {
                    if(validateEmail(email.getText().toString()))
                    {
                        BackgroundValidateEmail();
                    }
                    else
                    {
                        email.setError(getResources().getString(R.string.enter_valid_email));
                    }
                }
                else
                {
                    boolean isValid=validateFields();
                    if(isValid)
                    {
                        if(validateEmail(email.getText().toString()))
                        {
                            if(!mobileNo.getText().toString().isEmpty())
                            {
                                if(referral_code.getText().toString().trim().equals("")) {
                                    if (Utility.isNetworkAvailable(SignupActivity.this))
                                        BackgroundForGettingVerificationCode();
                                    else
                                        Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                }
                                else
                                {
                                    validatePromoCode(referral_code.getText().toString());
                                }
                            }
                            else
                            {
                                mobileNo.setError(getResources().getString(R.string.mobile_empty));
                            }
                        }
                        else
                        {
                            email.setError(getResources().getString(R.string.enter_valid_email));
                        }
                    }
                }
                break;
            }
            case R.id.profile_pic:
            {
                final Dialog dialog=Utility.showPopupWithTwoButtons(SignupActivity.this);
                TextView titleForpopup= (TextView) dialog.findViewById(R.id.title_popup);
                TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
                TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
                TextView no_button= (TextView) dialog.findViewById(R.id.no_button);
                titleForpopup.setVisibility(View.GONE);
                text_for_popup.setText(getResources().getString(R.string.selecto_photo));
                yes_button.setText(getResources().getString(R.string.gallery));
                no_button.setText(getResources().getString(R.string.camera));
                yes_button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        openGallery();
                    }
                });
                no_button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        takePicture();
                    }
                });
                dialog.show();
                break;
            }
            case R.id.txt_TandC:
            {
                Intent intent = new Intent(SignupActivity.this,TermsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                break;
            }
            case R.id.countryPicker:
            {
                showDialoagforcountrypicker();
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkUtil.stop_Location_Update();
    }

    /**
     * <h>BackgroundForGettingVerificationCode</h>
     * For sending the varification code.
     * The value returned is as void.
     * <pre>
     *    Used for sending the varification code to mobile number entered by doing the Okhttp call for an api called getVerificationCode.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void BackgroundForGettingVerificationCode()
    {
        dialogL=Utility.GetProcessDialogNew(SignupActivity.this,getResources().getString(R.string.waitForVerification));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.setCancelable(false);
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_mobile", countryCode.getText().toString()+mobileNo.getText().toString());
            jsonObject.put("ent_language", session.getLanguage());
            jsonObject.put("ent_resend", "0");
            Utility.printLog("params to getverification code "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getVerificationCode", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog("result for verification code "+result);
                try
                {
                    if(result!=null)
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        errFlag=jsonObject.getString("errFlag");
                        errMsg=jsonObject.getString("errMsg");

                        Utility.printLog("result for verification code "
                                + "errFlag "+errFlag
                                + "errMsg "+errMsg
                        );

                        if(dialogL!=null)
                        {
                            dialogL.dismiss();
                        }
                        if(errFlag!=null)
                        {
                            if(errFlag.equals("0"))
                            {
                                VariableConstants.COUNTER=0;
                                Intent intent=new Intent(SignupActivity.this,SignupVerificationActivity.class);
                                intent.putExtra("FIRSTNAME",firstName.getText().toString());
                                intent.putExtra("LASTNAME",lastName.getText().toString());
                                if(referral_code.getText().toString()!=null)
                                    intent.putExtra("REFERAL",referral_code.getText().toString());
                                intent.putExtra("EMAIL",email.getText().toString());
                                intent.putExtra("PASSWORD",password.getText().toString());
                                intent.putExtra("MOBILE",countryCode.getText().toString()+mobileNo.getText().toString());
                                intent.putExtra("BOOLEAN",picSelected);
                                intent.putExtra("TYPE",type);
                                Utility.printLog("pic selected falg in suignup"+type);
                                intent.putExtra("LATITUDE",String.valueOf(session.getCurrentLat())) ;
                                intent.putExtra("LONGITUDE",String.valueOf(session.getCurrentLong()));
                                startActivity(intent);
                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            }
                            else
                            {
                                mobileNo.setError(errMsg);
                                text_for_popup.setText(errMsg);
                                popupWithOneButton.show();
                            }
                        }
                        else
                        {
                            Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                        }
                    }
                    else
                    {
                        runOnUiThread(new Runnable()
                        {
                            public void run()
                            {
                                Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
                            }
                        });
                    }
                }
                catch(Exception e)
                {
                    Utility.printLog("catch reason "+e);
                    Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
                }
            }

            @Override
            public void onError(String error)
            {
                dialogL.cancel();
                Utility.printLog("on error for the login "+error);
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });
    }

    /**
     * <h>showDialoagforcountrypicker</h>
     * For displaying the country code of all the country.
     * The value returned is as void.
     * <pre>
     *    Used for displaying the country code of all the country.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */
    void showDialoagforcountrypicker(){
        final CountryPicker picker = CountryPicker.newInstance(getResources().getString(R.string.selectCountry));
        picker.show(getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
        picker.setListener(new CountryPickerListener()
        {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                countryCode.setText(dialCode);
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                flag.setBackgroundResource(getResId(drawableName));
                picker.dismiss();
            }
        });
    }


    /**
     * <h>openGallery</h>
     * For firing the ACTION_PICK Intent.
     * The value returned is as void.
     * <pre>
     *    Used for firing the ACTION_PICK of Intent, for getting the Images from the gallery. Instance must also contain the Type.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    /**
     * <h>takePicture</h>
     * For firing the ACTION_IMAGE_CAPTURE of MediaStore Intent.
     * The value returned is as void.
     * <pre>
     *    Used for firing the ACTION_IMAGE_CAPTURE of MediaStore Intent, for getting the Images from the Camera.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void takePicture()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            }
            else
            {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode != RESULT_OK)
        {
            return;
        }
        Bitmap bitmap;
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    startCropImage();
                } catch (Exception e)
                {
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if(path == null)
                {
                    return;
                }
                /**
                 * cropped image and show it in circular format
                 */
                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumWidth(),
                        getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumHeight(), true);
                Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                BitmapShader shader = new BitmapShader (bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);
                paint.setAntiAlias(true);
                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
                profile_pic.setImageBitmap(circleBitmap);
                picSelected=true;
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * <h>startCropImage</h>
     * For firing the CropImage Intent.
     * The value returned is as void.
     * <pre>
     *    Used for firing the CropImage, for croping the Images obtained from the gallery or Camera.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void startCropImage()
    {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    /**
     * <h>copyStream</h>
     * For coping the image file contents bytewise.
     * The value returned is as void.
     * <pre>
     *    Used for coping the image file contents bytewise.using the input and output Streams
     * </pre>
     *
     * @param input as a InputStream.
     * @param output as a OutputStream.
     * @return void
     * @since 1.0
     */

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * <h>validateFields</h>
     * For Validating the all primary input enterd by the user.
     * The value returned is as boolean.
     * <pre>
     *    Used for Validating the all primary input enterd by the user.
     *    Fields which are validated are..
     *    > firstName
     *    > mobileNo
     *    > email
     *    > password
     *    > confirm_signup_password
     * </pre>
     *
     * @param
     * @return boolean
     * @since 1.0
     */

    private boolean validateFields()
    {
        if(firstName.getText().toString().isEmpty() || firstName.getText().toString().equals(""))
        {
            firstName.setError(getResources().getString(R.string.first_name_empty));
            return false;
        }
        else  if(!firstName.getText().toString().matches("[a-zA-Z\u0600-\u06FF ]+"))
        {
            firstName.setError(getResources().getString(R.string.full_name_validation));
            return false;
        }
        else if(lastName.getText().toString().isEmpty() || lastName.getText().toString().equals(""))
        {
            lastName.setError(getResources().getString(R.string.last_name_empty));
            return false;
        }
        else  if(!lastName.getText().toString().matches("[a-zA-Z\u0600-\u06FF ]+"))
        {
            lastName.setError(getResources().getString(R.string.full_name_validation));
            return false;
        }
        else if(mobileNo.getText().toString().isEmpty())
        {
            mobileNo.setError(getResources().getString(R.string.mobile_empty));
            return false;
        }
        else if(email.getText().toString().isEmpty())
        {
            email.setError(getResources().getString(R.string.email_empty));
            return false;
        }
        else if(password.getText().toString().isEmpty())
        {
            password.setError(getResources().getString(R.string.password_empty));
            return false;
        }
        else if(confirm_signup_password.getText().toString().isEmpty())
        {
            confirm_signup_password.setError(getResources().getString(R.string.password_empty));
            return false;
        }
        else if(!confirm_signup_password.getText().toString().equals(password.getText().toString()))
        {
            confirm_signup_password.setError(getResources().getString(R.string.passwords_dint_match));
            return false;
        }
        else if(!chkBox.isChecked())
        {
            text_for_popup.setText(getResources().getString(R.string.accept_terms_conditions));
            popupWithOneButton.show();
            return  false;
        }
        return true;
    }

    /**
     * <h>validateEmail</h>
     * For Validating the email enterd by the user.
     * The value returned is as boolean.
     * <pre>
     *    Used for Validating the email enterd by the user.
     *    if the input email matches the expression returns the true value, or else ruturms false
     * </pre>
     *
     * @param email
     * @return boolean
     * @since 1.0
     */

    public boolean validateEmail(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
        {
            isValid = true;
        }
        return isValid;
    }


    /**
     * <h>BackgroundValidateMobileNo</h>
     * For checking the availability of the mobile number.
     * The value returned is as void.
     * <pre>
     *    Used for checking the availability of the mobile number by doing the Okhttp call for an api called checkMobile.
     *    Here it checks the mobile number entered is different from the other or not.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void BackgroundValidateMobileNo()
    {
        dialogL=Utility.GetProcessDialogNew(SignupActivity.this,getResources().getString(R.string.validating_mobile));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.setCancelable(false);
            dialogL.show();
        }

        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_mobile",countryCode.getText().toString()+mobileNo.getText().toString());
            jsonObject.put("ent_user_type","2");
            jsonObject.put("ent_language", session.getLanguage());
            Utility.printLog("params to login "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"checkMobile", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                if(result!=null)
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        errFlag=jsonObject.getString("errFlag");
                        errMsg=jsonObject.getString("errMsg");
                        if(dialogL!=null)
                        {
                            dialogL.dismiss();
                        }
                        if(errFlag!=null)
                        {
                            if(!errFlag.equals("0"))
                            {
                                mobileNo.setError(errMsg);
                            }
                        }
                        else
                        {
                            mobileNo.setText(null);
                            Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                        }
                    }
                    catch(Exception e)
                    {
                        Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                    }
                }
                else
                {
                    Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
                }
            }
            @Override
            public void onError(String error)
            {
                dialogL.cancel();
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });
    }

    /**
     * <h>BackgroundValidateEmail</h>
     * For checking the availability of the email.
     * The value returned is as void.
     * <pre>
     *    Used for checking the availability of the email by doing the Okhttp call for an api called validateEmailZip.
     *    Here it checks the emailId entered is different from the other or not.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void BackgroundValidateEmail()
    {
        final ProgressDialog dialogL;
        jsonObject=new JSONObject();
        dialogL=Utility.GetProcessDialogNew(SignupActivity.this,getResources().getString(R.string.validating_email));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.setCancelable(false);
            dialogL.show();
        }
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_email",email.getText().toString());
            jsonObject.put("ent_user_type", 2);
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_language", session.getLanguage());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"validateEmailZip", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                String errFlag = null,errMsg = null;
                Utility.printLog( "success for updated the language   " + result);
                if(dialogL!=null)
                {
                    dialogL.dismiss();
                }
                if(result!=null)
                {
                    try
                    {
                        jsonObject=new JSONObject(result);
                        errFlag=jsonObject.getString("errFlag");
                        errMsg=jsonObject.getString("errMsg");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(!errFlag.equals("0"))
                    {
                        email.setText("");
                        text_for_popup.setText(errMsg);
                        popupWithOneButton.show();
                        email.setText(null);
                    }
                    else
                    {
                        boolean isValid=validateFields();
                        if(isValid)
                        {
                            if(validateEmail(email.getText().toString()))
                            {
                                if(!mobileNo.getText().toString().isEmpty())
                                {
                                    if(referral_code.getText().toString().trim().equals("")) {
                                        if (Utility.isNetworkAvailable(SignupActivity.this))
                                            BackgroundForGettingVerificationCode();
                                        else
                                            Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                    }
                                    else
                                    {
                                        validatePromoCode(referral_code.getText().toString());
                                    }
                                }
                                else
                                {
                                    mobileNo.setError(getResources().getString(R.string.mobile_empty));
                                }
                            }
                            else
                            {
                                email.setError(getResources().getString(R.string.enter_valid_email));
                            }
                        }
                    }
                }
                else
                {
                    Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                }
            }

            @Override
            public void onError(String error)
            {
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });
    }


    /**
     * <h>validatePromoCode</h>
     * For checking the availability of the PromoCode.
     * The value returned is as void.
     * <pre>
     *    Used for checking the availability of the PromoCode by doing the Okhttp call for an api called verifyCode.
     *    Here it checks the emailId entered is different from the other or not.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void validatePromoCode(final String promoCode)
    {
        final ProgressDialog dialogL=Utility.GetProcessDialogNew(SignupActivity.this,getResources().getString(R.string.validateReferal));
        dialogL.setCancelable(true);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_coupon",promoCode);
            jsonObject.put("ent_lat", session.getCurrentLat());
            jsonObject.put("ent_long", session.getCurrentLong());
            jsonObject.put("ent_language", session.getLanguage());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"verifyCode", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String response)
            {
                Utility.printLog("Success of getting ValidatePromoCode "+response);
                JSONObject jsnResponse;
                try
                {
                    dialogL.dismiss();
                    jsnResponse = new JSONObject(response);
                    if(jsnResponse.getString("errFlag").equals("0"))
                    {
                        if (Utility.isNetworkAvailable(SignupActivity.this))
                            BackgroundForGettingVerificationCode();
                        else
                            Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
                    else
                    {
                        referral_code.setText(null);
                        text_for_popup.setText(jsnResponse.getString("errMsg"));
                        popupWithOneButton.show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error)
            {
                dialogL.dismiss();
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        initialize();
    }

    /**
     * <h>setTarget</h>
     * For processing the image.
     * The value returned is as Target.
     * <pre>
     *    Used for processing the imagefile  for cicular image conversion.
     * </pre>
     *
     * @param progressBar for showing the user progress about the image processing..
     * @return Target
     * @since 1.0
     */

    public Target setTarget(final ProgressBar progressBar)
    {
        Target target= new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
                /**
                 * to set the image from fb to imageview after making circular imagee
                 */
                bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumWidth(),
                        getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumHeight(), true);
                Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                BitmapShader shader = new BitmapShader (bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);
                paint.setAntiAlias(true);
                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
                profile_pic.setImageBitmap(circleBitmap);
                /**
                 * to put the bitmap image into the file for uploading
                 */
                FileOutputStream ostream = null;
                try {
                    ostream = new FileOutputStream(mFileTemp);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                    ostream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                progressBar.setVisibility(View.VISIBLE);
            }
        };
        return target;
    }

    @Override
    public void updateLocation(Location location) {
        Utility.printLog("current latlongs in SignupActivity "+location.getLatitude()+" "+location.getLongitude());
        /**
         * to store the current locaiton in shared prefernece
         */
        session.setCurrentLat((float) location.getLatitude());
        session.setCurrentLong((float) location.getLongitude());
    }

    @Override
    public void location_Error(String error) {

    }
}

