package com.roadyo.passenger.pojo;

import java.util.ArrayList;

public class AppointmentResponse {
	
 /*   "errNum": "31",
    "errFlag": "0",
    "errMsg": "Got Appointments!",
    "penCount": "3",
    "refIndex": [17,27,28],
    */

	/*    "errNum":"31",
    "errFlag":"0",
    "errMsg":"Got Bookings!",
    "appointments":[*/
	
	String errNum;
	String errFlag;
	String errMsg;
	private String lastcount;

	public String getLastcount() {
		return lastcount;
	}

	private ArrayList<AppointmentList> appointments;

	public ArrayList<AppointmentList> getUpcomingAppointments() {
		return UpcomingAppointments;
	}

	public void setUpcomingAppointments(ArrayList<AppointmentList> upcomingAppointments) {
		UpcomingAppointments = upcomingAppointments;
	}

	private ArrayList<AppointmentList> UpcomingAppointments;
	public String getErrNum() {
		return errNum;
	}
	public void setErrNum(String errNum) {
		this.errNum = errNum;
	}
	public String getErrFlag() {
		return errFlag;
	}
	public void setErrFlag(String errFlag) {
		this.errFlag = errFlag;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public ArrayList<AppointmentList> getAppointments() {
		return appointments;
	}

}
