package com.roadyo.passenger.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;

public class TermsActivity extends Activity implements OnClickListener
{
	private TextView terms1,terms2;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.termsandcond);

		iniializeVariables();
	}

	private void iniializeVariables()
	{
		/**
		 * to set status bar color
		 */
		Utility.setStatusBarColor(this);

		terms1=(TextView)findViewById(R.id.btn_terms_conditions);
		terms2=(TextView)findViewById(R.id.btn_privacy_policy);
		ImageButton back_btn=(ImageButton)findViewById(R.id.back_btn);
		RelativeLayout rl_TermsandCond = (RelativeLayout) findViewById(R.id.rl_signin);
		TextView title=(TextView) findViewById(R.id.title);
		title.setText(getResources().getString(R.string.terms_and_conditions));

		SessionManager sessionManager=new SessionManager(this);
		/**
		 * to set typeface
		 */
		if(sessionManager.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(this,title);
			Utility.setTypefaceMuliRegular(this,terms1);
			Utility.setTypefaceMuliRegular(this,terms2);
		}

		terms1.setOnClickListener(this);
		terms2.setOnClickListener(this);
		back_btn.setOnClickListener(this);
		rl_TermsandCond.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.btn_terms_conditions)
		{
			Intent intent=new Intent(TermsActivity.this,WebViewActivity.class);
			intent.putExtra("URL", getString(R.string.TERMS_CONDITIONS));
			intent.putExtra("TITLE", terms1.getText().toString());
			startActivity(intent);
			overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
		}
		if(v.getId()==R.id.btn_privacy_policy)
		{
			Intent intent=new Intent(TermsActivity.this,WebViewActivity.class);
			intent.putExtra("URL",getString(R.string.PRIVACY_POLICY));
			intent.putExtra("TITLE",terms2.getText().toString());
			startActivity(intent);
			overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
		}
		if(v.getId()==R.id.rl_signin)
		{
			finish();
			overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		}
		if(v.getId()==R.id.back_btn)
		{
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
