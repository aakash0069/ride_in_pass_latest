package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * Created by rahul on 22/11/16.
 */
public class Comments implements Serializable
{
    private String body;
    private String created_at;
    private ZendeskViaClass via;

    public ZendeskViaClass getVia() {
        return via;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getBody() {
        return body;
    }
}
