package com.roadyo.passenger.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.GeocodingResponse;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.threembed.utilities.DB_Fav_Locations;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.LocationUtil;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author AKbar
 */

public class ManualDropoffActivity extends FragmentActivity implements View.OnClickListener,LocationUtil.GetLocationListener
        ,OnMapReadyCallback,GoogleMap.OnCameraMoveStartedListener,GoogleMap.OnCameraIdleListener
{
    private GoogleMap googleMap;
    private LocationUtil networkUtil ;
    private Timer myTimer;
    private boolean flagForCurrentLoc=false;
    private TextView show_addr_text_view,appointment_location;
    private ImageView currenct_satellite_button;
    private ImageView map_pin;
    private ImageView cross_icon;
    private DatabasePickupHandler dbPickup;
    private Double currentLatitude,currentLongitude;
    private Double[] latlongToBeSent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.manual_drop_layout);
        initializeVariables();
    }
    private void initializeVariables()
    {
        Button save_location_button= (Button) findViewById(R.id.save_location_button);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /**
         * to set language support
         */
        Utility.setLanguageSupport(this);
        /**
         * to set the status bar color
         */
        Utility.setStatusBarColor(this);

        show_addr_text_view= (TextView) findViewById(R.id.show_addr_text_view);
        appointment_location= (TextView) findViewById(R.id.appointment_location);
        TextView title= (TextView) findViewById(R.id.title);
        ImageButton back_btn= (ImageButton) findViewById(R.id.back_btn);
        RelativeLayout rl_change_card= (RelativeLayout) findViewById(R.id.rl_signin);
        currenct_satellite_button= (ImageView) findViewById(R.id.currenct_satellite_button);
        ImageView currenct_location_button = (ImageView) findViewById(R.id.currenct_location_button);
        map_pin= (ImageView) findViewById(R.id.map_pin);
        cross_icon= (ImageView) findViewById(R.id.cross_icon);
        latlongToBeSent=new Double[2];
        dbPickup = new DatabasePickupHandler(this);
        show_addr_text_view.setSelected(true);


        save_location_button.setOnClickListener(this);
        currenct_location_button.setOnClickListener(this);
        currenct_satellite_button.setOnClickListener(this);
        rl_change_card.setOnClickListener(this);
        back_btn.setOnClickListener(this);

        SessionManager sessionManager=new SessionManager(this);
        if(sessionManager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(this,show_addr_text_view);
            Utility.setTypefaceMuliBold(this,title);
            Utility.setTypefaceMuliBold(this,save_location_button);
            Utility.setTypefaceMuliBold(this,appointment_location);
        }
        /**
         * to flip the arrow
         */
        if(sessionManager.getLanguageCode().equals("ar"))
        {
            back_btn.setScaleX(-1);
        }
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null) {
            Log.d("set_pin_location_ll",""+bundle.containsKey("HintText"));
            String msg = bundle.getString("HintText");
            title.setText(msg);
            appointment_location.setText(msg);
            save_location_button.setText(getString(R.string.confirm1) + " " + msg);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getCurrentLocation();

        myTimer = new Timer();
        TimerTask myTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Calling Location...");
                        VisibleRegion visibleRegion = googleMap.getProjection()
                                .getVisibleRegion();
                        Point x1 = googleMap.getProjection().toScreenLocation(
                                visibleRegion.farRight);
                        Point y = googleMap.getProjection().toScreenLocation(
                                visibleRegion.nearLeft);
                        Point centerPoint = new Point(x1.x / 2, y.y / 2);
                        LatLng centerFromPoint = googleMap.getProjection().fromScreenLocation(centerPoint);
                        double lat = centerFromPoint.latitude;
                        double lon = centerFromPoint.longitude;
                        latlongToBeSent[0]=lat;
                        latlongToBeSent[1]=lon;
                        Utility.printLog("lat long fromm map in manual "+lat +" "+lon);

                        String[] params = new String[]{"" + lat, "" + lon};
                        new BackgroundGetAddress().execute(params);
                    }
                });
            }
        };
        myTimer.schedule(myTimerTask, 0, 2000);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(googleMap!=null)
        {
            this.googleMap=googleMap;
            googleMap.setBuildingsEnabled(true);
            googleMap.setOnCameraMoveStartedListener(this);
            googleMap.setOnCameraIdleListener(this);

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                @Override
                public void onCameraChange(CameraPosition arg0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            flagForCurrentLoc=true;
                        }
                    }, 5000);
                }
            });
        }
    }

    @Override
    public void onCameraIdle() {
        Animation animation = new TranslateAnimation(0,0,-50, 0);
        animation.setDuration(100);
        animation.setFillAfter(true);
        map_pin.startAnimation(animation);
        cross_icon.setVisibility(View.GONE);
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            Animation animation = new TranslateAnimation(0,0,0, -50);
            animation.setDuration(100);
            animation.setFillAfter(true);
            map_pin.startAnimation(animation);
            cross_icon.setVisibility(View.VISIBLE);
        }
        else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            Animation animation = new TranslateAnimation(0,0,0, -50);
            animation.setDuration(100);
            animation.setFillAfter(true);
            map_pin.startAnimation(animation);
            cross_icon.setVisibility(View.VISIBLE);
        }
        else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            Animation animation = new TranslateAnimation(0,0,0, -50);
            animation.setDuration(100);
            animation.setFillAfter(true);
            map_pin.startAnimation(animation);
            cross_icon.setVisibility(View.VISIBLE);
        }
    }

    //to get the address on the homescreen
    class BackgroundGetAddress extends AsyncTask<String, Void, String>
    {
        List<Address> address;
        String lat,lng;
        @Override
        protected String doInBackground(String... params) {
            try {

                lat = params[0];
                lng = params[1];


                if(lat!=null && lng!=null)
                {
                    Geocoder geocoder = new Geocoder(ManualDropoffActivity.this);

                    address= geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);
                    final String[] params1 = new String[]{params[0], params[1]};
                    if(address.isEmpty())
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new BackgroundGeocodingTask().execute(params1);
                            }
                        });
                    }
                }
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(address!=null && address.size()>0)
            {
                show_addr_text_view.setText(address.get(0).getAddressLine(0)+", "+address.get(0).getAddressLine(1));
                Utility.printLog("akbar:location" + show_addr_text_view.getText().toString());

                List<DB_Fav_Locations> dbFavLocations = dbPickup.getFavAddress();
                ArrayList<String> addressList=new ArrayList<>();
                for(int i = 0; i< dbFavLocations.size(); i++)
                {
                    Utility.printLog("stored pick location "+ dbFavLocations.get(i).get_formattedAddress()+" ize "+ dbFavLocations.size());
                    addressList.add(dbFavLocations.get(i).get_formattedAddress());
                }

                if(addressList.contains(show_addr_text_view.getText().toString()))
                {
                    for(int i=0;i<addressList.size();i++)
                    {
                        if(show_addr_text_view.getText().toString().equals(addressList.get(i)))
                        {
                            Utility.printLog("i am inside if for fav ");
                            break;
                        }
                        else
                        {
                            Utility.printLog("i am inside else for fav ");
//                            appointment_location.setText(getResources().getString(R.string.drop_location));
                        }
                    }
                }
                else
                {
                    Utility.printLog("i am inside else for fav ");
                }
            }
        }
    }
    class  BackgroundGeocodingTask extends AsyncTask<String, Void, String>
    {
        GeocodingResponse response;
        @Override
        protected String doInBackground(String... params)
        {
            SessionManager sessionManager=new SessionManager(ManualDropoffActivity.this);
            String lat = params[0];
            String  lng = params[1];
            String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false&key="+
                    sessionManager.getServerKey();
            final String[] result = new String[1];
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response;
            try
            {
                response = client.newCall(request).execute();
                result[0] =response.body().string();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                Utility.printLog("result for exception "+e);
            }
            return result[0];
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            Utility.printLog("result for thef time "+result);
            if(result!=null)
            {
                Gson gson=new Gson();
                response=gson.fromJson(result, GeocodingResponse.class);
                if(response.getStatus().equals("OK"))
                {
                    show_addr_text_view.setText(response.getResults().get(0).getFormatted_address());
                }
            }
        }
    };

    private void getCurrentLocation()
    {
        if (networkUtil == null)
        {
            networkUtil = new LocationUtil(ManualDropoffActivity.this, this);
        }
        else
        {
            networkUtil.checkLocationSettings();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.save_location_button:
            {
                Utility.printLog("latlongs from manual drop in drop "+latlongToBeSent[0]+" "+latlongToBeSent[1]);
                networkUtil.stop_Location_Update();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("LATITUDE_SEARCH",latlongToBeSent[0]+"");
                returnIntent.putExtra("LONGITUDE_SEARCH",latlongToBeSent[1]+"");
                returnIntent.putExtra("SearchAddress",show_addr_text_view.getText().toString());
                setResult(RESULT_OK,returnIntent);
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                break;
            }
            case R.id.currenct_location_button:
            {
                LatLng latLng = new LatLng(currentLatitude, currentLongitude);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
                break;
            }
            case R.id.currenct_satellite_button:
            {
                if(googleMap.getMapType() != GoogleMap.MAP_TYPE_HYBRID)
                {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    currenct_satellite_button.setSelected(true);
                }
                else if(googleMap.getMapType() != GoogleMap.MAP_TYPE_NORMAL)
                {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    currenct_satellite_button.setSelected(false);
                }
                break;
            }
            case R.id.rl_signin:
            {
                finish();
                break;
            }
            case R.id.back_btn:
            {
                finish();
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkUtil.stop_Location_Update();
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer=null;
        }
    }

    @Override
    public void updateLocation(Location location) {
        currentLatitude=location.getLatitude();
        currentLongitude=location.getLongitude();
        if(!flagForCurrentLoc)
        {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
            latlongToBeSent[0]=location.getLatitude();
            latlongToBeSent[1]=location.getLongitude();
        }
    }

    @Override
    public void location_Error(String error) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==786)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Utility.printLog("i am inside 786 ");
                String current_address_local=data.getStringExtra("CURRENT_ADDRESS");
                Double curr_lat=data.getDoubleExtra("CURR_LAT",0);
                Double curr_long=data.getDoubleExtra("CURR_LONG",0);
                String address_desc=data.getStringExtra("ADDRESS_DESC");
                Utility.printLog("address from fav address "+current_address_local+" lat "+curr_lat+" long "+curr_long
                        +" descr "+address_desc+" id "+data.getIntExtra("ID",0));

                latlongToBeSent[0] =curr_lat;
                latlongToBeSent[1] =curr_long;
                appointment_location.setText(address_desc);
                show_addr_text_view.setText(current_address_local);
            }
        }
    }
}
