package com.roadyo.passenger.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.bloomingdalelimousine.ridein.R;
import com.roadyo.passenger.pojo.LanguagesClass;
import com.threembed.utilities.SessionManager;

import java.util.ArrayList;

import static com.threembed.utilities.Utility.setLocale;

/**
 * @author AKbar
 */
public class AdapterLanguages extends RecyclerView.Adapter<AdapterLanguages.MyViewHolder>
{
    private ArrayList<LanguagesClass> mData;
    private Context context;
    private SessionManager sessionManager;
    private ArrayList<CheckBox> checkboxes= new ArrayList<>();

    public AdapterLanguages(ArrayList<LanguagesClass> mData, Activity context)
    {
        this.mData=mData;
        this.context=context;
        sessionManager=new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(context).inflate(R.layout.single_row_langauges_list,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvLanguage.setText(mData.get(position).getName());

        holder.checkboxlayout.setTag(position);

        holder.checkboxlayout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    resetAll();
                    try
                    {
                        sessionManager.setLanguage(mData.get(position).getId());
                        sessionManager.setLanguageCode(mData.get(position).getCode());
                        setLocale(mData.get(position).getCode(),context);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        sessionManager.setLanguage("0");
                    }
                    compoundButton.setChecked(true);

                }else if(!b)
                {
                    compoundButton.setChecked(false);
                    sessionManager.setLanguage("0");
                }
            }
        });
        checkboxes.add(holder.checkboxlayout);
        if(mData.get(position).getId().equals(sessionManager.getLanguage()))
        {
            holder.checkboxlayout.setChecked(true);
        }
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvLanguage;
        CheckBox checkboxlayout;

        MyViewHolder(View itemView) {
            super(itemView);
            tvLanguage= (TextView) itemView.findViewById(R.id.tvLanguage);
            checkboxlayout= (CheckBox) itemView.findViewById(R.id.checkboxlayout);
        }
    }

    private void resetAll()
    {
      for(int i=0;i<checkboxes.size();i++)
      {
          checkboxes.get(i).setChecked(false);
      }
    }
}
