package com.roadyo.passenger.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.roadyo.passenger.laterUtil.APICallerForAddress;
import com.roadyo.passenger.laterUtil.AddressPojo;
import com.roadyo.passenger.laterUtil.AddressProvider;
import com.roadyo.passenger.laterUtil.LocationPlaces;
import com.roadyo.passenger.laterUtil.LocationUpdating;
import com.roadyo.passenger.laterUtil.MyLocationListener;
import com.threembed.utilities.SessionManager;
import com.bloomingdalelimousine.ridein.R;
import java.util.ArrayList;

public class ConfirmPickupForBookingActivity extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener{

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 109;
    private static final int REQUEST_DRIVERS_REQUEST_CODE = 99;
    private APICallerForAddress apiCallerForAddress = new APICallerForAddress();
    private TextView address_tv;
    private Resources resources;
    private GoogleMap googleMap;
    private boolean isItFromPlace = false;
    private AddressPojo addressPojo = new AddressPojo();
    private LocationUpdating locationUpdating;
    LatLng oldll = null,newll=null;
    private Marker marker;
    private double from_latitude;
    private double from_longitude;
    private String pickup_address;
    private String dropoff_address;
    private String to_latitude;
    private String to_longitude;
    private String selected_car_type_id;
    private String ent_fare_estimate="";
    private String ent_sub_type="";
    private ArrayList<String> nearestDrivers;
    private SessionManager session;
    boolean isAddressAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pickup_for_booking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new SessionManager(this);

        resources = getResources();

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        address_tv = (TextView) findViewById(R.id.address_tv);
        Button confirm_location_btn = (Button) findViewById(R.id.confirm_location_btn);
        confirm_location_btn.setOnClickListener(this);
        locationUpdating = new LocationUpdating(this);
        Bundle bundle1 = getIntent().getExtras();
        if(bundle1!=null && bundle1.containsKey("from_latitude")){
            from_latitude = bundle1.getDouble("from_latitude");
            from_longitude = bundle1.getDouble("from_longitude");
            pickup_address = bundle1.getString("PICKUP_ADDRESS");
            address_tv.setText(createSpanableString(pickup_address), TextView.BufferType.SPANNABLE);
            isAddressAvailable = true;
            dropoff_address = bundle1.getString("DROPOFF_ADDRESS");
            to_latitude = bundle1.getString("ToLatitude");
            to_longitude = bundle1.getString("ToLongitude");
            selected_car_type_id = bundle1.getString("Car_Type");
            ent_fare_estimate = bundle1.getString("ent_fare_estimate");
            ent_sub_type = bundle1.getString("SUB_TYPES");
            nearestDrivers = bundle1.getStringArrayList("my_drivers");
        }
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if(requestCode==REQUEST_DRIVERS_REQUEST_CODE){

            Intent intent = new Intent(ConfirmPickupForBookingActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }

        if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("TAG123", "Place: " + place.getName()
                        +"\n"+place.getAddress()
                );
                isItFromPlace = true;
                address_tv.setText(place.getAddress());
                LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude,place.getLatLng().longitude);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(MOUNTAIN_VIEW)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                if(place.getLatLng().latitude!=0.0 && place.getLatLng().longitude!=0.0)
                {
                    addressPojo.setPLACE_ID(place.getId());
                    addressPojo.setLATITUDE(place.getLatLng().latitude+"");
                    addressPojo.setLONGITUDE(place.getLatLng().longitude+"");
                    addressPojo.setFORMATED_ADDRESS((String) place.getAddress());
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("TAG123", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onMapReady(final GoogleMap gMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap=gMap;
        googleMap.setMyLocationEnabled(true);

        if(getIntent().hasExtra("ADDRESS_INFO")) {
            addressPojo = (AddressPojo) getIntent().getSerializableExtra("ADDRESS_INFO");
            isItFromPlace = true;
            LatLng MOUNTAIN_VIEW = new LatLng(Double.parseDouble(addressPojo.getLATITUDE()),Double.parseDouble(addressPojo.getLONGITUDE()));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(MOUNTAIN_VIEW)      // Sets the center of the map to Mountain View
                    .zoom(17)                   // Sets the zoom
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            address_tv.setText(createSpanableString(addressPojo.getFORMATED_ADDRESS()), TextView.BufferType.SPANNABLE);
        }
        Log.d("PickUpActivity","lat_long "+from_latitude+","+from_longitude);
        if(from_latitude!=0 && from_longitude !=0){
            newll = new LatLng(from_latitude,from_longitude);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(newll)      // Sets the center of the map to Mountain View
                    .zoom(17)                   // Sets the zoom
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {
            locationUpdating.startLocationUpdate(true, new MyLocationListener() {
                @Override
                public void getLoaction(final Location location) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            newll = new LatLng(location.getLatitude(),
                                    location.getLongitude());
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(newll)      // Sets the center of the map to Mountain View
                                    .zoom(17)                   // Sets the zoom
                                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                                    .build();                   // Creates a CameraPosition from the builder
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    });
                }
            });
        }
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                Log.d("TAG123", googleMap.getCameraPosition().target.toString());
            }
        });

        googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                if (!isItFromPlace) {
                    address_tv.setText(resources.getString(R.string.fetching_location));
                }
            }
        });

        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {

            }
        });
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (!isAddressAvailable) {
                    apiCallerForAddress.setForAddress(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, new AddressProvider() {
                        @Override
                        public void onAddress(final LocationPlaces places) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("TAG123", googleMap.getCameraPosition().target.toString() + "\n" + places.getAddress());
                                    address_tv.setText( createSpanableString(places.getAddress()), TextView.BufferType.SPANNABLE);
                                    addressPojo.setPLACE_ID(places.getPlace_id());
                                    addressPojo.setLATITUDE(places.getLatitude()+"");
                                    addressPojo.setLONGITUDE(places.getLongitude()+"");
                                    addressPojo.setFORMATED_ADDRESS(places.getAddress());
                                    session.storePickuplat("" + from_latitude);
                                    session.storePickuplng("" + from_longitude);
                                }
                            });
                        }
                        @Override
                        public void onError(final String error) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("TAG123", googleMap.getCameraPosition().target.toString() + "\n" + error);
                                    address_tv.setText(error);
                                }
                            });
                        }
                    });
                }else {
                    isAddressAvailable=false;
                    isItFromPlace=false;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm_location_btn:
                Intent intent = new Intent(this, RequestPickup.class);
                intent.putExtra("PICKUP_ADDRESS", pickup_address);
                intent.putExtra("FromLatitude", ""+from_latitude);
                intent.putExtra("FromLongitude", ""+from_longitude);
                intent.putExtra("DROPOFF_ADDRESS", dropoff_address);
                intent.putExtra("ToLatitude", to_latitude);
                intent.putExtra("ToLongitude", to_longitude);
                intent.putExtra("my_drivers", nearestDrivers);
                intent.putExtra("Car_Type", selected_car_type_id);
                intent.putExtra("ent_fare_estimate", ent_fare_estimate);
                intent.putExtra("CURRENT_MASTER_TYPE_ID", selected_car_type_id);
                intent.putExtra("SUB_TYPE", ent_sub_type);
                startActivity(intent);
                break;
        }
    }

    private SpannableStringBuilder createSpanableString(String first){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString str1= new SpannableString(getString(R.string.confirm_your_pickup_at)+" ");
        str1.setSpan(new ForegroundColorSpan(Color.GRAY), 0, str1.length(), 0);
        builder.append(str1);
        SpannableString str2= new SpannableString(first.toString());
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        str2.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str2.length(), 0);
        str2.setSpan(boldSpan, 0, str2.length(), 0);
        builder.append(str2);
        return builder;
    }
}
