package com.roadyo.passenger.pojo;

import java.io.Serializable;

public class AppointmentList implements Serializable{
	
/*	 "date": "2014-03-27",
         {
             "apntDt": "2014-03-27 14:30:00",
             "pPic": "myths_busted_doctor_tell_something_wrong_1850die-1850e30.jpg",
             "email": "logan@yahoo.com",
             "status": "2",
             "fname": "Logan",
             "phone": "",
             "apntTime": "14:30",
             "apntDate": "2014-03-27",
             "apptLat": 0,
             "apptLong": 0,
             "addrLine1": "hi",
             "addrLine2": "",
             "notes": ""
         }
     ]
 },*/

    /*    "apntDt":"2016-05-28 18:30:15",
    "pPic":"3380282979915.png",
    "email":"robin@gmail.com",
    "status":"Driver cancelled.",
    "apptType":"1",
    "fname":"Robin",
    "phone":"123",
    "apntTime":"06:30 pm",
    "cancel_status":"7",
    "cancelAmt":"0",
    "apntDate":"2016-05-28",
    "apptLat":"13.0292",
    "apptLong":"77.5897",
    "payStatus":"0",
    "addrLine1":"10th Cross St, RBI Colony, Ganganagar",
    "addrLine2":"",
    "dropLine1":"",
    "dropLine2":"",
    "notes":"",
    "bookType":"",
    "amount":"0",
    "statCode":"5",
    "distance":"0"*/
    private String apntDate;
    private String pPic;
    private String email;
    private String status;
    private String apptType;
    private String phone;
    private String apntTime;
    private String apntDt;
    private String payStatus;
    private String addrLine1;
    private String dropLine1;
    private String fname;

    private String vehicleType;
    private String vehicleType_Id;

    private Pickup_latlong pickup_latlong;

    private Drop_latlong drop_latlong;

    public Pickup_latlong getPickup_latlong ()
    {
        return pickup_latlong;
    }

    public void setPickup_latlong (Pickup_latlong pickup_latlong)
    {
        this.pickup_latlong = pickup_latlong;
    }

    public Drop_latlong getDrop_latlong ()
    {
        return drop_latlong;
    }

    public void setDrop_latlong (Drop_latlong drop_latlong)
    {
        this.drop_latlong = drop_latlong;
    }

    public String getVehicleType_Id() {
        return vehicleType_Id;
    }

    public void setVehicleType_Id(String vehicleType_Id) {
        this.vehicleType_Id = vehicleType_Id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public InvoiceClass getInvoice() {
        return invoice;
    }
    private InvoiceClass invoice;
    public String getR() {
        return r;
    }
    private String notes,r;
    public String getRouteImg() {
        return routeImg;
    }
    private String routeImg;
    public String getApntDt() {
        return apntDt;
    }
    private String distance;
    public String getDrop_dt() {
        return drop_dt;
    }
    private String cancel_status,bid,drop_dt;
    public String getApntDate() {
        return apntDate;
    }
    public String getpPic() {
        return pPic;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getApptType() {
        return apptType;
    }
    public String getFname() {
        return fname;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getApntTime() {
        return apntTime;
    }
    public String getPayStatus() {
        return payStatus;
    }
    public String getAddrLine1() {
        return addrLine1;
    }
    public String getDropLine1() {
        return dropLine1;
    }
    public String getNotes() {
        return notes;
    }
    public String getDistance() {
        return distance;
    }
    public String getCancel_status() {
        return cancel_status;
    }
    public String getBid() {
        return bid;
    }
}
	
	
	


