package com.roadyo.passenger.pubnu.pojo;

import com.roadyo.passenger.pojo.MandatoryVersion;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Akbar
 */
public class ConfigurationData implements Serializable
{
    private MandatoryVersion versions;
    private String PubnubIntervalHome;
    private String BookingAmountAllowUpto;
    private ArrayList<String> GoogleKeysArray;

    public ArrayList<String> getGoogleKeysArray() {
        return GoogleKeysArray;
    }

    public String getDistanceMatrixInterval() {
        return DistanceMatrixInterval;
    }

    private String DistanceMatrixInterval;

    public String getBookingAmountAllowUpto() {
        return BookingAmountAllowUpto;
    }

    public String getPubnubIntervalHome() {
        return PubnubIntervalHome;
    }

    public MandatoryVersion getVersions() {
        return versions;
    }


}
