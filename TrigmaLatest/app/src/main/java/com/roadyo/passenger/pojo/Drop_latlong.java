package com.roadyo.passenger.pojo;

import java.io.Serializable;

public class Drop_latlong  implements Serializable {
    private String drop_lat;

    private String drop_long;

    public String getDrop_lat() {
        return drop_lat;
    }

    public void setDrop_lat(String drop_lat) {
        this.drop_lat = drop_lat;
    }

    public String getDrop_long() {
        return drop_long;
    }

    public void setDrop_long(String drop_long) {
        this.drop_long = drop_long;
    }

    @Override
    public String toString() {
        return "ClassPojo [drop_lat = " + drop_lat + ", drop_long = " + drop_long + "]";
    }
}
