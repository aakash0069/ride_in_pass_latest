package com.roadyo.passenger.main;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.twiliocallmodule.TwilioInitializationClass;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.twilio.client.Twilio;
import org.json.JSONException;
import org.json.JSONObject;
import static com.threembed.utilities.Utility.setLocale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,View.OnClickListener
{
    public DrawerLayout drawer;
    private Fragment fragment;
    public TextView edit_profile;
    public TextView fragment_title;
    public RelativeLayout action_bar;
    public static RelativeLayout network_error_layout;
    public MenuItem menu_wallet;
    private boolean backPressedToExitOnce=false;
    public TextView user_name;
    public Window window;
    public TextView mobile;
    public boolean isFromRegistration;
    public ImageView fragment_title_logo;
    public boolean isFromSplash;
    private SessionManager session;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);
        new TwilioInitializationClass().initializeTwilioClientSDK(this);
        session = new SessionManager(this);
        window = MainActivity.this.getWindow();
        SessionManager sessionManager = new SessionManager(MainActivity.this);
        /**
         * to set the status bar color
         */
        Utility.setStatusBarColor(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        edit_profile= (TextView) findViewById(R.id.edit_profile);
        fragment_title= (TextView) findViewById(R.id.fragment_title);
        fragment_title_logo= (ImageView) findViewById(R.id.fragment_title_logo);
        action_bar= (RelativeLayout) findViewById(R.id.action_bar);
        network_error_layout= (RelativeLayout) findViewById(R.id.network_error_layout);
        RelativeLayout menu_layout= (RelativeLayout) findViewById(R.id.menu_layout);
        TextView error_btn= (TextView) findViewById(R.id.error_btn);
//        drawer.setBackgroundColor(getResources().getColor(R.color.white));
        action_bar.setBackgroundColor(getResources().getColor(R.color.background));

        /**
         * for help screen overlay
         */
        isFromRegistration=getIntent().getBooleanExtra("FROM_REGISTER",false);
        Utility.printLog("is from registration "+isFromRegistration);
        /**
         * to check for giving pubnub interval in homepage
         * if its from splash actiity then call the pubnub after interval
         */
        isFromSplash=getIntent().getBooleanExtra("COMING_FROM_SPLASH",false);
        fragment = new HomePageFragment();
        FragmentManager fragmentManager =
                getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment)
                .commit();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        navigationView.setCheckedItem(R.id.menu_home);
//        navigationView.setBackgroundColor(getResources().getColor(R.color.white));
        // find MenuItem you want to change
//        menu_wallet = menu.findItem(R.id.menu_wallet);
//        menu_wallet.setTitle(getResources().getString(R.string.my_wallet)+" "+"("+getResources().getString(R.string.currencuSymbol)+" "+ sessionManager.getWalletBal()+")");

        if(sessionManager.getLanguageCode().equals("ar") )
        {
            setLocale("ar",MainActivity.this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
        else
        {
            setLocale("en",MainActivity.this);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

//        View headerview = navigationView.getHeaderView(0);
//        CardView navigationHeader= (CardView)headerview.findViewById(R.id.navigationHeader);
//        user_name= (TextView)headerview.findViewById(R.id.user_name);
//        mobile= (TextView)headerview.findViewById(R.id.mobile);
//        ProgressBar progress_bar= (ProgressBar) headerview.findViewById(R.id.progress_bar);
//        user_image= (ImageView) headerview.findViewById(R.id.user_image);
        assert menu_layout != null;
        menu_layout.setOnClickListener(this);
//        user_name.setText(sessionManager.getUserName().toUpperCase());
//        mobile.setText(sessionManager.getPhoneNumber());

        /**
         * to set typeface
         */
        if(sessionManager.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(this,fragment_title);
            Utility.setTypefaceMuliRegular(this,error_btn);
            Utility.setTypefaceMuliRegular(this,edit_profile);
//            Utility.setTypefaceMuliBold(this,user_name);
//            Utility.setTypefaceMuliBold(this,mobile);
        }

        Utility.printLog("image in main activity "+sessionManager.getProfileImage());
        //Setting doctor pic
        /**
         * to set the image into image view and
         * add the write the image in the file
         */
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.containsKey("MESSAGE")){
            String msg = bundle.getString("MESSAGE");
            Utility.printLog("MESSAGE_RESULT_OK "+msg);
            requestResult(msg);
        }
    }


    public void requestResult(String msg){

        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(msg);

        // Initialize a new ClickableSpan to display red background
        ClickableSpan redClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                // Do something
//                Toast.makeText(RouteAndPaymentSelectActivity.this, "Working..", Toast.LENGTH_SHORT).show();
//                mCLayout.setBackgroundColor(Color.RED);

                Utility.printLog("clicked at this position s ");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+getString(R.string.customer_care_num)));
                startActivity(callIntent);
            }
        };

        if(msg.contains(getString(R.string.customer_care_num))) {
            ssBuilder.setSpan(
                    redClickableSpan, // Span to add
                    msg.indexOf(getString(R.string.customer_care_num)), // Start of the span (inclusive)
                    msg.indexOf(getString(R.string.customer_care_num)) + String.valueOf(getString(R.string.customer_care_num)).length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        }

        Dialog popupWithSingleButton=Utility.showPopupWithOneButton(this);
        TextView title_popup= (TextView) popupWithSingleButton.findViewById(R.id.title_popup);
        TextView text_for_popup= (TextView) popupWithSingleButton.findViewById(R.id.text_for_popup);
        TextView yes_button= (TextView) popupWithSingleButton.findViewById(R.id.yes_button);
        popupWithSingleButton.setCancelable(false);
        yes_button.setText(getResources().getString(R.string.ok));


        //aakash change
        if(msg.contains(getString(R.string.customer_care_num))) {
            title_popup.setVisibility(View.VISIBLE);
            title_popup.setText(getResources().getString(R.string.message));
        }
        else{
            title_popup.setVisibility(View.GONE);
        }


//        text_for_popup.setText(data.getStringExtra("MESSAGE"));
//        text_for_popup.setText(msg);

        // Display the spannable text to TextView
        text_for_popup.setText(ssBuilder);

        // Specify the TextView movement method
        text_for_popup.setMovementMethod(LinkMovementMethod.getInstance());


        popupWithSingleButton.show();
    }

    public Target setTarget(final ProgressBar progressBar, final ImageView user_image)
    {
        Target target= new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
                /**
                 * to set the image from fb to imageview after making circular imagee
                 */
                bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumWidth(),
                        getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumHeight(), true);
                Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                BitmapShader shader = new BitmapShader (bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);
                paint.setAntiAlias(true);
                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
                user_image.setImageBitmap(circleBitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                progressBar.setVisibility(View.VISIBLE);
            }
        };
        return target;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(!HomePageFragment.visibleStatus())
        {
            Fragment fragment = new HomePageFragment();
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.commit();
            navigationView.setCheckedItem(R.id.menu_home);
        }
        else {
            if (backPressedToExitOnce) {
                // finish();
                moveTaskToBack(true);
                overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
            } else {
                this.backPressedToExitOnce = true;
                Utility.showToast(this,getResources().getString(R.string.press_to_exist));
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressedToExitOnce = false;
                    }
                }, 2000);
            }
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Utility.printLog("clicked the navigation item "+id);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (id)
        {
            case R.id.menu_home:
            {
                fragmentTransaction.replace(R.id.frame_container, new HomePageFragment(),null);
                fragmentTransaction.commit();
                item.setChecked(true);
                break;
            }
            case R.id.menu_wallet:
            {
                Bundle bundle = new Bundle();
                bundle.putBoolean("REMOVE_RESTRICTION", false);
                fragment = new PaymentFragment();
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
                item.setChecked(true);
                break;
            }
            case R.id.menu_booking:
            {
//                fragment = new HistoryFragment();
                fragment = new BookingFragment();
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
//                item.setChecked(true);
                break;
            }
            case R.id.menu_support:
            {
                fragment = new SupportFragment();
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
                item.setChecked(true);
                break;
            }
            case R.id.menu_share:
            {
                fragment = new InviteFragment();
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
                item.setChecked(true);
                break;
            }
            case R.id.menu_info:
            {
                fragment = new AboutFragment();
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
                item.setChecked(true);
                break;
            }

            case R.id.menu_profile:
            {
                fragment = new ProfileFragment();
                fragmentTransaction.replace(R.id.frame_container,fragment,null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);
                item.setChecked(true);
                break;
            }
            case R.id.menu_logout:
            {
                logoutButtonListner();
                break;
            }

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.menu_layout:
            {
                if (drawer.isDrawerOpen(GravityCompat.START))
                {

                    drawer.closeDrawer(GravityCompat.START);
                }
                else
                {
                    View view = this.getCurrentFocus();
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    drawer.openDrawer(GravityCompat.START);
                }
                break;
            }
        }
    }


    /**
     * <h>logoutButtonListner</h>
     * For showing the AlertDialogue for Logout.
     * The value returned is as void.
     * <pre>
     *    Used for showing the AlertDialogue for Logout. if the user clicks on "Yes" it will call the BackgroundLogOutTask()
     *    method and and dissmiss the AlertDialogue. if the user clicks on "No" then only dismisses the AlertDialogue.
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void logoutButtonListner()
    {
        final Dialog popupWithTwoButtons=Utility.showPopupWithTwoButtons(MainActivity.this);
        TextView dialogText= (TextView) popupWithTwoButtons.findViewById(R.id.text_for_popup);
        TextView title_popup= (TextView) popupWithTwoButtons.findViewById(R.id.title_popup);
        TextView yes_button= (TextView) popupWithTwoButtons.findViewById(R.id.yes_button);
        TextView no_button= (TextView) popupWithTwoButtons.findViewById(R.id.no_button);
        title_popup.setText(getResources().getString(R.string.logout));
        dialogText.setText(getResources().getString(R.string.logout_alert_message));
        yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
                if(Utility.isNetworkAvailable(MainActivity.this))
                {
                    BackgroundLogOutTask();
                }
                else
                {
                    Utility.showToast(MainActivity.this,getResources().getString(R.string.network_connection_fail));
                }
            }
        });
        no_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
            }
        });
        popupWithTwoButtons.show();
    }

    /**
     * <h>BackgroundLogOutTask</h>
     * For logging out from the device.
     * The value returned is as void.
     * <pre>
     *    Used for logging out from the device. It calls the logout server api and once the response is hit back
     * </pre>
     *
     * @param
     * @return void
     * @since 1.0
     */

    private void BackgroundLogOutTask()
    {
        final ProgressDialog dialogL= Utility.GetProcessDialog(MainActivity.this);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject = new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_user_type", 2);
            jsonObject.put("ent_date_time", curenttime);
            Utility.printLog("logout params  "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"logout", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                if (dialogL!=null)
                {
                    dialogL.dismiss();
                }
                Utility.printLog("logout response "+result);
                if (result!=null)
                {
                    DatabasePickupHandler dbPickup = new DatabasePickupHandler(MainActivity.this);
                    dbPickup.deleteAllRows();

                    session.setIsLogin(false);
                    session.setCardToken("");
                    session.setLast4Digits("");
                    session.setBookingReponseFromPubnub("");
                    session.setPubnubResponseForNonBooking("");
                    session.setPubnubResponse("");
                    Intent intent=new Intent(MainActivity.this,SplashActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                    NotificationManager notificationManager = (NotificationManager)MainActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancelAll();
                }
                else
                {
                    if (dialogL!=null)
                    {
                        dialogL.dismiss();
                    }
                    Utility.showToast(MainActivity.this,getResources().getString(R.string.requestTimeout));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.showToast(MainActivity.this,getResources().getString(R.string.network_connection_fail));
            }
        });
    }


}
