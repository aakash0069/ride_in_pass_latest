package com.roadyo.passenger.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.threembed.utilities.Utility;

public class NetworkChangeReceiver extends BroadcastReceiver 
{
	@Override
	public void onReceive(final Context context, final Intent intent) 
	{
		String status = NetworkService.getConnectivityStatusString(context);
		String[] networkStatus = status.split(",");
		try
		{
			if("0".equals(networkStatus[1]))
			{
				MainActivity.network_error_layout.setVisibility(View.VISIBLE);
			}
			else
			{
				MainActivity.network_error_layout.setVisibility(View.GONE);
			}
		}
			catch (Exception e)
			{
				Utility.printLog("error");
			}
	}
}
