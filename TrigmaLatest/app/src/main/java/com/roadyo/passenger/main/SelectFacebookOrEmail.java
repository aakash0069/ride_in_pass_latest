package com.roadyo.passenger.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.LoginResponse;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectFacebookOrEmail extends AppCompatActivity implements View.OnClickListener{

    String selected_type;
    private TextView title_tv;
    private TextView facebook_tv;
    private TextView email_tv;
    private CallbackManager callbackManager;
    private SessionManager session;
    private RelativeLayout fb_login_rl;
    private RelativeLayout email_login_rl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_facebook_or_email);
        session = new SessionManager(this);

        title_tv = (TextView) findViewById(R.id.title_tv);
        facebook_tv = (TextView) findViewById(R.id.facebook_tv);
        email_tv = (TextView) findViewById(R.id.email_tv);
        fb_login_rl = (RelativeLayout) findViewById(R.id.fb_login_rl);
        email_login_rl = (RelativeLayout) findViewById(R.id.email_login_rl);

        Bundle bundle = getIntent().getBundleExtra("TITLE_INFO");

        if(bundle!=null && bundle.containsKey("SELECTED_TYPE")){
            selected_type = bundle.getString("SELECTED_TYPE");
            setSelectedTitle(selected_type);
        } else {
            finish();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FacebookSdk.sdkInitialize(SelectFacebookOrEmail.this);
        callbackManager = CallbackManager.Factory.create();
        loginFacebookSdk();

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        fb_login_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(SelectFacebookOrEmail.this))
                    LoginManager.getInstance().logInWithReadPermissions(SelectFacebookOrEmail.this, Arrays.asList("public_profile", "user_friends", "email"));
                else
                    Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });

        email_login_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_type.equals("1"))
                {
                    Intent intent=new Intent(SelectFacebookOrEmail.this,SigninActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                } else {
                    Intent intent=new Intent(SelectFacebookOrEmail.this,SignupActivity.class);
//					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
            }
        });
    }

    private void setSelectedTitle(String selected_type) {
        if(selected_type.equals("1")){
            title_tv.setText(getResources().getString(R.string.login_to_your_acc));
            facebook_tv.setText(getResources().getString(R.string.login)+" "+getResources().getString(R.string.with_facebook));
            email_tv.setText(getResources().getString(R.string.login)+" "+getResources().getString(R.string.with_email));
        } else {
            title_tv.setText(getResources().getString(R.string.create_account));
            facebook_tv.setText(getResources().getString(R.string.signup)+" "+getResources().getString(R.string.with_facebook));
            email_tv.setText(getResources().getString(R.string.signup)+" "+getResources().getString(R.string.with_email));
        }
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * <h>LoginFacebookSdk</h>
     * <p>
     *     This method is being called from onCreate method. this method is called
     *     when user click on LoginWithFacebook button. it contains three method onSuccess,
     *     onCancel and onError. if login will be successfull then success method will be
     *     called and in that method we obtain user all details like id, email, name, profile pic
     *     etc. onCancel method will be called if user click on facebook with login button and
     *     suddenly click back button. onError method will be called if any problem occurs like
     *     internet issue.
     * </p>
     */
    private void loginFacebookSdk()
    {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("LoginActivity", response.toString());
                        String fbid = object.optString("id");
                        String fb_emailId = object.optString("email");
                        String fb_firstName = object.optString("first_name");
                        String fb_lastName = object.optString("last_name");
                        String gender = object.optString("gender");
                        String fb_pic = "https://graph.facebook.com/" + fbid + "/picture";
                        Utility.printLog("fb response fbid"+fbid+" fb_emailId"+fb_emailId+" fb_firstName"+fb_firstName
                                +" fb_lastName"+fb_lastName+" fb_pic"+fb_pic);
                        UserLoginForSocial(fb_emailId,fbid,fb_firstName,fb_lastName,fb_pic,"2",gender);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }

            @Override
            public void onError(FacebookException error) {
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }

    /**
     * used for login using the social media values.
     * The value returned is as void.
     * <pre>
     *    Used for login using the social media values.
     * </pre>
     *
     * @param fb_emailId String
     * @param fbid String
     * @param fb_firstName String
     * @param fb_lastName String
     * @param fb_pic String
     * @param type String
     * @param gender String
     * @return void
     * @since 1.0
     */

    private void UserLoginForSocial(final String fb_emailId, final String fbid, final String fb_firstName,
                                    final String fb_lastName, final String fb_pic, final String type, final String gender )
    {
        final ProgressDialog dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SelectFacebookOrEmail.this,getResources().getString(R.string.loggingIn));
        dialogL.setCancelable(false);
        dialogL.show();
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_email",fb_emailId);
            jsonObject.put("ent_password", fbid);
            jsonObject.put("ent_dev_id",  session.getDeviceId());
            jsonObject.put("ent_push_token", session.getRegistrationId());
            jsonObject.put("ent_latitude", session.getCurrentLat());
            jsonObject.put("ent_longitude", session.getCurrentLong());
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_device_type","2");
            jsonObject.put("ent_login_type",type);
            jsonObject.put("ent_language", session.getLanguage());
            Utility.printLog("params to login "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"slaveLogin", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                dialogL.dismiss();
                Utility.printLog("reposne without parese "+result);

                getUserLoginInfoForSocial(result,fb_emailId,fbid,fb_firstName,fb_lastName,fb_pic,type,gender);
            }

            @Override
            public void onError(String error)
            {
                dialogL.dismiss();
                Utility.printLog("on error for the login "+error);
                Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
            }
        });
    }


    /**
     * used for parsing the login response.
     * The value returned is as void.
     * <pre>
     *    Used for parsing the login response. and validating the response.
     * </pre>
     *
     * @param fb_emailId String
     * @param fbid String
     * @param fb_firstName String
     * @param fb_lastName String
     * @param fb_pic String
     * @param type String
     * @param gender String
     * @return void
     * @since 1.0
     */
    private void getUserLoginInfoForSocial(String result,String fb_emailId,String fbid,String fb_firstName,String fb_lastName,String fb_pic,String type,String gender)
    {
        if(!result.equals(""))
        {
            Gson gson = new Gson();
            LoginResponse response = gson.fromJson(result, LoginResponse.class);
            Utility.printLog("Login Response "+result+" "+fb_pic);
            if(response.getErrFlag().equals("0"))
            {
                VariableConstants.COUNTER=0;
                session.storeSessionToken(response.getToken());
                session.setIsLogin(true);
                session.setCardToken("");
                session.setLast4Digits("");
                session.setBookingReponseFromPubnub("");
                session.setPubnubResponseForNonBooking("");
                session.setPubnubResponse("");
                session.storeLoginId(fb_emailId);
                session.storeServerChannel(response.getServerChn());
                session.storeChannelName(response.getChn());
                session.setPresenceChn(response.getPresenseChn());
                session.setStripeKey(response.getStipeKey());
                session.setPublishKey(response.getPub());
                session.setSubscribeKey(response.getSub());
                session.setSid(response.getSid());
                session.setUserName(response.getName());
                session.setPaymentUrl(response.getPaymentUrl());
                session.setProfileImage(response.getProfilePic());
                session.setPhoneNumber(response.getEmail());

                /**
                 * store all configured data from backen in SharedPReference
                 */
                session.setPubnubIntervalForHome(Long.parseLong(response.getConfigData().getPubnubIntervalHome()));
                session.setEtaIntervalForTracking(Long.parseLong(response.getConfigData().getDistanceMatrixInterval()));

                /**
                 * to set the google matrix keys from backend and
                 * set them in shared preference
                 */
                List<String> googleMatrixKeys = new ArrayList<>();
                googleMatrixKeys.addAll(response.getConfigData().getGoogleKeysArray());
                String jsonText = gson.toJson(googleMatrixKeys);
                session.setGoogleMatrixData(jsonText);

                /**
                 * to set the server key in shared reference
                 * from 0th index from backend
                 */
                if(!response.getConfigData().getGoogleKeysArray().isEmpty())
                    session.setServerKey(response.getConfigData().getGoogleKeysArray().get(0));

                Intent intent=new Intent(SelectFacebookOrEmail.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
            }

            else if(response.getErrFlag().equals("1") && response.getErrNum().equals("8"))
            {
                Utility.printLog("fb pic in login "+fb_pic);
                Intent intent=new Intent(SelectFacebookOrEmail.this,SignupActivity.class);
                intent.putExtra("EMAIL",fb_emailId);
                intent.putExtra("PASSWORD",fbid);
                intent.putExtra("FIRST_NAME",fb_firstName);
                intent.putExtra("LAST_NAME",fb_lastName);
                intent.putExtra("USER_PIC",fb_pic) ;
                intent.putExtra("TYPE",type) ;
                intent.putExtra("FROM_ACTIVITY",selected_type);
                intent.putExtra("GENDER",gender);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }

            else
            {
                Utility.showToast(getApplicationContext(),response.getErrMsg());
            }
        }
        else
        {
            Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
