package com.roadyo.passenger.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.roadyo.passenger.main.HistoryFragment;
import com.roadyo.passenger.main.MainActivity;
import com.roadyo.passenger.main.SplashActivity;
import com.roadyo.passenger.main.TripHistoryInfo;
import com.roadyo.passenger.main.UpdateLaterBookingActivity;
import com.roadyo.passenger.pojo.AddCardResponse;
import com.roadyo.passenger.pojo.AppointmentList;
import com.roadyo.passenger.pojo.Cancel_Pojo;
import com.roadyo.passenger.pojo.LiveBookingResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import com.bloomingdalelimousine.ridein.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * @author  Akbar
 *
 */
public class UpcomingTrips_Adapter extends RecyclerView.Adapter
{
    private Context mcontext;
    private ArrayList<AppointmentList> nubType = new ArrayList<>();
    private String currencysymbol;
    private Dialog cancelDialog;
    private MainActivity activity;
    private SessionManager session;
//    private RelativeLayout container;
//    private ImageView expanded_image;
//    private ProgressBar imageProgressBar;

    /**
     * Hold a reference to the current animator, so that it can be canceled mid-way.
     */
    private Animator mCurrentAnimator;

    /**
     * The system "short" animation time duration, in milliseconds. This duration is ideal for
     * subtle animations or animations that occur very frequently.
     */
    private int mShortAnimationDuration;
    private RecyclerView reasons_list;
    private Cancel_Adapter cancel_adapter;
    private ProgressDialog dialogL;
    private String getCancelResponse;


    public UpcomingTrips_Adapter(Context mcontext, ArrayList<AppointmentList> nubType)
    {
        this.mcontext = mcontext;
        this.nubType = nubType;
        this.activity = (MainActivity) mcontext;
//        this.container = container;
//        this.expanded_image = expanded_image;
//        this.imageProgressBar = imageProgressBar;
        currencysymbol = mcontext.getResources().getString(R.string.currencuSymbol);
        session=new SessionManager(mcontext);

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = mcontext.getResources().getInteger(android.R.integer.config_shortAnimTime);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.single_view_for_upcomming_trips,parent,false);
        return new ViewHldr(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof  ViewHldr)
        {
            final ViewHldr hldr = (ViewHldr) holder;
            Utility.printLog("getting apnt date "+nubType.get(position).getApntDt()+ " date "+nubType.get(position).getApntDate());
            hldr.tvvehicalname.setText(nubType.get(position).getFname());
            hldr.tvbookedtiming.setText(nubType.get(position).getApntDate()+" at "+convertToTime(nubType.get(position).getApntDt()));
            if(!nubType.get(position).getInvoice().getAmount().equals(""))
            {
                hldr.rateamount.setText(currencysymbol+" "+nubType.get(position).getInvoice().getAmount());
            }
            else
            {
                hldr.rateamount.setVisibility(View.GONE);
            }
            hldr.car_type_name_tv.setText(nubType.get(position).getVehicleType());
            hldr.status.setText(nubType.get(position).getStatus());
            hldr.pick_address.setText(nubType.get(position).getAddrLine1());
            hldr.pick_address.setSelected(true);
            hldr.drop_address.setSelected(true);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) mcontext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            if(!nubType.get(position).getDropLine1().equals(""))
                hldr.drop_address.setText(nubType.get(position).getDropLine1());
            else
                hldr.drop_layout.setVisibility(View.GONE);

            final String map_url = nubType.get(position).getRouteImg().trim()+"&size=100x100";
            final String map_url_large = nubType.get(position).getRouteImg().trim()+"&size=800x800";

            Utility.printLog("imageResId "+map_url);

//            map_url=map_url+"&size=200x200";

            Utility.printLog("imageResId "+map_url_large);

            Picasso.with(mcontext).load(map_url)
                    .into((ImageView) hldr.map_view_iv);

//            hldr.map_view_iv.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    zoomImageFromThumb(hldr.map_view_iv, map_url_large,container,expanded_image);
//                }
//            });

            hldr.edit_booking_details_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    sendMailOfBookingDetails(nubType.get(position).getBid());
//                    Toast.makeText(mcontext, "Edit clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mcontext, UpdateLaterBookingActivity.class);
                    intent.putExtra("LATER_TRIP_DETAIL_INFO", nubType.get(position));
                    ((MainActivity)mcontext).startActivity(intent);
                }
            });

            hldr.cancel_booking_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(mcontext, "Cancel clicked", Toast.LENGTH_SHORT).show();
                    cancelCheck();
                }
            });

            String url = nubType.get(position).getpPic();

//            if(!url.equals(""))
//            {
//                if(!url.contains(".gif"))
//                {
//                    Picasso.with(mcontext)
//                            .load(url)
//                            .transform(new CircleTransform())
//                            .into(hldr.imahisdriver);
//                }
//            }

            hldr.booking_id.setText(mcontext.getResources().getString(R.string.booking_id)+" "+nubType.get(position).getBid());

            if(session.getLanguageCode().equals("en"))
            {
                Utility.setTypefaceMuliBold(mcontext,hldr.rateamount);
                Utility.setTypefaceMuliRegular(mcontext, hldr.tvbookedtiming);
                Utility.setTypefaceMuliRegular(mcontext, hldr.pick_address);
                Utility.setTypefaceMuliRegular(mcontext, hldr.drop_address);
                Utility.setTypefaceMuliRegular(mcontext, hldr.tvvehicalname);
                Utility.setTypefaceMuliRegular(mcontext, hldr.status);
                Utility.setTypefaceMuliRegular(mcontext, hldr.booking_id);
            }
        }
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                Utility.printLog("state code in adapter "+nubType.get(position).getInvoice().getStateCode());
//                if(nubType.get(position).getInvoice().getStateCode().equals("4") || nubType.get(position).getInvoice().getStateCode().equals("5"))
//                {
//                    cancelDialog = new Dialog(mcontext);
//                    cancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    cancelDialog.setContentView(R.layout.cancelled_popup);
//                    cancelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                    cancelDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//                    cancelDialog.show();
//
//                    TextView apnt_time= (TextView) cancelDialog.findViewById(R.id.apnt_time);
//                    TextView drivername= (TextView) cancelDialog.findViewById(R.id.drivername);
//                    TextView back_button= (TextView) cancelDialog.findViewById(R.id.back_button);
//                    TextView cancelled_at= (TextView) cancelDialog.findViewById(R.id.cancelled_at);
//                    TextView cancel_status= (TextView) cancelDialog.findViewById(R.id.cancel_status);
//                    TextView bookingId= (TextView) cancelDialog.findViewById(R.id.bookingId);
//                    ImageView ivdriver= (ImageView) cancelDialog.findViewById(R.id.ivdriver);
//
//                    if(session.getLanguageCode().equals("en"))
//                    {
//                        Utility.setTypefaceMuliRegular(mcontext, apnt_time);
//                        Utility.setTypefaceMuliRegular(mcontext, drivername);
//                        Utility.setTypefaceMuliRegular(mcontext, cancelled_at);
//                        Utility.setTypefaceMuliRegular(mcontext, bookingId);
//                        Utility.setTypefaceMuliBold(mcontext, cancel_status);
//                    }
//
//                    if(nubType.get(position).getpPic()!=null)
//                    {
//                        Picasso.with(mcontext).load(nubType.get(position).getpPic())
//                                .transform(new CircleTransform())
//                                .resize(mcontext.getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumWidth(),
//                                        mcontext.getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumHeight())
//                                .into(ivdriver);
//                    }
//
//                    apnt_time.setText(convertToRequiredFormat(nubType.get(position).getApntDt())+" at "+convertToTime(nubType.get(position).getApntDt()));
//                    drivername.setText(nubType.get(position).getFname());
//                    bookingId.setText(mcontext.getResources().getString(R.string.booking_id)+" "+nubType.get(position).getBid());
//                    Utility.printLog("cancelled time "+nubType.get(position).getInvoice().getCancelTimeApplied()+" ");
//
//                    if(!nubType.get(position).getInvoice().getCancelTimeApplied().equals("0"))
//                    {                    cancel_status.setText(nubType.get(position).getInvoice().getAmount()+" "+mcontext.getResources().getString(R.string.currencuSymbol)
//                            +" "+mcontext.getResources().getString(R.string.cancel_reason)+" "+nubType.get(position).getInvoice().getCancelTime()+" "+mcontext.getResources().getString(R.string.minutes));
//                    }
//                    else
//                    {
//                        cancel_status.setText(mcontext.getResources().getString(R.string.no_payment_due));
//                    }
//                    cancelled_at.setText(mcontext.getResources().getString(R.string.cancelled_at)+" "+convertToRequiredFormat(nubType.get(position).getInvoice().getCancel_dt())+" | "+convertToTime(nubType.get(position).getInvoice().getCancel_dt()));
//                    back_button.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            cancelDialog.dismiss();
//                        }
//                    });
//
//                }
//                else
//                {
//                    Intent intent = new Intent(mcontext, TripHistoryInfo.class);
//                    intent.putExtra("EMAIL",nubType.get(position).getEmail());
//                    intent.putExtra("APPTDATE",nubType.get(position).getBid());
//                    intent.putExtra("DRIVER_IMAGE",nubType.get(position).getpPic());
//                    intent.putExtra("TOTAL_AMOUNT",nubType.get(position).getInvoice().getAmount());
//                    intent.putExtra("RATING",nubType.get(position).getR());
//                    intent.putExtra("BASE_FARE",nubType.get(position).getInvoice().getBaseFare());
//                    intent.putExtra("AIRPORT_FEE",nubType.get(position).getInvoice().getAirportFee());
//                    intent.putExtra("DISCOUNT",nubType.get(position).getInvoice().getDiscountVal());
//                    intent.putExtra("DISTANCE_FEE",nubType.get(position).getInvoice().getTripDistanceFee());
//                    intent.putExtra("TIME_FEE",nubType.get(position).getInvoice().getTripTimeFee());
//                    intent.putExtra("DISTANCE",nubType.get(position).getInvoice().getTripDistance());
//                    intent.putExtra("TIME",nubType.get(position).getInvoice().getTripTime());
//                    intent.putExtra("CODE",nubType.get(position).getInvoice().getCode());
//                    intent.putExtra("FIRST_NAME",nubType.get(position).getFname());
//                    intent.putExtra("PICK_ADD",nubType.get(position).getAddrLine1());
//                    intent.putExtra("DROP_ADDR",nubType.get(position).getDropLine1());
//                    intent.putExtra("BOOKING_ID",nubType.get(position).getBid());
//                    intent.putExtra("CASH_COLLECTED",nubType.get(position).getInvoice().getCashCollected());
//                    intent.putExtra("PAY_TYPE",nubType.get(position).getInvoice().getPayType());
//                    intent.putExtra("WALLET_AMT",nubType.get(position).getInvoice().getWalletDeducted());
//                    intent.putExtra("ADDED_TO_WALLET",nubType.get(position).getInvoice().getWalletDebitCredit());
//                    intent.putExtra("WAITING_FEE",nubType.get(position).getInvoice().getWaitingFee());
//                    intent.putExtra("WAITING_TIME",nubType.get(position).getInvoice().getWaitingTime());
//                    intent.putExtra("PREVIOUS_DUE",nubType.get(position).getInvoice().getSumOfLastDue());
//                    intent.putExtra("DUE_PAYMENT",nubType.get(position).getInvoice().getDuePayment());
//                    intent.putExtra("LAST_DIGITS",nubType.get(position).getInvoice().getCardlastDigit());
//                    intent.putExtra("SUBTOTAL",nubType.get(position).getInvoice().getSubtotal());
//                    intent.putExtra("CARD_DEDUCT",nubType.get(position).getInvoice().getCardDeduct());
//                    intent.putExtra("TITLE", convertToRequiredFormat(nubType.get(position).getApntDt())+" at "+convertToTime(nubType.get(position).getApntDt()));
//                    Utility.printLog("drop date in frag "+nubType.get(position).getInvoice().getCashCollected());
//                    intent.putExtra("COMPLETE_TIME", convertToRequiredFormat(nubType.get(position).getDrop_dt())+" | "+convertToTime(nubType.get(position).getDrop_dt()));
//                    mcontext.startActivity(intent);
//                }
//            }
//        });
    }

    @Override
    public long getItemId(int position) {
        return nubType.size();
    }

    @Override
    public int getItemCount() {
        return nubType.size();
    }

    private class ViewHldr extends RecyclerView.ViewHolder {
        ImageView imahisdriver,car_type_image_iv;
        ImageView map_view_iv;
        TextView tvbookedtiming,tvvehicalname,rateamount,status,pick_address,drop_address,booking_id,car_type_name_tv;
        LinearLayout drop_layout;
        Button edit_booking_details_btn,cancel_booking_btn;
        ViewHldr(View itemView) {
            super(itemView);
            imahisdriver = (ImageView) itemView.findViewById(R.id.imahisdriver);
            car_type_image_iv = (ImageView) itemView.findViewById(R.id.car_type_image_iv);
            map_view_iv = (ImageView) itemView.findViewById(R.id.map_view_iv);
            tvbookedtiming = (TextView) itemView.findViewById(R.id.tvbookedtiming);
            tvvehicalname = (TextView) itemView.findViewById(R.id.tvvehicalname);
            rateamount = (TextView) itemView.findViewById(R.id.rateamount);
            status = (TextView) itemView.findViewById(R.id.status);
            pick_address = (TextView) itemView.findViewById(R.id.pick_address);
            drop_address = (TextView) itemView.findViewById(R.id.drop_address);
            drop_layout = (LinearLayout) itemView.findViewById(R.id.drop_layout);
            booking_id = (TextView) itemView.findViewById(R.id.booking_id);
            car_type_name_tv = (TextView) itemView.findViewById(R.id.car_type_name_tv);
            edit_booking_details_btn = (Button) itemView.findViewById(R.id.edit_booking_details_btn);
            cancel_booking_btn = (Button) itemView.findViewById(R.id.cancel_booking_btn);
        }
    }

    private String convertToTime(String timeString)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);   //2016-05-26 12:05:49
        DateFormat targetFormat = new SimpleDateFormat("hh:mm:ss a", Locale.US);
        String formattedDate = null;
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(timeString);
            formattedDate = targetFormat.format(convertedDate);
        }
        catch (java.text.ParseException e)
        {
            e.printStackTrace();
        }
        return formattedDate;
    }

    private String convertToRequiredFormat(String timeString)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);   //2016-05-26 12:05:49
        DateFormat targetFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
        String formattedDate = null;
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(timeString);
            formattedDate = targetFormat.format(convertedDate);
        }
        catch (java.text.ParseException e)
        {
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * "Zooms" in a thumbnail view by assigning the high resolution image to a hidden "zoomed-in"
     * image view and animating its bounds to fit the entire activity content area. More
     * specifically:
     *
     * <ol>
     *   <li>Assign the high-res image to the hidden "zoomed-in" (expanded) image view.</li>
     *   <li>Calculate the starting and ending bounds for the expanded view.</li>
     *   <li>Animate each of four positioning/sizing properties (X, Y, SCALE_X, SCALE_Y)
     *       simultaneously, from the starting bounds to the ending bounds.</li>
     *   <li>Zoom back out by running the reverse animation on click.</li>
     * </ol>
     *
     * @param thumbView  The thumbnail view to zoom in.
     * @param imageResId The high-resolution version of the image represented by the thumbnail.
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void zoomImageFromThumb(final View thumbView, String imageResId, RelativeLayout container, final ImageView expanded_image) {
        // If there's an animation in progress, cancel it immediately and proceed with this one.
        if (mCurrentAnimator != null) {
            mCurrentAnimator.cancel();
        }

        Utility.printLog("imageResId "+imageResId);

        // Load the high-resolution "zoomed-in" image.
//        final ImageView expandedImageView = (ImageView) findViewById(R.id.expanded_image);
        final ImageView expandedImageView = expanded_image;
//        expandedImageView.setImageResource(imageResId);
//        Picasso.with(mcontext).load(imageResId).into(expanded_image);
        expanded_image.setImageDrawable(null);
        Picasso.with(mcontext).load(imageResId).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                imageProgressBar.setVisibility(View.GONE);
                expanded_image.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
//                imageProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                imageProgressBar.setVisibility(View.VISIBLE);
            }
        });

        // Calculate the starting and ending bounds for the zoomed-in image. This step
        // involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail, and the
        // final bounds are the global visible rectangle of the container view. Also
        // set the container view's offset as the origin for the bounds, since that's
        // the origin for the positioning animation properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
//        findViewById(R.id.container).getGlobalVisibleRect(finalBounds, globalOffset);
        container.getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, - globalOffset.y);
        finalBounds.offset(-globalOffset.x, - globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final bounds using the
        // "center crop" technique. This prevents undesirable stretching during the animation.
        // Also calculate the start scaling factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation begins,
        // it will position the zoomed-in view in the place of the thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations to the top-left corner of
        // the zoomed-in view (the default is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and scale properties
        // (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left,
                        finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top,
                        finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f));
        set.setDuration(mShortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCurrentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                mCurrentAnimator = null;
            }
        });
        set.start();
        mCurrentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down to the original bounds
        // and show the thumbnail instead of the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentAnimator != null) {
                    mCurrentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel, back to their
                // original values.
                AnimatorSet set = new AnimatorSet();
                set
                        .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView, View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView, View.SCALE_Y, startScaleFinal));
                set.setDuration(mShortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        mCurrentAnimator = null;
                    }
                });
                set.start();
                mCurrentAnimator = set;
            }
        });
    }


//    BookingHistorytoEmail
//    parameters -> ent_apntId, ent_sess_token, ent_dev_id, ent_user_type

    private void sendMailOfBookingDetails(String bid)
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_user_type", "2");
            jsonObject.put("ent_apntId", bid);
            Utility.printLog("params to BookingHistorytoEmail "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(mcontext.getString(R.string.BASE_URL) + "BookingHistorytoEmail", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result) {
                fetchData(result);
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void fetchData(String result)
    {
        HistoryFragment.progress_bar.setVisibility(View.GONE);

        Utility.printLog("BookingHistorytoEmail respose  "+result);
        Gson gson = new Gson();
        AddCardResponse response = gson.fromJson(result, AddCardResponse.class);
        if(response !=null)
        {
//            lastCount=response.getLastcount();
//            String errFlag =
            if(response.getErrFlag().equals("0"))
            {
                showAlert(mcontext,mcontext.getResources().getString(R.string.mail_is_sent));
            }
            else
            {
                Toast.makeText(mcontext, ""+response.getErrMsg(), Toast.LENGTH_SHORT).show();
            }
            if(response.getErrNum().equals("6") || response.getErrNum().equals("7") ||
                    response.getErrNum().equals("94") || response.getErrNum().equals("96"))
            {
                Utility.showToast(mcontext,response.getErrMsg());
                Intent i = new Intent(mcontext, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mcontext.startActivity(i);
                ((MainActivity)mcontext).overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }
        }
        else
//        if(isAdded())
        {
            Utility.showToast(mcontext,mcontext.getResources().getString(R.string.network_connection_fail));
        }
    }

    public void showAlert(Context context,String msg)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context,5);
        // set title
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.note));
        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton(context.getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {
                        //closing the application
                        dialog.dismiss();
//                        if(flag.equals("0")){
//                            finish();
//                            overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
//                        }
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }



    private void cancelCheck()
    {
        final Dialog mDialog =Utility.GetProcessDialog(activity);
        mDialog.show();
        final JSONObject[] jsonObject = {new JSONObject()};
        try
        {
            jsonObject[0].put("ent_booking_id", session.getBookingId());
            jsonObject[0].put("ent_sess_token",session.getSessionToken());
            jsonObject[0].put("ent_dev_id",session.getDeviceId());
            Utility.printLog("params to cancel check  "+ jsonObject[0]);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(mcontext.getString(R.string.BASE_URL)+"cancelAppointmentCheck", jsonObject[0], new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                mDialog.dismiss();
                Utility.printLog("result for cancel check details  "+result);
                String errMsg = null;
                if(result!=null) {
                    try {
                        jsonObject[0] =new JSONObject(result);
                        String errFlag=jsonObject[0].getString("errFlag");
                        errMsg=jsonObject[0].getString("errMsg");
                        String errNo=jsonObject[0].getString("errNo");
                        if(errFlag.equals("0"))
                        {
                            String cancelFlag=jsonObject[0].getString("flag");
                            cancellationPopup(cancelFlag,errMsg);
                        }
                        else
                        {
                            Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
                        }
                        if(errNo.equals("6") || errNo.equals("7") ||
                                errNo.equals("94") || errNo.equals("96"))
                        {
                            Utility.showToast(activity,errMsg);
                            Intent i = new Intent(activity, SplashActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            activity.startActivity(i);
                            activity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void cancellationPopup(final String errFlag, String errMsg)
    {
        final Dialog popupWithTwoButtons=Utility.showPopupWithTwoButtons(activity);
        TextView dialogText= (TextView) popupWithTwoButtons.findViewById(R.id.text_for_popup);
        TextView yes_button= (TextView) popupWithTwoButtons.findViewById(R.id.yes_button);
        TextView no_button= (TextView) popupWithTwoButtons.findViewById(R.id.no_button);

        dialogText.setText(errMsg);
        no_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
            }
        });
        yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
                if(errFlag.equals("0"))
                {
                    if(Utility.isNetworkAvailable(activity))
                    {
                        cancelAppointment();
                    }
                    else
                    {
                        Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
                    }
                }
                else
                {
                    if(Utility.isNetworkAvailable(activity))
                    {
                        showCancellationReasonDialog();
                    }
                    else
                    {
                        Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
                    }
                }
            }
        });
        popupWithTwoButtons.show();
    }

    private void cancelAppointment()
    {
        dialogL= Utility.GetProcessDialogNew(activity, activity.getResources().getString(R.string.cancelling_trip));
        dialogL.setCancelable(false);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_booking_id", session.getBookingId());
            jsonObject.put("ent_mid", session.getMid());
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_reason", "");
            Utility.printLog("params to cancel "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(mcontext.getString(R.string.BASE_URL)+"cancelAppointment", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String response)
            {
                getCancelResponse = response;
                Utility.printLog("response for the cancel "+response);
                getCancelInfo();
            }

            @Override
            public void onError(String error) {
                if (dialogL != null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void getCancelInfo()
    {
        Gson gson = new Gson();
        LiveBookingResponse cancelBooking = gson.fromJson(getCancelResponse, LiveBookingResponse.class);
        if(cancelBooking !=null)
        {
            if(cancelBooking.getErrFlag().equals("0"))
            {
                session.setBookingReponseFromPubnub("");
                session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                session.setDriverOnWay(false);
                session.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);
                session.storeAptDate(null);
                Utility.showToast(activity,cancelBooking.getErrMsg());
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(activity, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }

            else if(cancelBooking.getErrFlag().equals("1"))
            {
                session.setBookingReponseFromPubnub("");
                session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                session.setDriverOnWay(false);
                session.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);
                session.storeAptDate(null);
                Utility.ShowAlert(cancelBooking.getErrMsg(),activity);
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(activity, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }
        }
    }

    private void showCancellationReasonDialog()
    {
        Dialog cancellationReason = new Dialog(activity);
        cancellationReason.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancellationReason.setContentView(R.layout.cancel_reasons_layout);
        cancellationReason.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        cancellationReason.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        reasons_list = (RecyclerView) cancellationReason.findViewById(R.id.reasons_list);

        Button submitForCancellationReason = (Button)cancellationReason. findViewById(R.id.submit);
        TextView title_for_cancel = (TextView)cancellationReason. findViewById(R.id.title_for_cancel);
        ProgressBar progress_bar = (ProgressBar)cancellationReason. findViewById(R.id.progress_bar);

        /**
         * to set typeface
         */
        if(session.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(activity,title_for_cancel);
            Utility.setTypefaceMuliBold(activity,submitForCancellationReason);
        }
        if(Utility.isNetworkAvailable(activity))
        {
            gettingReasons(progress_bar);
        }
        else
        {
            Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        reasons_list.setLayoutManager(mLayoutManager);
        reasons_list.setItemAnimator(new DefaultItemAnimator());

        submitForCancellationReason.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                cancel_adapter.submitOnclick();
            }
        });
//        if(isAdded())
            cancellationReason.show();
    }

    private void gettingReasons(final ProgressBar progress_bar)
    {
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_lan","0");
            jsonObject.put("ent_user_type", "2");
            Utility.printLog("params to login "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(mcontext.getString(R.string.BASE_URL)+"getCancellationReson", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                progress_bar.setVisibility(View.GONE);
                reasons_list.setVisibility(View.VISIBLE);
                Utility.printLog("Login Response getting reasons "+result);
                if(result!=null)
                {
                    Gson gson=new Gson();
                    Cancel_Pojo cancel_pojo=gson.fromJson(result,Cancel_Pojo.class);
                    if(cancel_pojo.getErrFlag().equals("0"))
                    {
                        ArrayList<String> cancel_reasonsList=new ArrayList<>();
                        cancel_reasonsList.addAll(cancel_pojo.getGetdata());
                        cancel_adapter=new Cancel_Adapter(activity,cancel_pojo.getGetdata());
                        reasons_list.setAdapter(cancel_adapter);
                    }
                    if(cancel_pojo.getErrNum().equals("6") || cancel_pojo.getErrNum().equals("7") ||
                            cancel_pojo.getErrNum().equals("94") || cancel_pojo.getErrNum().equals("96"))
                    {
                        Utility.showToast(activity,cancel_pojo.getErrMsg());
                        Intent i = new Intent(activity, SplashActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(i);
                        activity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                    }
                }
                else
                {
                    Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.showToast(activity,activity.getResources().getString(R.string.network_connection_fail));
            }
        });
    }

}
