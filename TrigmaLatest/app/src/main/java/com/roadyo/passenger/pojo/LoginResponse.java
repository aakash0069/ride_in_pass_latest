package com.roadyo.passenger.pojo;

import com.roadyo.passenger.pubnu.pojo.ConfigurationData;

import java.util.ArrayList;

public class LoginResponse 
{
	String errNum;
	String errFlag;
	String errMsg;
	private String token;
	private String chn,serverChn;
	private String email;
	private String presenseChn;
	private String pub;
	private String sid;
	private String stipeKey;
	private String sub;
	private ArrayList<FavAddress> addresses;
	public String getName() {
		return name;
	}
	private String name;
	public String getProfilePic() {
		return profilePic;
	}
	private String profilePic;
	private ConfigurationData configData;

	public ConfigurationData getConfigData() {
		return configData;
	}

	public ArrayList<FavAddress> getAddresses() {
		return addresses;
	}

	public String getPaymentUrl() {
		return PaymentUrl;
	}
	private String PaymentUrl;
	public String getSid() {
		return sid;
	}
	public String getStipeKey() {
		return stipeKey;
	}
	public String getSub() {
		return sub;
	}
	public String getPub() {
		return pub;
	}
	public String getPresenseChn() {
		return presenseChn;
	}
	private ArrayList<CarsDetailList> types;
	public ArrayList<CarsDetailList> getCarsdetails()
	{
		return types;
	}
	public String getChn() {
		return chn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getErrNum() {
		return errNum;
	}
	public void setErrNum(String errNum) {
		this.errNum = errNum;
	}
	public String getErrFlag() {
		return errFlag;
	}
	public void setErrFlag(String errFlag) {
		this.errFlag = errFlag;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getToken() {
		return token;
	}
	public String getServerChn() {
		return serverChn;
	}
}
