package com.roadyo.passenger.pojo;

import java.io.Serializable;

public class Pickup_latlong implements Serializable
{
    private String pickup_long;

    private String pickup_lat;

    public String getPickup_long ()
    {
        return pickup_long;
    }

    public void setPickup_long (String pickup_long)
    {
        this.pickup_long = pickup_long;
    }

    public String getPickup_lat ()
    {
        return pickup_lat;
    }

    public void setPickup_lat (String pickup_lat)
    {
        this.pickup_lat = pickup_lat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pickup_long = "+pickup_long+", pickup_lat = "+pickup_lat+"]";
    }
}
