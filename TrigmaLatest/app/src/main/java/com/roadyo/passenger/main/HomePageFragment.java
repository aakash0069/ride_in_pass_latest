package com.roadyo.passenger.main;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.example.twiliocallmodule.ClientActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.FareCalData;
import com.roadyo.passenger.pojo.GetCarDetails;
import com.roadyo.passenger.pojo.NegativeFeedBackOfDriver;
import com.bloomingdalelimousine.ridein.R;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.roadyo.passenger.adapter.Cancel_Adapter;
import com.roadyo.passenger.pojo.Cancel_Pojo;
import com.roadyo.passenger.pojo.EtaPojo;
import com.roadyo.passenger.pojo.FareCalculation;
import com.roadyo.passenger.pojo.GeocodingResponse;
import com.roadyo.passenger.pojo.GetAppointmentDetails;
import com.roadyo.passenger.pojo.GetCardResponse;
import com.roadyo.passenger.pojo.InvoiceResponse;
import com.roadyo.passenger.pojo.LiveBookingResponse;
import com.roadyo.passenger.pubnu.pojo.PubnubResponseNew;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.threembed.utilities.BitmapCustomMarker;
import com.threembed.utilities.CircleTransform;
import com.threembed.utilities.DB_Fav_Locations;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.HttpConnection;
import com.threembed.utilities.LocationUtil;
import com.threembed.utilities.OkHttpRequest;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.PathJSONParser;
import com.threembed.utilities.PicassoMarker;
import com.threembed.utilities.Scaler;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.view.View.VISIBLE;
import static com.threembed.utilities.Utility.setLanguageForDialog;

public class HomePageFragment extends Fragment implements OnClickListener,LocationUtil.GetLocationListener,OnMapReadyCallback,
        GoogleMap.OnCameraMoveStartedListener,GoogleMap.OnCameraIdleListener, DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener
{
    public  GoogleMap googleMap;
    public  float zoom_level =17.0f;
    public  float zoom_level_booking =15.0f;
    private double currentLatitude,currentLongitude;
    private ImageView Driver_Profile_Pic;
    private TextView current_address,Driver_on_the_way_txt;
    private TextView Driver_Name;
    private TextView Driver_Car_Num;
    ImageView currenct_location_button;
    private TextView Dropoff_Location_Address,pickup_Distance,in_booking_cash_or_card_tv,tv_homepage_fareestimate;
    @SuppressLint("StaticFieldLeak")
    private static View view;
    private Timer myTimer;
    LocationUtil networkUtil ;
    private RelativeLayout cash_card_option_layout_rl;
    private RelativeLayout outer_map_layout;
    private RelativeLayout pickup;
    private RelativeLayout Rl_distance_time;
    private RelativeLayout all_types_layout;
    @SuppressLint("StaticFieldLeak")
    public static Activity mActivity;
    private boolean isFareQuotePressed=false;
    private String from_latitude,from_longitude,to_latitude,to_longitude,mDROPOFF_ADDRESS,mPICKUP_ADDRESS;
    private String Car_Type_Id=null;
    private HashMap<String,Marker> marker_map;
    private IntentFilter filter;
    private BroadcastReceiver receiver;
    private View popupLayout;
    private PopupWindow popup_share;
    private ProgressDialog dialogL;
    private SessionManager session;
    private String payment_type;
    private Timer pollingTimer = new Timer();
    private Timer myTimer_publish;
    private static boolean visibility=false;
    private ProgressBar pubnubProgressDialog;
    private Geocoder geocoder;
    private boolean IsreturnFromSearch=false;
    private String surgePrice="";
    private boolean firstTimeMarkersPlotting = true;
    private String laterBookingDate=null;
    private Button cancel_trip;
    private double[] dblArray;
    private TextView rate_unit;
    private Dialog mDialog;
    //    HorizontalScrollView scrollView;
    ArrayList<String> carTypeImagesForSelectedType= new ArrayList<>();
    int typeSize=0;
    ArrayList<String> unselectedImages=new ArrayList<>();
    ArrayList<String> mapImages=new ArrayList<>();
    ArrayList<String> carName=new ArrayList<>();
    private String current_master_type_id = "-1";
    public static ArrayList<Boolean> markerPlotingFirstTime = new ArrayList<>();
    private ArrayList<String> new_channels_to_subscribe = new ArrayList<>();
    //    private  LinearLayout allTypeLinearLayout;
    HorizontalScrollView horizontal_scrollView;
    private PubnubResponseNew filterResponse = new PubnubResponseNew();
    static int isTypeTapped=1;
    HashMap<String, String> markerId_Email_map;
    double lat=0;
    double longi=0;
    TextView drivvehicalrating;
    public static int displayWidth;
    //    private  ImageView previousArrow,nextArrow;
    String imageUrlToSendToSurge;
    Location mCurrentLoc,mPreviousLoc;
    private TextView Fare_Quote;
    private String reviewString;
    private PicassoMarker driverMarker;
    Dialog invoiceDialogue;
    boolean flagForDialog=false;
    private TextView drivsnd;
    private JSONObject jsonObject;
    private SubscribeCallback subscribeCallback;
    private int clickedPos=0;
    private  GetAppointmentDetails getAppointmentDetails;
    int height;
    int width,publishingTag=1;  //created  publishingTag to publish server channel
    private  double currentLocation[];
    private float ratings;
    //    private  ImageView currenct_satellite_button;
    private  TextView Appointment_location;

    private String databaseId;
    private DatabasePickupHandler dbPickup;
    private String getCancelResponse;
    List<DB_Fav_Locations>  dbFavLocations=new ArrayList<>();
    private  TextView appointment_location_drop;
    private GetCardResponse card_response;
    private String card_token="";
    private int colourForPathPlot;
    private boolean isContactDriver=false;
    private Polyline polylineFinal;
    private  PubNub pubnub;
    private boolean isMapMoved=false;
    private  String etaThreshold;
    private RelativeLayout rl_address,rl_action_bar_main;
    private MainActivity activity;
    private String allowNegativeValueUpto="";
    private RecyclerView reasons_list;
    private RecyclerView reasons_list_for_review;
    private Cancel_Adapter cancel_adapter;
    private ImageView shadow_icon;
    private String eta_latitude="",eta_longitude="";
    private View view3,view2;
    private LinearLayout promo_code_layout;
    private  RelativeLayout  show_address_relative;
    private double driverCarWidth,driverCarHeight;
    private PNConfiguration pnConfiguration;
    private String etaUrl="";
    private TextView current_location;
    private boolean toCheckAnimation=false;
    private LinearLayout fare_quote_layout;
    private int durationOnTheWay;
    private View view_for_image;
    private int previousView=0,totalCount=0;
    private ArrayList<ImageView> rightArrowImages;
    private Dialog mandatoryFieldPopup;
    private RelativeLayout relative_confirmation_navigation;

    private RelativeLayout rl_confirmation_Screen;
    private RelativeLayout rl_back_btn;
    private RelativeLayout drop_layout;
    private ImageButton btn_back;
    private Button now_button;
    private ImageView later_button;
    private String later_booking_date;
    private LinearLayout now_later_layout;
    private ImageView search_or_ring_iv;
    private LinearLayout rate_card_ll;
    private TextView car_type_tv;
    private LinearLayout choose_payment_type_ll;
    private View customView;
    private PopupWindow mPopupWindow;
    private LinearLayout cash_select_layout_ll;
    private LinearLayout card_select_layout_ll;
    private Button cash_card_option_cancel_btn;
    private TextView select_payment_type_tv;
    private ImageView select_payment_type_checkmark_iv;
    private Button request_pick_up_here;
    private ImageView mid_pointer;

    /**
     * to create the list of type ids to store the previous types ids
     * for deciding the inflation of car types
     */
    private ArrayList<String> previousTypeIdslist;
    /**
     * variable created to hide the map animmation
     * when open first time the homepage
     */
    private boolean checkForFirstTime;
    private ImageView pickup_location_search_iv;
    private ImageView dropoff_loation_search_iv;
    private String calculated_fare;
    private JSONArray all_car_types;
    private ArrayList<FareCalData> fare_for_all_cartypes;
    private LinearLayout driver_call_and_cancel_ll;
    private TextView driver_status_for_journey_tv;
    ArrayList<NegativeFeedBackOfDriver> negFbrsnList;
    private WhatWentWrong whatWentWrong;
    private Utility utility;

    private void methodAfterGettingResponse(String message) {
        Utility.printLog("message in pubnub "+message);
        Utility.printLog("ccar type id "+session.getCarTypeId());
        if (message != null)
        {
            String status;
            try
            {
                JSONObject jsonObject=new JSONObject(message);
                status=jsonObject.getString("a");
                switch (status) {
                    //user will get the a=4 when server sends the booking data when he sends the a=3
                    case "4":
                    {
                        session.setBookingReponseFromPubnub(message);
                        Gson gson = new Gson();
                        GetAppointmentDetails filterResponse = gson.fromJson(message, GetAppointmentDetails.class);
                        session.setCarTypeId(filterResponse.getData().getTypeId());
                        session.setMid(filterResponse.getData().getMid());
                        current_address.setText(filterResponse.getData().getAddr1());
                        session.storeBookingId(filterResponse.getData().getBid());
                        if(filterResponse.getData().getDropAddr1()!=null)
                            Dropoff_Location_Address.setText(filterResponse.getData().getDropAddr1());
                        session.storeShareLink(filterResponse.getData().getShare());
                        session.storeDocPH(filterResponse.getData().getMobile());
                        session.setSelectedImage(filterResponse.getData().getCarMapImage());
                        session.setCarImage(filterResponse.getData().getCarImage());
                        session.storeDocPic(filterResponse.getData().getpPic());
                        session.storePickuplat(filterResponse.getData().getPickLat());
                        session.storePickuplng(filterResponse.getData().getPickLong());
                        session.storeDocName(filterResponse.getData().getfName()+" "+filterResponse.getData().getlName());
                        currenct_location_button.setVisibility(View.GONE);

                        mid_pointer.setVisibility(View.GONE);
                        promo_code_layout.setVisibility(View.GONE);
                        rl_address.setVisibility(View.VISIBLE);
                        shadow_icon.setVisibility(View.GONE);
                        if(filterResponse.getData().getPaymentType().equals("1")){
                            in_booking_cash_or_card_tv.setText("$$  "+getString(R.string.card1));
                        } else if(filterResponse.getData().getPaymentType().equals("2")){
                            in_booking_cash_or_card_tv.setText("$$  "+getString(R.string.cash));
                        }
                        tv_homepage_fareestimate.setText(getString(R.string.currencuSymbol)+" "+filterResponse.getData().getFareEstimate());
                        pickup_location_search_iv.setVisibility(View.GONE);
                        dropoff_loation_search_iv.setVisibility(View.GONE);

                        try {
                            if(googleMap!=null)
                            {
                                googleMap.clear();
                                googleMap.setOnCameraMoveStartedListener(null);
                                googleMap.setOnCameraIdleListener(null);
                            }
                        }
                        catch (IllegalArgumentException e)
                        {
                            Utility.showToast(getActivity(),e.getMessage());
                        }

                        show_address_relative.setOnClickListener(null);
                        if(!session.getPickuplat().equals("") && !session.getPickuplng().equals(""))
                        {
                            LatLng latLng = new LatLng(Double.parseDouble(session.getPickuplat()), Double.parseDouble(session.getPickuplng()));
                            if(googleMap!=null)
                            {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                            }
                        }

                        switch (filterResponse.getData().getStatus())
                        {
                            //will receive the status = 6 id driver is on the way
                            case "6":
                            {
                                setHasOptionsMenu(false);
                                session.setDriverOnWay(true);
                                Utility.printLog("Wallah set as true Homepage 6");
                                session.setDriverArrived(false);
                                session.setTripBegin(false);
                                session.setInvoiceRaised(false);
                                colourForPathPlot=getResources().getColor(R.color.pin_colour);
                                if(!session.getBookingReponseFromPubnub().equals(""))
                                {
                                    //populatethe UI for on the way screen
                                    onTheWayUIUpdate();
                                }
                                //plot the driver car marker on map
                                try {
                                    if (!session.getSelectedImage().equals("") && googleMap!=null) {
                                        LatLng latLng = new LatLng(Double.parseDouble(filterResponse.getData().getDriverLat()),
                                                Double.parseDouble(filterResponse.getData().getDriverLong()));
                                        driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                        Picasso.with(getActivity()).load(session.getSelectedImage())
                                                .resize((int) driverCarWidth, (int) driverCarHeight)
                                                .into(driverMarker);
                                    }
                                } catch (Exception ignored) {
                                }
                                String timeUrl="https://maps.googleapis.com/maps/api/distancematrix/json?origins="
                                        +filterResponse.getData().getDriverLat()+","+filterResponse.getData().getDriverLong()+"&"+
                                        "destinations="+session.getPickuplat()+","
                                        +session.getPickuplng()+"&mode=driving"+"&"+"key="+session.getServerKey();
                               Utility.printLog("global eta "+etaUrl+" local eta "+timeUrl);
                                if(!etaUrl.equals(timeUrl))
                                {
                                    etaUrl=timeUrl;
                                    if(isAdded())
                                    {
                                        if(Utility.isNetworkAvailable(getActivity()))
                                        {
                                            getEta(timeUrl);
                                        }
                                        else
                                        {
                                            Utility.printLog("network_connection_fail 1");
                                            Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                                        }
                                    }
                                }
                                break;
                            }
                            //will receive the status = 7 id driver arrived
                            case "7":
                            {
                                Utility.printLog("INSIDE DRIVER REACHED: 7 message=" + message);
                                session.setDriverOnWay(false);
                                Utility.printLog("Wallah set as false Homepage 7");
                                session.setDriverArrived(true);
                                session.setTripBegin(false);
                                session.setInvoiceRaised(false);
                                Driver_on_the_way_txt.setText(getResources().getString(R.string.driverreached));
                                setHasOptionsMenu(false);
                                pickup.clearAnimation();
                                pickup_Distance.clearAnimation();
                                rate_unit.clearAnimation();
                                pickup_Distance.setVisibility(View.GONE);
                                rate_unit.setVisibility(View.GONE);
                                pickup.setVisibility(View.GONE);
                                if(!session.getBookingReponseFromPubnub().equals(""))
                                {
                                    //poplulate the UI for driver arrived screen
                                    onArrivedUIUpdate();
                                }
                                if (!session.getSelectedImage().equals("") && googleMap!=null) {
                                    LatLng latLng = new LatLng(Double.parseDouble(filterResponse.getData().getDriverLat()),
                                            Double.parseDouble(filterResponse.getData().getDriverLong()));
                                    driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                    Picasso.with(getActivity()).load( session.getSelectedImage())
                                            .resize((int) driverCarWidth, (int) driverCarHeight)
                                            .into(driverMarker);
                                    if(googleMap!=null)
                                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                                }
                                break;
                            }
                            //will receive the status = 8 id journey started
                            case "8":
                            {
                                Utility.printLog("INSIDE DRIVER REACHED: 8 message=" + message.toString());
                                session.setDriverOnWay(false);
                                session.setDriverArrived(false);
                                session.setTripBegin(true);
                                session.setInvoiceRaised(false);
                                Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
                                setHasOptionsMenu(false);
                                colourForPathPlot=getResources().getColor(R.color.background);
                                fare_quote_layout.setVisibility(View.GONE);

                                if(!session.getBookingReponseFromPubnub().equals(""))
                                {
                                    //populate the UI for journey start
                                    onJourneyStartedUIUpdate();
                                }

                                if (!session.getSelectedImage().equals("") && googleMap!=null) {
                                    LatLng latLng = new LatLng(Double.parseDouble(filterResponse.getData().getDriverLat()),
                                            Double.parseDouble(filterResponse.getData().getDriverLong()));
                                    driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                    Picasso.with(getActivity()).load( session.getSelectedImage())
                                            .resize((int) driverCarWidth, (int) driverCarHeight)
                                            .into(driverMarker);
                                    if(googleMap!=null)
                                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                                }
                                break;
                            }
                            //id booking is completed and the invoice is raised then open the invoice popup
                            case "9":
                            {
                                //start publishing a=1 again to get the home data
                                publishingTag=1;
                                Utility.printLog("onResume INSIDE DriverInvoiceRaised");
                                Gson gson1=new Gson();
                                InvoiceResponse invoiceResponse=gson1.fromJson(message,InvoiceResponse.class);
                                Utility.printLog("invoice data 0"+message);
                                Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
                                driver_call_and_cancel_ll.setVisibility(View.GONE);
                                driver_status_for_journey_tv.setVisibility(View.VISIBLE);
                                driver_status_for_journey_tv.setBackgroundColor(getResources().getColor(R.color.green));
                                showInvoiceDialog(invoiceResponse);
                                /**
                                 * to publish the user latlongs to server to get the data from server with 0 delay
                                 * if publishing is not yet started
                                 */
                                if(myTimer_publish==null)
                                    startPublishingWithTimer(0);
                                mid_pointer.setVisibility(View.VISIBLE);
                                pollingTimer.cancel();
                                break;
                            }
                        }
                        break;
                    }
                    //will get the a=5 from driver channel if driver cancels the booking
                    case "5":
                    {
                        String reason = "";
                        try {
                            JSONObject jsonObject1=new JSONObject(message);
                            reason=jsonObject1.getString("r");
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                        Utility.printLog("reason for cancellation "+reason);
                        //refresh the UI if driver cancels the booking
                        DriverCancelledAppointment(reason);
                        break;
                    }
                    //in booking user subscribes the driver channel and if driver send a=6 then drievr on the way
                    case "6":
                    {
                        if(myTimer_publish!=null)
                        {
                            myTimer_publish.cancel();
                            myTimer_publish=null;
                        }
                        if(myTimer!=null)
                        {
                            myTimer.cancel();
                            myTimer=null;
                        }
                        setHasOptionsMenu(false);
                        String latFromPub = "",longFromPub="";
                        session.setDriverOnWay(true);
                        Utility.printLog("Wallah set as true Homepage 6");
                        session.setDriverArrived(false);
                        session.setTripBegin(false);
                        session.setInvoiceRaised(false);
                        colourForPathPlot=getResources().getColor(R.color.pin_colour);
                        try {
                            JSONObject jsonObject1=new JSONObject(message);
                            latFromPub=jsonObject1.getString("lt");
                            longFromPub=jsonObject1.getString("lg");
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        if(!session.getBookingReponseFromPubnub().equals(""))
                        {
                            //populate the UI for on the way screen
                            onTheWayUIUpdate();
                        }

                        //plot the driver car marker on map
                        try {
                            if (!session.getSelectedImage().equals("")) {
                                LatLng latLng = new LatLng(Double.parseDouble(latFromPub), Double.parseDouble(longFromPub));
                                driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                Picasso.with(getActivity()).load(session.getSelectedImage())
                                        .resize((int) driverCarWidth, (int) driverCarHeight)
                                        .into(driverMarker);
                            }
                        } catch (Exception ignored) {
                        }
                        setHasOptionsMenu(false);
                        if (session.getLat_DOW() == null || session.getLon_DOW() == null) {
                            session.storeLat_DOW(latFromPub);
                            session.storeLon_DOW(longFromPub);
                        }

                        /**
                         * to plot the marker for pick location
                         */
                        Utility.printLog("latlong for pick "+session.getPickuplat()+" "+session.getPickuplng());

                        //to get the eta of the drier accepted booking and show on the marker plotted
                        Utility.printLog("lat long from driver "+ currentLatitude +" "+ currentLongitude+ " pub "+latFromPub+" "+longFromPub);
                        String timeUrl="https://maps.googleapis.com/maps/api/distancematrix/json?origins="+latFromPub+","+longFromPub+"&"+
                                "destinations="+session.getPickuplat()+","
                                +session.getPickuplng()+"&mode=driving"+"&"+"key="+session.getServerKey();
                        Utility.printLog("global eta "+etaUrl+" local eta "+timeUrl);
                        if(!etaUrl.equals(timeUrl))
                        {
                            eta_latitude = latFromPub;
                            eta_longitude = longFromPub;
                            etaUrl=timeUrl;

                            if(isAdded())
                            {
                                if(Utility.isNetworkAvailable(getActivity()))
                                {
                                    getEta(timeUrl);
                                }
                                else
                                {
                                    Utility.printLog("network_connection_fail 1");
                                    Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                                }
                            }
                            //to track the driver , and to move the car marker on map
                            UpdateDriverLocation_DriverOnTheWay(eta_latitude, eta_longitude);
                        }
                        // to plot the line for on theway
                        String url = getMapsApiDirectionsFromTourl(eta_latitude, eta_longitude);
                        Utility.printLog("getMapsApiDirectionsFromTourl ="
                                + url);
                        if(!url.equals(""))
                        {
                            ReadTask downloadTask = new ReadTask();
                            downloadTask.execute(url);
                        }
                        break;
                    }
                    //driver published a=7 id he has arrived at user location
                    case "7":
                    {
                        String latFromPub = null,longFromPub=null;
                        try {
                            JSONObject jsonObject1=new JSONObject(message.toString());
                            latFromPub=jsonObject1.getString("lt");
                            longFromPub=jsonObject1.getString("lg");
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                        //if driver arrived then remove the line plotted for tracking
                        if(polylineFinal!=null)
                        {
                            polylineFinal.remove();
                        }

                        Utility.printLog("INSIDE DRIVER REACHED: 7 message=" + message);
                        session.setDriverOnWay(false);
                        Utility.printLog("Wallah set as false Homepage 7");
                        session.setDriverArrived(true);
                        session.setTripBegin(false);
                        session.setInvoiceRaised(false);
                        session.storeLat_DOW(latFromPub);
                        session.storeLon_DOW(longFromPub);
                        Driver_on_the_way_txt.setText(getResources().getString(R.string.driverreached));
                        if(!session.getBookingReponseFromPubnub().equals(""))
                        {
                            //to update UI for driver arrived
                            onArrivedUIUpdate();
                        }
                        if (session.getLat_DOW() != null && session.getLon_DOW() != null) {
                            LatLng latLng = new LatLng(Double.parseDouble(session.getLat_DOW()), Double.parseDouble(session.getLon_DOW()));
                            //to plot the driver marker if he has arrived
                            if (!session.getSelectedImage().equals("")) {
                                driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                Picasso.with(getActivity()).load( session.getSelectedImage())
                                        .resize((int) driverCarWidth, (int) driverCarHeight)
                                        .into(driverMarker);
                            }
                            if(googleMap!=null)
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                        }
                        setHasOptionsMenu(false);
                        eta_latitude = latFromPub;
                        eta_longitude = longFromPub;
                        //to live track the driver marker
                        UpdatDriverLocation_DriverArrived(eta_latitude, eta_longitude);
                        pickup.clearAnimation();
                        pickup_Distance.clearAnimation();
                        rate_unit.clearAnimation();
                        pickup_Distance.setVisibility(View.GONE);
                        rate_unit.setVisibility(View.GONE);
                        pickup.setVisibility(View.GONE);
                        break;
                    }
                    //driver published a=8 id he started journey
                    case "8":
                    {
                        String latFromPub = null,longFromPub=null;
                        try {
                            JSONObject jsonObject1=new JSONObject(message);
                            latFromPub=jsonObject1.getString("lt");
                            longFromPub=jsonObject1.getString("lg");
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        fare_quote_layout.setVisibility(View.GONE);
                        Utility.printLog("INSIDE DRIVER REACHED: 8 message=" + message);
                        session.setDriverOnWay(false);
                        Utility.printLog("Wallah set as false Homepage 8");
                        session.setDriverArrived(false);
                        session.setTripBegin(true);
                        session.setInvoiceRaised(false);
                        Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
                        setHasOptionsMenu(false);
                        eta_latitude = latFromPub;
                        eta_longitude = longFromPub;
                        colourForPathPlot=getResources().getColor(R.color.background);

                        if(!session.getBookingReponseFromPubnub().equals(""))
                        {
                            //populate the UI
                            onJourneyStartedUIUpdate();
                        }
                        //plot the marker for driver location on map

                        if (eta_latitude != null && eta_longitude != null)//else adding driver current location
                        {
                            LatLng latLng = new LatLng(Double.parseDouble(eta_latitude), Double.parseDouble(eta_longitude));

                            if (!session.getSelectedImage().equals("")) {
                                driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                Picasso.with(getActivity()).load( session.getSelectedImage())
                                        .resize((int) driverCarWidth, (int) driverCarHeight)
                                        .into(driverMarker);
                            }
                            if(googleMap!=null)
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
                        }
                        //to track the driver and destination location
                        UpdateDriverLocation_JourneyStarted(eta_latitude, eta_longitude);
                        break;
                    }
                    //if driver published a=9 then publish in server channel a=3 to get the booking data
                    case "9":
                    {
                        JSONObject jsonObject2=new JSONObject(message);
                        String bookingId=jsonObject2.getString("bid");
                        Utility.printLog("onResume INSIDE DriverInvoiceRaised local "+bookingId+"" +
                                " session "+session.getBookingId());
                        if(session.getBookingId().equals(bookingId))
                        {
                            publishingTag = 3;
                            Utility.printLog("onResume INSIDE DriverInvoiceRaised");

                            Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
                            driver_call_and_cancel_ll.setVisibility(View.GONE);
                            driver_status_for_journey_tv.setVisibility(View.VISIBLE);
                            driver_status_for_journey_tv.setBackgroundColor(getResources().getColor(R.color.green));

                            /**
                             * to publish the user latlongs to server to get the data from server with 0 delay
                             * if publishing is not yet started
                             */
                            if(myTimer_publish==null)
                                startPublishingWithTimer(0);
                            mid_pointer.setVisibility(View.VISIBLE);
                            pollingTimer.cancel();
                        }
                        break;
                    }

                    //user gets a=2 if user publishes a=1
                    case "2":
                    {
                        Gson gson = new Gson();
                        filterResponse = gson.fromJson(message, PubnubResponseNew.class);
                        Utility.printLog("pubnub response in status 2 "+ message +" "+VariableConstants.CONFIRMATION_CALLED
                                +" type id "+current_master_type_id);
                        Utility.printLog("bug is "+1);

                        if(filterResponse.getTypes().size()>0)
                        {
                            Utility.printLog("bug is "+2);
                            session.setPubnubResponse(message);
                            /**
                             * if booking status is 0 then user is not in booking.
                             */
                            switch (filterResponse.getBookingStatus()) {
                                case "0":
                                    Utility.printLog("bug is " + 3);
                                    allowNegativeValueUpto = filterResponse.getBookingAmountAllowUpto();
                                    session.setBookingReponseFromPubnub("");


                                    if (invoiceDialogue != null)
                                        invoiceDialogue.dismiss();
                                    session.setPubnubResponseForNonBooking(message.toString());
                                    publishingTag = 1;

                                    /**
                                     * to check for mandatory update case
                                     * and show popup if mandatory update
                                     */
                                    if(Utility.currentVersion(getActivity()).compareToIgnoreCase
                                            (filterResponse.getConfigData().getVersions().getAndCust())<0)
                                    {
                                        /**
                                         * 	to show the mandatory update from playstore
                                         */
                                        if(mandatoryFieldPopup!=null)
                                            if (!mandatoryFieldPopup.isShowing())
                                                mandatoryFieldPopup.show();
                                    }
                                    else {
                                        /**
                                         * 	to show the mandatory update from playstore
                                         */
                                        if(mandatoryFieldPopup!=null)
                                            if (mandatoryFieldPopup.isShowing())
                                                mandatoryFieldPopup.dismiss();
                                        /**
                                         * store all configured data from backend in SharedPreference
                                         */
                                        session.setPubnubIntervalForHome(Long.parseLong(filterResponse.getConfigData().getPubnubIntervalHome()));
                                        session.setEtaIntervalForTracking(Long.parseLong(filterResponse.getConfigData().getDistanceMatrixInterval()));
                                    }

                                    /**
                                     * to set the google matrix keys from backend and
                                     * set them in shared preference
                                     */
                                    List<String> googleMatrixKeys = new ArrayList<>();
                                    googleMatrixKeys.addAll(filterResponse.getConfigData().getGoogleKeysArray());
                                    String jsonText = gson.toJson(googleMatrixKeys);
                                    session.setGoogleMatrixData(jsonText);

                                    /**
                                     * to set the server key in shared reference
                                     * from 0th index from backend
                                     */
                                    if(!filterResponse.getConfigData().getGoogleKeysArray().isEmpty())
                                        session.setServerKey(filterResponse.getConfigData().getGoogleKeysArray().get(0));

                                    /**
                                     * resetng all booking flags..
                                     */
                                    session.setDriverOnWay(false);
                                    session.setDriverArrived(false);
                                    session.setTripBegin(false);
                                    session.setInvoiceRaised(false);

                                    if (message != null && filterResponse != null && filterResponse.getMasArr() != null && filterResponse.getMasArr().size() > 0 && filterResponse.getTypes().size() > 0) {
                                        /**
                                         * To check the previous cartype id's with current type id's for updating all the child
                                         * views of all the current car types present
                                         */

                                        ArrayList<String> currentTypeIdslist = new ArrayList<>();
                                        all_car_types = new JSONArray();

                                        for (int i = 0; i < filterResponse.getTypes().size(); i++) {
                                            Utility.printLog("currentTypeIdslist " + filterResponse.getTypes().get(i).getType_id());
                                            currentTypeIdslist.add(filterResponse.getTypes().get(i).getType_id());
                                            all_car_types.put(i, filterResponse.getTypes().get(i).getType_id());
                                        }

                                        if (!(previousTypeIdslist.containsAll(currentTypeIdslist) && currentTypeIdslist.containsAll(previousTypeIdslist))) {

                                            /**
                                             * if current type id list doesnot contain then make the selected type
                                             * id as blank do that default selected will be 0th index
                                             */

                                            Utility.printLog("currentTypeIdslist a " + current_master_type_id);
                                            if (!currentTypeIdslist.contains(current_master_type_id))
                                                VariableConstants.TYPE = "";

                                            Utility.printLog("currentTypeIdslist b " + current_master_type_id);
                                            previousTypeIdslist.clear();
                                            previousTypeIdslist.addAll(currentTypeIdslist);
                                            carTypeImagesForSelectedType.clear();
                                            typeSize = filterResponse.getTypes().size();
                                            unselectedImages.clear();
                                            for (int i = 0; i < filterResponse.getTypes().size(); i++) {
                                                carTypeImagesForSelectedType.add(filterResponse.getTypes().get(i).getVehicle_img());
                                                unselectedImages.add(filterResponse.getTypes().get(i).getVehicle_img_off());
                                                mapImages.add(filterResponse.getTypes().get(i).getMapIcon());
                                                carName.add(filterResponse.getTypes().get(i).getType_name());
                                            }

                                            session.setSelectedImage(mapImages.get(0));

                                            if (VariableConstants.TYPE.equals("")) {
                                                current_master_type_id = filterResponse.getTypes().get(0).getType_id();
                                                carName.add(filterResponse.getTypes().get(0).getType_name());
                                                session.setCarTypeId(current_master_type_id);
                                            } else {
                                                current_master_type_id = filterResponse.getTypes().get(Integer.parseInt(VariableConstants.TYPE)).getType_id();
                                                carName.add(filterResponse.getTypes().get(Integer.parseInt(VariableConstants.TYPE)).getType_name());
                                                session.setCarTypeId(current_master_type_id);
                                            }
                                            for (int i = 0; i < filterResponse.getTypes().size(); i++) {
                                                markerPlotingFirstTime.add(i, true);
                                            }
                                        }

                                        /**
                                         *  To set serge price to global variable "surgePrice"
                                         *  for the current type ( clicked or 0'th index ).
                                         */
                                        if (surgePrice.equals("")) {
                                            if (VariableConstants.TYPE.equals("")) {
                                                if (!filterResponse.getTypes().get(0).getSurg_price().equals("")) {

                                                    surgePrice = filterResponse.getTypes().get(0).getSurg_price();

                                                } else {
                                                    surgePrice = "";
                                                }
                                            } else {
                                                if (!filterResponse.getTypes().get(Integer.parseInt(VariableConstants.TYPE)).getSurg_price().equals("")) {
                                                    surgePrice = filterResponse.getTypes().get(Integer.parseInt(VariableConstants.TYPE)).getSurg_price();
                                                } else {
                                                    surgePrice = "";
                                                }
                                            }
                                        }
                                    }

                                    if ((filterResponse != null && filterResponse.getA().equals("2")) && !(session.isDriverOnWay() || session.isDriverOnArrived()
                                            || session.isTripBegin() || session.isInvoiceRaised())) {
                                        if (filterResponse.getTypes() != null && filterResponse.getTypes().size() > 0) {
                                            /**
                                             * to calculate the eta of all drivers and sort them according to eta and plot the available driver markers
                                             */
                                            if (filterResponse.getMasArr().size() > 0) {
                                                //Akbar added this to show all drivers on home screen
                                                if (googleMap != null)
                                                    googleMap.clear();

                                                for (int i = 0; i < filterResponse.getMasArr().size(); i++) {
                                                    Utility.printLog("pubnub resposne  is map moved latlong " + currentLatitude + " " + currentLongitude +
                                                            "session latlongs " + session.getCurrentLat() + " lng " + session.getCurrentLong());
                                                    if (currentLatitude != 0 && currentLongitude != 0) {
                                                        etaThreshold = filterResponse.getEta();

                                                        /************************************************************************/
                                                            //clearing the map before adding the driver markers
                                                            if (filterResponse.getMasArr().get(i).getMas().size() > 0) {
                                                                for (int j = 0; j < filterResponse.getMasArr().get(i).getMas().size(); j++) {
                                                                    //hide the progress bar and show the eta
                                                                    pubnubProgressDialog.setVisibility(View.VISIBLE);
                                                                    pickup_Distance.setVisibility(View.GONE);
                                                                    rate_unit.setVisibility(View.GONE);
                                                                    if (!toCheckAnimation) {
                                                                        pubnubProgressDialog.clearAnimation();
                                                                        pubnubProgressDialog.setVisibility(View.GONE);
                                                                        pickup_Distance.clearAnimation();
                                                                        rate_unit.clearAnimation();
                                                                        pickup_Distance.setVisibility(View.VISIBLE);
                                                                        rate_unit.setVisibility(View.VISIBLE);
                                                                    }
                                                                    /**
                                                                     * to get the eta from pubnub which is in sec and convert it to minutes and show for 0th index
                                                                     */
                                                                    int dividedTime = (int) (Double.parseDouble(filterResponse.getMasArr().get(i).getMas()
                                                                            .get(0).getT()));
                                                                    Utility.printLog("divided time " + dividedTime);
                                                                    rate_unit.setText(getString(R.string.minute));
                                                                    pickup_Distance.setTextSize((float) 14.4);
                                                                    if (session.getLanguageCode().equals("en"))
                                                                        Utility.setTypefaceMuliBold(getActivity(), pickup_Distance);
                                                                    /**
                                                                     * if the eta is 0 then show it as 1 min
                                                                     * else show the gained eta
                                                                     */
                                                                    if (dividedTime == 0) {
                                                                        pickup_Distance.setText("1");
                                                                    } else {
                                                                        pickup_Distance.setText(dividedTime + "");
                                                                    }

                                                                    /**
                                                                     *to add the driver markers on the map whichever are online if the google map is ready i.e its not null
                                                                     */
                                                                    if (googleMap != null) {
                                                                        LatLng latLng = new LatLng(Double.parseDouble(filterResponse.getMasArr().get(i).getMas().get(j).getLt()),
                                                                                Double.parseDouble(filterResponse.getMasArr().get(i).getMas().get(j).getLg()));
                                                                        try {
                                                                            driverMarker = new PicassoMarker(googleMap.addMarker(new MarkerOptions().position(latLng)), getActivity());
                                                                            Picasso.with(getActivity()).load(filterResponse.getTypes().get(i).getMapIcon())
                                                                                    .resize((int) driverCarWidth, (int) driverCarHeight)
                                                                                    .into(driverMarker);
                                                                        } catch (IllegalArgumentException e) {
                                                                            Utility.printLog("exception in loading map image " + e);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            //if drivers size is 0 then clear all lists and show as n drivers available
                                                            else {
                                                                Utility.printLog("clearing the list 4");
                                                                Utility.printLog("inside else for the no drivers ");
                                                                pubnubProgressDialog.clearAnimation();
                                                                pubnubProgressDialog.setVisibility(View.GONE);
                                                                pickup_Distance.setVisibility(View.VISIBLE);
                                                                rate_unit.setVisibility(View.VISIBLE);
                                                                pickup_Distance.setTextSize(10);
                                                                if (session.getLanguageCode().equals("en"))
                                                                    Utility.setTypefaceMuliRegular(getActivity(), pickup_Distance);
                                                                pickup_Distance.setText(getString(R.string.no_camel));
                                                                rate_unit.setText(getString(R.string.drivers));
                                                            }
                                          /************************************************************************************/
                                                    }
                                                }
                                            }
                                        }
                                        //if there are no types available then sow snackbar for not avaialable area an clear map and all lists
                                        else {
                                            new_channels_to_subscribe.clear();
                                            /**
                                             * show snackbar for not avaialable area
                                             */
                                            Utility.showSnackBar(view, getActivity(), getActivity().getString(R.string.not_available_area));


                                            pubnubProgressDialog.clearAnimation();
                                            pubnubProgressDialog.setVisibility(View.GONE);
                                            pickup_Distance.setVisibility(View.VISIBLE);
                                            rate_unit.setVisibility(View.VISIBLE);
                                            pickup_Distance.setTextSize(10);
                                            if (session.getLanguageCode().equals("en"))
                                                Utility.setTypefaceMuliRegular(getActivity(), pickup_Distance);
                                            pickup_Distance.setText(getString(R.string.no_camel));
                                            rate_unit.setText(getString(R.string.drivers));
                                        }
                                    }
                                    break;
                                case "1":
                                    publishingTag = 3;
                                    /**
                                     * if user is in booking then close the timer and start again so that
                                     * pubnub will be called at 0th interval
                                     */
                                    if(myTimer_publish!=null)
                                    {
                                        myTimer_publish.cancel();
                                        myTimer_publish=null;
                                        startPublishingWithTimer(0);
                                    }
                                    break;
                                case "2":
                                    if (session.getBookingId().equals(filterResponse.getBid())) {
                                        Intent intent = new Intent(getActivity(), RequestPickup.class);
                                        intent.putExtra("ALREADY_SENT_BOOKING", true);
                                        intent.putExtra("EXPIRED_TIME", filterResponse.getExpiredTime());
                                        startActivityForResult(intent, 19);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            /**
                             * show snackbar for not avaialable area
                             */
                            Utility.showSnackBar(view,getActivity(),getActivity().getString(R.string.not_available_area));
                            previousTypeIdslist.clear();
                            pubnubProgressDialog.clearAnimation();
                            pickup_Distance.setVisibility(View.VISIBLE);
                            rate_unit.setVisibility(View.VISIBLE);
                            pickup_Distance.setTextSize(10);
                            if(session.getLanguageCode().equals("en"))
                                Utility.setTypefaceMuliRegular(getActivity(),pickup_Distance);
                            pubnubProgressDialog.setVisibility(View.GONE);
                            pickup_Distance.setText(getString(R.string.no_camel));
                            rate_unit.setText(getString(R.string.drivers));
                        }
                    }
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Utility.printLog("successcallback exception in pubnub response "+e.getMessage());
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Utility.printLog("CONTROL INSIDE onCreate");
        publishingTag=1;
        Utility.printLog("CONTROL INSIDE onCreate publishingTag "+publishingTag);
        session = new SessionManager(mActivity);
        VariableConstants.isComingFromSearch=false;
        VariableConstants.isComingFromScroll=false;
        VariableConstants.CONFIRMATION_CALLED=false;
        mDialog = Utility.GetProcessDialog(getActivity());
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener,PhoneStateListener.LISTEN_CALL_STATE);
        marker_map= new HashMap<>();

        filter = new IntentFilter();
        filter.addAction("com.bloomingdalelimousine.ridein.push");
        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                Utility.printLog("driver cancelled the aapointment "+intent.getStringExtra("ACTION_FOR_CANCELLED"));
                if(intent.getStringExtra("ACTION_FOR_CANCELLED") !=null)
                {
                    if(intent.getStringExtra("ACTION_FOR_CANCELLED").equals("10"))
                        DriverCancelledAppointment(session.getCancellationType());
                }
                else if(session.isDriverOnWay())
                {
                    setHasOptionsMenu(false);
                    session.setDriverOnWay(true);
                    Utility.printLog("Wallah set as true Homepage push 6");
                    session.setDriverArrived(false);
                    session.setTripBegin(false);
                    session.setInvoiceRaised(false);
                }
                else if(session.isDriverOnArrived())
                {
                    session.setDriverOnWay(false);
                    Utility.printLog("Wallah set as false Homepage push  7");
                    session.setDriverArrived(true);
                    session.setTripBegin(false);
                    session.setInvoiceRaised(false);
                    Driver_on_the_way_txt.setText(getResources().getString(R.string.driverreached));
                    setHasOptionsMenu(false);
                }
                else if(session.isTripBegin())
                {
                    session.setDriverOnWay(false);
                    Utility.printLog("Wallah set as false push Homepage 7");
                    session.setDriverArrived(false);
                    session.setTripBegin(true);
                    session.setInvoiceRaised(false);
                    Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
                    setHasOptionsMenu(false);
                }
                else if(session.isInvoiceRaised())
                {
                    Utility.printLog("onResume INSIDE push DriverInvoiceRaised");
                    publishingTag = 3;
                    /**
                     * to publish the user latlongs to server to get the data from server with 0 delay
                     * if publishing is not yet started
                     */
                    if(myTimer_publish==null)
                        startPublishingWithTimer(0);
                    pollingTimer.cancel();
                }
                else if(session.getNotificationFromAdmin())
                {
                    Utility.printLog("push notification ");
                    showPopupForOneButton(VariableConstants.PUSH_MESSAGE);
                    VariableConstants.PUSH_MESSAGE="";
                    NotificationManager notificationManager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                    if(notificationManager!=null)
                    {
                        notificationManager.cancelAll();
                    }
                }
            }
        };
    }

    private void showPopupForOneButton(String messsage)
    {
        Dialog dialog=Utility.showPopupWithOneButton(getActivity());
        dialog.setCancelable(false);
        TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
        TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
        TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
        title_popup.setText(getResources().getString(R.string.message));
        text_for_popup.setText(messsage);
        yes_button.setText(getResources().getString(R.string.ok));
        dialog.show();
    }

    /**
     * Initializing all variables in this view
     * @param v
     */
    private void initializeVariables(View v)
    {
        getActivity().registerReceiver(receiver, filter);

        negFbrsnList= new ArrayList<NegativeFeedBackOfDriver>();

        String[] reasonse = getResources().getStringArray(R.array.reasons_set);

        for (int i=0;i<reasonse.length;i++){
            NegativeFeedBackOfDriver neg = new NegativeFeedBackOfDriver();
            neg.setReason(reasonse[i]);
            negFbrsnList.add(neg);
        }

        pickup=(RelativeLayout) v.findViewById(R.id.relative_center);
        mid_pointer=(ImageView) v.findViewById(R.id.mid_pointer);
        pickup_location_search_iv=(ImageView) v.findViewById(R.id.pickup_location_search_iv);
        dropoff_loation_search_iv=(ImageView) v.findViewById(R.id.dropoff_loation_search_iv);
        current_address=(TextView)v.findViewById(R.id.show_addr_text_view);
        Dropoff_Location_Address=(TextView)v.findViewById(R.id.dropoff_location_address);
        in_booking_cash_or_card_tv=(TextView)v.findViewById(R.id.in_booking_cash_or_card_tv);
        tv_homepage_fareestimate=(TextView)v.findViewById(R.id.tv_homepage_fareestimate);
        show_address_relative = (RelativeLayout) v.findViewById(R.id.show_address_relative);
        LinearLayout relative_Dropoff_Location = (LinearLayout) v.findViewById(R.id.relative_dropoff_location);
        Fare_Quote = (TextView) v.findViewById(R.id.fare_quote);
        fare_quote_layout = (LinearLayout) v.findViewById(R.id.fare_quote_layout);
        drivvehicalrating= (TextView) v.findViewById(R.id.drivvehicalrating);
        Driver_Profile_Pic = (ImageView)v.findViewById(R.id.driver_profile_pic);
        Driver_Name = (TextView)v.findViewById(R.id.driver_name);
        Driver_on_the_way_txt = (TextView) v.findViewById(R.id.driver_on_the_way);
        Rl_distance_time = (RelativeLayout)v.findViewById(R.id.rl_distance_time);
        Driver_Car_Num = (TextView)v.findViewById(R.id.driver_car_plate_no);
        pickup_Distance = (TextView)v.findViewById(R.id.txt_pickup_distance);
        pubnubProgressDialog = (ProgressBar) v.findViewById(R.id.progressBar);
        all_types_layout=(RelativeLayout)v.findViewById(R.id.relative_all_car_types);
        rate_unit=(TextView) v.findViewById(R.id.rate_unit);
        Button contact_driver = (Button) v.findViewById(R.id.contact_driver);
        cancel_trip= (Button) v.findViewById(R.id.cancel_trip);
        utility=new Utility();

        Appointment_location = (TextView) view.findViewById(R.id.Appointment_location);
        dialogL= Utility.GetProcessDialog(getActivity());
        dialogL.setCancelable(false);
        drivsnd= (TextView) v.findViewById(R.id.drivsnd);
        currenct_location_button = (ImageView) v.findViewById(R.id.currenct_location_button);
        horizontal_scrollView = (HorizontalScrollView)view.findViewById(R.id.horizontalScrollView);
        now_later_layout= (LinearLayout) view.findViewById(R.id.now_later_layout);
        rate_card_ll= (LinearLayout) view.findViewById(R.id.rate_card_ll);

        choose_payment_type_ll= (LinearLayout) view.findViewById(R.id.choose_payment_type_ll);
        relative_confirmation_navigation= (RelativeLayout) v.findViewById(R.id.relative_confirmation_navigation);
        rl_confirmation_Screen= (RelativeLayout) v.findViewById(R.id.rl_confirmation_Screen);
        cash_card_option_layout_rl= (RelativeLayout) v.findViewById(R.id.cash_card_option_layout_rl);
        outer_map_layout= (RelativeLayout) v.findViewById(R.id.outer_map_layout);
        rl_back_btn= (RelativeLayout) v.findViewById(R.id.rl_back_btn);
        btn_back= (ImageButton) v.findViewById(R.id.btn_back);
        drop_layout= (RelativeLayout) v.findViewById(R.id.drop_layout);

        search_or_ring_iv = (ImageView)v.findViewById(R.id.search_or_ring_iv);
        now_button = (Button)v.findViewById(R.id.now_button);
        later_button = (ImageView)v.findViewById(R.id.later_button);

        request_pick_up_here = (Button)v.findViewById(R.id.request_pick_up_here);

        search_or_ring_iv.setImageResource(R.drawable.home_search_icon_off);

        rl_action_bar_main= activity.action_bar;

        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        customView = inflater.inflate(R.layout.card_cash_option_layout,null);
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        // Call requires API level 21
        if(Build.VERSION.SDK_INT>=21){
            mPopupWindow.setElevation(50.0f);
        }

        cash_select_layout_ll = (LinearLayout) customView.findViewById(R.id.cash_select_layout_ll);
        cash_card_option_cancel_btn = (Button) customView.findViewById(R.id.cash_card_option_cancel_btn);
        //on booking layouts..
        driver_call_and_cancel_ll = (LinearLayout) v.findViewById(R.id.driver_call_and_cancel_ll);
        driver_status_for_journey_tv = (TextView) v.findViewById(R.id.driver_on_the_way);
        select_payment_type_tv = (TextView) v.findViewById(R.id.select_payment_type_tv);
        select_payment_type_checkmark_iv = (ImageView) v.findViewById(R.id.select_payment_type_checkmark_iv);
        btn_back.setOnClickListener(this);
        appointment_location_drop= (TextView) v.findViewById(R.id.appointment_location_drop);
        dbPickup = new DatabasePickupHandler(getActivity());
        promo_code_layout= (LinearLayout) v.findViewById(R.id.promo_code_layout);
        rl_address= (RelativeLayout) v.findViewById(R.id.rl_address);

        shadow_icon= (ImageView) v.findViewById(R.id.shadow_icon);
        current_location= (TextView) v.findViewById(R.id.current_location);
        TextView promo_code_text= (TextView) v.findViewById(R.id.promo_code_text);
        view3=  v.findViewById(R.id.view3);
        view2=  v.findViewById(R.id.view2);
        view_for_image=  v.findViewById(R.id.view_for_image);
        rightArrowImages=new ArrayList<>();
        mandatoryFieldPopup=utility.mandatoryUpdate(getActivity());

        setCornersRadii();

        /**
         * setting the current location default
         * take from splash screen
         * which set from SharedPrefernce
         */
        currentLatitude=session.getCurrentLat();
        currentLongitude=session.getCurrentLong();

        currentLocation=new double[2];
        current_address.setSelected(true);
        Dropoff_Location_Address.setSelected(true);
        dblArray = Scaler.getScalingFactor(getActivity());
        driverCarWidth = dblArray[0] * 45;
        driverCarHeight = dblArray[1] * 50;
        session.setDriverCooments("");
        width = getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumWidth();
        height = getResources().getDrawable(R.drawable.ic_driver_confirm_profile_default_image).getMinimumHeight();
        pickup.setOnClickListener(this);
        now_button.setOnClickListener(this);
        later_button.setOnClickListener(this);
        request_pick_up_here.setOnClickListener(this);
        contact_driver.setOnClickListener(this);
        cancel_trip.setOnClickListener(this);
        fare_quote_layout.setOnClickListener(this);
        show_address_relative.setOnClickListener(this);
        Rl_distance_time.setOnClickListener(null);
        Driver_on_the_way_txt.setOnClickListener(null);
        relative_Dropoff_Location.setOnClickListener(this);
        currenct_location_button.setOnClickListener(this);
        Driver_Profile_Pic.setOnClickListener(this);
        promo_code_layout.setOnClickListener(this);
        current_location.setOnClickListener(this);
        pickup_location_search_iv.setOnClickListener(this);
        dropoff_loation_search_iv.setOnClickListener(this);
        rate_card_ll.setOnClickListener(this);
        choose_payment_type_ll.setOnClickListener(this);
        drop_layout.setOnClickListener(this);
        now_button.setSelected(true);
        later_button.setSelected(false);

        /**
         * to set the typeface
         */
        if(session.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(getActivity(),appointment_location_drop);
            Utility.setTypefaceMuliRegular(getActivity(),promo_code_text);
            Utility.setTypefaceMuliRegular(getActivity(),Dropoff_Location_Address);
            Utility.setTypefaceMuliRegular(getActivity(),Fare_Quote);
            Utility.setTypefaceMuliRegular(getActivity(),drivsnd);
            Utility.setTypefaceMuliRegular(getActivity(),drivvehicalrating);
            Utility.setTypefaceMuliRegular(getActivity(),Driver_on_the_way_txt);
            Utility.setTypefaceMuliRegular(getActivity(),Appointment_location);
            Utility.setTypefaceMuliRegular(getActivity(),current_address);
            Utility.setTypefaceMuliBold(getActivity(),Driver_Name);
            Utility.setTypefaceMuliBold(getActivity(),Driver_Car_Num);
        }

        /**
         * initialize pubnub
         */
        pnConfiguration = new PNConfiguration();

        /**
         * initializing the pubnub with keys from shared preferance
         */
        pnConfiguration.setUuid("s_"+session.getSid());

        if(!session.getPublishKey().equals("") && !session.getSubscribeKey().equals(""))
        {
            Utility.printLog("pub publish key 1"+session.getPublishKey());
            Utility.printLog("sub publish key 1"+session.getSubscribeKey());
            pnConfiguration.setSubscribeKey(session.getSubscribeKey());
            pnConfiguration.setPublishKey(session.getPublishKey());
        }

        pubnub = new PubNub(pnConfiguration);
    }

    public void setCornersRadii()
    {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(getResources().getColor(R.color.background_transperant));
        shape.setCornerRadius(15);
        Driver_on_the_way_txt.setBackgroundDrawable(shape);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        activity.fragment_title.setVisibility(View.VISIBLE);
        activity.fragment_title_logo.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            session = new SessionManager(getActivity());
            view = inflater.inflate(R.layout.homepage, container, false);
            visibility = true;
        } catch (InflateException e) {
			/* map is already there, just return view as it is */
        }

        activity = (MainActivity) getActivity();
        activity.fragment_title.setVisibility(View.GONE);
        activity.fragment_title_logo.setVisibility(View.VISIBLE);
        activity.action_bar.setVisibility(View.VISIBLE);
        activity.edit_profile.setVisibility(View.GONE);

        previousTypeIdslist = new ArrayList<String>();

        initializeVariables(view);

        markerId_Email_map = new HashMap<>();
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (filterResponse.getTypes() != null && filterResponse.getTypes().size() > 0)
        {
            current_master_type_id = filterResponse.getTypes().get(0).getType_id();
            session.setCarTypeId(current_master_type_id);
            for (int i = 0; i < filterResponse.getTypes().size(); i++)
            {
                markerPlotingFirstTime.add(i, true);
            }
        }

        subscribeCallback=new SubscribeCallback()
        {
            @Override
            public void status(PubNub pubnub, PNStatus status)
            {
                Utility.printLog("status for th epubnub "+status.getStatusCode());
                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // internet got lost, do some magic and call reconnect when ready
                    Utility.printLog("successCallback internet got lost PNUnexpectedDisconnectCategory");
                    pubnub.reconnect();
                } else if (status.getCategory() == PNStatusCategory.PNTimeoutCategory) {
                    // do some magic and call reconnect when ready
                    Utility.printLog("successCallback internet got lost PNTimeoutCategory");
                    pubnub.reconnect();
                } else {
                    Utility.printLog("successCallback internet got lost "+status.getCategory());
                    if(status.getCategory()==PNStatusCategory.PNConnectedCategory)
                    {
                        /**
                         * if the distance moved is more than the radius from backend
                         * then call pubnub to get data
                         */
                        if(myTimer_publish!=null)
                        {
                            myTimer_publish.cancel();
                            myTimer_publish=null;
                            //publishing to server channel
                            startPublishingWithTimer(0);
                        }

                    }
                }
            }
            @Override
            public void message(PubNub pubnub, final PNMessageResult message)
            {
                if(getActivity()!=null) {
                    if (isAdded())
                    {
                        try
                        {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run()
                                {
                                    Utility.printLog(" successCallback message from pubnub 1 bug is 0 ");
                                    methodAfterGettingResponse(message.getMessage().toString());
                                }
                            });
                        } catch (Exception e) {
                            Utility.printLog("exception in success " + e);
                        }
                    }
                }
            }
            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence)
            {
            }
        };
        return view;
    }



    @Override
    public void updateLocation(Location location)
    {
        if(location!=null)
        {
            currentLocation[0]=location.getLatitude();
            currentLocation[1]=location.getLongitude();
            lat = location.getLatitude();
            longi = location.getLongitude();
            Utility.printLog("location on updates "+location.getLatitude()+" "+location.getLongitude());
            if(!VariableConstants.isComingFromSearch) {
                VariableConstants.isComingFromSearch=true;
                LatLng latLng = new LatLng(lat, longi);
                if(googleMap!=null)
                {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                }
            }
        }
    }

    @Override
    public void location_Error(String error) {
    }

    public void setUpLaterTime() {
        String fulldate = Utility.getTime();
        String[] fulldatesplited = fulldate.split(":");
        int hh = Integer.parseInt(fulldatesplited[0]);
        int mm = Integer.parseInt(fulldatesplited[1]);
        TimePickerDialog timePickerDialog = new TimePickerDialog(activity,(TimePickerDialog.OnTimeSetListener) this,hh,mm,false);
        timePickerDialog.show();


    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear+1;
        Log.d("selected_date", "year " + year + " monthOfYear " + month + " dayOfMonth " + dayOfMonth);
        later_booking_date = year + "-" + oneToTwoDigitConvertor(month) + "-" + oneToTwoDigitConvertor(dayOfMonth);
        setUpLaterTime();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.d("selected_date_time", "hourOfDay " + hourOfDay + " minute " + minute );

        Calendar c = Calendar.getInstance();
        final int hour = c.get(Calendar.HOUR_OF_DAY);
        final int min = c.get(Calendar.MINUTE);

        if(later_booking_date.equals(Utility.getDate())) {
            if (hourOfDay == hour+1 && minute >= min) {

                laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
                //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
                Utility.printLog("laterBookingDate=" + laterBookingDate);
                now_button.setSelected(false);
                later_button.setSelected(true);

                pickup.callOnClick();

            } else if (hourOfDay > hour+1) {

                laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
                //booked_now_later.setText("YOU ARE BOOKING FOR LATER : "+day+"-"+month+"-"+year+" "+hour+":"+min);
                Utility.printLog("laterBookingDate=" + laterBookingDate);
                now_button.setSelected(false);
                later_button.setSelected(true);
                pickup.callOnClick();

            }
            else {
                Toast.makeText(activity, "Please select at least 1 hour later time from your current time!", Toast.LENGTH_SHORT).show();
            }
        } else {
            laterBookingDate = later_booking_date + " " + oneToTwoDigitConvertor(hourOfDay) + ":" + oneToTwoDigitConvertor(minute) + ":00";
            Utility.printLog("laterBookingDate=" + laterBookingDate);
            now_button.setSelected(false);
            later_button.setSelected(true);
            pickup.callOnClick();
        }
    }

    private String oneToTwoDigitConvertor(int num){
        if(num<10) {
            return "0"+num;
        } else {
            return ""+num;
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.now_button:
            {
                if(mPICKUP_ADDRESS!=null){

                    if(mDROPOFF_ADDRESS!=null) {

                        //aakash change
                        if(!mPICKUP_ADDRESS.equalsIgnoreCase(mDROPOFF_ADDRESS))
                        {

                        if(fare_for_all_cartypes!=null) {
                            Utility.printLog(
                                    "FromLatitude " + from_latitude + " FromLongitude " + from_longitude +
                                            " to_latitude " + to_latitude + " to_longitude " + to_longitude
                            );
                            session.setPromocode("");
                            Intent intent = new Intent(mActivity, RouteAndPaymentSelectActivity.class);
                            Bundle bundle = new Bundle();
                            intent.putExtra("ent_addr_line1", mPICKUP_ADDRESS);
                            intent.putExtra("from_latitude", from_latitude);
                            intent.putExtra("from_longitude", from_longitude);
                            intent.putExtra("ent_drop_addr_line1", mDROPOFF_ADDRESS);
                            intent.putExtra("to_latitude", to_latitude);
                            intent.putExtra("to_longitude", to_longitude);
                            intent.putExtra("calculated_fare", calculated_fare);
                            intent.putExtra("fare_for_all_cartypes", fare_for_all_cartypes);
                            intent.putExtra("PICK_DROP_INFO", bundle);
                            startActivity(intent);
                            mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                        } else {
                            getFareEstimate();
                        }

                    } else {
                        Toast.makeText(mActivity, ""+getString(R.string.location_cannot_be_same), Toast.LENGTH_SHORT).show();
                    }


                } else {
                        Toast.makeText(mActivity, ""+getString(R.string.please_provide_drop_off), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(mActivity, ""+getString(R.string.please_provide_pickup), Toast.LENGTH_SHORT).show();
                }

                break;
            }

            case R.id.later_button:
            {
                session.setPromocode("");
                Intent intent = new Intent(mActivity, LaterBookingActivity.class);
                intent.putExtra("fare_for_all_cartypes",fare_for_all_cartypes);
                startActivity(intent);
                mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
            }

            case R.id.pickup_location_search_iv:
            {
                VariableConstants.TYPE="";
                VariableConstants.SEARCH_CALLED=true;
                Intent addressIntent=new Intent(getActivity(), SearchAddressGooglePlacesActivity.class);
                addressIntent.putExtra("chooser", getResources().getString(R.string.pickup_location));
                startActivityForResult(addressIntent, 18);
                getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                break;
            }

            case R.id.dropoff_loation_search_iv:
            {
                Intent addressIntent=new Intent(getActivity(), SearchAddressGooglePlacesActivity.class);
                addressIntent.putExtra("chooser", getResources().getString(R.string.drop_location));
                if(!current_address.getText().toString().isEmpty()) {
                    addressIntent.putExtra("PICKUP_ADDRESS", current_address.getText().toString());
                }
                startActivityForResult(addressIntent, 30);
                getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                break;
            }
            case R.id.drop_layout:
            {
                Intent addressIntent=new Intent(getActivity(), SearchAddressGooglePlacesActivity.class);
                addressIntent.putExtra("chooser", getResources().getString(R.string.drop_location));
                if(!current_address.getText().toString().isEmpty()) {
                    addressIntent.putExtra("PICKUP_ADDRESS", current_address.getText().toString());
                }
                startActivityForResult(addressIntent, 30);
                getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                break;
            }


            case R.id.driver_profile_pic:
            {
                Dialog picDialog = new Dialog(getActivity());
                picDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                picDialog.setContentView(R.layout.photo_layout);
                picDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                picDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                ImageView driverProfileImage= (ImageView) picDialog.findViewById(R.id.bigger_image);

                if(session.getDocPic()!= null && !session.getDocPic().equalsIgnoreCase("null") && !session.getDocPic().equalsIgnoreCase(""))
                {
                    /**
                     * to set the image into image view and
                     * add the write the image in the file
                     */
                    driverProfileImage.setTag(setTargetForLargeImage(driverProfileImage));
                    Picasso.with(getActivity()).load(session.getDocPic()).into((Target) driverProfileImage.getTag()) ;
                }
                picDialog.show();

                break;
            }

            case R.id.currenct_location_button:
            {
                LatLng latLng = new LatLng(currentLocation[0], currentLocation[1]);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                Appointment_location.setText(getResources().getString(R.string.pickup_location));

                break;
            }
            case  R.id.current_location:
            {
                LatLng latLng = new LatLng(currentLocation[0], currentLocation[1]);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                break;
            }
            case R.id.contact_driver:
            {
                if(session.getDocPH()!=null)
                {
                    Log.d("mobile_num_obt",session.getDocPH());
                    Intent intent = new Intent(getActivity(),ClientActivity.class);
                    intent.putExtra("phonenumber",session.getDocPH());
                    intent.putExtra("TWILIO_API_LINK",getString(R.string.TWILIO_API_LINK));
                    startActivity(intent);
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.driver_phone_num_not_available));
                }
                break;
            }
            case R.id.cancel_trip:
            {
                if(Utility.isNetworkAvailable(getActivity()))
                {
                    cancelCheck();
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
                break;
            }
            case R.id.message_share:
            {
                popup_share.dismiss();
                String smsBody = "Track my journey in " + getResources().getString(R.string.app_name) + "@" + "\n" + session.getShareLink();
                Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"));
                intentsms.putExtra("sms_body", smsBody);
                startActivity(intentsms);
                break;
            }
            case  R.id.whatsapp_share:
            {
                popup_share.dismiss();
                PackageManager pm = getActivity().getPackageManager();
                try {
                    String smsBody = "Track my journey in " + getResources().getString(R.string.app_name) + "@" + "\n" + session.getShareLink();
                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    waIntent.setPackage("com.whatsapp");
                    waIntent.putExtra(Intent.EXTRA_TEXT, smsBody);
                    startActivity(Intent.createChooser(waIntent, "Share with"));
                } catch (NameNotFoundException e) {
                    Utility.showToast(getActivity(),getResources().getString(R.string.not_installed));
                }
                break;
            }
            case R.id.email_share:
            {
                popup_share.dismiss();
                File _sdCard = Utility.getSdCardPath();
                File _picDir = new File(_sdCard, getResources().getString(R.string.imagedire));
                Utility.deleteNon_EmptyDir(_picDir);
                File imageFile = Utility.createFile(getResources().getString(R.string.imagedire), (getResources().getString(R.string.imagefilename) + "_twit" + ".jpg"));
                //get a bitmap object from this image
                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                OutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(imageFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                try {
                    outStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String smsBody = "Track my journey in" + getResources().getString(R.string.app_name) + "@" + "\n" + session.getShareLink();
                String mail = "";
                Uri uri = Uri.fromFile(new File(imageFile.getAbsolutePath()));
                Intent email = new Intent(Intent.ACTION_SEND/*Intent.ACTION_VIEW*/);
                email.setType("message/rfc822");
                email.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                email.putExtra(Intent.EXTRA_STREAM, uri);
                email.putExtra(Intent.EXTRA_EMAIL, mail);
                email.putExtra(Intent.EXTRA_TEXT, smsBody);
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                break;
            }
            case R.id.cancel_share:
            {
                popup_share.dismiss();
                break;
            }
            case R.id.relative_cancel:
            {
                popup_share.dismiss();
                break;
            }
        }
    }

    private void cancelCheck()
    {
        final Dialog mDialog =Utility.GetProcessDialog(getActivity());
        mDialog.show();
        final JSONObject[] jsonObject = {new JSONObject()};
        try
        {
            jsonObject[0].put("ent_booking_id", session.getBookingId());
            jsonObject[0].put("ent_sess_token",session.getSessionToken());
            jsonObject[0].put("ent_dev_id",session.getDeviceId());
            Utility.printLog("params to cancel check  "+ jsonObject[0]);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"cancelAppointmentCheck", jsonObject[0], new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                mDialog.dismiss();
                Utility.printLog("result for cancel check details  "+result);
                String errMsg = null;
                if(result!=null) {
                    try {
                        jsonObject[0] =new JSONObject(result);
                        String errFlag=jsonObject[0].getString("errFlag");
                        errMsg=jsonObject[0].getString("errMsg");
                        String errNo=jsonObject[0].getString("errNo");
                        if(errFlag.equals("0"))
                        {
                            String cancelFlag=jsonObject[0].getString("flag");
                            cancellationPopup(cancelFlag,errMsg);
                        }
                        else
                        {
                            Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                        }
                        if(errNo.equals("6") || errNo.equals("7") ||
                                errNo.equals("94") || errNo.equals("96"))
                        {
                            Utility.showToast(getActivity(),errMsg);
                            Intent i = new Intent(getActivity(), SplashActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(i);
                            getActivity().overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Utility.printLog("on sucess inside if");
                from_latitude=data.getStringExtra("FROM_LATITUDE");
                from_longitude=data.getStringExtra("FROM_LONGITUDE");
                to_latitude=data.getStringExtra("TO_LATITUDE");
                to_longitude=data.getStringExtra("TO_LONGITUDE");
                String fare = data.getStringExtra("FARE");
                String from_searchAddress=data.getStringExtra("from_SearchAddress");
                String to_searchAddress=data.getStringExtra("to_SearchAddress");
                current_address.setText(from_searchAddress);
                Dropoff_Location_Address.setText(to_searchAddress);
                Fare_Quote.getLayoutParams().height= LayoutParams.WRAP_CONTENT;
                Fare_Quote.setText(fare);
            }
            else
            {
                Utility.printLog("on sucess inside else");
            }
        }

        String locationName = null;
        if(requestCode==16)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                if(isFareQuotePressed)
                {
                    String latitudeString=data.getStringExtra("LATITUDE_SEARCH");
                    String logitudeString=data.getStringExtra("LONGITUDE_SEARCH");
                    String searchAddress=data.getStringExtra("SearchAddress");
                    locationName = 	data.getStringExtra("ADDRESS_NAME");
                    to_latitude=latitudeString;
                    to_longitude=logitudeString;
                    Utility.printLog("onActivityResult latitudeString...." + latitudeString + "...logitudeString..." + logitudeString);
                    if(searchAddress!=null)
                    {
                        if(locationName !=null && locationName.length()>0)
                        {
                            mDROPOFF_ADDRESS= locationName +" "+searchAddress;
                            Dropoff_Location_Address.setText(locationName +" "+searchAddress);
                        }
                        else
                        {
                            mDROPOFF_ADDRESS=searchAddress;
                            Dropoff_Location_Address.setText(searchAddress);
                        }
                    }
                }
                String latitudeString=data.getStringExtra("LATITUDE_SEARCH");
                String logitudeString=data.getStringExtra("LONGITUDE_SEARCH");
                String searchAddress=data.getStringExtra("SearchAddress");
                to_latitude=latitudeString;
                to_longitude=logitudeString;
                Utility.printLog("onActivityResult latitudeString...." + latitudeString + "...logitudeString..." + logitudeString);
                if(searchAddress!=null)
                {
                    mDROPOFF_ADDRESS=searchAddress;
                    Dropoff_Location_Address.setText(searchAddress);
                }

                if(Utility.isNetworkAvailable(getActivity())) {
                    getFareEstimate();
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
        }
        if(requestCode==30)
        {




            if(resultCode == Activity.RESULT_OK)
            {
                if(isFareQuotePressed)
                {
                    String latitudeString=data.getStringExtra("LATITUDE_SEARCH");
                    String logitudeString=data.getStringExtra("LONGITUDE_SEARCH");
                    String searchAddress=data.getStringExtra("SearchAddress");
                    locationName = 	data.getStringExtra("ADDRESS_NAME");
                    to_latitude=latitudeString;
                    to_longitude=logitudeString;
                    Utility.printLog("onActivityResult latitudeString...." + latitudeString + "...logitudeString..." + logitudeString);
                    if(searchAddress!=null)
                    {
                        if(locationName !=null && locationName.length()>0)
                        {
                            mDROPOFF_ADDRESS= locationName +" "+searchAddress;
                            Dropoff_Location_Address.setText(locationName +" "+searchAddress);
                        }
                        else
                        {
                            mDROPOFF_ADDRESS=searchAddress;
                            Dropoff_Location_Address.setText(searchAddress);
                        }
                    }
                }
                String latitudeString=data.getStringExtra("LATITUDE_SEARCH");
                String logitudeString=data.getStringExtra("LONGITUDE_SEARCH");
                String searchAddress=data.getStringExtra("SearchAddress");
                to_latitude=latitudeString;
                to_longitude=logitudeString;
                Utility.printLog("latlongs from manual drop "+latitudeString+" "+logitudeString+" address "+searchAddress);

                dbFavLocations = dbPickup.getFavAddress();
                ArrayList<String> addressList=new ArrayList<>();
                for(int i=0;i<dbFavLocations.size();i++)
                {
                    Utility.printLog("stored pick location "+dbFavLocations.get(i).get_formattedAddress()+" ize "+dbFavLocations.size()+" id "+dbFavLocations.get(i).getID());
                    addressList.add(dbFavLocations.get(i).get_formattedAddress());
                }

                if(searchAddress!=null)
                {
                    if(locationName !=null && locationName.length()>0)
                    {
                        mDROPOFF_ADDRESS= locationName +" "+searchAddress;
                        Dropoff_Location_Address.setText(locationName +" "+searchAddress);
                    }
                    else
                    {
                        mDROPOFF_ADDRESS=searchAddress;
                        Dropoff_Location_Address.setText(searchAddress);
                    }
                }
                if(searchAddress!=null && (session.isDriverOnWay() || session.isDriverOnArrived() || session.isTripBegin()))
                {
                    mDROPOFF_ADDRESS=searchAddress;
                    if(Utility.isNetworkAvailable(getActivity()))
                    {
                        /**
                         * update the drop address if user is in booking
                         */
                        BackgroundUpdateAddress();
                    }
                }

                for(int i=0;i<addressList.size();i++)
                {
                    if(searchAddress.equals(addressList.get(i)))
                    {
                        Utility.printLog("i am inside if for fav drop");
                        appointment_location_drop.setText(dbFavLocations.get(i).get_description());
                        break;
                    }
                    else
                    {
                        Utility.printLog("i am inside else for fav drop");
                        appointment_location_drop.setText(getResources().getString(R.string.drop_location));
                    }
                }
                if(Utility.isNetworkAvailable(getActivity())) {
                    getFareEstimate();
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
        }

        if(requestCode==18)
        {
            if(data!=null)
            {
                IsreturnFromSearch=true;
                String latitudeString=data.getStringExtra("LATITUDE_SEARCH");
                String logitudeString=data.getStringExtra("LONGITUDE_SEARCH");
                mPICKUP_ADDRESS=data.getStringExtra("SearchAddress");
                String description=data.getStringExtra("DESCRIPTION");
                Utility.printLog("id from google "+data.getIntExtra("ID",0));
                Utility.printLog("description from ggogle "+description);
                databaseId=data.getStringExtra("ID");
                if(mPICKUP_ADDRESS!=null)
                {
                    current_address.setText(mPICKUP_ADDRESS);
                }

                dbFavLocations = dbPickup.getFavAddress();

                ArrayList<String> addressList=new ArrayList<>();
                for(int i=0;i<dbFavLocations.size();i++)
                {
                    Utility.printLog("stored pick location "+dbFavLocations.get(i).get_formattedAddress()+" ize "+dbFavLocations.size());
                    addressList.add(dbFavLocations.get(i).get_formattedAddress());
                }

                for(int i=0;i<addressList.size();i++)
                {
                    if(mPICKUP_ADDRESS.equals(addressList.get(i)))
                    {
                        Utility.printLog("i am inside if for fav ");
                        Appointment_location.setText(dbFavLocations.get(i).get_description());

                        break;
                    }
                    else
                    {
                        Utility.printLog("i am inside else for fav ");
                        Appointment_location.setText(getResources().getString(R.string.pickup_location));

                    }
                }

                from_latitude=latitudeString;
                from_longitude=logitudeString;
                double searchlocatinlat = Double.parseDouble(latitudeString);
                double searchlocatinlng = Double.parseDouble(logitudeString);
                currentLatitude =Double.parseDouble(latitudeString);
                currentLongitude =Double.parseDouble(logitudeString);

                LatLng latLng = new LatLng(searchlocatinlat, searchlocatinlng);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));

                pubnubProgressDialog.clearAnimation();
                pickup_Distance.clearAnimation();
                rate_unit.clearAnimation();
                pubnubProgressDialog.setClickable(false);
                pubnubProgressDialog.setVisibility(VISIBLE);
                shadow_icon.setVisibility(VISIBLE);
                pickup_Distance.setVisibility(View.GONE);
                rate_unit.setVisibility(View.GONE);
            }
        }

        if(requestCode==19)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                isFareQuotePressed=false;
                to_latitude=null;
                to_longitude=null;
                mDROPOFF_ADDRESS=null;
                Dropoff_Location_Address.setHint(getResources().getString(R.string.addDropOfLocation));
                pickup.setVisibility(View.GONE);//Akbar commented

                now_later_layout.setVisibility(View.GONE);

                currenct_location_button.setVisibility(View.VISIBLE);
                VariableConstants.CONFIRMATION_CALLED=false;
                mid_pointer.setVisibility(View.GONE);

                //rk
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                rl_action_bar_main.setVisibility(View.VISIBLE);
                relative_confirmation_navigation.setVisibility(View.GONE);
                drop_layout.setVisibility(View.GONE);
                rl_confirmation_Screen.setVisibility(View.GONE);

                now_later_layout.setVisibility(View.VISIBLE);
                pickup.setVisibility(View.GONE);
                search_or_ring_iv.setImageResource(R.drawable.home_search_icon_off);

                if(!session.getBookingReponseFromPubnub().equals(""))
                {
                    methodAfterGettingResponse(session.getBookingReponseFromPubnub());
                }
                Utility.printLog("inside RESULT_OK true "+session.getBookingReponseFromPubnub());
            }
            else
            {
                Utility.printLog("inside RESULT_OK false");
                isFareQuotePressed=false;
                to_latitude=null;
                to_longitude=null;
                mDROPOFF_ADDRESS=null;
                Dropoff_Location_Address.setHint(getResources().getString(R.string.addDropOfLocation));
                pickup.setVisibility(View.GONE);//Akbar commented
                all_types_layout.setVisibility(View.GONE);

                currenct_location_button.setVisibility(View.VISIBLE);


                now_later_layout.setVisibility(View.VISIBLE);
                VariableConstants.CONFIRMATION_CALLED=false;

                //rk
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                rl_action_bar_main.setVisibility(View.VISIBLE);
                relative_confirmation_navigation.setVisibility(View.GONE);
                drop_layout.setVisibility(View.GONE);
                rl_confirmation_Screen.setVisibility(View.GONE);
                now_later_layout.setVisibility(View.VISIBLE);
                pickup.setVisibility(View.GONE);//Akbar commented
                search_or_ring_iv.setImageResource(R.drawable.home_search_icon_off);

                setHasOptionsMenu(false);
                session.setDriverOnWay(false);
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);

                Dialog popupWithSingleButton=Utility.showPopupWithOneButton(getActivity());
                TextView title_popup= (TextView) popupWithSingleButton.findViewById(R.id.title_popup);
                TextView text_for_popup= (TextView) popupWithSingleButton.findViewById(R.id.text_for_popup);
                TextView yes_button= (TextView) popupWithSingleButton.findViewById(R.id.yes_button);
                popupWithSingleButton.setCancelable(false);
                yes_button.setText(getResources().getString(R.string.ok));
                title_popup.setVisibility(View.GONE);
                text_for_popup.setText(data.getStringExtra("MESSAGE"));
                popupWithSingleButton.show();
                Utility.printLog("inside RESULT_OK false "+session.getBookingReponseFromPubnub());
            }
        }


        if(requestCode==20)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Utility.printLog("inside RESULT_OK true");
                //getActivity().getActionBar().show();
                isFareQuotePressed=false;
                to_latitude=null;
                to_longitude=null;
                mDROPOFF_ADDRESS=null;
                Dropoff_Location_Address.setHint(getResources().getString(R.string.addDropOfLocation));
                pickup.setVisibility(View.GONE);//Akbar commented
                Fare_Quote.setVisibility(View.INVISIBLE);

                laterBookingDate = null;//after completing the booking resetting the booking date
                Utility.ShowAlert(data.getStringExtra("LaterMsg"), getActivity());
            }
            else
            {
                Utility.printLog("inside RESULT_OK true");
                //getActivity().getActionBar().show();
                isFareQuotePressed=false;
                to_latitude=null;
                to_longitude=null;
                mDROPOFF_ADDRESS=null;
                Dropoff_Location_Address.setHint(getResources().getString(R.string.addDropOfLocation));
                pickup.setVisibility(View.GONE);//Akbar commented
                Fare_Quote.setVisibility(View.INVISIBLE);


                laterBookingDate = null;//after completing the booking resetting the booking date
            }
        }

        if(requestCode==786)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Utility.printLog("i am inside 786 ");
                String current_address_local=data.getStringExtra("CURRENT_ADDRESS");
                Double curr_lat=data.getDoubleExtra("CURR_LAT",0);
                Double curr_long=data.getDoubleExtra("CURR_LONG",0);
                String address_desc=data.getStringExtra("ADDRESS_DESC");
                Utility.printLog("address from fav address "+current_address_local+" lat "+curr_lat+" long "+curr_long
                        +" descr "+address_desc+" id "+data.getIntExtra("ID",0));
                databaseId=data.getStringExtra("ID");
                from_latitude=curr_lat+"";
                from_longitude=curr_long+"";
                currentLatitude =curr_lat;
                currentLongitude =curr_long;

                Appointment_location.setText(address_desc);
                current_address.setText(current_address_local);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(googleMap!=null)
        {
            this.googleMap=googleMap;
            googleMap.setMyLocationEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setTiltGesturesEnabled(true);
            googleMap.setMyLocationEnabled(true);

            googleMap.setOnCameraMoveStartedListener(this);
            googleMap.setOnCameraIdleListener(this);

            Utility.printLog("current latlong "+session.getCurrentLat()+" "+session.getCurrentLong());
            LatLng latLng=new LatLng(session.getCurrentLat(),session.getCurrentLong());
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));

            googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition arg0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            IsreturnFromSearch = false;
                            isMapMoved=true;
                        }
                    }, 5000);
                }
            });
        }
    }

    @Override
    public void onCameraMoveStarted(int reason)
    {
        if(isAdded())
        {
            /**
             * to make the variable true if user gestured on map
             * then make it true and start the animation
             */
            if(reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE)
            {
                checkForFirstTime=true;
            }

            /**
             * if the user is not in booking and its opened first time
             * animate the elemnts on the map
             */
            if(!(session.isDriverOnWay() || session.isDriverOnArrived() || session.isTripBegin()) && checkForFirstTime)
            {
                toCheckAnimation=true;
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    pubnubProgressDialog.setClickable(false);
                    pubnubProgressDialog.setVisibility(VISIBLE);
                    shadow_icon.setVisibility(VISIBLE);
                    pickup_Distance.setVisibility(View.GONE);
                    rate_unit.setVisibility(View.GONE);
                    /**
                     * animation for the action bar and layout
                     */
                    Animation action_bar_slide_up=AnimationUtils.loadAnimation(getActivity(),R.anim.action_bar_slide_up);
                    rl_address.startAnimation(action_bar_slide_up);
                    Animation animation = new TranslateAnimation(0,0,0, -50);
                    animation.setDuration(100);
                    animation.setFillAfter(true);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_API_ANIMATION) {
                    pubnubProgressDialog.setClickable(false);
                    pubnubProgressDialog.setVisibility(VISIBLE);
                    shadow_icon.setVisibility(VISIBLE);
                    pickup_Distance.setVisibility(View.GONE);
                    rate_unit.setVisibility(View.GONE);
                    /**
                     * animation for the action bar and layout
                     */
                    Animation action_bar_slide_up=AnimationUtils.loadAnimation(getActivity(),R.anim.action_bar_slide_up);
                    rl_address.startAnimation(action_bar_slide_up);
                    Animation animation = new TranslateAnimation(0,0,0, -50);
                    animation.setDuration(100);
                    animation.setFillAfter(true);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_DEVELOPER_ANIMATION) {
                    pubnubProgressDialog.setClickable(false);
                    pubnubProgressDialog.setVisibility(VISIBLE);
                    shadow_icon.setVisibility(VISIBLE);
                    pickup_Distance.setVisibility(View.GONE);
                    rate_unit.setVisibility(View.GONE);
                    /**
                     * animation for the action bar and layout
                     */
                    Animation action_bar_slide_up=AnimationUtils.loadAnimation(getActivity(),R.anim.action_bar_slide_up);
                    rl_address.startAnimation(action_bar_slide_up);
                    Animation animation = new TranslateAnimation(0,0,0, -50);
                    animation.setDuration(100);
                    animation.setFillAfter(true);
                    pubnubProgressDialog.startAnimation(animation);
                }
            }
        }
    }

    @Override
    public void onCameraIdle() {
        if(!(session.isDriverOnWay() || session.isDriverOnArrived() || session.isTripBegin()) && checkForFirstTime)
        {
            //to stop the animation
            toCheckAnimation=false;
            Animation animation = new TranslateAnimation(0,0,-50, 0);
            animation.setDuration(100);
            animation.setFillAfter(true);
            pubnubProgressDialog.startAnimation(animation);
            Animation action_bar_slide_down=AnimationUtils.loadAnimation(getActivity(),R.anim.action_bar_slide_down);
            rl_address.startAnimation(action_bar_slide_down);
            shadow_icon.setVisibility(View.GONE);

            /**
             * if the distance moved is more than the radius from backend
             * then call pubnub to get data
             */
            if(myTimer_publish!=null)
            {
                myTimer_publish.cancel();
                myTimer_publish=null;
                /**
                 * to publish the user latlongs to server to get the data from server with 0 delay
                 * if publishing is not yet started
                 */
                startPublishingWithTimer(0);
            }

        }
    }

    //plotting the line for the tracking
    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
            }
            return data;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }
    /**
     * To get the directions url between pickup location and drop location
     */
    private String getMapsApiDirectionsFromTourl(String destLat,String destLong) {
        Utility.printLog("getMapsApiDirectionsFromTourl HomePageFragment Driver reached  ");
        Utility.printLog("getDropofflat=" + session.getDropofflat()
                + " getDropofflng=" + session.getDropofflng());
        String url="";
        if(!session.getDropofflat().equals("") && !session.getDropofflng().equals(""))
        {
            LatLng latLngDrop = new LatLng(Double.parseDouble(session
                    .getDropofflat()), Double.parseDouble(session.getDropofflng()));
            if(googleMap!=null)
            {
                googleMap.addMarker(new MarkerOptions().position(latLngDrop)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.home_markers_dropoff)));
                /*
                 * String waypoints = "waypoints=optimize:true|" +
                 * session.getPickuplat() + "," +session.getPickuplng() + "|" + "|" +
                 * session.getDropofflat() + "," + session.getDropofflng();
                 */
                String waypoints = "origin=" + destLat + ","
                        + destLong + "&" + "destination="
                        + session.getPickuplat() + "," +  session.getPickuplng();

                String sensor = "sensor=false";
                String params = waypoints + "&" + sensor;
                String output = "json";
                url = "https://maps.googleapis.com/maps/api/directions/"
                        + output + "?" + params;
            }
        }
        return url;
    }

    /**
     * plotting the directions in google map
     */
    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            Utility.printLog("HomePageFragment Driver on the way onPostExecute");
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;
            // traversing through routes
            if(routes!=null)
            {
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    polyLineOptions.addAll(points);
                    polyLineOptions.width(8);
                    polyLineOptions.geodesic(true);
                    polyLineOptions.color(colourForPathPlot);
                }
                if (polyLineOptions != null)
                    polylineFinal=googleMap.addPolyline(polyLineOptions);
            }
        }
    }

    /**
     * to ge the fare estimate from pick to drop
     */
    private void getFareEstimate()
    {
        final ProgressDialog dialogL;
        dialogL=com.threembed.utilities.Utility.GetProcessDialog(getActivity());

        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_type_id",session.getCarTypeId());
            if(all_car_types!=null)
                jsonObject.put("ent_type_Ids",all_car_types);
            jsonObject.put("ent_curr_lat", String.valueOf(currentLatitude));
            jsonObject.put("ent_curr_long", String.valueOf(currentLongitude));
            //if driver is not in booking then send the locations from homepage in idle state
            if(!(session.isDriverOnWay() || session.isDriverOnArrived() || session.isTripBegin()))
            {
                jsonObject.put("ent_from_lat", from_latitude);
                jsonObject.put("ent_from_long", from_longitude);
                jsonObject.put("ent_to_lat", to_latitude);
                jsonObject.put("ent_to_long", to_longitude);
            }
            //is driver is in booking then send the location when use ris in booiking
            else
            {
                jsonObject.put("ent_from_lat", session.getPickuplat());
                jsonObject.put("ent_from_long", session.getPickuplng());
                jsonObject.put("ent_to_lat", session.getDropofflat());
                jsonObject.put("ent_to_long", session.getDropofflng());
            }
            jsonObject.put("ent_date_time", curenttime);
            Utility.printLog("fare estimate params "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"fareCalculatorforTypes", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                dialogL.dismiss();
                if(result!=null)
                {
                    Gson gson=new Gson();
                    FareCalculation fare_resp = gson.fromJson(result, FareCalculation.class);
                    if(fare_resp.getErrFlag().equals("0"))
                    {
                        Fare_Quote.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
                        Fare_Quote.setText(getResources().getString(R.string.currencuSymbol)+" "+fare_resp.getFare());
                        calculated_fare=fare_resp.getFare();
                        fare_for_all_cartypes = fare_resp.getData();
                    }
                    else
                    {
                        Utility.showToast(getActivity(),fare_resp.getErrMsg());
                    }
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
                Utility.printLog("fare estimate in home page "+result);
            }

            @Override
            public void onError(String error)
            {
                if (dialogL != null) {
                    dialogL.dismiss();
                }
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private String getDurationString(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        return twoDigitString(hours)+"H" + ":" + twoDigitString(minutes)+"M" + ":" + twoDigitString(seconds)+"S";
    }

    private String twoDigitString(int number) {
        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void BackgroundSubmitReview(String positiveFeedback, final Dialog dialog)
    {
        jsonObject=new JSONObject();
        if(dialogL!=null)
        {
            dialogL.show();
        }
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_dri_email", session.getDriverEmail());
            jsonObject.put("ent_booking_id", session.getBookingId());
            jsonObject.put("ent_rating_num", ratings+"");
            jsonObject.put("ent_mid", session.getMid());
            jsonObject.put("ent_positive_review", "");
            jsonObject.put("ent_review_msg",positiveFeedback);
            jsonObject.put("ent_date_time",curenttime);
            Utility.printLog("params to submit review "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"updateSlaveReview", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String response)
            {
                String errFlag = null,errMsg = null,errNum=null;
                Utility.printLog( "success for updated the language review " + response);
                if(response!=null)
                {
                    if (dialogL!=null)
                    {
                        dialogL.dismiss();
                        dialogL = null;
                    }
                    session.setBookingReponseFromPubnub("");
                    session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                    try
                    {
                        jsonObject=new JSONObject(response);
                        errFlag=jsonObject.getString("errFlag");
                        errMsg=jsonObject.getString("errMsg");
                        errNum=jsonObject.getString("errNum");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utility.showToast(getActivity(),e.getMessage());
                    }
                    if(errNum.equals("6") || errNum.equals("7") ||
                            errNum.equals("94") || errNum.equals("96"))
                    {
                        Utility.showToast(getActivity(),errMsg);
                        Intent i = new Intent(getActivity(), SplashActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        getActivity().startActivity(i);
                    }
                    if(errFlag.equals("0"))
                    {
                        invoiceDialogue.dismiss();
                        dialog.dismiss();
                        session.setComments("");
                        session.storeBookingId("");
                        session.storeAptDate(null);
                        session.setInvoiceRaised(false);
                        session.setDriverArrived(false);
                        session.setTripBegin(false);
                        session.setDriverOnWay(false);
                        publishingTag=1;

                        List<String> subscribed_channels = pubnub.getSubscribedChannels();
                        if(!subscribed_channels.contains(session.getChannelName()))
                        {
                            pubnub.subscribe()
                                    .channels(Arrays.asList(session.getChannelName())) // subscribe to channel groups
                                    .withPresence()// also subscribe to related presence information
                                    .execute();
                        }
                        Utility.showToast(getActivity(),getResources().getString(R.string.bookingCompleted));
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                    else
                    {
                        session.storeAptDate(null);
                        session.storeBookingId("");
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setTitle(getResources().getString(R.string.error));
                        alertDialogBuilder
                                .setMessage(errMsg)
                                .setCancelable(false)
                                .setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog,int id)
                                    {
                                        dialog.cancel();
                                        Intent i = new Intent(getActivity(), MainActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.ShowAlert(getResources().getString(R.string.network_connection_fail), getActivity());
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        networkUtil.stop_Location_Update();
        Utility.printLog("on ause called ");
        getActivity().unregisterReceiver(receiver);
        visibility=false;
        if(myTimer_publish!=null)
        {
            myTimer_publish.cancel();
            myTimer_publish=null;
        }
        publishingTag=1;

        if(mandatoryFieldPopup!=null)
            if (mandatoryFieldPopup.isShowing())
                mandatoryFieldPopup.dismiss();

        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer=null;
        }
        pubnub.unsubscribeAll();
        pubnub.removeListener(subscribeCallback);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Utility.printLog("CONTROL INSIDE onDestroy");
        visibility=false;
        VariableConstants.isComingFromSearch=false;
        VariableConstants.isComingFromScroll=false;
        session.setCurrentLat((float) lat);
        session.setCurrentLong((float) longi);
        if(networkUtil!=null)
            networkUtil.stop_Location_Update();
    }

    private boolean isGpsEnabled()
    {
        LocationManager service = (LocationManager) mActivity.getSystemService(mActivity.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private void showLocationAlert(){
        final Dialog dialog= new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.location_alert_layout);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        setLanguageForDialog(dialog);
        Button turn_on_location_btn= (Button) dialog.findViewById(R.id.turn_on_location_btn);
        Button enter_pickup_loction_btn= (Button) dialog.findViewById(R.id.enter_pickup_loction_btn);
        turn_on_location_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        enter_pickup_loction_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(!isGpsEnabled()){
            showLocationAlert();
        }
        /**
         * to get the current location
         */
        getCurrentLocation();
        Utility.printLog("pubnub response in session "+session.getPubnubResponse());
        Utility.printLog("pubnub response in booking session "+session.getBookingReponseFromPubnub());
        Utility.printLog("pubnub response in booking nonbooking "+session.getPubnubResponseForNonbooking());
        visibility=true;
        NotificationManager notificationManager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        if(!VariableConstants.PUSH_MESSAGE.equals(""))
        {
            showPopupForOneButton(VariableConstants.PUSH_MESSAGE);
            VariableConstants.PUSH_MESSAGE="";
        }

        if(notificationManager!=null)
        {
            notificationManager.cancelAll();
        }

        getActivity().registerReceiver(receiver, filter);

        Utility.printLog("onResume session.isdriverOnWay() " + session.isDriverOnWay());
        Utility.printLog("onResume session.isdriverOnArrived() " + session.isDriverOnArrived());
        Utility.printLog("onResume session.isInvoiceRaised() " + session.isInvoiceRaised());
        Utility.printLog("onResume session.isTripBegin() " + session.isTripBegin());

        /**
         * to stop the animating the map to current location
         */
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run()
            {
                if(myTimer==null)
                {
                    myTimer = new Timer();
                    TimerTask myTimerTask = new TimerTask() {
                        @Override
                        public void run() {
                            if (getActivity() != null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        VisibleRegion visibleRegion = googleMap.getProjection()
                                                .getVisibleRegion();
                                        Point x1 = googleMap.getProjection().toScreenLocation(
                                                visibleRegion.farRight);
                                        Point y = googleMap.getProjection().toScreenLocation(
                                                visibleRegion.nearLeft);
                                        Point centerPoint = new Point(x1.x / 2, y.y / 2);
                                        LatLng centerFromPoint = googleMap.getProjection().fromScreenLocation(centerPoint);

                                        Utility.printLog("ltlong from center of map "+centerFromPoint.latitude+" "+centerFromPoint.longitude);
                                        Utility.printLog("ltlong from center of map rouded "+String.format("%.3f", centerFromPoint.latitude)+" "+String.format("%.3f", centerFromPoint.longitude));
                                        Utility.printLog("ltlong from center of map current rouded "+String.format("%.3f", lat)+" "+String.format("%.3f", longi));
                                        Utility.printLog("ltlong from center of map current "+lat+" "+longi);

                                        double lat = centerFromPoint.latitude;
                                        double lon = centerFromPoint.longitude;

                                        if ((lat == currentLatitude && lon == currentLongitude)) {
                                            Utility.printLog("Update Address: FALSE");
                                        } else {
                                            Utility.printLog("on location change car id=" + Car_Type_Id);
                                            Utility.printLog("sss PUBLISHING a:3...");
                                            if (lat != 0.0 && lon != 0.0) {
                                                Utility.printLog("Update Address: TRUE" + Car_Type_Id);
                                                currentLatitude = lat;
                                                currentLongitude = lon;
                                                from_latitude = String.valueOf(lat);
                                                from_longitude = String.valueOf(lon);

                                                String[] params = new String[]{"" + lat, "" + lon};
                                                if (isAdded()) {
                                                    if (!IsreturnFromSearch) {
                                                        Utility.printLog("here addres search start");
                                                        if(!(session.isDriverOnWay() || session.isDriverOnArrived() || session.isTripBegin()))
                                                            new BackgroundGetAddress().execute(params);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    };
                    myTimer.schedule(myTimerTask, 0, 1000);
                }
            }
        }, 2000);


        if(!session.getPubnubResponse().equals("") && !session.isDriverOnWay() && !session.isDriverOnArrived())
        {
            methodAfterGettingResponse(session.getPubnubResponse());
        }

        if(!session.getBookingReponseFromPubnub().equals("") )
        {
            methodAfterGettingResponse(session.getBookingReponseFromPubnub());
        }
        else
        {
            if(!session.getPubnubResponseForNonbooking().equals(""))
                methodAfterGettingResponse(session.getPubnubResponseForNonbooking());
        }

        new BackgroundSubscribeMyChannel().execute();

        /**
         * getting the message in channel
         */
        pubnub.addListener(subscribeCallback);

        /**
         * if the user comes from splash screen then he is logged in user
         * so  publish to server after defined interval
         */
        Utility.printLog("is it from spalsh "+activity.isFromSplash);
        if(activity.isFromSplash)
        {
            /**
             * to publish the user latlongs to server to get the data from server with after delay mentioned in config
             * if publishing is not yet started
             */
            if(myTimer_publish==null)
                startPublishingWithTimer(session.getPubnubIntervalForHome()*1000);
        }
        else {
            /**
             * to publish the user latlongs to server to get the data from server with 0 delay
             * if publishing is not yet started
             */
            if(myTimer_publish==null)
                startPublishingWithTimer(0);
        }
    }

    private void onTheWayUIUpdate()
    {
        Utility.printLog("onResume INSIDE ON_THE_WAY");
        pickup.setVisibility(VISIBLE);
        if(myTimer_publish!=null)
        {
            myTimer_publish.cancel();
            myTimer_publish=null;
        }
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer=null;
        }
        setHasOptionsMenu(false);
        Utility.printLog("Appointments details inside isDriverOnWay");

        try
        {
            if(driverMarker!=null)
            {
                if(driverMarker.getmMarker()!=null)
                    driverMarker.getmMarker().remove();
            }
        }
        catch (IllegalArgumentException e)
        {
            Utility.printLog("exception in removing marker "+e);
        }

        if(polylineFinal!=null)
        {
            polylineFinal.remove();
        }

        if(!session.getBookingReponseFromPubnub().equals(""))
        {
            Utility.printLog("response from sesssions "+session.getBookingReponseFromPubnub());
            Gson gson=new Gson();
            getAppointmentDetails=gson.fromJson(session.getBookingReponseFromPubnub(),GetAppointmentDetails.class);
            session.storePickuplat(getAppointmentDetails.getData().getPickLat());
            session.storePickuplng(getAppointmentDetails.getData().getPickLong());
            session.storeDropofflat(getAppointmentDetails.getData().getDesLat());
            session.storeDropofflng(getAppointmentDetails.getData().getDesLong());
            session.setSelectedImage(getAppointmentDetails.getData().getCarMapImage());
            session.setCarImage(getAppointmentDetails.getData().getCarImage());
            session.storeDocPic(getAppointmentDetails.getData().getpPic());
            session.setPickUpAddress(getAppointmentDetails.getData().getAddr1());
            session.storeDriverRating(getAppointmentDetails.getData().getR());
            Driver_Car_Num.setText(""+getAppointmentDetails.getData().getPlateNo());
            session.storeDocName(getAppointmentDetails.getData().getfName()+" "+getAppointmentDetails.getData().getlName());
            session.storeAptDate(getAppointmentDetails.getData().getApptDt());
            session.storeDocPH(getAppointmentDetails.getData().getMobile());
            session.setMid(getAppointmentDetails.getData().getMid());

            if(session.getDocPic()!= null && !session.getDocPic().equalsIgnoreCase("null") && !session.getDocPic().equalsIgnoreCase(""))
            {
                /**
                 * to set the image into image view and
                 * add the write the image in the file
                 */
                Driver_Profile_Pic.setTag(setTarget(Driver_Profile_Pic));
                Picasso.with(getActivity()).load(session.getDocPic()).into((Target) Driver_Profile_Pic.getTag()) ;
            }
            Driver_Name.setText(getAppointmentDetails.getData().getMake()+" "+getAppointmentDetails.getData().getModel());
            drivsnd.setText(session.getDocName());
            if(!isContactDriver)
            {
                Rl_distance_time.setVisibility(VISIBLE);
            }

            Driver_on_the_way_txt.setVisibility(VISIBLE);
            current_location.setVisibility(VISIBLE);
            if(session.getDriverRating()!=null)
            {
                drivvehicalrating.setText(roundoff(session.getDriverRating()));
            }

            rl_action_bar_main.setVisibility(View.VISIBLE);
            relative_confirmation_navigation.setVisibility(View.GONE);
            drop_layout.setVisibility(View.VISIBLE);
            rl_confirmation_Screen.setVisibility(View.GONE);

            now_later_layout.setVisibility(View.GONE);
            pickup.setVisibility(View.GONE);
            search_or_ring_iv.setImageResource(R.drawable.ring_green);
            mid_pointer.setVisibility(View.GONE);

        }
        pickup.clearAnimation();
        pickup_Distance.clearAnimation();
        pubnubProgressDialog.clearAnimation();
        rate_unit.clearAnimation();

        pickup_Distance.setVisibility(View.GONE);
        rate_unit.setVisibility(View.GONE);
        pickup.setVisibility(View.GONE);
        pubnubProgressDialog.setVisibility(View.GONE);

        driver_call_and_cancel_ll.setVisibility(View.VISIBLE);
        driver_status_for_journey_tv.setVisibility(View.GONE);
    }

    private String roundoff(String val){
        if(val!=null && !val.equals("")) {
            double d = Double.parseDouble(val);
            DecimalFormat f = new DecimalFormat("#.#");
            System.out.println(f.format(d));
            return f.format(d);
        } else {
            return "";
        }
    }

    private void onArrivedUIUpdate()
    {
        Utility.printLog("onResume INSIDE Driver Arrived");
        if(myTimer_publish!=null)
        {
            myTimer_publish.cancel();
            myTimer_publish=null;
        }
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer=null;
        }
        setHasOptionsMenu(false);
        Utility.printLog("Appointments details inside isDriverOnArrived");

        try
        {
            if(googleMap!=null)
            {
                googleMap.clear();
            }
        }
        catch (IllegalArgumentException e)
        {
            Utility.printLog("googleMap.clear Catch reason ");
        }


        Gson gson=new Gson();
        getAppointmentDetails=gson.fromJson(session.getBookingReponseFromPubnub(),GetAppointmentDetails.class);
        Utility.printLog("data for arrived tatus "+session.getBookingReponseFromPubnub());
        session.storePickuplat(getAppointmentDetails.getData().getPickLat());
        session.storePickuplng(getAppointmentDetails.getData().getPickLong());
        session.storeDropofflat(getAppointmentDetails.getData().getDesLat());
        session.storeDropofflng(getAppointmentDetails.getData().getDesLong());
        session.setSelectedImage(getAppointmentDetails.getData().getCarMapImage());
        session.setCarImage(getAppointmentDetails.getData().getCarImage());
        session.storeDocPic(getAppointmentDetails.getData().getpPic());
        session.storeDriverRating(getAppointmentDetails.getData().getR());
        session.storeDocName(getAppointmentDetails.getData().getfName()+" "+getAppointmentDetails.getData().getlName());
        Driver_Car_Num.setText(""+getAppointmentDetails.getData().getPlateNo());
        session.storeAptDate(getAppointmentDetails.getData().getApptDt());
        session.storeDocPH(getAppointmentDetails.getData().getMobile());
        session.setMid(getAppointmentDetails.getData().getMid());

        if(session.getDocPic()!= null && !session.getDocPic().equalsIgnoreCase("null") && !session.getDocPic().equalsIgnoreCase(""))
        {
            /**
             * to set the image into image view and
             * add the write the image in the file
             */
            Driver_Profile_Pic.setTag(setTarget(Driver_Profile_Pic));
            Picasso.with(getActivity()).load(session.getDocPic()).into((Target) Driver_Profile_Pic.getTag()) ;
        }

        Driver_Name.setText(getAppointmentDetails.getData().getMake()+" "+getAppointmentDetails.getData().getModel());
        drivsnd.setText(session.getDocName());
        if(!isContactDriver)
        {
            Rl_distance_time.setVisibility(VISIBLE);
        }

        Driver_on_the_way_txt.setVisibility(VISIBLE);
        current_location.setVisibility(VISIBLE);
        Driver_on_the_way_txt.setText(getResources().getString(R.string.driverreached));
        if(session.getDriverRating()!=null) {
            drivvehicalrating.setText(roundoff(session.getDriverRating()));
        }
        Utility.printLog("visibility for the pin "+pickup.getVisibility());
        pickup.clearAnimation();
        pickup_Distance.clearAnimation();
        rate_unit.clearAnimation();
        pubnubProgressDialog.clearAnimation();


        pickup_Distance.setVisibility(View.GONE);
        rate_unit.setVisibility(View.GONE);
        pickup.setVisibility(View.GONE);
        pubnubProgressDialog.setVisibility(View.GONE);

        rl_action_bar_main.setVisibility(View.VISIBLE);
        relative_confirmation_navigation.setVisibility(View.GONE);
        drop_layout.setVisibility(View.VISIBLE);
        rl_confirmation_Screen.setVisibility(View.GONE);

        now_later_layout.setVisibility(View.GONE);
        pickup.setVisibility(View.GONE);
        search_or_ring_iv.setImageResource(R.drawable.ring_green);
        mid_pointer.setVisibility(View.GONE);

        driver_call_and_cancel_ll.setVisibility(View.GONE);
        driver_status_for_journey_tv.setVisibility(View.VISIBLE);
        driver_status_for_journey_tv.setBackgroundColor(getResources().getColor(R.color.red_color));
    }

    public void onJourneyStartedUIUpdate()
    {
        Utility.printLog("onResume INSIDE Driver Arrived");

        if(myTimer_publish!=null)
        {
            myTimer_publish.cancel();
            myTimer_publish=null;
        }
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer=null;
        }
        setHasOptionsMenu(false);
        Utility.printLog("Appointments details inside isTripBegin");

        try
        {
            if(googleMap!=null)
            {
                googleMap.clear();
            }
        }
        catch (IllegalArgumentException e)
        {
            Utility.printLog("googleMap.clear Catch reason ");
        }

        Gson gson=new Gson();
        getAppointmentDetails=gson.fromJson(session.getBookingReponseFromPubnub(),GetAppointmentDetails.class);

        session.storePickuplat(getAppointmentDetails.getData().getPickLat());
        session.storePickuplng(getAppointmentDetails.getData().getPickLong());
        session.storeDropofflat(getAppointmentDetails.getData().getDesLat());
        session.storeDropofflng(getAppointmentDetails.getData().getDesLong());
        session.setSelectedImage(getAppointmentDetails.getData().getCarMapImage());
        session.setCarImage(getAppointmentDetails.getData().getCarImage());
        session.storeDocPic(getAppointmentDetails.getData().getpPic());
        session.storeDriverRating(getAppointmentDetails.getData().getR());
        session.storeDocName(getAppointmentDetails.getData().getfName()+" "+getAppointmentDetails.getData().getlName());
        Driver_Car_Num.setText(""+getAppointmentDetails.getData().getPlateNo());
        session.storeAptDate(getAppointmentDetails.getData().getApptDt());
        session.storeDocPH(getAppointmentDetails.getData().getMobile());
        session.setMid(getAppointmentDetails.getData().getMid());

        if (!session.getDropofflat().equals("")
                && !session.getDropofflng().equals("")) {
            String url = getMapsApiDirectionsFromTourl(session.getDropofflat(),session.getDropofflng());
            Utility.printLog("getMapsApiDirectionsFromTourl ="
                    + url);
            if(!url.equals(""))
            {
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
            }
        }

        if(session.getDocPic()!= null && !session.getDocPic().equalsIgnoreCase("null") && !session.getDocPic().equalsIgnoreCase(""))
        {
            /**
             * to set the image into image view and
             * add the write the image in the file
             */
            Driver_Profile_Pic.setTag(setTarget(Driver_Profile_Pic));
            Picasso.with(getActivity()).load(session.getDocPic()).into((Target) Driver_Profile_Pic.getTag()) ;
        }
        Driver_Name.setText(getAppointmentDetails.getData().getMake()+" "+getAppointmentDetails.getData().getModel());
        drivsnd.setText(session.getDocName());
        cancel_trip.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);

        Driver_on_the_way_txt.setVisibility(VISIBLE);
        current_location.setVisibility(VISIBLE);
        Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
        if(!isContactDriver)
        {
            Rl_distance_time.setVisibility(VISIBLE);
        }
        if(session.getDriverRating()!=null)
        {
            drivvehicalrating.setText(roundoff(session.getDriverRating()));
        }
        pickup.clearAnimation();
        pickup_Distance.clearAnimation();
        rate_unit.clearAnimation();
        pubnubProgressDialog.clearAnimation();

        pickup_Distance.setVisibility(View.GONE);
        rate_unit.setVisibility(View.GONE);
        pickup.setVisibility(View.GONE);
        pubnubProgressDialog.setVisibility(View.GONE);


        rl_action_bar_main.setVisibility(View.VISIBLE);
        relative_confirmation_navigation.setVisibility(View.GONE);
        drop_layout.setVisibility(View.VISIBLE);
        rl_confirmation_Screen.setVisibility(View.GONE);

        now_later_layout.setVisibility(View.GONE);
        pickup.setVisibility(View.GONE);
        search_or_ring_iv.setImageResource(R.drawable.ring_green);
        mid_pointer.setVisibility(View.GONE);
        Driver_on_the_way_txt.setText(getResources().getString(R.string.journeystarted));
        driver_call_and_cancel_ll.setVisibility(View.GONE);
        driver_status_for_journey_tv.setVisibility(View.VISIBLE);
        driver_status_for_journey_tv.setBackgroundColor(getResources().getColor(R.color.green));
    }

    public void showInvoiceDialog(final InvoiceResponse invoiceResponse)
    {
        session.storeAptDate(invoiceResponse.getData().getApptDt());
        invoiceDialogue = new Dialog(getActivity());
        invoiceDialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        invoiceDialogue.setContentView(R.layout.invoice_popup2);
        invoiceDialogue.setCancelable(false);
        invoiceDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        invoiceDialogue.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        /**
         * to set language surrport for dialog
         */
        setLanguageForDialog(invoiceDialogue);

        RatingBar ratingBar = (RatingBar) invoiceDialogue.findViewById(R.id.invoice_driver_rating);
        Utility.ratingBarColorChanger(ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Utility.printLog("rating rates in rating bar on change"+ratingBar.getRating());
                ratings=ratingBar.getRating();
                if(rating<=3){

                    final Dialog feedback_Dialogue;
                    feedback_Dialogue = new Dialog(getActivity());
                    feedback_Dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    feedback_Dialogue.setContentView(R.layout.what_went_wrog);
                    feedback_Dialogue.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    feedback_Dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                    /**
                     * to set language surrport for dialog
                     */
                    setLanguageForDialog(feedback_Dialogue);

                    final Button done_set_reson_btn = (Button) feedback_Dialogue.findViewById(R.id.done_set_reson_btn);
                    done_set_reson_btn.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            feedback_Dialogue.dismiss();
                        }
                    });

                    reasons_list_for_review = (RecyclerView) feedback_Dialogue.findViewById(R.id.reasons_list);

                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    reasons_list_for_review.setLayoutManager(mLayoutManager);
                    reasons_list_for_review.setItemAnimator(new DefaultItemAnimator());

                    whatWentWrong=new WhatWentWrong(getActivity(),negFbrsnList);
                    reasons_list_for_review.setAdapter(whatWentWrong);

                    feedback_Dialogue.show();
                }
            }
        });


        ImageView car_info_iv = (ImageView) invoiceDialogue.findViewById(R.id.car_info_iv);
        final EditText leave_comment_et = (EditText) invoiceDialogue.findViewById(R.id.leave_comment_et);
        TextView amount = (TextView) invoiceDialogue.findViewById(R.id.toatal_amount_tv);
        TextView pickup_title_tv = (TextView) invoiceDialogue.findViewById(R.id.pickup_title_tv);
        TextView drop_off_title_tv = (TextView) invoiceDialogue.findViewById(R.id.drop_off_title_tv);
        TextView pickupaddresstv = (TextView) invoiceDialogue.findViewById(R.id.pickup_value_tv);
        TextView dropaddresstv = (TextView) invoiceDialogue.findViewById(R.id.drop_off_value_tv);
        TextView rideDate = (TextView) invoiceDialogue.findViewById(R.id.last_trip_date_value_tv);
        Button submit_review_btn = (Button) invoiceDialogue.findViewById(R.id.submit_review_btn);
        TextView receipt = (TextView) invoiceDialogue.findViewById(R.id.view_reciept_tv);
        session.setPickUpAddress(invoiceResponse.getData().getAddr1());
        dropaddresstv.setSelected(true);
        pickupaddresstv.setSelected(true);

        if(invoiceResponse.getData().getInvoice().getPickupDt()!=null && !invoiceResponse.getData().getInvoice().getPickupDt().equals(""))
            pickup_title_tv.setText(""+getString(R.string.pickup1)+" ( "+Utility.getTimeFormated(invoiceResponse.getData().getInvoice().getPickupDt().trim())+" )");

        if(invoiceResponse.getData().getInvoice().getDropDt()!=null && !invoiceResponse.getData().getInvoice().getDropDt().equals(""))
            drop_off_title_tv.setText(""+getString(R.string.dropoff)+" ( "+Utility.getTimeFormated(invoiceResponse.getData().getInvoice().getDropDt().trim())+" )");

        String amt = getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getSubTotal()));
        Utility.printLog("subtotal_obtained "+amt);
        amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getSubTotal())));

        //to set the amount in invoice
        if(invoiceResponse.getData().getInvoice().getPayType().equals("2"))
        {
            if(!invoiceResponse.getData().getInvoice().getCashCollected().equals(""))
            {
                amount.setText(getResources().getString(R.string.currencuSymbol) + " " +
                        round(Double.parseDouble(invoiceResponse.getData().getInvoice().getCashCollected())));

            }
        }
        else
        {
            amount.setText(getResources().getString(R.string.currencuSymbol) + " " +
                    round(Double.parseDouble(invoiceResponse.getData().getInvoice().getCardDeduct())));
        }

        submit_review_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(getActivity()))
                {
                    //aakash change
                    if(ratings!=0) {
                        String leave_comment = "";
                        if (!leave_comment_et.getText().toString().isEmpty())
                            leave_comment = leave_comment_et.getText().toString();
                        BackgroundSubmitReview(leave_comment, invoiceDialogue);
                    }else{
                        Utility.showToast(getActivity(),getResources().getString(R.string.please_rate_driver));
                    }
                } else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
        });




        car_info_iv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog driver_info_Dialogue;
                driver_info_Dialogue = new Dialog(getActivity());
                driver_info_Dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
                driver_info_Dialogue.setContentView(R.layout.driver_info_layout);
                driver_info_Dialogue.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                driver_info_Dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                /**
                 * to set language surrport for dialog
                 */
                setLanguageForDialog(driver_info_Dialogue);

                TextView driver_name_value_tv = (TextView) driver_info_Dialogue.findViewById(R.id.driver_name_value_tv);
                TextView car_number_value_tv = (TextView) driver_info_Dialogue.findViewById(R.id.car_number_value_tv);
                TextView driver_license_value_tv = (TextView) driver_info_Dialogue.findViewById(R.id.driver_license_value_tv);
                TextView trip_time_value_tv = (TextView) driver_info_Dialogue.findViewById(R.id.trip_time_value_tv);
                TextView trip_distance_value_tv = (TextView) driver_info_Dialogue.findViewById(R.id.trip_distance_value_tv);

                String name="";
                if(invoiceResponse.getData().getfName()!=null && !invoiceResponse.getData().getfName().equals(""))
                    name = invoiceResponse.getData().getfName().trim();

                if(invoiceResponse.getData().getlName()!=null && !invoiceResponse.getData().getlName().equals(""))
                    name = name+" "+invoiceResponse.getData().getlName().trim();

                driver_name_value_tv.setText(Utility.nameFormator(name)
                );

                if(invoiceResponse.getData().getPlateNo()!=null && !invoiceResponse.getData().getPlateNo().equals(""))
                    car_number_value_tv.setText(invoiceResponse.getData().getPlateNo().trim());

                if(invoiceResponse.getData().getMls_issued_ptc_driver_licence()!=null && !invoiceResponse.getData().getMls_issued_ptc_driver_licence().equals(""))
                    driver_license_value_tv.setText(invoiceResponse.getData().getMls_issued_ptc_driver_licence().trim());

                if(invoiceResponse.getData().getInvoice().getDur()!=null && !invoiceResponse.getData().getInvoice().getDur().equals(""))
                    trip_time_value_tv.setText(invoiceResponse.getData().getInvoice().getDur().trim());

                if(invoiceResponse.getData().getInvoice().getDis()!=null && !invoiceResponse.getData().getInvoice().getDis().equals(""))
                    trip_distance_value_tv.setText(invoiceResponse.getData().getInvoice().getDis().trim());


                TextView done_tv = (TextView) driver_info_Dialogue.findViewById(R.id.done_tv);
                done_tv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        driver_info_Dialogue.dismiss();
                    }
                });

                driver_info_Dialogue.show();

            }
        });

        receipt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog receiptDialogue;
                receiptDialogue = new Dialog(getActivity());
                receiptDialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
                receiptDialogue.setContentView(R.layout.receipt_layout);
                receiptDialogue.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                receiptDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                /**
                 * to set language surrport for dialog
                 */
                setLanguageForDialog(receiptDialogue);

                TextView meterFee = (TextView) receiptDialogue.findViewById(R.id.meterFee);
                TextView airportValue = (TextView) receiptDialogue.findViewById(R.id.airportValue);
                TextView invoice_discount_amount = (TextView) receiptDialogue.findViewById(R.id.invoice_discount_amount);
                TextView invoice_total_amount = (TextView) receiptDialogue.findViewById(R.id.invoice_total_amount);
                Button submitButton = (Button) receiptDialogue.findViewById(R.id.submitButton);
                TextView distanceValue = (TextView) receiptDialogue.findViewById(R.id.distanceValue);
                TextView timeValue = (TextView) receiptDialogue.findViewById(R.id.timeValue);
                TextView title = (TextView) receiptDialogue.findViewById(R.id.title);
                TextView bookingId = (TextView) receiptDialogue.findViewById(R.id.bookingId);
                TextView distanceText= (TextView) receiptDialogue.findViewById(R.id.distanceText);
                TextView timeText= (TextView) receiptDialogue.findViewById(R.id.timeText);
                TextView invoice_subtotal_txt= (TextView) receiptDialogue.findViewById(R.id.invoice_subtotal_txt);
                TextView airportText= (TextView) receiptDialogue.findViewById(R.id.airportText);
                TextView invoice_discount_txt= (TextView) receiptDialogue.findViewById(R.id.invoice_discount_txt);
                TextView wallet_txt= (TextView) receiptDialogue.findViewById(R.id.wallet_txt);
                TextView wallet_amt= (TextView) receiptDialogue.findViewById(R.id.wallet_amt);
                TextView back_button= (TextView) receiptDialogue.findViewById(R.id.back_button);
                TextView cash_collected_amt= (TextView) receiptDialogue.findViewById(R.id.cash_collected_amt);
                TextView cash_collected_txt= (TextView) receiptDialogue.findViewById(R.id.cash_collected_txt);
                TextView invoice_total_txt= (TextView) receiptDialogue.findViewById(R.id.invoice_total_txt);
                TextView added_or_removed_wallet_txt= (TextView) receiptDialogue.findViewById(R.id.added_or_removed_wallet_txt);
                TextView added_or_removed_wallet_amt= (TextView) receiptDialogue.findViewById(R.id.added_or_removed_wallet_amt);
                RelativeLayout invoice_discount_layout= (RelativeLayout) receiptDialogue.findViewById(R.id.invoice_discount_layout);
                RelativeLayout airportLayout= (RelativeLayout) receiptDialogue.findViewById(R.id.airportLayout);
                RelativeLayout wallet_layout= (RelativeLayout) receiptDialogue.findViewById(R.id.wallet_layout);
                RelativeLayout added_or_removed_wallet= (RelativeLayout) receiptDialogue.findViewById(R.id.added_or_removed_wallet);
                TextView waiting_time= (TextView) receiptDialogue.findViewById(R.id.waiting_time);
                TextView waiting_time_text= (TextView) receiptDialogue.findViewById(R.id.waiting_time_text);
                TextView payment_method_text= (TextView) receiptDialogue.findViewById(R.id.payment_method_text);

                RelativeLayout waiting_time_layout= (RelativeLayout) receiptDialogue.findViewById(R.id.waiting_time_layout);
                RelativeLayout distanceLayout= (RelativeLayout) receiptDialogue.findViewById(R.id.distanceLayout);
                RelativeLayout timeLayout= (RelativeLayout) receiptDialogue.findViewById(R.id.timeLayout);
                RelativeLayout cash_collected_layout= (RelativeLayout) receiptDialogue.findViewById(R.id.cash_collected_layout);
                RelativeLayout invoice_total_layout= (RelativeLayout) receiptDialogue.findViewById(R.id.invoice_total_layout);

                /**
                 * to se the typeface
                 */
                if(session.getLanguageCode().equals("en"))
                {
                    Utility.setTypefaceMuliRegular(getActivity(),meterFee);
                    Utility.setTypefaceMuliRegular(getActivity(),payment_method_text);
                    Utility.setTypefaceMuliRegular(getActivity(),waiting_time_text);
                    Utility.setTypefaceMuliRegular(getActivity(),waiting_time);
                    Utility.setTypefaceMuliRegular(getActivity(),added_or_removed_wallet_amt);
                    Utility.setTypefaceMuliRegular(getActivity(),added_or_removed_wallet_txt);
                    Utility.setTypefaceMuliRegular(getActivity(),cash_collected_txt);
                    Utility.setTypefaceMuliRegular(getActivity(),cash_collected_amt);
                    Utility.setTypefaceMuliRegular(getActivity(),submitButton);
                    Utility.setTypefaceMuliRegular(getActivity(),back_button);
                    Utility.setTypefaceMuliRegular(getActivity(),wallet_amt);
                    Utility.setTypefaceMuliRegular(getActivity(),wallet_txt);
                    Utility.setTypefaceMuliRegular(getActivity(),invoice_discount_txt);
                    Utility.setTypefaceMuliRegular(getActivity(),airportText);
                    Utility.setTypefaceMuliRegular(getActivity(),invoice_subtotal_txt);
                    Utility.setTypefaceMuliRegular(getActivity(),timeText);
                    Utility.setTypefaceMuliRegular(getActivity(),distanceText);
                    Utility.setTypefaceMuliRegular(getActivity(),bookingId);
                    Utility.setTypefaceMuliRegular(getActivity(),invoice_discount_amount);
                    Utility.setTypefaceMuliRegular(getActivity(),airportValue);
                    Utility.setTypefaceMuliRegular(getActivity(),distanceValue);
                    Utility.setTypefaceMuliRegular(getActivity(),timeValue);
                    Utility.setTypefaceMuliBold(getActivity(),invoice_total_amount);
                    Utility.setTypefaceMuliBold(getActivity(),title);
                    Utility.setTypefaceMuliBold(getActivity(),invoice_total_txt);
                }

                back_button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        receiptDialogue.dismiss();
                    }
                });

                if(invoiceResponse.getData().getInvoice().getPayType().equals("2"))
                {
                    if(!invoiceResponse.getData().getInvoice().getCashCollected().equals(""))
                    {
                        if(Double.parseDouble(invoiceResponse.getData().getInvoice().getCashCollected())>0)
                        {
                            cash_collected_amt.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(invoiceResponse.getData().getInvoice().getCashCollected())));
                        }
                        else
                        {
                            cash_collected_layout.setVisibility(View.GONE);
                        }
                    }
                    else
                        cash_collected_layout.setVisibility(View.GONE);
                }
                else
                {
                    if(!invoiceResponse.getData().getInvoice().getAmount().equals(""))
                    {
                        if(Double.parseDouble(invoiceResponse.getData().getInvoice().getAmount())>0)
                        {
                            cash_collected_txt.setText(getResources().getString(R.string.paid_by_card)+" *"+session.getLast4());
                            cash_collected_amt.setText(getResources().getString(R.string.currencuSymbol)+" "+
                                    round(Double.parseDouble(invoiceResponse.getData().getInvoice().getCardDeduct())));
                        }
                        else
                        {
                            cash_collected_layout.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        cash_collected_layout.setVisibility(View.GONE);
                    }
                }

                if(!invoiceResponse.getData().getInvoice().getWalletDebitCredit().equals("") && !invoiceResponse.getData().getInvoice().getWalletDebitCredit().equals("0"))
                {
                    added_or_removed_wallet.setVisibility(View.VISIBLE);
                    added_or_removed_wallet_amt.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(invoiceResponse.getData().getInvoice().getWalletDebitCredit())));
                }

                if(!invoiceResponse.getData().getInvoice().getDis().equals("") && !invoiceResponse.getData().getInvoice().getDis().equals(null))
                {
                    distanceText.setText(getResources().getString(R.string.distnceFee)+"("+round(Double.parseDouble(invoiceResponse.getData().getInvoice().getDis()))+" "+getResources().getString(R.string.distanceUnit)+")");
                }

                if(!invoiceResponse.getData().getInvoice().getDur().equals("") && !invoiceResponse.getData().getInvoice().getDur().equals(null))
                {
                    timeText.setText(getResources().getString(R.string.timeFare)+"("+getDurationString(Integer.parseInt(invoiceResponse.getData().getInvoice().getDur()))+")");
                }

                if(!invoiceResponse.getData().getInvoice().getWalletDeducted().equals("") && !invoiceResponse.getData().getInvoice().getWalletDeducted().equals("0"))
                {
                    wallet_layout.setVisibility(VISIBLE);
                    wallet_amt.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getWalletDeducted())));
                }
                if(!invoiceResponse.getData().getInvoice().getCode().equals(""))
                {
                    invoice_discount_txt.setText(getResources().getString(R.string.discunt)+" ("+invoiceResponse.getData().getInvoice().getCode()+")");
                }
                bookingId.setText(getString(R.string.booking_id)+session.getBookingId());
                submitButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        receiptDialogue.dismiss();
                    }
                });

                if (!invoiceResponse.getData().getInvoice().getBaseFee().equals(""))
                    meterFee.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getBaseFee())));

                Utility.printLog("min fare in reciept "+invoiceResponse.getData().getInvoice().getMin_fare()
                +"\n"+"subtotal "+invoiceResponse.getData().getInvoice().getSubTotal());
                if(Double.parseDouble(invoiceResponse.getData().getInvoice().getMin_fare())>=
                        Double.parseDouble(invoiceResponse.getData().getInvoice().getSubTotal()))
                {
                    meterFee.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().
                            getMin_fare())));
                    invoice_subtotal_txt.setText(getString(R.string.min_fair1));
                    invoice_total_txt.setText(getString(R.string.total));
                    waiting_time_layout.setVisibility(View.GONE);
                    distanceLayout.setVisibility(View.GONE);
                    timeLayout.setVisibility(View.GONE);
                    invoice_total_layout.setVisibility(View.GONE);
                    cash_collected_txt.setText(getString(R.string.total));

                }


                if(!invoiceResponse.getData().getInvoice().getAirportFee().equals(""))
                {
                    if(Double.parseDouble(invoiceResponse.getData().getInvoice().getAirportFee())>0)
                    {
                        airportValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getAirportFee())));
                    }
                    else
                    {
                        airportLayout.setVisibility(View.GONE);
                    }
                }
                else
                {
                    airportLayout.setVisibility(View.GONE);
                }
                if(!invoiceResponse.getData().getInvoice().getDiscountVal().equals(""))
                {
                    if(Double.parseDouble(invoiceResponse.getData().getInvoice().getDiscountVal())>0)
                    {
                        invoice_discount_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getDiscountVal())));
                    }else
                    {
                        invoice_discount_layout.setVisibility(View.GONE);
                    }
                }
                else
                {
                    invoice_discount_layout.setVisibility(View.GONE);
                }
                distanceValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getDistanceFee())));
                timeValue.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getTimeFee())));

                if(invoiceResponse.getData().getInvoice().getWaitingFee()!=null && !invoiceResponse.getData().getInvoice().getWaitingFee().equals(""))
                {
                    if(Double.parseDouble(invoiceResponse.getData().getInvoice().getWaitingFee())!=0)
                    {
                        waiting_time.setText(getResources().getString(R.string.currencuSymbol)+" "+round(Double.parseDouble(invoiceResponse.getData().getInvoice().getWaitingFee())));
                    }
                    else
                    {
                        waiting_time_layout.setVisibility(View.GONE);
                    }
                    if(!invoiceResponse.getData().getInvoice().getWaitingTime().equals(""))
                    {
                        waiting_time_text.setText(getResources().getString(R.string.waiting_time)+" ("+getDurationString(Integer.parseInt(invoiceResponse.getData().getInvoice().getWaitingTime()))+")");
                    }
                }
                else
                {
                    waiting_time_layout.setVisibility(View.GONE);
                }

                invoice_total_amount.setText(getResources().getString(R.string.currencuSymbol) + " " + round(Double.parseDouble(invoiceResponse.getData().getInvoice().getSubTotal())));

                receiptDialogue.show();
            }
        });


        /**
         * to set the typeface
         */
        if(session.getLanguageCode().equals("en"))
        {
//            Utility.setTypefaceMuliRegular(getActivity(),lastRide);
//            Utility.setTypefaceMuliRegular(getActivity(),rideDate);
            Utility.setTypefaceMuliRegular(getActivity(),pickupaddresstv);
            Utility.setTypefaceMuliRegular(getActivity(),dropaddresstv);
//            Utility.setTypefaceMuliRegular(getActivity(),driverName);
            Utility.setTypefaceMuliRegular(getActivity(),receipt);
//            Utility.setTypefaceMuliRegular(getActivity(),txt_rating);
            Utility.setTypefaceMuliBold(getActivity(),amount);
        }

        /**
         * to set the fav address
         */
        if(invoiceResponse.getData().getInvoice().getFavouritepickup().equals(""))
            pickupaddresstv.setText(session.getPickUpAddress());
        else
            pickupaddresstv.setText(session.getPickUpAddress()+" ("+invoiceResponse.getData().getInvoice().getFavouritepickup()+")");

        if(invoiceResponse.getData().getInvoice().getFavouritedrop().equals(""))
            dropaddresstv.setText(invoiceResponse.getData().getDropAddr1());
        else
            dropaddresstv.setText(invoiceResponse.getData().getDropAddr1()+" ("+invoiceResponse.getData().getInvoice().getFavouritedrop()+")");

        Utility.printLog("drop addresss "+session.getDropAddress());
        Dropoff_Location_Address.setText(invoiceResponse.getData().getDropAddr1());

//        rideDate.setText(getResources().getString(R.string.booking_id)+" "+session.getBookingId());

        Utility.printLog("invoiceResponse.getData() "+invoiceResponse.getData().getApptDt());
        rideDate.setText(invoiceResponse.getData().getApptDt());
        if(!flagForDialog)
        {
            invoiceDialogue.show();
            flagForDialog=true;
        }
    }

    private void showCancellationReasonDialog()
    {
        Dialog cancellationReason = new Dialog(getActivity());
        cancellationReason.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancellationReason.setContentView(R.layout.cancel_reasons_layout);
        cancellationReason.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        cancellationReason.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        reasons_list = (RecyclerView) cancellationReason.findViewById(R.id.reasons_list);

        Button submitForCancellationReason = (Button)cancellationReason. findViewById(R.id.submit);
        TextView title_for_cancel = (TextView)cancellationReason. findViewById(R.id.title_for_cancel);
        ProgressBar progress_bar = (ProgressBar)cancellationReason. findViewById(R.id.progress_bar);

        /**
         * to set typeface
         */
        if(session.getLanguageCode().equals("en"))
        {
            Utility.setTypefaceMuliRegular(getActivity(),title_for_cancel);
            Utility.setTypefaceMuliBold(getActivity(),submitForCancellationReason);
        }
        if(Utility.isNetworkAvailable(getActivity()))
        {
            gettingReasons(progress_bar);
        }
        else
        {
            Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        reasons_list.setLayoutManager(mLayoutManager);
        reasons_list.setItemAnimator(new DefaultItemAnimator());

        submitForCancellationReason.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v) {
                cancel_adapter.submitOnclick();
            }
        });
        if(isAdded())
            cancellationReason.show();
    }

    private void gettingReasons(final ProgressBar progress_bar)
    {
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_lan","0");
            jsonObject.put("ent_user_type", "2");
            Utility.printLog("params to login "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getCancellationReson", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                progress_bar.setVisibility(View.GONE);
                reasons_list.setVisibility(View.VISIBLE);
                Utility.printLog("Login Response getting reasons "+result);
                if(result!=null)
                {
                    Gson gson=new Gson();
                    Cancel_Pojo cancel_pojo=gson.fromJson(result,Cancel_Pojo.class);
                    if(cancel_pojo.getErrFlag().equals("0"))
                    {
                        ArrayList<String> cancel_reasonsList=new ArrayList<>();
                        cancel_reasonsList.addAll(cancel_pojo.getGetdata());
                        cancel_adapter=new Cancel_Adapter(getActivity(),cancel_pojo.getGetdata());
                        reasons_list.setAdapter(cancel_adapter);
                    }
                    if(cancel_pojo.getErrNum().equals("6") || cancel_pojo.getErrNum().equals("7") ||
                            cancel_pojo.getErrNum().equals("94") || cancel_pojo.getErrNum().equals("96"))
                    {
                        Utility.showToast(getActivity(),cancel_pojo.getErrMsg());
                        Intent i = new Intent(getActivity(), SplashActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        getActivity().startActivity(i);
                        getActivity().overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                    }
                }
                else
                {
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error)
            {
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void getCurrentLocation()
    {
        if (networkUtil == null)
        {
            networkUtil = new LocationUtil(getActivity(), this);
        }
        else
        {
            networkUtil.checkLocationSettings();
        }
    }

    /**
     * publishing for every 7 seconds to get the new drivers and also to remove the drivers who are going out
     */
    private void startPublishingWithTimer(long delayInPublish)
    {
        Utility.printLog("CONTROL INSIDE startPublishingWithTimer");

        if(myTimer_publish!= null)
        {
            Utility.printLog("Timer already started");
            return;
        }
        myTimer_publish = new Timer();

        TimerTask myTimerTask_publish = new TimerTask() {
            @Override
            public void run() {
                if (currentLatitude == 0.0 || currentLongitude == 0.0) {
                    Utility.printLog("startPublishingWithTimer getServerChannel no location");
                } else
                {
                    Utility.printLog("Timer already started publishing else "+publishingTag
                    );
                    //if user is in booking then publish to pubnub
                    //else call API to get the types and drivers
                    switch (publishingTag)
                    {
                        case 1:
                        {
                            if(Utility.isNetworkAvailable(getActivity()))
                                getPubnubDataInAPI(getString(R.string.GETTING_PUBNUB_URL1));
                            else
                                Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                            break;
                        }
                        case 3:
                        {
                            if(Utility.isNetworkAvailable(getActivity()))
                                getPubnubDataInAPI(getString(R.string.GETTING_PUBNUB_URL2));
                            else
                                Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                            break;
                        }
                    }
                }
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, delayInPublish, session.getPubnubIntervalForHome()*1000);
    }

    private void getPubnubDataInAPI(String url)
    {
        jsonObject=new JSONObject();
        try
        {
            jsonObject.put("a",publishingTag);
            jsonObject.put("pid",session.getLoginId());
            jsonObject.put("lt", String.valueOf(currentLatitude));
            jsonObject.put("lg", String.valueOf(currentLongitude));
            jsonObject.put("chn",session.getChannelName());
            jsonObject.put("st","3");
            jsonObject.put("tp","1");
            jsonObject.put("ent_splash","0");
            jsonObject.put("addr",current_address.getText().toString());
            jsonObject.put("sid",session.getSid());
            Utility.printLog("params to check session "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(url, jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(final String result)
            {
                Utility.printLog( "success from get pubnub   " + result);
            }
            @Override
            public void onError(String error)
            {
                Utility.printLog( "success from get pubnub  error " + error);
                if(isAdded())
                    Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        Utility.printLog("CONTROL INSIDE onAttach");
        mActivity = getActivity();
    }

    private class PhoneCallListener extends PhoneStateListener
    {
        @Override
        public void onCallStateChanged(int state, String incomingNumber)
        {
            if (TelephonyManager.CALL_STATE_RINGING == state)
            {
            }
        }
    }

    /**
     *subscribing to my server channel to listen all available drivers around you
     */
    private class BackgroundSubscribeMyChannel extends AsyncTask<String,Void,String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            /**
             * listening to passenger channel
             */
            List<String> subscribed_channels = pubnub.getSubscribedChannels();
            if(!subscribed_channels.contains(session.getChannelName()))
            {
                pubnub.subscribe()
                        .channels(Arrays.asList(session.getChannelName())) // subscribe to channel groups
                        .withPresence()// also subscribe to related presence information
                        .execute();
            }
            return null;
        }
    }

    /**
     * checking whether this fragment is in foreground or not
     */
    public static boolean visibleStatus()
    {
        return visibility;
    }

    private String round(double value)
    {
        @SuppressLint("DefaultLocale") String s = String.format("%.2f", value);
        Utility.printLog("rounded value=" + s);
        return s;
    }


    /**
     *  Updating driver location when Driver On The Way
     * @param latitude,longitude
     *
     */
    private void UpdateDriverLocation_DriverOnTheWay(String latitude,String longitude)
    {
        double driver_current_latitude=Double.parseDouble(latitude);
        double driver_cuttent_longitude=Double.parseDouble(longitude);
        session.storeLat_DOW(latitude);
        session.storeLon_DOW(longitude);
        /********************************************************************************************************/
        Location driverLocation = new Location("starting_point");
        driverLocation.setLatitude(driver_current_latitude);
        driverLocation.setLongitude(driver_cuttent_longitude);

        mCurrentLoc=driverLocation;

        if(mPreviousLoc==null)
        {
            mPreviousLoc=driverLocation;
        }

        final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);

        if(driverMarker!=null)
        {
            if(driverMarker.getmMarker()!=null)
            {
                driverMarker.getmMarker().setPosition(new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude()));
                driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                driverMarker.getmMarker().setRotation(bearing);
                driverMarker.getmMarker().setFlat(true);

                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                Projection proj = googleMap.getProjection();
                Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
                final LatLng startLatLng = proj.fromScreenLocation(startPoint);
                final long duration = 800;
                final Interpolator interpolator = new LinearInterpolator();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed
                                / duration);
                        double lng = t * mCurrentLoc.getLongitude() + (1 - t)
                                * startLatLng.longitude;
                        double lat = t * mCurrentLoc.getLatitude() + (1 - t)
                                * startLatLng.latitude;
                        driverMarker.getmMarker().setPosition(new LatLng(lat, lng));
                        driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                        driverMarker.getmMarker().setRotation(bearing);
                        driverMarker.getmMarker().setFlat(true);

                        if (t < 1.0) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        }
                    }
                });
            }
        }
        mPreviousLoc=mCurrentLoc;
/********************************************************************************************************/
        LatLng latLng=new LatLng(driver_current_latitude,driver_cuttent_longitude);
        if(googleMap!=null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
    }

    /**
     * Updating driver location when Driver Arrived
     * @param latitude,longitude
     *
     */
    private void UpdatDriverLocation_DriverArrived(String latitude,String longitude)
    {
        double driver_current_latitude=Double.parseDouble(latitude);
        double driver_cuttent_longitude=Double.parseDouble(longitude);

        session.storeLat_DOW(filterResponse.getLt());
        session.storeLon_DOW(filterResponse.getLg());

        Utility.printLog("INSIDE DRIVER REACHED:4 current lat=" + driver_current_latitude +
                " lng=" + driver_cuttent_longitude);
        LatLng latLng=new LatLng(driver_current_latitude,driver_cuttent_longitude);
        if(googleMap!=null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level));
        /********************************************************************************************************/
        Location driverLocation = new Location("starting_point");
        driverLocation.setLatitude(driver_current_latitude);
        driverLocation.setLongitude(driver_cuttent_longitude);
        mCurrentLoc=driverLocation;

        if(mPreviousLoc==null)
        {
            mPreviousLoc=driverLocation;
        }
        final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
        if(driverMarker!=null)
        {
            if(driverMarker.getmMarker()!=null)
            {
                driverMarker.getmMarker().setPosition(new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude()));
                driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                driverMarker.getmMarker().setRotation(bearing);
                driverMarker.getmMarker().setFlat(true);
                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                Projection proj = googleMap.getProjection();
                Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
                final LatLng startLatLng = proj.fromScreenLocation(startPoint);
                final long duration = 500;
                final Interpolator interpolator = new LinearInterpolator();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed
                                / duration);
                        double lng = t * mCurrentLoc.getLongitude() + (1 - t)
                                * startLatLng.longitude;
                        double lat = t * mCurrentLoc.getLatitude() + (1 - t)
                                * startLatLng.latitude;
                        driverMarker.getmMarker().setPosition(new LatLng(lat, lng));
                        driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                        driverMarker.getmMarker().setRotation(bearing);
                        driverMarker.getmMarker().setFlat(true);

                        if (t < 1.0) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        }
                    }
                });
            }
        }
        mPreviousLoc=mCurrentLoc;
/********************************************************************************************************/
    }

    /**
     * Updating driver location when Driver Journey Started
     * @param latitude ,longitude
     */
    private void UpdateDriverLocation_JourneyStarted(String latitude,String longitude)
    {
        double driver_current_latitude=Double.parseDouble(latitude);
        double driver_cuttent_longitude=Double.parseDouble(longitude);

        Utility.printLog("INSIDE DRIVER TripBegin:4 current lat=" + driver_current_latitude
                + " lng=" + driver_cuttent_longitude);
        session.storeLat_DOW(filterResponse.getLt());
        session.storeLon_DOW(filterResponse.getLg());
/********************************************************************************************************/
        Location driverLocation = new Location("starting_point");
        driverLocation.setLatitude(driver_current_latitude);
        driverLocation.setLongitude(driver_cuttent_longitude);
        mCurrentLoc=driverLocation;
        if(mPreviousLoc==null)
        {
            mPreviousLoc=driverLocation;
        }
        final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
        if(driverMarker!=null)
        {
            if(driverMarker.getmMarker()!=null)
            {
                driverMarker.getmMarker().setPosition(new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude()));
                driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                driverMarker.getmMarker().setRotation(bearing);
                driverMarker.getmMarker().setFlat(true);

                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                Projection proj = googleMap.getProjection();
                Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(),mPreviousLoc.getLongitude()));
                final LatLng startLatLng = proj.fromScreenLocation(startPoint);
                final long duration = 500;
                final Interpolator interpolator = new LinearInterpolator();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed
                                / duration);
                        double lng = t * mCurrentLoc.getLongitude() + (1 - t)
                                * startLatLng.longitude;
                        double lat = t * mCurrentLoc.getLatitude() + (1 - t)
                                * startLatLng.latitude;
                        driverMarker.getmMarker().setPosition(new LatLng(lat, lng));
                        driverMarker.getmMarker().setAnchor(0.5f, 0.5f);
                        driverMarker.getmMarker().setRotation(bearing);
                        driverMarker.getmMarker().setFlat(true);

                        if (t < 1.0) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        }
                    }
                });
            }
        }
        mPreviousLoc=mCurrentLoc;
/********************************************************************************************************/
        LatLng latLng=new LatLng(driver_current_latitude,driver_cuttent_longitude);
        if(googleMap!=null)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom_level_booking));
//            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    /**
     *  Driver cancelled the appointment
     */
    private void DriverCancelledAppointment(String reason)
    {
        session.setDriverCancelledApt(false);
        session.setDriverOnWay(false);
        Utility.printLog("Wallah set as false Homepage  5" +reason);
        session.setDriverArrived(false);
        session.setInvoiceRaised(false);
        session.setTripBegin(false);
        session.setBookingReponseFromPubnub("");
        session.setPubnubResponse(session.getPubnubResponseForNonbooking());
        try
        {
            if(googleMap!=null)
            {
                googleMap.clear();
            }
        }
        catch (IllegalArgumentException e)
        {
            Utility.printLog("googleMap.clear Catch reason ");
        }
        publishingTag=1;
        List<String> subscribed_channels = pubnub.getSubscribedChannels();
        if(!subscribed_channels.contains(session.getChannelName()))
        {
            pubnub.subscribe()
                    .channels(Arrays.asList(session.getChannelName())) // subscribe to channel groups
                    .withPresence()// also subscribe to related presence information
                    .execute();
        }

        /**
         * to publish the user latlongs to server to get the data from server with 0 delay
         * if publishing is not yet started
         */
        if(myTimer_publish==null)
            startPublishingWithTimer(0);

        isFareQuotePressed=false;
        to_latitude=null;
        to_longitude=null;
        pickup.setVisibility(VISIBLE);
        Driver_on_the_way_txt.setVisibility(View.INVISIBLE);
        current_location.setVisibility(View.INVISIBLE);
        Rl_distance_time.setVisibility(View.GONE);

        Intent i = new Intent(mActivity, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(i);
        mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
    }

    //to get the address on the homescreen
    private class BackgroundGetAddress extends AsyncTask<String, Void, String>
    {

        List<Address> address;
        String lat,lng;
        @Override
        protected String doInBackground(String... params) {

            Utility.printLog("Called on move camera.........");

            try {

                lat = params[0];
                lng = params[1];

                if(lat!=null && lng!=null)
                {
                    if(getActivity()!=null)
                    {
                        geocoder=new Geocoder(getActivity());
                    }
                    if(geocoder!=null)
                    {
                        address=geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);
                    }
                    else
                    {
                        if(isAdded())
                        {
                            Utility.printLog("is added in getting address "+isAdded());
                            if(Utility.isNetworkAvailable(getActivity()))
                            {
                                getAddressGeocoding();
                            }
                            else
                            {
                                Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                            }
                        }
                    }
                }
            } catch (IOException e) {
                Utility.printLog("exception in getting address "+e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(address!=null && address.size()>0)
            {

                //aakash change

                current_address.setText(address.get(0).getAddressLine(0)/*+", "+address.get(0).getAddressLine(1)*/);
                mPICKUP_ADDRESS=current_address.getText().toString();
                Utility.printLog("FAV ADDRESS akbar:location" + mPICKUP_ADDRESS);
                dbFavLocations = dbPickup.getFavAddress();
                ArrayList<String> addressList=new ArrayList<>();
                for(int i=0;i<dbFavLocations.size();i++)
                {
                    Utility.printLog("FAV ADDRESS FAV LOC stored pick location "+dbFavLocations.get(i).get_formattedAddress());
                    addressList.add(dbFavLocations.get(i).get_formattedAddress());
                }

                if(addressList.contains(mPICKUP_ADDRESS))
                {
                    for(int i=0;i<addressList.size();i++)
                    {
                        if(mPICKUP_ADDRESS.equals(addressList.get(i)))
                        {
                            if(isAdded())
                            {
                                Utility.printLog("i am inside if for fav ");
                                Appointment_location.setText(dbFavLocations.get(i).get_description());
                                databaseId=dbFavLocations.get(i).getID();
                            }
                            break;
                        }
                        else
                        {
                            if(isAdded())
                            {
                                Utility.printLog("i am inside else for fav ");
                                Appointment_location.setText(getResources().getString(R.string.pickup_location));

                            }
                        }
                    }
                }
                else
                {
                    if(isAdded())
                    {
                        Appointment_location.setText(getResources().getString(R.string.pickup_location));
                    }
                }
            }
            else
            {
                if(isAdded())
                {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(Utility.isNetworkAvailable(getActivity()))
                            {
                                getAddressGeocoding();
                            }
                            else
                            {
                                Utility.showToast(getActivity(),getString(R.string.network_connection_fail));
                            }
                        }
                    });
                }
            }
        }
    }

    public void getAddressGeocoding()
    {
        String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+currentLatitude+","+
                currentLongitude+"&sensor=false&key="+ session.getServerKey();
        OkHttpRequest.doJsonRequest(url, new OkHttpRequest.JsonRequestCallback() {

            @Override
            public void onSuccess(String result) {
                if(result!=null)
                {
                    Gson gson=new Gson();
                    GeocodingResponse response=gson.fromJson(result, GeocodingResponse.class);
                    if(response.getStatus().equals("OK"))
                    {
                        if(isAdded())
                        {
                            current_address.setText(response.getResults().get(0).getFormatted_address());
                        }
                        mPICKUP_ADDRESS=current_address.getText().toString();
                        Utility.printLog("FAV ADDRESS akbar:location" + current_address);
                        dbFavLocations = dbPickup.getFavAddress();
                        ArrayList<String> addressList=new ArrayList<>();
                        for(int i=0;i<dbFavLocations.size();i++)
                        {
                            Utility.printLog("FAV ADDRESS FAV LOC stored pick location "+dbFavLocations.get(i).get_formattedAddress());
                            addressList.add(dbFavLocations.get(i).get_formattedAddress());
                        }
                        if(addressList.contains(mPICKUP_ADDRESS))
                        {
                            for(int i=0;i<addressList.size();i++)
                            {
                                if(mPICKUP_ADDRESS.equals(addressList.get(i)))
                                {
                                    if(isAdded())
                                    {
                                        Utility.printLog("i am inside if for fav ");
                                        Appointment_location.setText(dbFavLocations.get(i).get_description());
                                        databaseId=dbFavLocations.get(i).getID();
                                    }
                                    break;
                                }
                                else
                                {
                                    if(isAdded())
                                    {
                                        Utility.printLog("i am inside else for fav ");
                                        Appointment_location.setText(getResources().getString(R.string.pickup_location));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(isAdded())
                            {
                                Appointment_location.setText(getResources().getString(R.string.pickup_location));
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void BackgroundUpdateAddress()
    {
        if(mDialog!=null)
        {
            mDialog.dismiss();
            mDialog = null;
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id",session.getDeviceId());
            jsonObject.put("ent_booking_id",session.getBookingId());
            jsonObject.put("ent_drop_addr1",mDROPOFF_ADDRESS);
            jsonObject.put("ent_lat",to_latitude);
            jsonObject.put("ent_long",to_longitude);
            jsonObject.put("ent_date_time",curenttime);
            jsonObject.put("ent_mid",session.getMid());
            Utility.printLog("params to update "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"updateDropOff", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                Utility.printLog("add card response "+result);
                if (mDialog!=null)
                {
                    mDialog.dismiss();
                    mDialog.cancel();
                    mDialog = null;
                }
                if(result!=null)
                {
                    Gson gson = new Gson();
                    GetCardResponse response=gson.fromJson(result,GetCardResponse.class);
                    if(response.getErrFlag().equals("0"))
                    {
                        if(isAdded())
                        {
                            Utility.showToast(getActivity(),response.getErrMsg());
                            Dropoff_Location_Address.setText(mDROPOFF_ADDRESS);
                        }
                    }
                }
                else
                {
                    Utility.ShowAlert(getResources().getString(R.string.server_error), getActivity());
                }
            }

            @Override
            public void onError(String error)
            {
                if (mDialog!=null)
                {
                    mDialog.dismiss();
                    mDialog.cancel();
                    mDialog = null;
                }
                Utility.printLog("on error for the login "+error);
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    public void getEta(final String url)
    {
        OkHttpRequest.doJsonRequest(url, new OkHttpRequest.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("successcallback for distancematrix " + url);
                if(result!=null)
                {
                    Gson gson=new Gson();
                    EtaPojo etaPojo=gson.fromJson(result,EtaPojo.class);
                    if(isAdded())
                    {
                        if(etaPojo.getStatus().equals("OK"))
                        {
                            if(!etaPojo.getRows().get(0).getElements().get(0).getStatus().equals("ZERO_RESULTS"))
                            {
                                int duration= (int) (Math.round(Double.parseDouble(etaPojo.getRows().get(0).getElements().get(0).getDuration().getValue())/60));
                                durationOnTheWay= duration;

                                Utility.printLog("duration to be set on the pick marker "+duration+" "+durationOnTheWay);
                                if(durationOnTheWay==0)
                                {
                                    durationOnTheWay=1;
                                }

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        LatLng latLng=new LatLng(Double.parseDouble(session.getPickuplat()),Double.parseDouble(session.getPickuplng()));
                                        BitmapCustomMarker bitmapCustomMarker=new BitmapCustomMarker(getActivity(),durationOnTheWay+"");
                                        googleMap.addMarker(new MarkerOptions().position(latLng)
                                                .title("PICK").flat(false)
                                                .icon(BitmapDescriptorFactory.fromBitmap(bitmapCustomMarker.createBitmap())));
                                    }
                                });

                                double distanceInKm=Double.parseDouble(etaPojo.getRows().get(0).
                                        getElements().get(0).getDistance().getValue())/1000;
                                if(distanceInKm>1)
                                {
                                    Driver_on_the_way_txt.setText(getResources().getString(R.string.driver_on_the_way)+","+" "+round(distanceInKm)+" "+getResources().getString(R.string.distanceUnit)+" "+getResources().getString(R.string.away));   //on my way to PQ, 400 meters away
                                }
                                else
                                {
                                    Driver_on_the_way_txt.setText(getResources().getString(R.string.driver_on_the_way)+","+" "+etaPojo.getRows().get(0).
                                            getElements().get(0).getDistance().getValue()+" "+getResources().getString(R.string.meter)+" "+getResources().getString(R.string.away));   //on my way to PQ, 400 meters away
                                }
                            }
                        }
                        else
                        {
                            pubnubProgressDialog.setVisibility(View.VISIBLE);
                            pickup_Distance.setVisibility(View.GONE);
                            rate_unit.setVisibility(View.GONE);

                            //Retrieve the distance matrix keys saved in shared preference and remove the one used before
                            String jsonTextList = session.getGoogleMatrixData();
                            String[] savedDistanceMatrixKeys = gson.fromJson(jsonTextList, String[].class);

                            if(savedDistanceMatrixKeys.length>0)
                            {
                                // change the array to a list of String
                                //List<String> savedKeysList  = Arrays.asList(savedDistanceMatrixKeys);
                                List<String> savedKeysList = new LinkedList<>(Arrays.asList(savedDistanceMatrixKeys));
                                //removed 0th index google key
                                savedKeysList.remove(0);
                                //save the list after removing 0th index key and set next key to use
                                if(!savedKeysList.isEmpty())
                                {
                                    session.setServerKey(savedKeysList.get(0));
                                    /**
                                     * to set the new list after removing a key
                                     */
                                    String jsonText = gson.toJson(savedKeysList);
                                    session.setGoogleMatrixData(jsonText);
                                }

                                /**
                                 * if distance matrix exceeds then stop publishing timer and then start it at 0th interval
                                 * then call pubnub to get data
                                 */
                                if(myTimer_publish!=null)
                                {
                                    myTimer_publish.cancel();
                                    myTimer_publish=null;
                                    /**
                                     * to publish the user latlongs to server to get the data from server with 0 delay
                                     * if publishing is not yet started
                                     */
                                    startPublishingWithTimer(0);
                                }
                            }
                        }
                    }
                }
            }
            @Override
            public void onError(String error)
            {
                if(isAdded())
                {
                    Utility.printLog("error in getting dist marix "+error);
                }
            }
        });
    }

    public Target setTarget(final ImageView imageView)
    {
        Target target= new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                /**
                 * to set the image from fb to imageview after making circular imagee
                 */
                if(getActivity()!=null && isAdded())
                {
                    if(imageView.getWidth()>0 && imageView.getHeight()>0)
                    {
                        bitmap = Bitmap.createScaledBitmap(bitmap, imageView.getWidth(),
                                imageView.getHeight(), true);
                        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                        BitmapShader shader = new BitmapShader (bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                        Paint paint = new Paint();
                        paint.setShader(shader);
                        paint.setAntiAlias(true);
                        Canvas c = new Canvas(circleBitmap);
                        c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
                        imageView.setImageBitmap(circleBitmap);
                    }
                }
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
            }
        };
        return target;
    }

    public Target setTargetForLargeImage(final ImageView imageView)
    {
        Target target= new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                /**
                 * to set the image from fb to imageview after making circular imagee
                 */
                if(getActivity()!=null && isAdded())
                {
                    Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, view_for_image.getWidth(),
                            view_for_image.getHeight(), true);
                    imageView.setImageBitmap(bitmap1);
                }
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
            }
        };
        return target;
    }

    private void cancellationPopup(final String errFlag, String errMsg)
    {
        final Dialog popupWithTwoButtons=Utility.showPopupWithTwoButtons(getActivity());
        TextView dialogText= (TextView) popupWithTwoButtons.findViewById(R.id.text_for_popup);
        TextView yes_button= (TextView) popupWithTwoButtons.findViewById(R.id.yes_button);
        TextView no_button= (TextView) popupWithTwoButtons.findViewById(R.id.no_button);

        dialogText.setText(errMsg);
        no_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
            }
        });
        yes_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWithTwoButtons.dismiss();
                if(errFlag.equals("0"))
                {
                    if(Utility.isNetworkAvailable(getActivity()))
                    {
                        cancelAppointment();
                    }
                    else
                    {
                        Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                    }
                }
                else
                {
                    if(Utility.isNetworkAvailable(getActivity()))
                    {
                        showCancellationReasonDialog();
                    }
                    else
                    {
                        Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
                    }
                }
            }
        });
        popupWithTwoButtons.show();
    }

    private void cancelAppointment()
    {
        dialogL= Utility.GetProcessDialogNew(getActivity(), getResources().getString(R.string.cancelling_trip));
        dialogL.setCancelable(false);
        if (dialogL!=null)
        {
            dialogL.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            Utility utility=new Utility();
            String curenttime=utility.getCurrentGmtTime();
            jsonObject.put("ent_sess_token",session.getSessionToken());
            jsonObject.put("ent_dev_id", session.getDeviceId());
            jsonObject.put("ent_booking_id", session.getBookingId());
            jsonObject.put("ent_mid", session.getMid());
            jsonObject.put("ent_date_time", curenttime);
            jsonObject.put("ent_reason", "");
            Utility.printLog("params to cancel "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"cancelAppointment", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String response)
            {
                getCancelResponse = response;
                Utility.printLog("response for the cancel "+response);
                getCancelInfo();
            }

            @Override
            public void onError(String error) {
                if (dialogL != null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
            }
        });
    }

    private void getCancelInfo()
    {
        Gson gson = new Gson();
        LiveBookingResponse cancelBooking = gson.fromJson(getCancelResponse, LiveBookingResponse.class);
        if(cancelBooking !=null)
        {
            if(cancelBooking.getErrFlag().equals("0"))
            {
                session.setBookingReponseFromPubnub("");
                session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                session.setDriverOnWay(false);
                session.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);
                session.storeAptDate(null);
                Utility.showToast(getActivity(),cancelBooking.getErrMsg());
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(mActivity, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(i);
                mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }

            else if(cancelBooking.getErrFlag().equals("1"))
            {
                session.setBookingReponseFromPubnub("");
                session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                session.setDriverOnWay(false);
                session.setBookingCancelled(true);
                Utility.printLog("Wallah set as false Homepage cancel 2");
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);
                session.storeAptDate(null);
                Utility.ShowAlert(cancelBooking.getErrMsg(),getActivity());
                if(dialogL!=null) {
                    dialogL.dismiss();
                    dialogL = null;
                }
                Intent i = new Intent(mActivity, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mActivity.startActivity(i);
                mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }
        }
    }

    /**
     * Created by embed on 25/8/16
     */
    private class WhatWentWrong extends RecyclerView.Adapter
    {
        private Context mcontext;
        private ArrayList<NegativeFeedBackOfDriver> reasonsList;
        private ProgressDialog dialogL;
        private String getCancelResponse;
        private SessionManager sessionManager;
        private ArrayList<ImageView> buttons;

        WhatWentWrong(Context mcontext, ArrayList<NegativeFeedBackOfDriver> reasonsList)
        {
            this.mcontext = mcontext;
            this.reasonsList = reasonsList;
            sessionManager=new SessionManager(mcontext);
            buttons= new ArrayList<>();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(mcontext).inflate(R.layout.cancel_row,parent,false);
            return new ViewHldr(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof  ViewHldr)
            {
                final ViewHldr hldr = (ViewHldr) holder;
                hldr.mText.setText(reasonsList.get(position).getReason());

//                buttons.add(hldr.arrowImage);
                if(reasonsList.get(position).isChecked()) {
                    hldr.arrowImage.setVisibility(View.VISIBLE);
                } else {
                    hldr.arrowImage.setVisibility(View.GONE);
                }

                hldr.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(reasonsList.get(position).isChecked()){
                            reasonsList.get(position).setChecked(false);
                            hldr.arrowImage.setVisibility(View.GONE);
                        } else {
                            reasonsList.get(position).setChecked(true);
                            hldr.arrowImage.setVisibility(View.VISIBLE);
                        }
//                        resetAll();
//                        hldr.arrowImage.setVisibility(View.VISIBLE);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            Utility.printLog("size of list in adapetr "+reasonsList.size());
            return reasonsList.size();
        }

        private class ViewHldr extends RecyclerView.ViewHolder {

            ImageView arrowImage;
            TextView mText;
            RelativeLayout linearLayout;

            ViewHldr(View inflate) {
                super(inflate);
                mText = (TextView) inflate.findViewById(R.id.cancel_reason_text);
                arrowImage = (ImageView) inflate.findViewById(R.id.arrowImage);
                linearLayout= (RelativeLayout) inflate.findViewById(R.id.reason_layout);

                if(sessionManager.getLanguageCode().equals("en"))
                {
                    Utility.setTypefaceMuliRegular(mcontext,mText);
                }
            }
        }

        private void getCancelInfo()
        {
            Gson gson = new Gson();
            LiveBookingResponse cancelBooking = gson.fromJson(getCancelResponse, LiveBookingResponse.class);
            if(cancelBooking !=null)
            {
                if(cancelBooking.getErrFlag().equals("0"))
                {
                    sessionManager.setBookingReponseFromPubnub("");
                    sessionManager.setPubnubResponse(sessionManager.getPubnubResponseForNonbooking());
                    sessionManager.setDriverOnWay(false);
                    sessionManager.setBookingCancelled(true);
                    Utility.printLog("Wallah set as false Homepage cancel 2");
                    sessionManager.setDriverArrived(false);
                    sessionManager.setTripBegin(false);
                    sessionManager.setInvoiceRaised(false);
                    sessionManager.storeAptDate(null);
                    Utility.showToast(mcontext,cancelBooking.getErrMsg());
                    if(dialogL!=null) {
                        dialogL.dismiss();
                        dialogL = null;
                    }
                    Intent i = new Intent(mcontext, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mcontext.startActivity(i);
                    ((Activity)mcontext).overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);

                }

                else if(cancelBooking.getErrFlag().equals("1"))
                {
                    sessionManager.setBookingReponseFromPubnub("");
                    sessionManager.setPubnubResponse(sessionManager.getPubnubResponseForNonbooking());
                    sessionManager.setDriverOnWay(false);
                    sessionManager.setBookingCancelled(true);
                    Utility.printLog("Wallah set as false Homepage cancel 2");
                    sessionManager.setDriverArrived(false);
                    sessionManager.setTripBegin(false);
                    sessionManager.setInvoiceRaised(false);
                    sessionManager.storeAptDate(null);
                    Utility.ShowAlert(cancelBooking.getErrMsg(),mcontext);
                    if(dialogL!=null) {
                        dialogL.dismiss();
                        dialogL = null;
                    }
                    Intent i = new Intent(mcontext, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mcontext.startActivity(i);
                    ((Activity)mcontext).overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);

                }
            }
        }
    }
}