package com.roadyo.passenger.pojo;

import java.util.ArrayList;

public class GetAppointmentDetails {
	/*  "a":4,
    "status":"2",
    "data":{
    }*/
	private String a,status;
	private StatusData data;
	public String getA() {
		return a;
	}
	public String getStatus() {
		return status;
	}
	public StatusData getData() {
		return data;
	}
}
