package com.roadyo.passenger.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.LoginResponse;
import com.bloomingdalelimousine.ridein.R;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.ReadSms;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.UploadAmazonS3;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static android.view.View.VISIBLE;

public class SignupVerificationActivity extends AppCompatActivity
{
	TextView resend_verificationcode_bt;
	RelativeLayout signup_back;
	String firstName,lastName,email,password,mobileNo,referal,type;
	Button signup_payment_skip;
	SessionManager sessionManager;
	public static File mFileTemp;
	String lat,longitude;
	boolean picSelected;
	private ProgressDialog dialogL;
	private String errFlag,errMsg,gender,dob;
	private Dialog popupDialog;
	private TextView text_for_popup;
	private ReadSms readSms;
	private EditText otp_et;
	private TextView mobile_number_tv;
	private TextView change_number_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.signup_verification_layout);
		Bundle bundle=getIntent().getExtras();
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
		{
			mFileTemp = new File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}
		else
		{
			mFileTemp = new File(getFilesDir(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}
		readSms = new ReadSms() {
			@Override
			protected void onSmsReceived(String str) {
				/**
				 * to filter the OTP from last 5 numbers
				 */
				Utility.printLog("received sms "+str);
				String otp = str.substring(Math.max(str.length() - 6, 0));
				Utility.printLog("filtered string "+otp);
				otp_et.setText(String.valueOf(otp));
			}
		};
		IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		intentFilter.setPriority(1000);
		this.registerReceiver(readSms, intentFilter);
		if(bundle!=null)
		{
			firstName=bundle.getString("FIRSTNAME");
			lastName=bundle.getString("LASTNAME");
			email=bundle.getString("EMAIL");
			password=bundle.getString("PASSWORD");
			mobileNo=bundle.getString("MOBILE");
			referal=bundle.getString("REFERAL");
			picSelected=bundle.getBoolean("BOOLEAN");
			lat=bundle.getString("LATITUDE");
			longitude=bundle.getString("LONGITUDE");
			type=bundle.getString("TYPE");
			gender=bundle.getString("GENDER");
			dob=bundle.getString("DOB");
			Utility.printLog("pic selected falg "+lat+" "+picSelected);
		}
		initializations();
	}

	@Override
	protected void onPause() {
		super.onPause();
		try
		{
			this.unregisterReceiver(readSms);
		}
		catch (IllegalArgumentException e)
		{
			Utility.printLog("exception "+e);
		}
	}

	/**
	 * <h>initialize</h>
	 * For initialising the all view componets.
	 * The value returned is as void.
	 * <pre>
	 *    Used for initialising the all view componets
	 * </pre>
	 * @since 1.0
	 */
	public void initializations()
	{
		/**
		 * to set language support
		 */
		Utility.setLanguageSupport(this);
		/**
		 * to set the status bar color
		 */
		Utility.setStatusBarColor(this);

		resend_verificationcode_bt=(TextView)findViewById(R.id.resend_verificationcode_bt);
		TextView enter_text=(TextView)findViewById(R.id.enter_text);
		TextView didnotSent=(TextView)findViewById(R.id.didnotSent);
		RelativeLayout second_dot=(RelativeLayout) findViewById(R.id.second_dot);
		TextView title=(TextView)findViewById(R.id.title);
		View line1= findViewById(R.id.line1);
		TextView verification_code_text=(TextView)findViewById(R.id.verification_code_text);
		mobile_number_tv=(TextView)findViewById(R.id.mobile_number_tv);
		change_number_tv=(TextView)findViewById(R.id.change_number_tv);
		resend_verificationcode_bt=(TextView)findViewById(R.id.resend_verificationcode_bt);
		signup_back=(RelativeLayout) findViewById(R.id.rl_signin);
		ImageButton back_btn=(ImageButton) findViewById(R.id.back_btn);
		signup_payment_skip=(Button) findViewById(R.id.signup_payment_skip_button);
		sessionManager=new SessionManager(SignupVerificationActivity.this);
		otp_et= (EditText) findViewById(R.id.otp_et);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			second_dot.setBackground(getResources().getDrawable(R.drawable.pink_circle_background));
			line1.setBackground(getResources().getDrawable(R.color.pin_colour));
		}

		popupDialog=Utility.showPopupWithOneButton(SignupVerificationActivity.this);
		TextView title_popup = (TextView) popupDialog.findViewById(R.id.title_popup);
		text_for_popup= (TextView) popupDialog.findViewById(R.id.text_for_popup);
		TextView yes_button= (TextView) popupDialog.findViewById(R.id.yes_button);
		title_popup.setText(getResources().getString(R.string.alert));
		yes_button.setText(getResources().getString(R.string.ok));
		mobile_number_tv.setText(""+mobileNo);

		/**
		 * to set typeface
		 */
		if(sessionManager.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(SignupVerificationActivity.this,resend_verificationcode_bt);
			Utility.setTypefaceMuliRegular(SignupVerificationActivity.this,signup_payment_skip);
			Utility.setTypefaceMuliRegular(SignupVerificationActivity.this,title);
			Utility.setTypefaceMuliRegular(SignupVerificationActivity.this,didnotSent);
			Utility.setTypefaceMuliBold(SignupVerificationActivity.this,verification_code_text);
			Utility.setTypefaceMuliBold(SignupVerificationActivity.this,enter_text);
		}

		/**
		 * to give focus to next edit text
		 */
		giveFocusToNextEditText();


		signup_payment_skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (otp_et.getText().toString().isEmpty() || otp_et.getText().toString().equals("")
						|| otp_et.getText().toString().length()<5
						){
					text_for_popup.setText(getResources().getString(R.string.enter_verificationcode));
					popupDialog.setCancelable(false);
					popupDialog.show();
				} else
				{
					BackgroundForValid_VerificationCode();
				}
			}
		});
		resend_verificationcode_bt.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				BackgroundForGettingVerificationCode();
			}
		});

		change_number_tv.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				changeMobileNumberAlert();
			}
		});

		signup_back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
				overridePendingTransition(R.anim.left_in, R.anim.right_out);
			}
		});
		if (back_btn != null) {
			back_btn.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					finish();
					overridePendingTransition(R.anim.left_in, R.anim.right_out);
				}
			});
		}
		/**
		 * to flip the arrow
		 */
		if(sessionManager.getLanguageCode().equals("ar"))
		{
			if (back_btn != null) {
				back_btn.setScaleX(-1);
			}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}


	/**
	 * <h>giveFocusToNextEditText</h>
	 * For moving the cursor on next editText.
	 * The value returned is as void.
	 * <pre>
	 *    Used for moving the cursor on next editText, there by calling the otp_edittextX.requestFocus().
	 * </pre>
	 *
	 * @return void
	 * @since 1.0
	 */

	private void giveFocusToNextEditText()
	{
		otp_et.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}
			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}
			@Override
			public void afterTextChanged(Editable editable)
			{
				if(!otp_et.getText().toString().isEmpty() && !otp_et.getText().toString().equals("")
						&& otp_et.getText().toString().length()==5)
				{
					/**
					 * to verify the otp
					 */
					BackgroundForValid_VerificationCode();
				}
			}
		});
	}


	/**
	 * <h>BackgroundForGettingVerificationCode</h>
	 * For sending the varification code.
	 * The value returned is as void.
	 * <pre>
	 *    Used for sending the varification code to mobile number entered by doing the Okhttp call for an api called getVerificationCode.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */

	private void BackgroundForGettingVerificationCode()
	{
		dialogL= Utility.GetProcessDialogNew(SignupVerificationActivity.this, getResources().getString(R.string.waitForVerification));
		dialogL.setCancelable(true);
		if (dialogL!=null)
		{
			dialogL.setCancelable(false);
			dialogL.show();
		}
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_mobile",mobileNo);
			jsonObject.put("ent_language", sessionManager.getLanguage());
			Utility.printLog("params to login "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getVerificationCode", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				try
				{
					if(result!=null)
					{
						JSONObject jsonObject=new JSONObject(result);
						errFlag=jsonObject.getString("errFlag");
						errMsg=jsonObject.getString("errMsg");

						if(dialogL!=null)
						{
							dialogL.dismiss();
						}
						if(errFlag!=null)
						{
							Utility.showToast(getApplicationContext(),errMsg);
						}
						else
						{
							Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
						}
					}
					else
					{
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
							}
						});
					}
				}
				catch(Exception e)
				{
					Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				dialogL.cancel();
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
			}
		});
	}


	/**
	 * <h>BackgroundForValid_VerificationCode</h>
	 * For Verifing the OTP.
	 * The value returned is as void.
	 * <pre>
	 *    Used for Verifing the OTP entered by doing the Okhttp call for an api called getVerificationCode.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */
	private void BackgroundForValid_VerificationCode()
	{
		dialogL= Utility.GetProcessDialogNew(SignupVerificationActivity.this, getResources().getString(R.string.please_wait));
		dialogL.setCancelable(true);
		if (dialogL!=null)
		{
			dialogL.setCancelable(false);
			dialogL.show();
		}
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_phone",mobileNo);
			jsonObject.put("ent_code",otp_et.getText().toString());
			jsonObject.put("ent_language", sessionManager.getLanguage());
			jsonObject.put("ent_register", "0");
			jsonObject.put("ent_user_type", 2);
			Utility.printLog("params to getverification code "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"verifyPhone", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{

				Utility.printLog("params to getverification code result "+result);
				try
				{
					if(result!=null)
					{
						JSONObject jsonObject=new JSONObject(result);
						errFlag=jsonObject.getString("errFlag");
						errMsg=jsonObject.getString("errMsg");

						if(dialogL!=null)
						{
							dialogL.dismiss();
						}
						if(errFlag!=null)
						{
							if(errFlag.equals("0"))
							{
								if(picSelected)
								{
									if(Utility.isNetworkAvailable(SignupVerificationActivity.this))
									{
										uploadToAmazon();
									}
									else
									{
										Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
									}
								}
								else
								{
									if(Utility.isNetworkAvailable(SignupVerificationActivity.this))
									{
										BackgroundTaskSignUp();
									}
									else
									{
										Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
									}
								}
							}
							else
							{
								text_for_popup.setText(errMsg);
								popupDialog.show();
								otp_et.setText("");
//								otp_edittext1.setText(null);
//								otp_edittext2.setText(null);
//								otp_edittext3.setText(null);
//								otp_edittext4.setText(null);
//								otp_edittext5.setText(null);
//								otp_edittext1.requestFocus();
							}
						}
						else
						{
							Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
						}
					}
					else
					{
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
							}
						});
					}
				}
				catch(Exception e)
				{
					Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				dialogL.cancel();
				Utility.printLog("on error for the login "+error);
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
			}
		});
	}


	/**
	 * <h>uploadToAmazon</h>
	 * For uploading the Image file on the amazon server.
	 * The value returned is as void.
	 * <pre>
	 *    Used for uploading the Image file on the amazon server.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */
	private void uploadToAmazon()
	{
		dialogL.show();
		UploadAmazonS3 amazonS3=UploadAmazonS3.getInstance(this,getString(R.string.AMAZON_POOL_ID));
		amazonS3.uploadData( getString(R.string.BUCKET_NAME), Utility.renameFile(VariableConstants.TEMP_PHOTO_FILE_NAME,mobileNo.substring(1)+".jpg"), new UploadAmazonS3.Upload_CallBack() {
			@Override
			public void sucess(String sucess) {
				if(Utility.isNetworkAvailable(SignupVerificationActivity.this))
				{
					dialogL.dismiss();
					Utility.printLog("amazon upload success ");
					BackgroundTaskSignUp();
				}
				else
				{
					dialogL.dismiss();
					Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
				}
			}
			@Override
			public void error(String errormsg) {
				dialogL.dismiss();
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
				Utility.printLog("error while uploading "+errormsg);
			}
		});
	}


	/**
	 *
	 * cb
	 *
	 * <h>BackgroundTaskSignUp</h>
	 * For requesting the registration on the server.
	 * The value returned is as void.
	 * <pre>
	 *    Used for requesting the registration on the server there by calling api "slaveSignup"
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */

	private void BackgroundTaskSignUp()
	{
		dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SignupVerificationActivity.this,getResources().getString(R.string.signing_you_up));
		dialogL.setCancelable(true);
		if(dialogL!=null)
		{
			dialogL.setCancelable(false);
			dialogL.show();
		}
		JSONObject jsonObject=new JSONObject();

		Utility.printLog("params_to_signup "
				+"\n"+"ent_first_name = "+firstName
				+"\n"+"ent_last_name = "+lastName
				+"\n"+"email = "+email
				+"\n"+"ent_password = "+password
				+"\n"+"mobileNo = "+mobileNo
				+"\n"+"lat = "+lat
				+"\n"+"longitude = "+longitude
				+"\n"+"referal = "+referal
				+"\n"+"sessionManager.getDeviceId() = "+sessionManager.getDeviceId()
				+"\n"+"sessionManager.getRegistrationId() = "+sessionManager.getRegistrationId()
		);

		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_first_name",firstName);
			jsonObject.put("ent_last_name",lastName);
			jsonObject.put("ent_email",email);
			jsonObject.put("ent_password",password);
			jsonObject.put("ent_mobile",mobileNo);
			if(lat!=null && longitude !=null)
			{
				jsonObject.put("ent_latitude",lat);
				jsonObject.put("ent_longitude",longitude);
			}
			else
			{
				jsonObject.put("ent_latitude","29.980087");
				jsonObject.put("ent_longitude","30.9789833");
			}
			if(referal!=null)
				jsonObject.put("ent_referral_code",referal);
			jsonObject.put("ent_terms_cond","true");
			jsonObject.put("ent_pricing_cond", "true");
			jsonObject.put("ent_dev_id",sessionManager.getDeviceId());
			jsonObject.put("ent_push_token",sessionManager.getRegistrationId());
			jsonObject.put("ent_device_type","2");
			jsonObject.put("ent_date_time",curenttime);
			jsonObject.put("ent_login_type",type);
			jsonObject.put("ent_dev_model", Build.MODEL);
			jsonObject.put("ent_dev_os",Build.VERSION.RELEASE);
			jsonObject.put("ent_app_version",Utility.currentVersion(this));
			jsonObject.put("ent_manf",Build.MANUFACTURER);
			jsonObject.put("ent_code",otp_et.getText().toString());
//			jsonObject.put("ent_code",otp_edittext1.getText().toString()+otp_edittext2.getText().toString()+
//					otp_edittext3.getText().toString()+otp_edittext4.getText().toString()+
//					otp_edittext5.getText().toString());
			jsonObject.put("ent_device_type","2");

//			if(gender.equalsIgnoreCase(getResources().getString(R.string.male)))
//			{
//				jsonObject.put("ent_gender","1");
//			}
//			else
//			{
//				jsonObject.put("ent_gender","2");
//			}
//			jsonObject.put("ent_dob",dob);
			if(picSelected)
			{
				jsonObject.put("ent_propic",getString(R.string.AMAZON_IMAGE_LINK)+mobileNo.substring(1)+".jpg");
			}
			else
			{
				jsonObject.put("ent_propic","");
			}
			jsonObject.put("ent_language", sessionManager.getLanguage());
			Utility.printLog("params to getverification code "+jsonObject);
		}
		catch (JSONException e)
		{
			if(dialogL!=null)
			{
				dialogL.dismiss();
			}
			Utility.printLog("params to getverification JSONException "+e);
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"slaveSignup", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				if (result!=null)
				{
					Utility.printLog("response for get code "+result);
					Gson gson = new Gson();
					LoginResponse response=gson.fromJson(result, LoginResponse.class);
					Utility.printLog("response for get code "+result);
					if(response!=null)
					{
						if(response.getErrFlag().equals("0"))
						{
							if(dialogL!=null)
							{
								dialogL.dismiss();
							}
							if(response.getToken()!=null)
								sessionManager.storeSessionToken(response.getToken());
							sessionManager.storeLoginId(email);
							sessionManager.setIsLogin(true);
							sessionManager.storeServerChannel(response.getServerChn());
							sessionManager.storeChannelName(response.getChn());
							sessionManager.setPresenceChn(response.getPresenseChn());
							sessionManager.setPublishKey(response.getPub());
							sessionManager.setSubscribeKey(response.getSub());
							sessionManager.setStripeKey(response.getStipeKey());
							sessionManager.setSid(response.getSid());
							sessionManager.setPaymentUrl(response.getPaymentUrl());
							sessionManager.setProfileImage( getString(R.string.AMAZON_IMAGE_LINK)+mobileNo.substring(1)+".jpg");
							sessionManager.setUserName(response.getName());
							sessionManager.setPhoneNumber(mobileNo);


							/**
							 * store all configured data from backen in SharedPReference
							 */
							sessionManager.setPubnubIntervalForHome(Long.parseLong(response.getConfigData().getPubnubIntervalHome()));
							sessionManager.setEtaIntervalForTracking(Long.parseLong(response.getConfigData().getDistanceMatrixInterval()));

						    if(sessionManager.getEmailId().equals(""))
							{
								/**
								 * to set the google matrix keys from backend and
								 * set them in shared preference
								 */
								List<String> emailIdArrayList = new ArrayList<>();
								emailIdArrayList.add(email);
								String jsonText = gson.toJson(emailIdArrayList);
								//sessionManager.setEmailId(jsonText);   //akbar commented
							}
							else
							{
								//Retrieve the distance matrix keys saved in shared preference and remove the one used before
								String[] emailIdArray = gson.fromJson(sessionManager.getEmailId(), String[].class);
								//List<String> emailIdArray  = Arrays.asList(emailIdArray);
								List<String> savedEmailIdList = new LinkedList<>(Arrays.asList(emailIdArray));
								savedEmailIdList.add(email);
								/**
								 * to set the new list after adding an email
								 */
								String jsonText = gson.toJson(savedEmailIdList);
								//sessionManager.setEmailId(jsonText);//akbar commented
							}

							/**
							 * to set the google matrix keys from backend and
							 * set them in shared preference
							 */
							List<String> googleMatrixKeys = new ArrayList<>();
							googleMatrixKeys.addAll(response.getConfigData().getGoogleKeysArray());
							String jsonText = gson.toJson(googleMatrixKeys);
							sessionManager.setGoogleMatrixData(jsonText);

							/**
							 * to set the server key in shared reference
							 * from 0th index from backend
							 */
							if(!response.getConfigData().getGoogleKeysArray().isEmpty())
								sessionManager.setServerKey(response.getConfigData().getGoogleKeysArray().get(0));


							Utility.showToast(getApplicationContext(),getString(R.string.signup_success));
							sessionManager.setIsLogin(true);
							sessionManager.setCardToken("");
							sessionManager.setLast4Digits("");
							sessionManager.setBookingReponseFromPubnub("");
							sessionManager.setPubnubResponseForNonBooking("");
							sessionManager.setPubnubResponse("");
//							Intent intent=new Intent(SignupVerificationActivity.this,SignupPayment.class);
							Intent intent=new Intent(SignupVerificationActivity.this,MainActivity.class);
							startActivity(intent);
							finish();
							overridePendingTransition(R.anim.right_in, R.anim.left_out);
						}
						else
						{
							Utility.showToast(getApplicationContext(),response.getErrMsg());
							if(dialogL!=null)
							{
								dialogL.dismiss();
							}
						}
					}
					else
					{
						Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
						if(dialogL!=null)
						{
							dialogL.dismiss();
						}
					}
				}else
				{
					Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
				}
			}

			@Override
			public void onError(String error)
			{
				Utility.printLog("response for get code "+error);
				if(dialogL!=null)
				{
					dialogL.dismiss();
				}
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
			}
		});
	}


	private void changeMobileNumberAlert()
	{
		final Dialog popupWithTwoButtons=Utility.showPopupWithTwoButtons(SignupVerificationActivity.this);
		popupWithTwoButtons.setCancelable(false);
		TextView dialogText= (TextView) popupWithTwoButtons.findViewById(R.id.text_for_popup);
		TextView title_popup= (TextView) popupWithTwoButtons.findViewById(R.id.title_popup);
		final RelativeLayout promocode_layout= (RelativeLayout) popupWithTwoButtons.findViewById(R.id.promocode_layout);
		popupWithTwoButtons.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

		/**
		 * to set language support
		 */
		Utility.setLanguageForDialog(popupWithTwoButtons);

		TextView yes_button= (TextView) popupWithTwoButtons.findViewById(R.id.yes_button);
		TextView no_button= (TextView) popupWithTwoButtons.findViewById(R.id.no_button);
		final EditText et_promoCode= (EditText) popupWithTwoButtons.findViewById(R.id.et_promoCode);

		final TextInputLayout number_edittext_layout= (TextInputLayout) popupWithTwoButtons.findViewById(R.id.promocode_edittext_layout);
		final TextView clear_number= (TextView) popupWithTwoButtons.findViewById(R.id.clear_promo);
		final RelativeLayout progress_bar_layout= (RelativeLayout) popupWithTwoButtons.findViewById(R.id.progress_bar_layout);
//		et_promoCode.setText(session.getPromocode());


		//aakash change
		number_edittext_layout.setHint("Enter Mobile Number");
		clear_number.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				et_promoCode.setText("");
			}
		});
		et_promoCode.setHint(getString(R.string.mobile_no));
		et_promoCode.setInputType(InputType.TYPE_CLASS_NUMBER);
		et_promoCode.setText(mobileNo);
		//et_promoCode.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);

		et_promoCode.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				if(s.length()>0)
				{
					clear_number.setVisibility(VISIBLE);
					et_promoCode.setSelection(et_promoCode.getText().length());
				}
				else
				{
					clear_number.setVisibility(View.GONE);
				}
				number_edittext_layout.setError(null);
				number_edittext_layout.setErrorEnabled(false);
			}
		});
		yes_button.setText(getResources().getString(R.string.change));
		no_button.setText(getResources().getString(R.string.cancel));
		dialogText.setVisibility(View.GONE);
		title_popup.setVisibility(View.GONE);
		promocode_layout.setVisibility(View.VISIBLE);
		if(sessionManager.getLanguageCode().equals("en"))
			Utility.setTypefaceMuliRegular(SignupVerificationActivity.this,et_promoCode);
		et_promoCode.setMaxLines(1);
		et_promoCode.setSingleLine(true);
		yes_button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(et_promoCode.getText().toString().trim().equals("") && et_promoCode.getText().toString().length()<10)
				{
//					sessionManager.setPromocode("");
					number_edittext_layout.setErrorEnabled(true);
					number_edittext_layout.setError(getResources().getString(R.string.invalid_mobile_number));
					popupWithTwoButtons.dismiss();
				}
				else
				{
					number_edittext_layout.setError(null);
					number_edittext_layout.setErrorEnabled(false);
					BackgroundForGettingVerificationCode(et_promoCode.getText().toString().trim(),clear_number,progress_bar_layout,
							popupWithTwoButtons,number_edittext_layout);
				}
			}
		});
		no_button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				promocode_layout.setVisibility(View.GONE);
				number_edittext_layout.setError(null);
				et_promoCode.setText(null);
				popupWithTwoButtons.dismiss();
			}
		});
		popupWithTwoButtons.show();
	}


	/**
	 * <h>BackgroundForGettingVerificationCode</h>
	 * For sending the varification code.
	 * The value returned is as void.
	 * <pre>
	 *    Used for sending the varification code to mobile number entered by doing the Okhttp call for an api called getVerificationCode.
	 * </pre>
	 *
	 * @param
	 * @return void
	 * @since 1.0
	 */

	private void BackgroundForGettingVerificationCode
	(final String mobileNo1, TextView clear_number, final RelativeLayout progress_bar_layout, final Dialog popupWithTwoButtons, final TextInputLayout number_edittext_layout)
	{

		dialogL= Utility.GetProcessDialogNew(SignupVerificationActivity.this, getResources().getString(R.string.waitForVerification));
		dialogL.setCancelable(true);
		if (dialogL!=null)
		{
			dialogL.setCancelable(false);
			dialogL.show();
		}
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_mobile",mobileNo);
			jsonObject.put("ent_language", sessionManager.getLanguage());
			Utility.printLog("params to login "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getVerificationCode", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				try
				{
					if(result!=null)
					{
						JSONObject jsonObject=new JSONObject(result);
						errFlag=jsonObject.getString("errFlag");
						errMsg=jsonObject.getString("errMsg");

						if(dialogL!=null)
						{
							dialogL.dismiss();
						}
						if(errFlag!=null) {
							if (errFlag.equals("0")) {

								if(popupWithTwoButtons!=null)
									popupWithTwoButtons.dismiss();

								mobileNo=mobileNo1;
								mobile_number_tv.setText(mobileNo);
								Utility.showToast(getApplicationContext(), errMsg);
							} else {
								Utility.showToast(getApplicationContext(), errMsg);
							}
						}
						else
						{
							Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
						}
					}
					else
					{
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								Utility.showToast(getApplicationContext(),getString(R.string.requestTimeout));
							}
						});
					}
				}
				catch(Exception e)
				{
					Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				dialogL.cancel();
				Utility.showToast(getApplicationContext(),getString(R.string.network_connection_fail));
			}
		});
	}


}
