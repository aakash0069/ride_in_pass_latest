package com.roadyo.passenger.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Akbar
 */

public class LangaugesPojo implements Serializable
{
    private ArrayList<LanguagesClass> languages;
    private MandatoryVersion versions;

    public MandatoryVersion getVersions() {
        return versions;
    }

    public ArrayList<LanguagesClass> getLanguages() {
        return languages;
    }
}
