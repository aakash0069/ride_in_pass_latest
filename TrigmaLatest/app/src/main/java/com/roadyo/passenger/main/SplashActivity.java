package com.roadyo.passenger.main;

import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.bloomingdalelimousine.ridein.R;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.LangaugesPojo;
import com.roadyo.passenger.pojo.LanguagesClass;
import com.roadyo.passenger.pubnu.pojo.PubnubResponseNew;
import com.threembed.utilities.LocationUtil;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import java.util.ArrayList;
import io.fabric.sdk.android.Fabric;
import static com.threembed.utilities.Utility.setLocale;

public class SplashActivity extends FragmentActivity implements OnClickListener,LocationUtil.GetLocationListener
{
	private LinearLayout login_register;
	private SessionManager session;
	private Animation logiButtonAnimation;
	private JSONObject jsonObject;
	private LocationUtil networkUtil ;
	private ProgressDialog dialogL;
	public static boolean visibility=false;
	private double currentLatitude=0,currentLongitude=0;
	private Button signin,register;
	private RelativeLayout notification_layout;
	private TextView notification_content;
	private Dialog mandatoryFieldPopup;
	private Utility utility;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);
		intializeVariables() ;

	}

	private void ShowAlert()
	{
		Dialog dialog=Utility.showPopupWithOneButton(SplashActivity.this);
		TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
		TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
		TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
		title_popup.setText(getResources().getString(R.string.alert));
		yes_button.setText(getResources().getString(R.string.ok));
		text_for_popup.setText(getResources().getString(R.string.network_connection_fail));
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			/**
			 * Check for the integer request code originally supplied to startResolutionForResult(). in LOcationUtil
			 */
			case LocationUtil.REQUEST_CHECK_SETTINGS:
				switch (resultCode) {
					case RESULT_OK:
						/**
						 * to restart the location after enabling the OK button on popup
						 */
						networkUtil.startLocationUpdates();
						break;
					case RESULT_CANCELED:
						/**
						 * If user cancels the enable request for gps then call again to show popup for enablisng gps
						 */
						networkUtil.checkLocationSettings();
						break;
				}
				break;
		}
	}

	private void BackgroundSessionCheck()
	{
		jsonObject=new JSONObject();
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_sess_token",session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_user_type", 2);
			jsonObject.put("ent_date_time", curenttime);
			Utility.printLog("params to check session "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"checkSession", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				String errFlag ,errMsg ;
				Utility.printLog( "success for check session  " + result);
				if(result!=null)
				{
					try
					{
						jsonObject=new JSONObject(result);
						errFlag=jsonObject.getString("errFlag");
						errMsg=jsonObject.getString("errMsg");

						assert errFlag != null;
						if(errFlag.equals("0"))
						{
							/**
							 * to get the current location
							 */
							getCurrentLocation();
						}
						else
						{
							if(Utility.isNetworkAvailable(SplashActivity.this)) {
								getLanguages();
							}
							else {
								Utility.showToast(SplashActivity.this, getString(R.string.network_connection_fail));
							}

							Utility.showToast(SplashActivity.this,errMsg);
							session.setIsLogin(false);
							login_register.setVisibility(View.VISIBLE);
							login_register.startAnimation(logiButtonAnimation);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else
				{
					Utility.showToast(SplashActivity.this,getResources().getString(R.string.network_connection_fail));
				}
			}

			@Override
			public void onError(String error)
			{
				Utility.showToast(SplashActivity.this,getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	private void getPubnubDataInAPi()
	{
		jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_sess_token",session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("lt",String.valueOf(currentLatitude));
			jsonObject.put("lg", String.valueOf(currentLongitude));
			jsonObject.put("sid",session.getSid());
			jsonObject.put("ent_splash","1");
			Utility.printLog("params to check session "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.GETTING_PUBNUB_URL1), jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				Utility.printLog( "success for check session  pubnub" + result);
				if(result!=null)
				{
					Gson gson = new Gson();
					PubnubResponseNew pubnubResponse = gson.fromJson(result, PubnubResponseNew.class);
					/**
					 * to check for mandatory update case
					 * and show popup if mandatory update
					 */
					if(Utility.currentVersion(SplashActivity.this).compareToIgnoreCase
							(pubnubResponse.getConfigData().getVersions().getAndCust())<0)
					{
						/**
						 * 	to show the mandatory update from playstore
						 */
						if(mandatoryFieldPopup!=null)
							if (!mandatoryFieldPopup.isShowing())
								mandatoryFieldPopup.show();
					}
					else {
						if(mandatoryFieldPopup!=null)
							if (mandatoryFieldPopup.isShowing())
								mandatoryFieldPopup.dismiss();

						session.setPubnubResponse(result);
						Utility.printLog(" successCallback message from pubnub  splash " +result);
						networkUtil.stop_Location_Update();
						Intent intent=new Intent(SplashActivity.this,MainActivity.class);
						intent.putExtra("COMING_FROM_SPLASH",true);
						startActivity(intent);
						finish();
						overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
					}
				}
				else
				{
					Utility.showToast(SplashActivity.this,getResources().getString(R.string.network_connection_fail));
				}
			}
			@Override
			public void onError(String error)
			{
				Utility.showToast(SplashActivity.this,getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	private void intializeVariables()
	{
		session=new SessionManager(SplashActivity.this);
		Utility.printLog("language in session "+session.getLanguageCode());
		utility=new Utility();
		mandatoryFieldPopup=utility.mandatoryUpdate(SplashActivity.this);
		visibility = true;
		login_register=(LinearLayout)findViewById(R.id.login_buttons);
		signin = (Button) findViewById(R.id.signin);
		register = (Button) findViewById(R.id.register);
		logiButtonAnimation= AnimationUtils.loadAnimation(this,R.anim.bounce);
		dialogL=com.threembed.utilities.Utility.GetProcessDialogNew(SplashActivity.this,getResources().getString(R.string.loggingIn));
		dialogL.setCancelable(false);
		notification_layout= (RelativeLayout) findViewById(R.id.notification_layout);
		TextView notification_title = (TextView) findViewById(R.id.notification_title);
		notification_content= (TextView) findViewById(R.id.notification_content);
		TextView notification_submit_button = (TextView) findViewById(R.id.notification_submit_button);

		String pushData=getIntent().getStringExtra("PUSH_DATA");
		Utility.printLog("data from push in splahs "+pushData);

		if(!VariableConstants.PUSH_MESSAGE.equals(""))
		{
			showPopupForOneButton(VariableConstants.PUSH_MESSAGE);
			VariableConstants.PUSH_MESSAGE="";
		}
		///////////////////////////////////////////////////////////////////////////////////////////////
		// Get fcm  token and device id
		session.storeRegistrationId(FirebaseInstanceId.getInstance().getToken());

		/**
		 * if reg id is null then refresh once again
		 */
		if(session.getRegistrationId()==null)
		{
			FirebaseInstanceIdService firebaseInstanceIdService=new FirebaseInstanceIdService();
			firebaseInstanceIdService.onTokenRefresh();
		}
		Utility.printLog("token generated "+session.getRegistrationId());
		session.storeDeviceId(getDeviceId());
		Utility.printLog("device id out "+session.getDeviceId());
		//////////////////////////////////////////////////////////////////////////////////////////////

		signin.setOnClickListener(this);
		register.setOnClickListener(this);

		notification_submit_button.setOnClickListener(this);

		if(session.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliBold(this, notification_title);
			Utility.setTypefaceMuliBold(this,notification_content);
			Utility.setTypefaceMuliBold(this, notification_submit_button);
		}
	}
	@Override
	public void updateLocation(Location location)
	{
		if(location!=null)
		{
			currentLatitude = location.getLatitude();
			currentLongitude = location.getLongitude();
			Utility.printLog("current lat long "+ currentLatitude +" "+ currentLongitude);
			session.setCurrentLat((float) currentLatitude);
			session.setCurrentLong((float) currentLongitude);

			if(session.isLoggedIn())
				getPubnubDataInAPi();
		}
	}

	@Override
	public void location_Error(String error)
	{
		Utility.printLog("error in getting location "+error);
	}

	private void showPopupForOneButton(String messsage)
	{
		notification_layout.setVisibility(View.VISIBLE);
		notification_content.setText(messsage.replace("\\n","\n") );
		NotificationManager notificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
		if(notificationManager!=null)
		{
			notificationManager.cancelAll();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		switch (session.getLanguageCode())
		{
			case "ar":
			{
				setLocale("ar",this);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
				}
				signin.setText("دخول");
				register.setText("تسجيل");
				/**
				 * setting the typeface
				 */
				signin.setTypeface(null);
				register.setTypeface(null);
				break;
			}
			case "en":
			{
				setLocale("en",this);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
				}
				signin.setText("Login");
				register.setText("Register");

				/**
				 * setting the typeface
				 */
				Utility.setTypefaceMuliRegular(SplashActivity.this, signin);
				Utility.setTypefaceMuliRegular(SplashActivity.this, register);
				break;
			}
		}

		if(Utility.isNetworkAvailable(SplashActivity.this))
		{
			if(session.isLoggedIn())
			{
				if(Utility.isNetworkAvailable(SplashActivity.this))
				{
					BackgroundSessionCheck();
				}
				else
				{
					ShowAlert();
				}
			}
			else
			{
				if(Utility.isNetworkAvailable(SplashActivity.this)) {
					getLanguages();
				}
				else {
					Utility.showToast(SplashActivity.this, getString(R.string.network_connection_fail));
				}
				login_register.setVisibility(View.VISIBLE);
				login_register.startAnimation(logiButtonAnimation);
			}
		}
		else
		{
			Utility.showToast(SplashActivity.this,getString(R.string.network_connection_fail));
		}
	}


    public void getLanguages()
    {
        JSONObject jsonObject1 = new JSONObject();
        OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getLangueages",jsonObject1, new OkHttpRequestObject.JsonRequestCallback()
        {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("repsne from getlanguages "+result);
                if(result!=null)
                {
                    if(session.getLanguageCode().equals(""))
                    {
                        session.setLanguageCode("en");
                        session.setLanguage("0");
                    }
                    Gson gson=new Gson();
                    LangaugesPojo langaugesPojo=gson.fromJson(result,LangaugesPojo.class);
                    /**
                     * to check for mandatory update case
                     * and show popup if mandatory update
                     */
                    if(Utility.currentVersion(SplashActivity.this).compareToIgnoreCase(langaugesPojo.getVersions().getAndCust())<0)
                    {
						/**
						 * 	to show the mandatory update from playstore
						 */
						if(mandatoryFieldPopup!=null)
							if (!mandatoryFieldPopup.isShowing())
								mandatoryFieldPopup.show();
                    }
                }
                else
                {
                    Utility.showToast(SplashActivity.this,getString(R.string.network_connection_fail));
                }
            }
            @Override
            public void onError(String error) {
            }
        });
    }

	private void getCurrentLocation()
	{
		if (networkUtil == null)
		{
			networkUtil = new LocationUtil(SplashActivity.this, this);
		}
		else
		{
			networkUtil.checkLocationSettings();
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.register:
			{
				if(Utility.isNetworkAvailable(SplashActivity.this))
				{
					Intent intent = new Intent(SplashActivity.this,SelectFacebookOrEmail.class);
					Bundle bundle = new Bundle();
					bundle.putString("TITLE",getResources().getString(R.string.register));
					bundle.putString("SELECTED_TYPE","2");
					intent.putExtra("TITLE_INFO",bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
				}
				else
				{
					ShowAlert();
				}
				break;
			}
			case R.id.signin:
			{
				if(Utility.isNetworkAvailable(SplashActivity.this))
				{
					Intent intent = new Intent(SplashActivity.this,SelectFacebookOrEmail.class);
					Bundle bundle = new Bundle();
					bundle.putString("TITLE",getResources().getString(R.string.login));
					bundle.putString("SELECTED_TYPE","1");
					intent.putExtra("TITLE_INFO",bundle);
					startActivity(intent);
					overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
				}
				else
				{
					ShowAlert();
				}
				break;
			}
			case R.id.notification_submit_button:
			{
				notification_layout.setVisibility(View.GONE);
				break;
			}
		}
	}
	@Override
	protected void onPause()
	{
		super.onPause();
		visibility=false;
		if(networkUtil!=null)
		{
			networkUtil.stop_Location_Update();
		}
		if(mandatoryFieldPopup!=null)
			if (mandatoryFieldPopup.isShowing())
				mandatoryFieldPopup.dismiss();
	}
	@Override
	public void onBackPressed() {
		if(dialogL!=null && dialogL.isShowing())
		{
			dialogL.dismiss();
		}
		finish();
		overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
	}


	//to get device id
	@SuppressLint("HardwareIds")
	public  String getDeviceId()
	{
		TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}
}
