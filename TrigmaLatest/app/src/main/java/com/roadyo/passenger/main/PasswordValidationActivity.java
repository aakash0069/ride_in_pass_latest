package com.roadyo.passenger.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.bloomingdalelimousine.ridein.R;

public class PasswordValidationActivity extends AppCompatActivity {

    private String OTP;
    private EditText temp_pass_et;
    private TextInputLayout temp_pw_layout;
    private Button proceed;
    private String ResetData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_validation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        OTP = "";
        ResetData = "";

        temp_pw_layout = (TextInputLayout) findViewById(R.id.temp_pw_layout);
        temp_pass_et = (EditText) findViewById(R.id.temp_pass_et);
        proceed = (Button) findViewById(R.id.proceed);

        Bundle bundle = getIntent().getExtras();

        if(bundle!=null && bundle.containsKey("OTP")){
            OTP = bundle.getString("OTP");
        }

        if(bundle!=null && bundle.containsKey("ResetData")){
            ResetData = bundle.getString("ResetData");
        }

        temp_pass_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                temp_pw_layout.setError(null);
            }
        });

        RelativeLayout rl_signin = (RelativeLayout) findViewById(R.id.rl_signin);
        ImageButton back_btn = (ImageButton) findViewById(R.id.back_btn);
        rl_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("OTP_OBTAINED"," OTP "+OTP+" USER ADDED "+temp_pass_et.getText().toString());

                if(temp_pass_et.getText().toString().equals(OTP)){
                    Intent intent = new Intent(PasswordValidationActivity.this,PasswordUpdateActivity.class);
                    intent.putExtra("ResetData",ResetData);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
                } else {
                    temp_pw_layout.setError(getString(R.string.invalid_password));
                }
            }
        });


    }

}
