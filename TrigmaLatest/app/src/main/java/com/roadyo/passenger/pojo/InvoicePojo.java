package com.roadyo.passenger.pojo;

/**
 * Created by rahul on 30/9/16.
 */
public class InvoicePojo
{
    String discountType;
    String discountVal;
    String min_fare;
    String code;
    String tipPercent;
    String distanceFee;
    String timeFee;
    String airportFee;
    String tip;
    String pickupDt;
    String dropDt;
    String dis;
    String dur;
    String subTotal;
    String payType;
    String fName;
    String addr1;
    private String Favouritepickup;

    public String getCardDeduct() {
        return cardDeduct;
    }

    private String cardDeduct;

    public String getFavouritedrop() {
        return Favouritedrop;
    }

    public String getFavouritepickup() {
        return Favouritepickup;
    }

    private String Favouritedrop;

    public String getWaitingTime() {
        return waitingTime;
    }

    private String waitingTime;
    private String waitingFee;

    public String getWaitingFee() {
        return waitingFee;
    }

    public String getWalletDebitCredit() {
        return walletDebitCredit;
    }

    String walletDebitCredit;

    public String getCashCollected() {
        return CashCollected;
    }

    String CashCollected;

    public String getRouteImg() {
        return routeImg;
    }

    String routeImg;

    public String getWalletDeducted() {
        return walletDeducted;
    }

    String walletDeducted;

    public String getDropAddr1() {
        return dropAddr1;
    }

    public String getAddr1() {
        return addr1;
    }

    String dropAddr1;

    public String getR() {
        return r;
    }

    String r;

    public String getlName() {
        return lName;
    }

    public String getfName() {
        return fName;
    }

    String lName;

    public String getpPic() {
        return pPic;
    }

    String pPic;

    public String getAmount() {
        return amount;
    }

    String amount;

    public String getBaseFee() {
        return baseFee;
    }

    String baseFee;
    private String sumOfLastDue;

    public String getDuePayment() {
        return duePayment;
    }

    private String duePayment;

    public String getSumOfLastDue() {
        return sumOfLastDue;
    }

    public String getDiscountVal() {
        return discountVal;
    }

    public String getMin_fare() {
        return min_fare;
    }

    public String getCode() {
        return code;
    }

    public String getTipPercent() {
        return tipPercent;
    }

    public String getDistanceFee() {
        return distanceFee;
    }

    public String getTimeFee() {
        return timeFee;
    }

    public String getAirportFee() {
        return airportFee;
    }

    public String getTip() {
        return tip;
    }

    public String getPickupDt() {
        return pickupDt;
    }

    public String getDropDt() {
        return dropDt;
    }

    public String getDis() {
        return dis;
    }

    public String getDur() {
        return dur;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public String getPayType() {
        return payType;
    }

    public String getDiscountType() {

        return discountType;
    }
}
