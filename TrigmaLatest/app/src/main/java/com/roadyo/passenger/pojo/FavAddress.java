package com.roadyo.passenger.pojo;

import java.io.Serializable;

/**
 * @author  AKbar
 */
public class FavAddress implements Serializable
{
    /*    "aid":"58762625c791521f7d2839f6",
    "address1":"54, RBI Colony, Hebbal",
    "address2":"",
    "zipCode":"",
    "addressType":"",
    "location":{
        "longitude":77.58956708014,
        "latitude":13.028899541901
    },
    "title":"home"*/
    private String addressID;
    private String address1;
    private String locationType;
    private String lat;


    private String lng;

    public String getAddressID() {
        return addressID;
    }

    public String getAddress1() {
        return address1;
    }

    public String getLocationType() {
        return locationType;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
