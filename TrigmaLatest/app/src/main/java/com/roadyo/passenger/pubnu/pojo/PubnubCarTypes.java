package com.roadyo.passenger.pubnu.pojo;

import java.io.Serializable;

public class PubnubCarTypes implements Serializable
{
/*	    "surg_price":"4",
    "min_fare":10,
    "type_desc":false,
    "vehicle_img":"1978931867187.png",
    "type_name":"Bicycle",
    "vehicle_img_off":"2422107296424.png",
    "basefare":10,
    "price_per_min":0.5,
    "price_per_km":0.2,
    "MapIcon":"10523588162905.png",
    "max_size":5,
    "type_id":11
	*/

	/*    "type_id":29,
    "type_name":"Silver ",
    "max_size":4,
    "basefare":20,
    "min_fare":15,
    "price_per_min":10,
    "price_per_km":10,
    "type_desc":"Silver",
    "MapIcon":"2265326514108.png",
    "vehicle_img":"1656556598172.png",
    "vehicle_img_off":"10612498630692.png",
    "surg_price":"",
    "order":0*/

	//*******************************************************************************8

//	public String getVehicle_img() {
//		return vehicle_img;
//	}
//
//	public String getVehicle_img_off() {
//		return vehicle_img_off;
//	}
//
//	public String getMapIcon() {
//		return MapIcon;
//	}
//
//	String min_fare;
//	String type_desc;
//	String type_name;
//	String basefare;
//	String price_per_min;
//	String price_per_km;
//	String max_size;
//	String type_id;
//	String vehicle_img;
//	String vehicle_img_off;
//	String MapIcon;
//
//	public String getSurg_price() {
//		return surg_price;
//	}
//
//
//	String surg_price;
//
//	public String getMin_fare() {
//		return min_fare;
//	}
//
//	public String getType_name() {
//		return type_name;
//	}
//
//	public String getBasefare() {
//		return basefare;
//	}
//
//	public String getPrice_per_min() {
//		return price_per_min;
//	}
//
//	public String getPrice_per_km() {
//		return price_per_km;
//	}
//
//	public String getMax_size() {
//		return max_size;
//	}
//
//	public String getType_id() {
//		return type_id;
//	}


	private String min_fare;

	private String max_size;

	private String type_id;

	private String surg_price;

	private String type_desc;

	private String order;

	private String servicefare;

	private String vehicle_img;

	private String type_name;

	private String vehicle_img_off;

	private String basefare;

	private String price_per_min;

	private String price_per_km;

	private String MapIcon;

	private String eta ="...";

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getMin_fare ()
	{
		return min_fare;
	}

	public void setMin_fare (String min_fare)
	{
		this.min_fare = min_fare;
	}

	public String getMax_size ()
	{
		return max_size;
	}

	public void setMax_size (String max_size)
	{
		this.max_size = max_size;
	}

	public String getType_id ()
	{
		return type_id;
	}

	public void setType_id (String type_id)
	{
		this.type_id = type_id;
	}

	public String getSurg_price ()
	{
		return surg_price;
	}

	public void setSurg_price (String surg_price)
	{
		this.surg_price = surg_price;
	}

	public String getType_desc ()
	{
		return type_desc;
	}

	public void setType_desc (String type_desc)
	{
		this.type_desc = type_desc;
	}

	public String getOrder ()
	{
		return order;
	}

	public void setOrder (String order)
	{
		this.order = order;
	}

	public String getServicefare ()
	{
		return servicefare;
	}

	public void setServicefare (String servicefare)
	{
		this.servicefare = servicefare;
	}

	public String getVehicle_img ()
	{
		return vehicle_img;
	}

	public void setVehicle_img (String vehicle_img)
	{
		this.vehicle_img = vehicle_img;
	}

	public String getType_name ()
	{
		return type_name;
	}

	public void setType_name (String type_name)
	{
		this.type_name = type_name;
	}

	public String getVehicle_img_off ()
	{
		return vehicle_img_off;
	}

	public void setVehicle_img_off (String vehicle_img_off)
	{
		this.vehicle_img_off = vehicle_img_off;
	}

	public String getBasefare ()
	{
		return basefare;
	}

	public void setBasefare (String basefare)
	{
		this.basefare = basefare;
	}

	public String getPrice_per_min ()
	{
		return price_per_min;
	}

	public void setPrice_per_min (String price_per_min)
	{
		this.price_per_min = price_per_min;
	}

	public String getPrice_per_km ()
	{
		return price_per_km;
	}

	public void setPrice_per_km (String price_per_km)
	{
		this.price_per_km = price_per_km;
	}

	public String getMapIcon ()
	{
		return MapIcon;
	}

	public void setMapIcon (String MapIcon)
	{
		this.MapIcon = MapIcon;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [min_fare = "+min_fare+", max_size = "+max_size+", type_id = "+type_id+", surg_price = "+surg_price+", type_desc = "+type_desc+", order = "+order+", servicefare = "+servicefare+", vehicle_img = "+vehicle_img+", type_name = "+type_name+", vehicle_img_off = "+vehicle_img_off+", basefare = "+basefare+", price_per_min = "+price_per_min+", price_per_km = "+price_per_km+", MapIcon = "+MapIcon+"]";
	}

}
