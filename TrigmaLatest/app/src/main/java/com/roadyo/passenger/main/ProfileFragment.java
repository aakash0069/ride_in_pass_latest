package com.roadyo.passenger.main;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bloomingdalelimousine.ridein.R;
import com.google.gson.Gson;
import com.roadyo.passenger.pojo.GetMyProfileResponse;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.threembed.utilities.DatabasePickupHandler;
import com.threembed.utilities.InternalStorageContentProvider;
import com.threembed.utilities.OkHttpRequestObject;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.UploadAmazonS3;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import eu.janmuller.android.simplecropimage.CropImage;

public class ProfileFragment extends Fragment implements OnClickListener
{
	private static final String TAG = "HomePageFragment";
	private View view;
	private EditText first_name,phone,last_name;
	private ImageView profile_pic;
	private final int REQUEST_CODE_GALLERY      = 0x1;
	private final int REQUEST_CODE_TAKE_PICTURE = 0x2;
	private final int REQUEST_CODE_CROP_IMAGE   = 0x3;
	private File mFileTemp;
	private String getprofileServerResponse;
	private boolean editpressed=true,savepressed=false;
	private  EditText email;
	private SessionManager session;
	private ProgressBar progress_bar;
	private MainActivity activity;
	private RelativeLayout actionbar_layout;
	ProgressDialog dialogL;
	String errFlag,errMsg;
	private String dobText="";
	private boolean isPicSelected=false;
	private ScrollView detail_layout;
	private Button edit_or_save_btn;

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		setHasOptionsMenu(true);
		if (view != null)
		{
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}
		try
		{
			view = inflater.inflate(R.layout.profile_layout, container, false);

		} catch (InflateException e)
		{
			/* map is already there, just return view as it is */
			Log.e(TAG, "onCreateView  InflateException "+e);
		}

		initializeVariables(view);

//		activity.edit_profile.setOnClickListener(new OnClickListener() {
		edit_or_save_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(editpressed)
				{
					showAccountVerificationAlert();
				}
				else if(savepressed)
				{
					Utility.printLog("inside options savepressed");
					final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

					disableEdittext();
					if((!first_name.getText().toString().trim().isEmpty()))
					{
						if((!last_name.getText().toString().trim().isEmpty()))
						{
							if (!email.getText().toString().trim().isEmpty())
							{
								if (android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
									if (isPicSelected) {
										if (Utility.isNetworkAvailable(getActivity())) {
											uploadToAmazon();
										} else {
											Utility.showToast(getActivity(), getResources().getString(R.string.network_connection_fail));
										}
									} else {
										if (Utility.isNetworkAvailable(getActivity())) {
											new BackgroundForUpdateProfile().execute();
										} else {
											Utility.showToast(getActivity(), getResources().getString(R.string.network_connection_fail));
										}
									}
									disableEdittext();
//									activity.edit_profile.setText(getResources().getString(R.string.edit));
									edit_or_save_btn.setText(getResources().getString(R.string.edit1));
									editpressed = true;
									savepressed = false;
								} else {
									email.setError(getResources().getString(R.string.invalid_email));
									enableEdittext();
								}
							} else {
								email.setError(getResources().getString(R.string.email_empty));
								enableEdittext();
							}
						}
						else
						{
							last_name.setError(getResources().getString(R.string.last_name_empty));
							enableEdittext();
						}
					}
					else
					{
						first_name.setError(getResources().getString(R.string.first_name_empty));
						enableEdittext();
					}
				}
			}
		});

		if(Utility.isNetworkAvailable(getActivity()))
		{
			new BackgroundTaskGetProfile().execute();
		}
		else
		{
			Intent homeIntent=new Intent("com.threembed.roadyo.internetStatus");
			homeIntent.putExtra("STATUS", "0");
			getActivity().sendBroadcast(homeIntent);
		}

		disableEdittext();

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mFileTemp = new File(Environment.getExternalStorageDirectory(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}
		else
		{
			mFileTemp = new File(getActivity().getFilesDir(), VariableConstants.TEMP_PHOTO_FILE_NAME);
		}

		return view;
	}

	/**
	 * <h>uploadToAmazon</h>
	 * For uploading Image file to Amazon s3 bucket.
	 * The value returned is as void.
	 * <pre>
	 *    Used for uploading Image file to Amazon s3 bucket.
	 * </pre>
	 * @since 1.0
	 */

	private void uploadToAmazon()
	{
		dialogL.show();
		UploadAmazonS3 amazonS3=UploadAmazonS3.getInstance(getActivity(),
				getString(R.string.AMAZON_POOL_ID));
		amazonS3.uploadData(getString(R.string.BUCKET_NAME),
				Utility.renameFile(VariableConstants.TEMP_PHOTO_FILE_NAME,phone.getText().toString().substring(1)+".jpg"),
				new UploadAmazonS3.Upload_CallBack() {
			@Override
			public void sucess(String sucess) {
				if(Utility.isNetworkAvailable(getActivity()))
				{
					dialogL.dismiss();
					/**
					 * to set the image into image view and
					 * add the write the image in the file
					 */
					Utility.printLog("amazon upload success ");
					new BackgroundForUpdateProfile().execute();
				}
				else
				{
					dialogL.dismiss();
					Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
				}
			}
			@Override
			public void error(String errormsg) {
				dialogL.dismiss();
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();

//		activity.edit_profile.setVisibility(View.VISIBLE);
		edit_or_save_btn.setVisibility(View.VISIBLE);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		editpressed=true;
		savepressed=false;
//		activity.edit_profile.setText(getResources().getString(R.string.edit));
		edit_or_save_btn.setText(getResources().getString(R.string.edit));
	}


	/**
	 * <h>initialize</h>
	 * For initialising the all view componets.
	 * The value returned is as void.
	 * <pre>
	 *    Used for initialising the all view componets
	 * </pre>
	 * @since 1.0
	 */

	private void initializeVariables(View v) {
		first_name=(EditText) v.findViewById(R.id.profile_first_name);
		last_name=(EditText) v.findViewById(R.id.profile_last_name);
		email=(EditText)v.findViewById(R.id.profile_email);
		phone=(EditText)v.findViewById(R.id.profile_mobile_no);
		session=new SessionManager(getActivity());
		progress_bar= (ProgressBar) v.findViewById(R.id.progress_bar);
		actionbar_layout= (RelativeLayout) v.findViewById(R.id.actionbar_layout);
		dialogL=Utility.GetProcessDialog(getActivity());
		detail_layout = (ScrollView) v.findViewById(R.id.detail_layout);


		profile_pic=(ImageView)v.findViewById(R.id.profile_image);
		activity = (MainActivity) getActivity();
		activity.fragment_title.setText(getResources().getString(R.string.view_profile));
		activity.action_bar.setVisibility(View.VISIBLE);
		activity.fragment_title_logo.setVisibility(View.VISIBLE);
		activity.fragment_title.setVisibility(View.GONE);
//		activity.edit_profile.setVisibility(View.VISIBLE);
		activity.edit_profile.setVisibility(View.GONE);

		edit_or_save_btn=(Button)v.findViewById(R.id.edit_or_save_btn);

		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				email.setError(null);
			}
		});
		first_name.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				first_name.setError(null);
			}
		});

		last_name.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				last_name.setError(null);
			}
		});

		/**
		 * to set typeface
		 */
		if(session.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(getActivity(),first_name);
			Utility.setTypefaceMuliRegular(getActivity(),last_name);
			Utility.setTypefaceMuliRegular(getActivity(),email);
			Utility.setTypefaceMuliRegular(getActivity(),phone);
		}
	}

	/**
	 * <h>BackgroundLogOutTask</h>
	 * For logging out from the device.
	 * The value returned is as void.
	 * <pre>
	 *    Used for logging out from the device. It calls the logout server api and once the response is hit back
	 * </pre>
	 * @since 1.0
	 */

	private void BackgroundLogOutTask()
	{
		final ProgressDialog dialogL= Utility.GetProcessDialog(getActivity());
		if (dialogL!=null)
		{
			dialogL.show();
		}
		JSONObject jsonObject = new JSONObject();
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_sess_token",session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_user_type", 2);
			jsonObject.put("ent_date_time", curenttime);
			Utility.printLog("logout params  "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"logout", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
				if (dialogL!=null)
				{
					dialogL.dismiss();
				}
				Utility.printLog("logout response "+result);
				if (result!=null)
				{
					DatabasePickupHandler dbPickup = new DatabasePickupHandler(getActivity());
					dbPickup.deleteAllRows();
					session.setIsLogin(false);
					session.setCardToken("");
//					session.setCardType("");
					session.setLast4Digits("");
					session.setBookingReponseFromPubnub("");
					session.setPubnubResponseForNonBooking("");
					session.setPubnubResponse("");
					Intent intent=new Intent(getActivity(),SplashActivity.class);
					startActivity(intent);
					getActivity().finish();
					NotificationManager notificationManager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.cancelAll();
				}
				else
				{
					if (dialogL!=null)
					{
						dialogL.dismiss();
					}
					Utility.showToast(getActivity(),getResources().getString(R.string.requestTimeout));
				}
			}
			@Override
			public void onError(String error)
			{
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	/**
	 * <h>showAccountVerificationAlert</h>
	 * For showing the AlertDialogue to verifying the password for the profile editing.
	 * The value returned is as void.
	 * <pre>
	 *    Used for showing the AlertDialogue to verifying the password for the profile editing.
	 *    if the user clicks on "Submit" it will call the verifyAccount()
	 *    method and and dissmiss the AlertDialogue once the respose is obtained.
	 * </pre>
	 * @since 1.0
	 */

	private void showAccountVerificationAlert()
	{
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.verify_pass_layout);
		dialog.setCancelable(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		/**
		 * to set language support
		 */
		Utility.setLanguageForDialog(dialog);

		final EditText text = (EditText) dialog.findViewById(R.id.et_promoCode);
		final TextView popupTitle = (TextView) dialog.findViewById(R.id.popupTitle);
		final TextView submit = (TextView) dialog.findViewById(R.id.apply_promo);
		final RelativeLayout progress_bar_layout = (RelativeLayout) dialog.findViewById(R.id.progress_bar_layout);
		TextView cancel = (TextView) dialog.findViewById(R.id.cancel_promo);
		final TextInputLayout edit_pass_layout = (TextInputLayout) dialog.findViewById(R.id.edit_pass_layout);

		/**
		 * to set the typeface
		 */
		if(session.getLanguageCode().equals("en"))
		{
			Utility.setTypefaceMuliRegular(getActivity(),cancel);
			Utility.setTypefaceMuliRegular(getActivity(),submit);
			Utility.setTypefaceMuliRegular(getActivity(),popupTitle);
			Utility.setTypefaceMuliRegular(getActivity(),text);
		}

		submit.setText(getResources().getString(R.string.submitcapz));
		text.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);

		submit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(text.getText().toString().trim().equals(""))
				{
					text.setError(getResources().getString(R.string.enter_pass));
				}
				else
				{
					verifyAccount(text,dialog,progress_bar_layout,edit_pass_layout);
				}
			}
		});

		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		cancel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	/**
	 * <h>disableEdittext</h>
	 * For disabling all the Edittext.
	 * The value returned is as void.
	 * <pre>
	 *    Used for disabling all the Edittext.
	 * </pre>
	 * @since 1.0
	 */
	private void disableEdittext()
	{
		first_name.setFocusableInTouchMode(false);
		first_name.setFocusable(false);
		last_name.setFocusableInTouchMode(false);
		last_name.setFocusable(false);
		email.setFocusableInTouchMode(false);
		email.setFocusable(false);
		phone.setFocusableInTouchMode(false);
		phone.setFocusable(false);
		profile_pic.setOnClickListener(null);
	}

	/**
	 * <h>enableEdittext</h>
	 * For enabling all the Edittext.
	 * The value returned is as void.
	 * <pre>
	 *    Used for disabling all the Edittext.
	 * </pre>
	 * @since 1.0
	 */
	private void enableEdittext()
	{
		first_name.setFocusableInTouchMode(true);
		first_name.setFocusable(true);
		last_name.setFocusableInTouchMode(true);
		last_name.setFocusable(true);
		email.setFocusableInTouchMode(true);
		email.setFocusable(true);
		profile_pic.setOnClickListener(this);
		phone.setFocusableInTouchMode(false);
		phone.setFocusable(false);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.profile_image:
			{
				final Dialog dialog=Utility.showPopupWithTwoButtons(getActivity());
				TextView titleForpopup= (TextView) dialog.findViewById(R.id.title_popup);
				TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
				TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
				TextView no_button= (TextView) dialog.findViewById(R.id.no_button);
				titleForpopup.setVisibility(View.GONE);
				text_for_popup.setText(getResources().getString(R.string.selecto_photo));
				yes_button.setText(getResources().getString(R.string.gallery));
				no_button.setText(getResources().getString(R.string.camera));
				yes_button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						openGallery();
					}
				});
				no_button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						takePicture();
					}
				});
				dialog.show();
				break;
			}
		}
	}

	/**
	 * <h>openGallery</h>
	 * For firing the ACTION_PICK Intent.
	 * The value returned is as void.
	 * <pre>
	 *    Used for firing the ACTION_PICK of Intent, for getting the Images from the gallery. Instance must also contain the Type.
	 * </pre>
	 * @since 1.0
	 */
	private void openGallery()
	{
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
	}

	/**
	 * <h>takePicture</h>
	 * For firing the ACTION_IMAGE_CAPTURE of MediaStore Intent.
	 * The value returned is as void.
	 * <pre>
	 *    Used for firing the ACTION_IMAGE_CAPTURE of MediaStore Intent, for getting the Images from the Camera.
	 * </pre>
	 * @since 1.0
	 */

	private void takePicture()
	{

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {
			Uri mImageCaptureUri;
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				mImageCaptureUri = Uri.fromFile(mFileTemp);
			}
			else
			{
				mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
			}
			intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra("return-data", true);
			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
		} catch (ActivityNotFoundException e) {

			Log.d("", "cannot take picture", e);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		Utility.printLog("Control comming in onActivityResult: req: "+requestCode+" ,resultCode: "+resultCode);
		if (requestCode == 11)
		{
			if(resultCode==Activity.RESULT_OK)
			{
				Utility.printLog("Control comming in req=1");
				new BackgroundTaskGetProfile().execute();
			}

			return;
		}


		if (resultCode !=Activity.RESULT_OK) {
			return;
		}

		Bitmap bitmap;
		switch (requestCode) {
			case REQUEST_CODE_GALLERY:
				Utility.printLog("Control comming in req=REQUEST_CODE_GALLERY");
				try {
					InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
					FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
					copyStream(inputStream, fileOutputStream);
					fileOutputStream.close();
					assert inputStream != null;
					inputStream.close();
					startCropImage();
				} catch (Exception e) {
					Log.e(TAG, "Error while creating temp file", e);
				}

				break;
			case REQUEST_CODE_TAKE_PICTURE:
				Utility.printLog("Control comming in req=REQUEST_CODE_TAKE_PICTURE");
				startCropImage();
				break;
			case REQUEST_CODE_CROP_IMAGE:
				Utility.printLog("Control comming in req=REQUEST_CODE_CROP_IMAGE");
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				if (path == null) {
					return;
				}
				/**
				 * cropped image and show it in circular format
				 */
				bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
				bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumWidth(),
						getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumHeight(), true);
				profile_pic.setImageBitmap(Utility.getRoundedCornerBitmap(bitmap,30));
				isPicSelected=true;
				enableEdittext();
				edit_or_save_btn.setText(getResources().getString(R.string.save));
				editpressed=false;
				savepressed=true;
				break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * <h>copyStream</h>
	 * For coping the image file contents bytewise.
	 * The value returned is as void.
	 * <pre>
	 *    Used for coping the image file contents bytewise.using the input and output Streams
	 * </pre>
	 *
	 * @param input as a InputStream.
	 * @param output as a OutputStream.
	 * @return void
	 * @since 1.0
	 */
	public static void copyStream(InputStream input, OutputStream output)
			throws IOException
	{
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1)
		{
			output.write(buffer, 0, bytesRead);
		}
	}

	/**
	 * <h>startCropImage</h>
	 * For firing the CropImage Intent.
	 * The value returned is as void.
	 * <pre>
	 *    Used for firing the CropImage, for croping the Images obtained from the gallery or Camera.
	 * </pre>
	 */

	private void startCropImage()
	{
		Intent intent = new Intent(getActivity(), CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
		intent.putExtra(CropImage.SCALE, true);
		intent.putExtra(CropImage.ASPECT_X, 4);
		intent.putExtra(CropImage.ASPECT_Y, 4);
		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}

	/**
	 * <h>BackgroundTaskGetProfile</h>
	 * For getting the profile details in backgrund.
	 * The value returned is as void.
	 * <pre>
	 *    Used for getting the profile details in backgrund.
	 * </pre>
	 * @since 1.0
	 */

	class BackgroundTaskGetProfile extends AsyncTask<String,Void,String>
	{
		GetMyProfileResponse response;
		@Override
		protected String doInBackground(String... params) {
			getUserProfile();
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
	}

	/**
	 * <h>BackgroundLogOutTask</h>
	 * For getting the profile details.
	 * The value returned is as void.
	 * <pre>
	 *    Used for getting the profile details. It calls the logout server api and once the response is hit back
	 * </pre>
	 * @since 1.0
	 */

	private void getUserProfile()
	{
		JSONObject jsonObject=new JSONObject();
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_sess_token",session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_date_time", curenttime);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"getProfile", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String response)
			{
				getprofileServerResponse = response;
				Utility.printLog("Success of getting user Info"+response);
				getUserInfo();
			}
			@Override
			public void onError(String error)
			{
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		});
	}


	/**
	 * <h>verifyAccount</h>
	 * For getting the profile details in backgrund.
	 * The value returned is as void.
	 * <pre>
	 *    Used for getting the profile details in backgrund.
	 * </pre>
	 *
	 * @param password Sting
	 * @param dialog Dialogue
	 * @param progress_bar_layout RelativeLayout
	 * @param edit_pass_layout TextInputLayout
	 * @since 1.0
	 */

	private void verifyAccount(final EditText password, final Dialog dialog, final RelativeLayout progress_bar_layout, final TextInputLayout edit_pass_layout)
	{
		progress_bar_layout.setVisibility(View.VISIBLE);
		edit_pass_layout.setVisibility(View.GONE);
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("ent_password",password.getText().toString());
			jsonObject.put("ent_sid", session.getSid());
			Utility.printLog("params to verify "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"checkPassword", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String response)
			{
				progress_bar_layout.setVisibility(View.GONE);
				edit_pass_layout.setVisibility(View.VISIBLE);
				Utility.printLog("password response "+response);
				if(response!=null)
				{
					try {
						JSONObject jsonObject1=new JSONObject(response);
						String errFlag=jsonObject1.getString("errFlag");
						if(errFlag.equals("0"))
						{
							Utility.printLog("inside options editpressed");
							enableEdittext();
//							activity.edit_profile.setText(getResources().getString(R.string.save));
							edit_or_save_btn.setText(getResources().getString(R.string.save));
							editpressed=false;
							savepressed=true;
							dialog.dismiss();
						}
						else
						{
							password.setError(getResources().getString(R.string.pass_invalid));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else
				{
					Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
				}
			}
			@Override
			public void onError(String error)
			{
				progress_bar_layout.setVisibility(View.GONE);
				edit_pass_layout.setVisibility(View.VISIBLE);
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	/**
	 * <h>getUserInfo</h>
	 * For parsing the getProfile response.
	 * The value returned is as void.
	 * <pre>
	 *    Used for parsing the getProfile response.
	 * </pre>
	 * @since 1.0
	 */
	private void getUserInfo()
	{
		actionbar_layout.setVisibility(View.GONE);
		detail_layout .setVisibility(View.VISIBLE);
		Gson gson = new Gson();
		GetMyProfileResponse getprofile = gson.fromJson(getprofileServerResponse, GetMyProfileResponse.class);
		if(getprofile.getErrNum().equals("6") || getprofile.getErrNum().equals("7") ||
				getprofile.getErrNum().equals("94") || getprofile.getErrNum().equals("96"))
		{
			Utility.showToast(getActivity(),getprofile.getErrMsg());
			Intent i = new Intent(getActivity(), SplashActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			getActivity().startActivity(i);
			getActivity().overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		}
		if(getprofile.getErrFlag().equals("0") && isAdded())
		{
//			first_name.setText("");
//			first_name.append(getprofile.getfName()+" "+getprofile.getlName());
			first_name.setText(getprofile.getfName());
			last_name.setText(getprofile.getlName());
			email.setText(getprofile.getEmail());
			phone.setText(getprofile.getPhone());
			session.setPhoneNumber(getprofile.getPhone());
//			session.setEmailId(getprofile.getEmail());
			session.setUserName(getprofile.getfName());
			//Setting doctor pic
			String url= getprofile.getpPic();
			/**
			 * to set the image into image view and
			 * add the write the image in the file
			 */
			profile_pic.setTag(setTarget(progress_bar));
			Picasso.with(getActivity()).load(url).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE).into((Target) profile_pic.getTag()) ;
//			activity.user_name.setText(getprofile.getfName().toUpperCase()+" "+getprofile.getlName().toUpperCase());
//			activity.mobile.setText(getprofile.getPhone());
			session.setProfileImage(getprofile.getpPic());
			session.setUserName(getprofile.getfName());
		}
	}

	/**
	 * <h>setTarget</h>
	 * For downloading the Image file.
	 * The value returned is as void.
	 * <pre>
	 *    Used for downloading the Image file. it will convert to circular page.
	 * </pre>
	 * @return Target
	 * @since 1.0
	 */

	public Target setTarget(final ProgressBar progressBar)
	{
		Target target= new Target() {

			@Override
			public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
			{
				if(isAdded())
				{
					if(progressBar!=null)
						progressBar.setVisibility(View.GONE);
					/**
					 * to set the image from fb to imageview after making circular imagee
					 */
					bitmap = Bitmap.createScaledBitmap(bitmap, getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumWidth(),
							getResources().getDrawable(R.drawable.view_profile_avatar_icon).getMinimumHeight(), true);
//					Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
//					BitmapShader shader = new BitmapShader (bitmap,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//					Paint paint = new Paint();
//					paint.setShader(shader);
//					paint.setAntiAlias(true);
//					Canvas c = new Canvas(circleBitmap);
//					c.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, bitmap.getWidth()/2, paint);
//					profile_pic.setImageBitmap(bitmap);
					profile_pic.setImageBitmap(Utility.getRoundedCornerBitmap(bitmap,30));
//					activity.user_image.setImageBitmap(circleBitmap);
				}

				/**
				 * to put the bitmap image into the file for uploading
				 */
				FileOutputStream ostream;
				try {
					ostream = new FileOutputStream(mFileTemp);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
					ostream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onBitmapFailed(Drawable errorDrawable) {
				if(progressBar!=null)
					progressBar.setVisibility(View.GONE);

			}

			@Override
			public void onPrepareLoad(Drawable placeHolderDrawable)
			{
				progressBar.setVisibility(View.VISIBLE);
			}
		};
		return target;
	}


	/**
	 * <h>BackgroundForUpdateProfile</h>
	 * For updating the Profile in background.
	 * The value returned is as void.
	 * <pre>
	 *    Used for updating the Profile.Here it extends the AsyncTask for run it in background.
	 * </pre>
	 *
	 * @return void
	 * @since 1.0
	 */
	class BackgroundForUpdateProfile extends AsyncTask<String,Void,String>
	{
		ProgressDialog dialogL;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialogL= Utility.GetProcessDialog(getActivity());
			if (dialogL!=null) {
				dialogL.show();
			}
		}
		@Override
		protected String doInBackground(String... params) {
			UpdateUserProfile(dialogL);
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}
	}

	/**
	 * <h>UpdateUserProfile</h>
	 * For updating the Profile.
	 * The value returned is as void.
	 * <pre>
	 *    Used for updating the Profile.Here it extends the AsyncTask for run it in background.
	 * </pre>
	 *
	 * @return void
	 * @since 1.0
	 */

	private void UpdateUserProfile(final ProgressDialog dialogL)
	{
		JSONObject jsonObject=new JSONObject();
		try
		{
			Utility utility=new Utility();
			String curenttime=utility.getCurrentGmtTime();
			jsonObject.put("ent_sess_token",session.getSessionToken());
			jsonObject.put("ent_dev_id", session.getDeviceId());
			jsonObject.put("ent_first_name", first_name.getText().toString());
			jsonObject.put("ent_last_name", last_name.getText().toString());
			jsonObject.put("ent_email", email.getText().toString());
			jsonObject.put("ent_phone", phone.getText().toString());
			jsonObject.put("ent_date_time",curenttime);
			if(isPicSelected)
				jsonObject.put("ent_propic", getString(R.string.AMAZON_IMAGE_LINK)+phone.getText().toString().substring(1)+".jpg");
			else
				jsonObject.put("ent_propic", "");

			jsonObject.put("ent_dob",dobText);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(getString(R.string.BASE_URL)+"updateProfile", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String response)
			{
				getprofileServerResponse = response;
				UpdatedUserInfo(dialogL);
			}
			@Override
			public void onError(String error)
			{
				dialogL.dismiss();
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		});
	}

	/**
	 * <h>UpdatedUserInfo</h>
	 * For parsing the updateProfile response.
	 * The value returned is as void.
	 * <pre>
	 *    Used for parsing the updateProfile response.
	 * </pre>
	 *
	 * @return void
	 * @since 1.0
	 */

	private void UpdatedUserInfo(ProgressDialog dialogL)
	{
		dialogL.dismiss();
		try
		{
			JSONObject jsnResponse = new JSONObject(getprofileServerResponse);
			String jsonErrorParsingFlag = jsnResponse.getString("errFlag");
			String jsonErrorParsingMsg = jsnResponse.getString("errMsg");
			Utility.printLog("jsonErrorParsing is ---> "+jsonErrorParsingMsg);
			Utility.printLog("parseResponse  " + getprofileServerResponse);
			if(jsonErrorParsingFlag.equals("0"))
			{
//				activity.user_name.setText(first_name.getText().toString().toUpperCase());
//				activity.mobile.setText(phone.getText().toString().toUpperCase());
			}
			else
			{
				Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
			}
		}
		catch(JSONException e)
		{
			e.printStackTrace();
			Utility.showToast(getActivity(),getResources().getString(R.string.network_connection_fail));
		}
	}
}