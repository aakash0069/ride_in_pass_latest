package com.roadyo.passenger.pojo;

/**
 * Created by embed on 31/5/17.
 */
public class DistETAandMID_SortingPojo implements Comparable<DistETAandMID_SortingPojo> {
    private String mid;
    private int time;
    private String dist;
    public DistETAandMID_SortingPojo(String name, int time, String dist) {
        this.mid = name;
        this.time = time;
        this.dist = dist;
    }
    public String getMid() {
        return mid;
    }
    public int getTime() {
        return time;
    }
    public String getDist() {
        return dist;
    }
    @Override
    public int compareTo(DistETAandMID_SortingPojo candidate) {

//        return (this.getTime() < candidate.getTime() ? -1 :
//                (this.getTime() == candidate.getTime() ? 0 : 1));

        DistETAandMID_SortingPojo dataList= (DistETAandMID_SortingPojo) candidate;
        return (this.getTime()<dataList.getTime() ? -1:(this.getTime() == candidate.getTime() ?  0 : 1));

    }
    @Override
    public String toString() {
        return " mid: " + this.mid + ", time: " + this.time + ", dist: " + this.dist;
    }
}