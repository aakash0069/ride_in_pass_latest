package com.roadyo.passenger.pojo;

import java.io.Serializable;

public class FareCalData implements Serializable
{
	private String fare;

	private String type_id;

	public String getFare ()
	{
		return fare;
	}

	public void setFare (String fare)
	{
		this.fare = fare;
	}

	public String getType_id ()
	{
		return type_id;
	}

	public void setType_id (String type_id)
	{
		this.type_id = type_id;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [fare = "+fare+", type_id = "+type_id+"]";
	}
}
