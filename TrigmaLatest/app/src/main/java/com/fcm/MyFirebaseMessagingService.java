package com.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.bloomingdalelimousine.ridein.R;
import com.roadyo.passenger.main.HomePageFragment;
import com.roadyo.passenger.main.MainActivity;
import com.roadyo.passenger.main.SplashActivity;
import com.threembed.utilities.SessionManager;
import com.threembed.utilities.Utility;
import com.threembed.utilities.VariableConstants;
import java.util.List;
import java.util.Map;

/**
 * @author Akbar
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String action,message,reason;
    public static final int NOTIFICATION_ID = 1;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            Utility.printLog(TAG+ "Messagedata: " + remoteMessage.getData());
            Map map=remoteMessage.getData();

            if(map.containsKey("action"))
            {
                action= (String) map.get("action");
            }
            if(map.containsKey("payload"))
            {
                message= (String) map.get("payload");
            }
            if(map.containsKey("r"))
            {
                reason= (String) map.get("r");
            }
            SessionManager session =new SessionManager(this);
            boolean isbkrnd=isApplicationSentToBackground(this);
            Utility.printLog("isApplicationSentToBackground "+isbkrnd);

            if("10".equals(action))
            {
                if(reason!=null)
                {
                    session.setCancellationType(reason);
                    session.setBookingReponseFromPubnub("");
                    session.setPubnubResponse(session.getPubnubResponseForNonbooking());
                    VariableConstants.PUSH_MESSAGE=getString(R.string.driver_cancelled)+" "+ reason;

                    if (HomePageFragment.visibleStatus())
                    {
                        Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                        homeIntent.putExtra("ACTION_FOR_CANCELLED",action);
                        sendBroadcast(homeIntent);
                    }
                    else if (!isbkrnd)
                    {
                        Intent i = new Intent();
                        i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                }
            }

            else if("420".equals(action))
            {
                session.isPushNotificationFromAdmin(true);
                VariableConstants.PUSH_MESSAGE=message;
                if (HomePageFragment.visibleStatus())
                {
                    Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    sendBroadcast(homeIntent);
                }
                else if(isbkrnd && session.isLoggedIn())
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                else if(!session.isLoggedIn())
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.SplashActivity");
                    i.putExtra("PUSH_DATA",message);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                else if(SplashActivity.visibility)
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.SplashActivity");
                    i.putExtra("PUSH_DATA",message);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK  | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
            else if("425".equals(action))
            {
                session.isPushNotificationFromAdmin(true);
                VariableConstants.PUSH_MESSAGE=message;
                if (HomePageFragment.visibleStatus())
                {
                    Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    sendBroadcast(homeIntent);
                }
                else if (!isbkrnd)
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
            //action =421 because all drivers are busy and we need to return to homepage by showing a popup from dispatch screen
            else if ("421".equals(action))
            {
                if(!isbkrnd)
                {
                    Intent dispatchIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    dispatchIntent.putExtra("ACTION","421");
                    sendBroadcast(dispatchIntent);
                }
                else
                {
                    if(!session.getAllBusyPushMessage().equals(""))
                        session.setAllBusyPushMessage(message);
                }
            }

            else if("12".equals(action))
            {
                session.setIsLogin(false);
                Intent i = new Intent(this, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            //Driver Reached
            else if("7".equals(action) && (!session.isDriverOnArrived() ))
            {
                session.setDriverOnWay(false);
                session.setDriverArrived(true);
                session.setTripBegin(false);
                session.setInvoiceRaised(false);

                if(HomePageFragment.visibleStatus())
                {
                    Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    homeIntent.putExtra("ACTION","REACHED");
                    sendBroadcast(homeIntent);
                }
                else if(!isbkrnd)
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                    //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }

            else if("8".equals(action) && (!session.isTripBegin() ))//&& ("1".equals(type) )
            {
                session.setDriverOnWay(false);
                session.setDriverArrived(false);
                session.setTripBegin(true);//roshani
                session.setInvoiceRaised(false);

                if(HomePageFragment.visibleStatus())
                {
                    Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    homeIntent.putExtra("ACTION","COMPLETE");
                    sendBroadcast(homeIntent);
                }
                else if(!isbkrnd)
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                    //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }

            else if(( ("9".equals(action))))//"1".equals(type) &&
            {
                session.setDriverOnWay(false);
                session.setDriverArrived(false);
                session.setTripBegin(false);
                session.setInvoiceRaised(true);

                if(HomePageFragment.visibleStatus())
                {
                    Intent homeIntent=new Intent("com.bloomingdalelimousine.ridein.push");
                    homeIntent.putExtra("ACTION","COMPLETE");
                    sendBroadcast(homeIntent);
                }
                else if(!isbkrnd)
                {
                    Intent i = new Intent();
                    i.setClassName("com.bloomingdalelimousine.ridein","com.roadyo.passenger.main.MainActivity");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
            else if (action != null && action.equals("111")) {
                session.setIsLogin(false);
                Intent i = new Intent(this, SplashActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            sendNotification(message,action);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            Log.d(TAG, "MessageNotification Body: " + remoteMessage.getNotification().getBody());
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     * @param msg,action FCM message body received.
     */
    private void sendNotification(String msg, String action) {
        Log.i(TAG, "sendNotification  msg " + msg);
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //To set large icon in notification
        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        //Assign inbox style notification
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setBigContentTitle(this.getString(R.string.app_name));
//        bigText.setSummaryText(email);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(this.getString(R.string.app_name))
                .setContentText(msg)
                .setPriority(NotificationCompat.FLAG_SHOW_LIGHTS) //must give priority to High, Max which will considered as heads-up notification)
                .setAutoCancel(true)
                .setContentText(msg)
//                .setDefaults(
//                        Notification.DEFAULT_SOUND
//                                | Notification.DEFAULT_VIBRATE
//                                | Notification.DEFAULT_LIGHTS
//                )
                .setLargeIcon(icon1)
                .setStyle(bigText)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(10 /* ID of notification */, notificationBuilder.build());


        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on....", ""+isScreenOn);
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");

            wl_cpu.acquire(10000);
        }
    }

    public static boolean isApplicationSentToBackground(final Context context)
    {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty())
        {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }
}
