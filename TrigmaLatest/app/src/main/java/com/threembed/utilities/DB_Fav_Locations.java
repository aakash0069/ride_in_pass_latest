package com.threembed.utilities;

public class DB_Fav_Locations
{
    //private variables
    private String _id;
    private String _formattedAddress;
    private String _addressName;
    private String _latitude;
    private String _longitude;
    String _description;

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_latitude() {
        return _latitude;
    }

    public String get_formattedAddress() {
        return _formattedAddress;
    }

    // getting ID
    public String getID(){
        return this._id;
    }

    // setting id
    public void setID(String id){
        this._id = id;
    }

    // getting name
    public String getFormattedAddress()
    {
        return this._formattedAddress;
    }
    // setting name
    public void setFormattedAddress(String address){
        this._formattedAddress = address;
    }

    // getting phone number
    public String getLatitude(){
        return this._latitude;
    }

    // setting phone number
    public void setLatitude(String latitude){
        this._latitude = latitude;
    }

    public String getLongitude() {
        return _longitude;
    }
    public void setLongitude(String longitude) {
        this._longitude = longitude;
    }
    public String getAddressName() {
        return _addressName;
    }
}
