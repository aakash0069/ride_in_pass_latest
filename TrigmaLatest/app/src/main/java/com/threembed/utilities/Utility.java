package com.threembed.utilities;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.bloomingdalelimousine.ridein.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class Utility
{
	public static void printLog(String msg)
	{
		if(true)
		{
			Log.i("RoadYo",msg);
		}
	}

	public static  String currentVersion(Context context)
	{
		String currentVersion = "";
		try {
			currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
			Log.d("currentVersion",currentVersion);
		} catch (PackageManager.NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return currentVersion;
	}

	public static void openPlaystore(Context context)
	{
		Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			context.startActivity(goToMarket);
		} catch (ActivityNotFoundException e)
		{
			context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
		}
	}


	public static void showToast(Context context,String message)
	{
		Toast.makeText(context,message,Toast.LENGTH_LONG).show();
	}

	public static void showSnackBar(View view,Context context,String message)
	{
		Typeface muliRegular=Typeface.createFromAsset(context.getAssets(), "fonts/Hind-Regular.ttf");
		Snackbar snackbar = Snackbar.make(view,message, Snackbar.LENGTH_LONG);
		View snackBarView = snackbar.getView();
		snackBarView.setBackgroundResource(R.drawable.back_btn_selector);
		TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
		textView.setTextColor(context.getResources().getColor(R.color.white));
		textView.setTypeface(muliRegular);
		snackbar.show();
	}

	public static void setTypefaceMuliRegular(Context context,TextView textView)
	{
		Typeface muliRegular=Typeface.createFromAsset(context.getAssets(), "fonts/Hind-Regular.ttf");
		textView.setTypeface(muliRegular);
	}

	public static void setTypefaceMuliBold(Context context,TextView textView)
	{
		Typeface muliBold= Typeface.createFromAsset(context.getAssets(), "fonts/Hind-Bold.ttf");
		textView.setTypeface(muliBold);
	}

	public static void setTypefaceMuliLight(Context context,TextView textView)
	{
		Typeface muliLight= Typeface.createFromAsset(context.getAssets(), "fonts/Hind-Light.ttf");
		textView.setTypeface(muliLight);
	}

	public static void setLocale(String lang,Context context)
	{
		try {
			Utility.printLog("Locale lang "+lang);
			Locale myLocale = new Locale(lang);
			Resources res = context.getResources();
			DisplayMetrics dm = res.getDisplayMetrics();
			Configuration conf = res.getConfiguration();
			conf.locale = myLocale;
			res.updateConfiguration(conf, dm);
		} catch (Exception e) {
			Utility.printLog("select_language inside Exception" + e.toString());
		}
	}

	/**
	 * ReName any file
	 * @param oldName
	 * @param newName
	 */
	public static File  renameFile(String oldName,String newName){
		File dir = Environment.getExternalStorageDirectory();
		File toFile = null;
		if(dir.exists()){
			File from = new File(dir,oldName);
			toFile = new File(dir,newName);
			if(from.exists())
				from.renameTo(toFile);
		}
		return  toFile;
	}

	public static void resetAll(ArrayList<ImageView> rightArrowImages,Context context) {
		for (int i=0;i<rightArrowImages.size();i++)
		{
			rightArrowImages.get(i).setImageDrawable(context.getResources().getDrawable(R.drawable.icon));
		}
	}

	public static void resetAll(ArrayList<ImageView> rightArrowImages,Context context,int index,boolean isChild) {

		printLog("index "+index+" isChild "+isChild);

		for (int i=0;i<rightArrowImages.size();i++)
		{
			if(index==i) {
				rightArrowImages.get(i).setImageDrawable(context.getResources().getDrawable(R.drawable.icon));
			} else {
				rightArrowImages.get(i).setImageDrawable(context.getResources().getDrawable(R.drawable.icon));
			}
		}
	}

	public static String getMonth(int month) {
		return new DateFormatSymbols().getMonths()[month-1];
	}

	public static void setLanguageSupport(Context context)
	{
		SessionManager sessionManager=new SessionManager(context);
		Utility.printLog("language in session utiltit "+sessionManager.getLanguageCode());
		switch (sessionManager.getLanguageCode())
		{
			case "ar":
			{
				setLocale("ar",context);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					((Activity)context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
				}
				break;
			}
			case "en":
			{
				setLocale("en",context);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					((Activity)context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
				}
				break;
			}
		}
	}

	public static HttpResponse doPost(String url, Map<String, String> kvPairs)
			throws ClientProtocolException, IOException
	{
		// HttpClient httpclient = new DefaultHttpClient();

		DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
		HttpClient httpclient = defaultHttpClient;

		HttpPost httppost = new HttpPost(url);

		if (kvPairs != null || kvPairs.isEmpty() == false)
		{
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(kvPairs.size());
			String k, v;
			Iterator<String> itKeys = kvPairs.keySet().iterator();

			while (itKeys.hasNext()) {
				k = itKeys.next();
				v = kvPairs.get(k);
				nameValuePairs.add(new BasicNameValuePair(k, v));
			}

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
		}

		HttpResponse response;
		response = httpclient.execute(httppost);
		return response;
	}

	public static String requiredFormat(String date)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		DateFormat targetFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
		String formattedDate = null;
		Date convertedDate;
		try {
			convertedDate = dateFormat.parse(date);
			formattedDate = targetFormat.format(convertedDate);
		}
		catch (java.text.ParseException e)
		{
			e.printStackTrace();
		}
		return formattedDate;
	}
	public static String required_time(String date)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		DateFormat targetFormat = new SimpleDateFormat("hh:mm:ss a", Locale.US);
		String formattedDate = null;
		Date convertedDate;
		try {
			convertedDate = dateFormat.parse(date);
			formattedDate = targetFormat.format(convertedDate);
		}
		catch (java.text.ParseException e)
		{
			e.printStackTrace();
		}
		return formattedDate;
	}
	public static void setLanguageForDialog(Dialog dialog)
	{
		SessionManager sessionManager=new SessionManager(dialog.getContext());
		Utility.printLog("language in session utiltit "+sessionManager.getLanguageCode());
		switch (sessionManager.getLanguageCode())
		{
			case "ar":
			{
				setLocale("ar",dialog.getContext());
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
				}
				break;
			}
			case "en":
			{
				setLocale("en",dialog.getContext());
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
					dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
				}
				break;
			}
		}
	}

	public static void setStatusBarColor(Context context)
	{
		Window window = ((Activity)context).getWindow();
		// clear FLAG_TRANSLUCENT_STATUS flag:
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}
		// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		}
		// finally change the color
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			window.setStatusBarColor(context.getResources().getColor(R.color.background));
			//window.setStatusBarColor(R.color.transparent);
		}
	}

	public static Dialog showPopupWithTwoButtons(Context context)
	{
		Dialog dialog= new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_dialog_with_2buttons);
		dialog.setCancelable(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		setLanguageForDialog(dialog);

		TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
		TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
		TextView no_button= (TextView) dialog.findViewById(R.id.no_button);
		TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
		setTypefaceMuliRegular(context,title_popup);
		setTypefaceMuliRegular(context,text_for_popup);
		setTypefaceMuliBold(context,yes_button);
		setTypefaceMuliBold(context,no_button);
		return dialog;
	}

	public static int setCreditCardLogo(String type) {
		int drawable=0;
		if(type.equalsIgnoreCase("visa"))
		{
			drawable= R.drawable.visa;
		}
		if(type.equalsIgnoreCase("MasterCard"))
		{
			drawable= R.drawable.master_card;
		}
		if(type.equalsIgnoreCase("American Express"))
		{
			drawable= R.drawable.amex;
		}
		if(type.equals("Discover"))
		{
			drawable= R.drawable.discover;
		}
		if(type.equals("JCB"))
		{
			drawable= R.drawable.diners_club;
		}
		return drawable;
	}

	public static Dialog showPopupWithOneButton(Context context)
	{
		final Dialog dialog= new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_dialog_with_1button);
		dialog.setCancelable(true);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
		setLanguageForDialog(dialog);

		TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
		TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
		TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
		setTypefaceMuliRegular(context,title_popup);
		setTypefaceMuliRegular(context,text_for_popup);
		setTypefaceMuliBold(context,yes_button);
		yes_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}
	public static int getStatusBarHeight(Context context) {
		int result = 0;
		int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = context.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	/**
	 * to get the resource id from the drawable name
	 * @param drawableName
	 * @return
	 */
	public static int getResId(String drawableName)
	{
		try
		{
			Class<R.drawable> res = R.drawable.class;
			Field field = res.getField(drawableName);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("CountryCodePicker", "Failure to get drawable id.", e);
		}
		return -1;
	}

	public static ProgressDialog GetProcessDialog(Activity activity)
	{
		// prepare the dialog box
		ProgressDialog dialog = new ProgressDialog(activity,5);
		// make the progress bar cancelable
		dialog.setCancelable(false);
		// set a message text
		dialog.setMessage(activity.getResources().getString(R.string.please_wait));
		// show it
		return dialog;
	}

	public static ProgressDialog GetProcessDialogNew(Activity activity,String msg)
	{
		// prepare the dialog box
		ProgressDialog dialog = new ProgressDialog(activity,5);
		// make the progress bar cancelable
		dialog.setCancelable(false);
		// set a message text
		dialog.setMessage(msg);

		// show it
		return dialog;
	}

	public static boolean isNetworkAvailable(Context context)
	{
		ConnectivityManager connectivity  = null;
		boolean isNetworkAvail = false;
		try
		{
			connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null)
			{
				NetworkInfo[] info = connectivity.getAllNetworkInfo();

				if (info != null)
				{
					for (int i = 0; i < info.length; i++)
					{
						if (info[i].getState() == NetworkInfo.State.CONNECTED)
						{

							return true;
						}
					}
				}
			}
			return false;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return isNetworkAvail;
	}

	public static void ShowAlert(String msg,Context context)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context,5);
		// set title
		alertDialogBuilder.setTitle(context.getString(R.string.note));
		// set dialog message
		alertDialogBuilder
				.setMessage(msg)
				.setCancelable(false)
				.setNegativeButton(context.getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog,int id)
					{
						//closing the application
						dialog.dismiss();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	public static AlertDialog alert(String msg,Context context)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context,5);
		// set title
		alertDialogBuilder.setTitle(context.getString(R.string.note));
		// set dialog message
		alertDialogBuilder
				.setMessage(msg)
				.setCancelable(false)
				.setNegativeButton(context.getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog,int id)
					{
						//closing the application
						dialog.dismiss();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		return alertDialog;
	}

	public String getCurrentGmtTime()
	{
		String curentdateTime=null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
		curentdateTime = sdf.format(new Date());
		return curentdateTime;
	}

	public static File getSdCardPath()
	{
		File sdCardPath=null;
		sdCardPath=Environment.getExternalStorageDirectory();
		return sdCardPath;
	}
	//---------------------------delete non empty directory------------------------------

	public static boolean deleteNon_EmptyDir(File dir)
	{
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			if (children!=null&&children.length>0)
			{
				for (int i=0; i<children.length; i++)
				{
					boolean success = deleteNon_EmptyDir(new File(dir, children[i]));

					if (!success)
					{
						return false;
					}
				}
			}

		}
		return dir.delete();
	}

	//------------------------------ To create file---------------------------

	public static File createFile(String directoryname,String filename)
	{
		File createdirectoty,createdFileName;
		createdirectoty = new File(Environment.getExternalStorageDirectory() + "/"+directoryname);
		if (!createdirectoty.exists())
		{     //Utility.printLog(TAG, "createFile directory is not created yet");
			createdirectoty.mkdir();
			createdFileName = new File(createdirectoty, filename);
		}
		else
		{
			createdFileName = new File(createdirectoty, filename);
			System.out.println("my signed file is from else block  is  "+createdFileName);
		}
		return createdFileName;
	}

	//GETTING CURRENT DATE
	public static String getDate(){
		Calendar calendar = Calendar.getInstance(Locale.US);
		Date date = calendar.getTime();
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
		return formater.format(date);
	}

	//GETTING CURRENT DATE
	public static String getTime(){
		Calendar calendar = Calendar.getInstance(Locale.US);
		Date date = calendar.getTime();
		SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss",Locale.ENGLISH);
		return formater.format(date);
	}

	/**
	 * to get distance from 2 points
	 * @param lat1 previous latitude
	 * @param lon1 previous longitude
	 * @param lat2 current latitude
	 * @param lon2 current longitude
	 * @return returning the distance between 2 points in meters
	 */
	public float getDistanceBetweenTwoPoints(double lat1,double lon1,double lat2,double lon2)
	{
		Location loc1 = new Location("");
		loc1.setLatitude(lat1);
		loc1.setLongitude(lon1);

		Location loc2 = new Location("");
		loc2.setLatitude(lat2);
		loc2.setLongitude(lon2);
		return loc1.distanceTo(loc2);
	}

	/**
	 * to get latlongs from center of map
	 * @param googleMap getting the map object
	 * @return should return the latlong object
	 */
	public LatLng getCenterPointLatlong(GoogleMap googleMap)
	{
		VisibleRegion visibleRegion = googleMap.getProjection()
				.getVisibleRegion();
		Point x1 = googleMap.getProjection().toScreenLocation(
				visibleRegion.farRight);
		Point y = googleMap.getProjection().toScreenLocation(
				visibleRegion.nearLeft);
		Point centerPoint = new Point(x1.x / 2, y.y / 2);
		LatLng centerFromPoint = googleMap.getProjection().fromScreenLocation(centerPoint);
		Utility.printLog("latlong from idle of map "+centerFromPoint.latitude+" long"+centerFromPoint.longitude);
		return centerFromPoint;
	}

	/**
	 * to push the distance matrix call to the backend
	 * to store the google API call
	 */
	public void updateDistanceMatrixToBackend(String url,Context context)
	{
		SessionManager sessionManager=new SessionManager(context);
		JSONObject jsonObject=new JSONObject();
		try
		{
			jsonObject.put("url",url);
			jsonObject.put("dev_type","2");
			jsonObject.put("sid",sessionManager.getSid());
			Utility.printLog("params to add fav address  "+jsonObject);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		OkHttpRequestObject.postRequest(context.getString(R.string.BASE_URL)+"updateGooogleCalls", jsonObject, new OkHttpRequestObject.JsonRequestCallback()
		{
			@Override
			public void onSuccess(String result)
			{
			}
			@Override
			public void onError(String error)
			{
			}
		});
	}

	public static void ratingBarColorChanger(RatingBar ratingBar){
		LayerDrawable stars = (LayerDrawable) ratingBar
				.getProgressDrawable();
//		stars.getDrawable(2).setColorFilter(Color.parseColor("#ece032"),
		stars.getDrawable(2).setColorFilter(Color.parseColor("#ece032"),
				PorterDuff.Mode.SRC_ATOP); // for filled stars
//		stars.getDrawable(1).setColorFilter(Color.parseColor("#ece032"),
		stars.getDrawable(1).setColorFilter(Color.parseColor("#111111"),
				PorterDuff.Mode.SRC_ATOP); // for half filled stars
//		stars.getDrawable(0).setColorFilter(Color.parseColor("#1a3746"),
		stars.getDrawable(0).setColorFilter(Color.parseColor("#eeeeee"),
				PorterDuff.Mode.SRC_ATOP); // for empty stars
	}

	public static String nameFormator(String name){

		name = name.trim();
		if(name.contains(" ")){
			String[]splited_names = name.split(" ");
			String nemname = "";
			if(splited_names.length>0){
				for (int i = 0; i<splited_names.length;i++){
					if(splited_names.length-1==i){
						nemname = nemname+camelCase(splited_names[i]);
					} else {
						nemname = nemname+camelCase(splited_names[i])+" ";
					}
				}
			}
			return nemname;
		} else {
			return camelCase(name);
		}

	}

	public static String camelCase(String name){
		if(name.length()>0){
			if(name.length()==1) {
				return name.toUpperCase();
			} else {
				name = (""+name.charAt(0)).toUpperCase()+name.substring(1,name.length());
				return name;
			}
		} else {
			return "";
		}
	}


	//GETTING CURRENT DATE
	public static String getTimeFormated(String dateString){

//        dateString = "Monday 29th June 2015";
//the output format

		printLog("SimpleDateFormat_called "+dateString);

		String datetime="";
		//dateString;
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat d= new SimpleDateFormat("hh:mm a");
		try {
			Date convertedDate = inputFormat.parse(dateString);
			datetime = d.format(convertedDate);

		}catch (ParseException e)
		{

		}

		return datetime;
	}


	//GETTING CURRENT DATE
	public static String getFormatedDate(String dateString){

//        dateString = "Monday 29th June 2015";
//the output format
		SimpleDateFormat simple = new SimpleDateFormat("dd MMMM yyyy");
		Date date;
		try {
			//the input format of the original date
//            date = new SimpleDateFormat("EEEE dd MMMM yyyy")
			date = new SimpleDateFormat("yyyy-MM-dd")
					.parse(dateString);
			Log.e("result date", "Date:" + simple.format(date));
			return simple.format(date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	//GETTING CURRENT DATE
	public static String getFormatedTime(String dateString){

//        dateString = "Monday 29th June 2015";
//the output format
		SimpleDateFormat simple = new SimpleDateFormat("hh:mm a");
		Date date;
		try {
			//the input format of the original date
//            date = new SimpleDateFormat("EEEE dd MMMM yyyy")
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.parse(dateString);
			Log.e("result date", "Date:" + simple.format(date));
			return simple.format(date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
				.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}


	public static void scrollToView(final ScrollView scrollView, final View view) {

		// View needs a focus
		view.requestFocus();

		// Determine if scroll needs to happen
		final Rect scrollBounds = new Rect();
		scrollView.getHitRect(scrollBounds);
		if (!view.getLocalVisibleRect(scrollBounds)) {
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					scrollView.smoothScrollTo(0, view.getBottom());
				}
			});
		}
	}
	/**
	 * to show the mandatory update from playstore
	 */
	public Dialog mandatoryUpdate(final Context context)
	{
		Dialog mandatoryFieldPopup=Utility.showPopupWithOneButton(context);
		TextView dialogText= (TextView) mandatoryFieldPopup.findViewById(R.id.text_for_popup);
		TextView title_popup= (TextView) mandatoryFieldPopup.findViewById(R.id.title_popup);
		TextView yes_button= (TextView) mandatoryFieldPopup.findViewById(R.id.yes_button);
		title_popup.setVisibility(View.GONE);
		mandatoryFieldPopup.setCancelable(false);
		dialogText.setText(context.getString(R.string.mandatory_update));
		yes_button.setText(context.getString(R.string.common_google_play_services_update_button).toUpperCase());
		yes_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Utility.isNetworkAvailable(context))
				{
					openPlaystore(context);
				}
				else
				{
					ShowAlert(context.getString(R.string.network_connection_fail), context);
				}
			}
		});
		return  mandatoryFieldPopup;
	}

}
