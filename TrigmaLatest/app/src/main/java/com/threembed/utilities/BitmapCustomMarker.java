package com.threembed.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bloomingdalelimousine.ridein.R;

/**
 * Created by embed on 15/9/16.
 */
public class BitmapCustomMarker
{
    String sname;
    static int i=1;
    Bitmap bitmap;
    Context context;
    public BitmapCustomMarker(Context context, String sname)
    {
        this.context=context;
        this.sname=sname;
        Utility.printLog("time in marker "+sname);
    }

    public Bitmap createBitmap()
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout=inflater.inflate(R.layout.custom_marker_layout, null);
        RelativeLayout lyBitmap= (RelativeLayout) layout.findViewById(R.id.map_image);
        TextView tveta= (TextView) layout.findViewById(R.id.pick_eta_on_the_way);
        tveta.setText(sname);

        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();

        layout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        layout.layout(0, 0, lyBitmap.getMeasuredWidth(), lyBitmap.getMeasuredHeight());
        bitmap =layout.getDrawingCache();
        return bitmap;
    }
}
