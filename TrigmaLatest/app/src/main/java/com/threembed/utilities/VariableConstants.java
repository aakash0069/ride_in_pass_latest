package com.threembed.utilities;

public class VariableConstants 
{
	public static final String TEMP_PHOTO_FILE_NAME = "Trigma.jpg";
	public static String PUSH_MESSAGE="";
	public static boolean isComingFromSearch=false;
	public static boolean isComingFromScroll=false;
	public static boolean CONFIRMATION_CALLED=false;
	public static boolean SEARCH_CALLED=false;
	public static String TYPE="";
	public static int COUNTER=0;
	public static int COUNTER_LATER=0;
}
