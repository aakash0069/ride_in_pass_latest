package com.threembed.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager
{
//    // Shared Preferences
//    private SharedPreferences pref;
//    // Editor for Shared preferences
//    private Editor editor;
//    // Sharedpref file name
//
//    private static final String LAT_DOW = "roadyo_lat_dow";
//    private static final String LON_DOW = "roadyo_lon_dow";
//    private static final String PREF_NAME = "roadyo_AndroidHivePref";
//    private static final String REGISTRATION_ID = "roadyo_registration_id";
//    private static final String SESSIONTOKEN= "roadyo_session_token";
//    private static final String DEVICE_ID="roadyo_dev_id";
//    private static final String IS_LOGIN = "roadyo_IsLoggedIn";
//    private static final String MY_ID="roadyo_my_id";
//    private static final String PUBNUB_CHANNEL = "roadyo_channel";
//    private static final String IS_Driver_ON_WAY = "roadyo_IsDriverOnTheWay";
//    private static final String IS_TRIP_BEGIN = "roadyo_IsDriverTripBegin";
//    private static final String INVOICE_RAISED = "roadyo_invoice_raised";
//    private static final String IS_Driver_ARRIVED = "roadyo_is_Driver_arrived";
//    private static final String IS_Driver_Cancelled_apt = "roadyo_driver_cancelled_apt";
//    private static final String Booking_Id = "roadyo_book_id";
//    private static final String Appointment_Channel = "roadyo_apt_channel";
//    private static final String Server_Channel="roadyo_server_channel";
//    private static final String DOC_NAME_DOW = "roadyo_name";
//    private static final String CAR_IMAGE="roadyo_image";
//    private static final String DOC_PH_DOW = "roadyo_ph_num";
//    private static final String PICK_ADDRESS="pickup_address";
//    private static final String DROP_ADDRESS="drop_address";
//    private static final String DOC_PIC_DOW = "roadyo_pic_url";
//    private static final String DOC_EMAIL = "roadyo_doc_email";
//    private static final String APT_DATE = "roadyo_apt_date";
//    private static final String Pickuplat="roadyo_pickuplat";
//    private static final String PickupLng="roadyo_pickuplng";
//    private static final String Dropofflat="roadyo_dropofflat";
//    private static final String Dropofflng="roadyo_dropofflng";
//    private static final String BOOKING_CANCELLED="roadyo_booking_cancelled";
//    private static final String Car_Types = "roadyo_car_types";
//    private static final String Driver_Rating="roadyo_rating";
//    private static final String Share_Link="roadyo_share_link";
//    private static final String SELECED_IMAGE="iage";
//    private static final String PRESENCE="presence";
//    private static final String CANCELLATION_TYPE="cancellation_type";
//    private static final String COMMENT="comments";
//    private static final String PUBNUB_RESPONSE="pubnub_response";
//    private static final String CURRENT_LAT="current_lat";
//    private static final String CURRENT_LONG="current_long";
//    private static final String STRIPE_KEY="stripe_key";
//    private static final String PUBLISH_KEY="publish_key";
//    private static final String SUBSCRIBE_KEY="subscribe_key";
//    private static final String SID="sid";
//    private static final String BOOKING_FROM_PUBNUB="bookingResponse";
//    private static final String MID="mid";
//    private static final String DRIVER_COMMENTS="driver_comments";
//    private static final String WALLET_BAL="wallet_bal";
//    private static final String PUBNUB_RESPONSE_FOR_NONBOOKING="non_booking";
//    private static final String PUSH_NOTIFICATION_FROM_ADMIN="notification_from_admin";
//    private static final String PAYMENT_URL="payment_url";
//    private static final String PROFILE_IMAGE="profile_image";
//    private static final String USER_NAME="userName";
//    private static final String LAST4="last4";
//    private static final String CARD_TOKEN="card_token";
//    private static final String CAR_TYPE_ID="car_type_id";
//    private static final String PROMOCODE="promocode";
//    private static final String PHONE_NUMBER="phone_number";
//    private static final String LANGUAGE="language";
//    private static final String LANGUAGE_CODE="language_code";
//    private static final String SERVER_KEY="server_key";
//
//
//    // Constructor
//    public SessionManager(Context context)
//    {
//        int PRIVATE_MODE = 0;
//        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//        editor = pref.edit();
//        editor.commit();
//    }
//    public void storeShareLink(String model)
//    {
//        editor.putString(Share_Link, model);
//        editor.commit();
//    }
//    public String getShareLink()
//    {
//        return pref.getString(Share_Link, null);
//    }
//
//    public void setPresenceChn(String model)
//    {
//        editor.putString(PRESENCE, model);
//        editor.commit();
//    }
//
//    public void storeDriverRating(String model)
//    {
//        editor.putString(Driver_Rating, model);
//        editor.commit();
//    }
//    public String getDriverRating()
//    {
//        return pref.getString(Driver_Rating, null);
//    }
//    public void storeLat_DOW(String AptDate)
//    {
//        editor.putString(LAT_DOW, AptDate);
//        editor.commit();
//    }
//    public String getLat_DOW() {
//        return pref.getString(LAT_DOW, null);
//    }
//
//    public void storeLon_DOW(String AptDate)
//    {
//        editor.putString(LON_DOW, AptDate);
//        editor.commit();
//    }
//    public String getLon_DOW()
//    {
//        return pref.getString(LON_DOW, null);
//    }
//    public void storePickuplat(String pickuplat)
//    {
//        Utility.printLog("Pickuplat in storePickuplat="+pickuplat);
//        editor.putString(Pickuplat, pickuplat);
//        editor.commit();
//    }
//    public String getPickuplat()
//    {
//        Utility.printLog("Pickuplat in getPickuplat= "+pref.getString(Pickuplat, ""));
//        return pref.getString(Pickuplat, "");
//    }
//    public void storePickuplng(String pickupLng)
//    {
//        Utility.printLog("pickupLng in storePickuplng="+pickupLng);
//        editor.putString(PickupLng, pickupLng);
//        editor.commit();
//    }
//    public String getPickuplng()
//    {
//        Utility.printLog("pickupLng in getPickuplng= "+pref.getString(PickupLng, ""));
//        return pref.getString(PickupLng, "");
//    }
//
//    public void storeDropofflat(String DropofflatLng)
//    {
//        editor.putString(Dropofflat, DropofflatLng);
//        editor.commit();
//    }
//    public String getDropofflat()
//    {
//        return pref.getString(Dropofflat, "");
//    }
//    public void storeDropofflng(String DropofflatLng)
//    {
//        editor.putString(Dropofflng, DropofflatLng);
//        editor.commit();
//    }
//    public String getDropofflng()
//    {
//        return pref.getString(Dropofflng, "");
//    }
//    public void storeAptDate(String AptDate)
//    {
//        editor.putString(APT_DATE, AptDate);
//        editor.commit();
//    }
//    public String getAptDate()
//    {
//        return pref.getString(APT_DATE, null);
//    }
//
//    public String getBookingId()
//    {
//        return pref.getString(Booking_Id, null);
//    }
//
//    public void storeBookingId(String value)
//    {
//        editor.putString(Booking_Id, value);
//        editor.commit();
//    }
//
//    public String getCurrentAptChannel()
//    {
//        return pref.getString(Appointment_Channel, "");
//    }
//
//    public void storeCurrentAptChannel(String value)
//    {
//        editor.putString(Appointment_Channel, value);
//        editor.commit();
//    }
//    public String getDriverEmail()
//    {
//        return pref.getString(DOC_EMAIL, null);
//    }
//
//    public void storeLoginId(String LoginId)
//    {
//        editor.putString(MY_ID, LoginId);
//        editor.commit();
//    }
//    public String getLoginId()
//    {
//        return pref.getString(MY_ID, null);
//    }
//
//    public void storeRegistrationId(String registrationId)
//    {
//        editor.putString(REGISTRATION_ID, registrationId);
//        editor.commit();
//    }
//    public String getRegistrationId()
//    {
//        return pref.getString(REGISTRATION_ID, null);
//    }
//
//    public void storeSessionToken(String session)
//    {
//        editor.putString(SESSIONTOKEN,session);
//        editor.commit();
//    }
//    public String getSessionToken()
//    {
//        return pref.getString(SESSIONTOKEN,null);
//    }
//
//    public void storeDeviceId(String session)
//    {
//        editor.putString(DEVICE_ID,session);
//        editor.commit();
//    }
//    public String getDeviceId()
//    {
//        return pref.getString(DEVICE_ID,"");
//    }
//    public void storeServerChannel(String session)
//    {
//        editor.putString(Server_Channel,session);
//        editor.commit();
//    }
//    public String getServerChannel()
//    {
//        return pref.getString(Server_Channel,"");
//    }
//
//    public boolean isLoggedIn()
//    {
//        return pref.getBoolean(IS_LOGIN, false);
//    }
//
//    public void setIsLogin(boolean value2)
//    {
//        editor.putBoolean(IS_LOGIN, value2);
//        editor.commit();
//    }
//    public boolean isDriverCancelledApt()
//    {
//        return pref.getBoolean(IS_Driver_Cancelled_apt, false);
//    }
//    public void setDriverCancelledApt(boolean value2)
//    {
//        editor.putBoolean(IS_Driver_Cancelled_apt, value2);
//        editor.commit();
//    }
//    public boolean isDriverOnWay()
//    {
//        Utility.printLog("Wallah is");
//        boolean flag =pref.getBoolean(IS_Driver_ON_WAY, false);
//        Utility.printLog("Wallah is"+ flag);
//        return flag;
//    }
//    public void setDriverOnWay(boolean value1)
//    {
//        Utility.printLog("Wallah set");
//        editor.putBoolean(IS_Driver_ON_WAY, value1);
//        editor.commit();
//        boolean flag =pref.getBoolean(IS_Driver_ON_WAY, false);
//        Utility.printLog("Wallah set as " + flag);
//    }
//    public boolean isTripBegin()
//    {
//        return pref.getBoolean(IS_TRIP_BEGIN, false);
//    }
//    public void setTripBegin(boolean trip)
//    {
//        editor.putBoolean(IS_TRIP_BEGIN, trip);
//        editor.commit();
//    }
//    public boolean isDriverOnArrived()
//    {
//        return pref.getBoolean(IS_Driver_ARRIVED, false);
//    }
//    public void setDriverArrived(boolean value3)
//    {
//        editor.putBoolean(IS_Driver_ARRIVED, value3);
//        editor.commit();
//    }
//    public boolean isInvoiceRaised()
//    {
//        return pref.getBoolean(INVOICE_RAISED, false);
//    }
//    public void setInvoiceRaised(boolean value4)
//    {
//        editor.putBoolean(INVOICE_RAISED, value4);
//        editor.commit();
//    }
//    public void setBookingCancelled(boolean cancel)
//    {
//        editor.putBoolean(BOOKING_CANCELLED, cancel);
//        editor.commit();
//    }
//    public void storeDocName(String name)
//    {
//        editor.putString(DOC_NAME_DOW,name);
//        editor.commit();
//    }
//    public String getDocName()
//    {
//        return pref.getString(DOC_NAME_DOW,null);
//    }
//
//    public void setCarImage(String name)
//    {
//        editor.putString(CAR_IMAGE,name);
//        editor.commit();
//    }
//    public void storeDocPH(String name) {
//        editor.putString(DOC_PH_DOW,name);
//        editor.commit();
//    }
//    public String getDocPH() {
//        return pref.getString(DOC_PH_DOW,null);
//    }
//
//    public void setPickUpAddress(String name) {
//        editor.putString(PICK_ADDRESS,name);
//        editor.commit();
//    }
//    public String getPickUpAddress() {
//        return pref.getString(PICK_ADDRESS,"");
//    }
//
//    public void setSelectedImage(String name) {
//        editor.putString(SELECED_IMAGE,name);
//        editor.commit();
//    }
//    public String getSelectedImage() {
//        return pref.getString(SELECED_IMAGE,"");
//    }
//
//
//    public String getDropAddress() {
//        return pref.getString(DROP_ADDRESS,"");
//    }
//
//    public void storeDocPic(String name) {
//        editor.putString(DOC_PIC_DOW,name);
//        editor.commit();
//    }
//    public String getDocPic() {
//        return pref.getString(DOC_PIC_DOW,null);
//    }
//
//    public void storeChannelName(String channel) {
//        editor.putString(PUBNUB_CHANNEL,channel);
//        editor.commit();
//    }
//    public String getChannelName() {
//        return pref.getString(PUBNUB_CHANNEL,"");
//    }
//
//    public void storeCarTypes(String name)
//    {
//        editor.putString(Car_Types,name);
//        editor.commit();
//    }
//
//    public String getLatitude()
//    {
//        return pref.getString(SESSIONTOKEN,null);
//    }
//
//    public String getCancellationType()
//    {
//        return pref.getString(CANCELLATION_TYPE,"");
//    }
//
//    public void setCancellationType(String name)
//    {
//        editor.putString(CANCELLATION_TYPE, name);
//        editor.commit();
//    }
//
//    public void setComments(String name)
//    {
//        editor.putString(COMMENT, name);
//        editor.commit();
//    }
//    public void setPubnubResponse(String pubnubResponse)
//    {
//        editor.putString(PUBNUB_RESPONSE, pubnubResponse);
//        editor.commit();
//    }
//    public String getPubnubResponse()
//    {
//        return pref.getString(PUBNUB_RESPONSE,"");
//    }
//
//    public void setCurrentLat(float currentLat)
//    {
//        editor.putFloat(CURRENT_LAT, currentLat);
//        editor.commit();
//    }
//    public float getCurrentLat()
//    {
//        return pref.getFloat(CURRENT_LAT,0);
//    }
//
//    public void setCurrentLong(float currentLong)
//    {
//        editor.putFloat(CURRENT_LONG, currentLong);
//        editor.commit();
//    }
//    public float getCurrentLong()
//    {
//        return pref.getFloat(CURRENT_LONG,0);
//    }
//
//    public void setStripeKey(String stripeKey)
//    {
//        editor.putString(STRIPE_KEY, stripeKey);
//        editor.commit();
//    }
//    public void setPublishKey(String publishKey)
//    {
//        editor.putString(PUBLISH_KEY, publishKey);
//        editor.commit();
//    }
//    public String getPublishKey()
//    {
//        return pref.getString(PUBLISH_KEY,"");
//    }
//    public void setSubscribeKey(String subscribeKey)
//    {
//        editor.putString(SUBSCRIBE_KEY, subscribeKey);
//        editor.commit();
//    }
//    public String getSubscribeKey()
//    {
//        return pref.getString(SUBSCRIBE_KEY,"");
//    }
//
//    public void setSid(String sid)
//    {
//        editor.putString(SID, sid);
//        editor.commit();
//    }
//    public String getSid()
//    {
//        return pref.getString(SID, "");
//    }
//
//    public void setBookingReponseFromPubnub(String bookingReponseFromPubnub)
//    {
//        editor.putString(BOOKING_FROM_PUBNUB, bookingReponseFromPubnub);
//        editor.commit();
//    }
//    public String getBookingReponseFromPubnub()
//    {
//        return pref.getString(BOOKING_FROM_PUBNUB, "");
//    }
//
//    public void setMid(String mid)
//    {
//        editor.putString(MID, mid);
//        editor.commit();
//    }
//    public String getMid()
//    {
//        return pref.getString(MID, "");
//    }
//
//    public void setPubnubResponseForNonBooking(String nonBooking)
//    {
//        editor.putString(PUBNUB_RESPONSE_FOR_NONBOOKING, nonBooking);
//        editor.commit();
//    }
//    public String getPubnubResponseForNonbooking()
//    {
//        return pref.getString(PUBNUB_RESPONSE_FOR_NONBOOKING, "");
//    }
//    public void isPushNotificationFromAdmin(boolean notification)
//    {
//        editor.putBoolean(PUSH_NOTIFICATION_FROM_ADMIN, notification);
//        editor.commit();
//    }
//    public boolean getNotificationFromAdmin()
//    {
//        return pref.getBoolean(PUSH_NOTIFICATION_FROM_ADMIN, false);
//    }
//
//    public void setDriverCooments(String driverComments)
//    {
//        editor.putString(DRIVER_COMMENTS, driverComments);
//        editor.commit();
//    }
//    public String getDriverCooments()
//    {
//        return pref.getString(DRIVER_COMMENTS, "");
//    }
//
//    public void setWalletBal(String walletBal)
//    {
//        editor.putString(WALLET_BAL, walletBal);
//        editor.commit();
//    }
//    public String getWalletBal()
//    {
//        return pref.getString(WALLET_BAL, "");
//    }
//    public void setPaymentUrl(String paymentUrl)
//    {
//        editor.putString(PAYMENT_URL, paymentUrl);
//        editor.commit();
//    }
//    public String getPaymentUrl()
//    {
//        return pref.getString(PAYMENT_URL, "");
//    }
//
//    public void setProfileImage(String profileImage)
//    {
//        editor.putString(PROFILE_IMAGE, profileImage);
//        editor.commit();
//    }
//    public void setLast4Digits(String model)
//    {
//        editor.putString(LAST4, model);
//        editor.commit();
//    }
//    public String getLast4()
//    {
//        return pref.getString(LAST4, "");
//    }
//    public void setCardToken(String model)
//    {
//        editor.putString(CARD_TOKEN, model);
//        editor.commit();
//    }
//    public String getCardToken()
//    {
//        return pref.getString(CARD_TOKEN, "");
//    }
//    public String getProfileImage()
//    {
//        return pref.getString(PROFILE_IMAGE, "");
//    }
//    public void setUserName(String userName)
//    {
//        editor.putString(USER_NAME, userName);
//        editor.commit();
//    }
//    public String getUserName()
//    {
//        return pref.getString(USER_NAME, "");
//    }
//    public void setCarTypeId(String carTypeId)
//    {
//        editor.putString(CAR_TYPE_ID, carTypeId);
//        editor.commit();
//    }
//    public String getCarTypeId()
//    {
//        return pref.getString(CAR_TYPE_ID, "");
//    }
//    public void setPromocode(String promocode)
//    {
//        editor.putString(PROMOCODE, promocode);
//        editor.commit();
//    }
//    public String getPromocode()
//    {
//        return pref.getString(PROMOCODE, "");
//    }
//
//    public void setPhoneNumber(String phoneNumber)
//    {
//        editor.putString(PHONE_NUMBER, phoneNumber);
//        editor.commit();
//    }
//    public String getPhoneNumber()
//    {
//        return pref.getString(PHONE_NUMBER, "");
//    }
//
//    public void setLanguage(String model)
//    {
//        editor.putString(LANGUAGE, model);
//        editor.commit();
//    }
//
//    public String getLanguage()
//    {
//        return pref.getString(LANGUAGE, "");
//    }
//
//    public void setLanguageCode(String languageCode)
//    {
//        editor.putString(LANGUAGE_CODE, languageCode);
//        editor.commit();
//    }
//    public String getLanguageCode()
//    {
//        return pref.getString(LANGUAGE_CODE, "");
//    }
//
//    public void setServerKey(String languageCode)
//    {
//        editor.putString(SERVER_KEY, languageCode);
//        editor.commit();
//    }
//    public String getServerKey()
//    {
//        return pref.getString(SERVER_KEY, "");
//    }


    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private Editor editor;
    // Sharedpref file name

    private static final String LAT_DOW = "roadyo_lat_dow";
    private static final String LON_DOW = "roadyo_lon_dow";
    private static final String PREF_NAME = "roadyo_AndroidHivePref";
    private static final String REGISTRATION_ID = "roadyo_registration_id";
    private static final String SESSIONTOKEN= "roadyo_session_token";
    private static final String DEVICE_ID="roadyo_dev_id";
    private static final String IS_LOGIN = "roadyo_IsLoggedIn";
    private static final String MY_ID="roadyo_my_id";
    private static final String PUBNUB_CHANNEL = "roadyo_channel";
    private static final String IS_Driver_ON_WAY = "roadyo_IsDriverOnTheWay";
    private static final String IS_TRIP_BEGIN = "roadyo_IsDriverTripBegin";
    private static final String INVOICE_RAISED = "roadyo_invoice_raised";
    private static final String IS_Driver_ARRIVED = "roadyo_is_Driver_arrived";
    private static final String Booking_Id = "roadyo_book_id";
    private static final String Server_Channel="roadyo_server_channel";
    private static final String DOC_NAME_DOW = "roadyo_name";
    private static final String CAR_IMAGE="roadyo_image";
    private static final String DOC_PH_DOW = "roadyo_ph_num";
    private static final String PICK_ADDRESS="pickup_address";
    private static final String DROP_ADDRESS="drop_address";
    private static final String DOC_PIC_DOW = "roadyo_pic_url";
    private static final String DOC_EMAIL = "roadyo_doc_email";
    private static final String APT_DATE = "roadyo_apt_date";
    private static final String Pickuplat="roadyo_pickuplat";
    private static final String PickupLng="roadyo_pickuplng";
    private static final String Dropofflat="roadyo_dropofflat";
    private static final String Dropofflng="roadyo_dropofflng";
    private static final String BOOKING_CANCELLED="roadyo_booking_cancelled";
    private static final String Driver_Rating="roadyo_rating";
    private static final String Share_Link="roadyo_share_link";
    private static final String SELECED_IMAGE="iage";
    private static final String PRESENCE="presence";
    private static final String CANCELLATION_TYPE="cancellation_type";
    private static final String COMMENT="comments";
    private static final String PUBNUB_RESPONSE="pubnub_response";
    private static final String CURRENT_LAT="current_lat";
    private static final String CURRENT_LONG="current_long";
    private static final String STRIPE_KEY="stripe_key";
    private static final String PUBLISH_KEY="publish_key";
    private static final String SUBSCRIBE_KEY="subscribe_key";
    private static final String SID="sid";
    private static final String BOOKING_FROM_PUBNUB="bookingResponse";
    private static final String MID="mid";
    private static final String DRIVER_COMMENTS="driver_comments";
    private static final String WALLET_BAL="wallet_bal";
    private static final String PUBNUB_RESPONSE_FOR_NONBOOKING="non_booking";
    private static final String PUSH_NOTIFICATION_FROM_ADMIN="notification_from_admin";
    private static final String PAYMENT_URL="payment_url";
    private static final String PROFILE_IMAGE="profile_image";
    private static final String USER_NAME="userName";
    private static final String LAST4="last4";
    private static final String CARD_TOKEN="card_token";
    private static final String CAR_TYPE_ID="car_type_id";
    private static final String PROMOCODE="promocode";
    private static final String PHONE_NUMBER="phone_number";
    private static final String LANGUAGE="language";
    private static final String LANGUAGE_CODE="language_code";
    private static final String SERVER_KEY="server_key";
    private static final String PUBNUB_INTERVAL_HOME="pubnub_interval_home";
    private static final String PUBNUB_INTERVAL_DISPATCHING="pubnub_interval_dispatching";
    private static final String GOOGLE_MATRIX_KEYS="google_matrix_keys";
    private static final String ETA_INTERVAL_FOR_TRACKING="eta_interval_for_tracking";
    private static final String PAYMENT_TYPE="PaymentType";
    private static final String Car_Types = "roadyo_car_types";
    private static final String IS_Driver_Cancelled_apt = "roadyo_driver_cancelled_apt";
    private static final String ALL_BUSY_PUSH = "all_busy";
    private static final String EMAIL_ID = "email_id";

    // Constructor
    public SessionManager(Context context)
    {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
    }
    public void storeShareLink(String model)
    {
        editor.putString(Share_Link, model);
        editor.commit();
    }
    public String getShareLink()
    {
        return pref.getString(Share_Link, null);
    }

    public void setPresenceChn(String model)
    {
        editor.putString(PRESENCE, model);
        editor.commit();
    }

    public void storeDriverRating(String model)
    {
        editor.putString(Driver_Rating, model);
        editor.commit();
    }
    public String getDriverRating()
    {
        return pref.getString(Driver_Rating, null);
    }
    public void storeLat_DOW(String AptDate)
    {
        editor.putString(LAT_DOW, AptDate);
        editor.commit();
    }
    public String getLat_DOW() {
        return pref.getString(LAT_DOW, null);
    }

    public void storeLon_DOW(String AptDate)
    {
        editor.putString(LON_DOW, AptDate);
        editor.commit();
    }
    public String getLon_DOW()
    {
        return pref.getString(LON_DOW, null);
    }
    public void storePickuplat(String pickuplat)
    {
        Utility.printLog("Pickuplat in storePickuplat="+pickuplat);
        editor.putString(Pickuplat, pickuplat);
        editor.commit();
    }
    public String getPickuplat()
    {
        Utility.printLog("Pickuplat in getPickuplat= "+pref.getString(Pickuplat, ""));
        return pref.getString(Pickuplat, "");
    }
    public void storePickuplng(String pickupLng)
    {
        Utility.printLog("pickupLng in storePickuplng="+pickupLng);
        editor.putString(PickupLng, pickupLng);
        editor.commit();
    }
    public String getPickuplng()
    {
        Utility.printLog("pickupLng in getPickuplng= "+pref.getString(PickupLng, ""));
        return pref.getString(PickupLng, "");
    }

    public void storeDropofflat(String DropofflatLng)
    {
        editor.putString(Dropofflat, DropofflatLng);
        editor.commit();
    }
    public String getDropofflat()
    {
        return pref.getString(Dropofflat, "");
    }
    public void storeDropofflng(String DropofflatLng)
    {
        editor.putString(Dropofflng, DropofflatLng);
        editor.commit();
    }
    public String getDropofflng()
    {
        return pref.getString(Dropofflng, "");
    }
    public void storeAptDate(String AptDate)
    {
        editor.putString(APT_DATE, AptDate);
        editor.commit();
    }
    public String getBookingId()
    {
        return pref.getString(Booking_Id, "");
    }

    public void storeBookingId(String value)
    {
        editor.putString(Booking_Id, value);
        editor.commit();
    }

    public String getDriverEmail()
    {
        return pref.getString(DOC_EMAIL, null);
    }

    public void storeLoginId(String LoginId)
    {
        editor.putString(MY_ID, LoginId);
        editor.commit();
    }
    public String getLoginId()
    {
        return pref.getString(MY_ID, null);
    }

    public void storeRegistrationId(String registrationId)
    {
        editor.putString(REGISTRATION_ID, registrationId);
        editor.commit();
    }
    public String getRegistrationId()
    {
        return pref.getString(REGISTRATION_ID, null);
    }

    public void storeSessionToken(String session)
    {
        editor.putString(SESSIONTOKEN,session);
        editor.commit();
    }
    public String getSessionToken()
    {
        return pref.getString(SESSIONTOKEN,null);
    }

    public void storeDeviceId(String session)
    {
        editor.putString(DEVICE_ID,session);
        editor.commit();
    }
    public String getDeviceId()
    {
        return pref.getString(DEVICE_ID,"");
    }
    public void storeServerChannel(String session)
    {
        editor.putString(Server_Channel,session);
        editor.commit();
    }
    public String getServerChannel()
    {
        return pref.getString(Server_Channel,"");
    }

    public boolean isLoggedIn()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void setIsLogin(boolean value2)
    {
        editor.putBoolean(IS_LOGIN, value2);
        editor.commit();
    }
    public boolean isDriverOnWay()
    {
        Utility.printLog("Wallah is");
        boolean flag =pref.getBoolean(IS_Driver_ON_WAY, false);
        Utility.printLog("Wallah is"+ flag);
        return flag;
    }
    public void setDriverOnWay(boolean value1)
    {
        Utility.printLog("Wallah set");
        editor.putBoolean(IS_Driver_ON_WAY, value1);
        editor.commit();
        boolean flag =pref.getBoolean(IS_Driver_ON_WAY, false);
        Utility.printLog("Wallah set as " + flag);
    }
    public boolean isTripBegin()
    {
        return pref.getBoolean(IS_TRIP_BEGIN, false);
    }
    public void setTripBegin(boolean trip)
    {
        editor.putBoolean(IS_TRIP_BEGIN, trip);
        editor.commit();
    }
    public boolean isDriverOnArrived()
    {
        return pref.getBoolean(IS_Driver_ARRIVED, false);
    }
    public void setDriverArrived(boolean value3)
    {
        editor.putBoolean(IS_Driver_ARRIVED, value3);
        editor.commit();
    }
    public boolean isInvoiceRaised()
    {
        return pref.getBoolean(INVOICE_RAISED, false);
    }
    public void setInvoiceRaised(boolean value4)
    {
        editor.putBoolean(INVOICE_RAISED, value4);
        editor.commit();
    }
    public void setBookingCancelled(boolean cancel)
    {
        editor.putBoolean(BOOKING_CANCELLED, cancel);
        editor.commit();
    }
    public void storeDocName(String name)
    {
        editor.putString(DOC_NAME_DOW,name);
        editor.commit();
    }
    public String getDocName()
    {
        return pref.getString(DOC_NAME_DOW,null);
    }

    public void setCarImage(String name)
    {
        editor.putString(CAR_IMAGE,name);
        editor.commit();
    }
    public void storeDocPH(String name) {
        editor.putString(DOC_PH_DOW,name);
        editor.commit();
    }
    public String getDocPH() {
        return pref.getString(DOC_PH_DOW,null);
    }

    public void setPickUpAddress(String name) {
        editor.putString(PICK_ADDRESS,name);
        editor.commit();
    }
    public String getPickUpAddress() {
        return pref.getString(PICK_ADDRESS,"");
    }

    public void setSelectedImage(String name) {
        editor.putString(SELECED_IMAGE,name);
        editor.commit();
    }
    public String getSelectedImage() {
        return pref.getString(SELECED_IMAGE,"");
    }


    public String getDropAddress() {
        return pref.getString(DROP_ADDRESS,"");
    }

    public void storeDocPic(String name) {
        editor.putString(DOC_PIC_DOW,name);
        editor.commit();
    }
    public String getDocPic() {
        return pref.getString(DOC_PIC_DOW,null);
    }

    public void storeChannelName(String channel) {
        editor.putString(PUBNUB_CHANNEL,channel);
        editor.commit();
    }
    public String getChannelName() {
        return pref.getString(PUBNUB_CHANNEL,"");
    }

    public String getLatitude()
    {
        return pref.getString(SESSIONTOKEN,null);
    }

    public String getCancellationType()
    {
        return pref.getString(CANCELLATION_TYPE,"");
    }

    public void setCancellationType(String name)
    {
        editor.putString(CANCELLATION_TYPE, name);
        editor.commit();
    }

    public void setComments(String name)
    {
        editor.putString(COMMENT, name);
        editor.commit();
    }
    public void setPubnubResponse(String pubnubResponse)
    {
        editor.putString(PUBNUB_RESPONSE, pubnubResponse);
        editor.commit();
    }
    public String getPubnubResponse()
    {
        return pref.getString(PUBNUB_RESPONSE,"");
    }

    public void setCurrentLat(float currentLat)
    {
        editor.putFloat(CURRENT_LAT, currentLat);
        editor.commit();
    }
    public float getCurrentLat()
    {
        return pref.getFloat(CURRENT_LAT,0);
    }

    public void setCurrentLong(float currentLong)
    {
        editor.putFloat(CURRENT_LONG, currentLong);
        editor.commit();
    }
    public float getCurrentLong()
    {
        return pref.getFloat(CURRENT_LONG,0);
    }

    public String getStripeKey()
    {
        return pref.getString(STRIPE_KEY, "");
    }

    public void setStripeKey(String stripeKey)
    {
        editor.putString(STRIPE_KEY, stripeKey);
        editor.commit();
    }
    public void setPublishKey(String publishKey)
    {
        editor.putString(PUBLISH_KEY, publishKey);
        editor.commit();
    }
    public String getPublishKey()
    {
        return pref.getString(PUBLISH_KEY,"");
    }
    public void setSubscribeKey(String subscribeKey)
    {
        editor.putString(SUBSCRIBE_KEY, subscribeKey);
        editor.commit();
    }
    public String getSubscribeKey()
    {
        return pref.getString(SUBSCRIBE_KEY,"");
    }

    public void setSid(String sid)
    {
        editor.putString(SID, sid);
        editor.commit();
    }
    public String getSid()
    {
        return pref.getString(SID, "");
    }

    public void setBookingReponseFromPubnub(String bookingReponseFromPubnub)
    {
        editor.putString(BOOKING_FROM_PUBNUB, bookingReponseFromPubnub);
        editor.commit();
    }
    public String getBookingReponseFromPubnub()
    {
        return pref.getString(BOOKING_FROM_PUBNUB, "");
    }

    public void setMid(String mid)
    {
        editor.putString(MID, mid);
        editor.commit();
    }
    public String getMid()
    {
        return pref.getString(MID, "");
    }

    public void setPubnubResponseForNonBooking(String nonBooking)
    {
        editor.putString(PUBNUB_RESPONSE_FOR_NONBOOKING, nonBooking);
        editor.commit();
    }
    public String getPubnubResponseForNonbooking()
    {
        return pref.getString(PUBNUB_RESPONSE_FOR_NONBOOKING, "");
    }
    public void isPushNotificationFromAdmin(boolean notification)
    {
        editor.putBoolean(PUSH_NOTIFICATION_FROM_ADMIN, notification);
        editor.commit();
    }
    public boolean getNotificationFromAdmin()
    {
        return pref.getBoolean(PUSH_NOTIFICATION_FROM_ADMIN, false);
    }

    public void setDriverCooments(String driverComments)
    {
        editor.putString(DRIVER_COMMENTS, driverComments);
        editor.commit();
    }
    public String getDriverCooments()
    {
        return pref.getString(DRIVER_COMMENTS, "");
    }

    public void setWalletBal(String walletBal)
    {
        editor.putString(WALLET_BAL, walletBal);
        editor.commit();
    }
    public String getWalletBal()
    {
        return pref.getString(WALLET_BAL, "");
    }
    public void setPaymentUrl(String paymentUrl)
    {
        editor.putString(PAYMENT_URL, paymentUrl);
        editor.commit();
    }
    public String getPaymentUrl()
    {
        return pref.getString(PAYMENT_URL, "");
    }

    public void setProfileImage(String profileImage)
    {
        editor.putString(PROFILE_IMAGE, profileImage);
        editor.commit();
    }
    public void setLast4Digits(String model)
    {
        editor.putString(LAST4, model);
        editor.commit();
    }
    public String getLast4()
    {
        return pref.getString(LAST4, "");
    }
    public void setCardToken(String model)
    {
        editor.putString(CARD_TOKEN, model);
        editor.commit();
    }
    public String getCardToken()
    {
        return pref.getString(CARD_TOKEN, "");
    }
    public String getProfileImage()
    {
        return pref.getString(PROFILE_IMAGE, "");
    }
    public void setUserName(String userName)
    {
        editor.putString(USER_NAME, userName);
        editor.commit();
    }
    public String getUserName()
    {
        return pref.getString(USER_NAME, "");
    }
    public void setCarTypeId(String carTypeId)
    {
        editor.putString(CAR_TYPE_ID, carTypeId);
        editor.commit();
    }
    public String getCarTypeId()
    {
        return pref.getString(CAR_TYPE_ID, "");
    }
    public void setPromocode(String promocode)
    {
        editor.putString(PROMOCODE, promocode);
        editor.commit();
    }
    public String getPromocode()
    {
        return pref.getString(PROMOCODE, "");
    }

    public void setPhoneNumber(String phoneNumber)
    {
        editor.putString(PHONE_NUMBER, phoneNumber);
        editor.commit();
    }
    public String getPhoneNumber()
    {
        return pref.getString(PHONE_NUMBER, "");
    }

    public void setLanguage(String model)
    {
        editor.putString(LANGUAGE, model);
        editor.commit();
    }

    public String getLanguage()
    {
        return pref.getString(LANGUAGE, "");
    }

    public void setLanguageCode(String languageCode)
    {
        editor.putString(LANGUAGE_CODE, languageCode);
        editor.commit();
    }
    public String getLanguageCode()
    {
        return pref.getString(LANGUAGE_CODE, "");
    }
    public void setServerKey(String languageCode)
    {
        editor.putString(SERVER_KEY, languageCode);
        editor.commit();
    }
    public String getServerKey()
    {
        return pref.getString(SERVER_KEY, "");
    }
    public void setPubnubIntervalForHome(long pubnubIntervalForHome)
    {
        editor.putLong(PUBNUB_INTERVAL_HOME, pubnubIntervalForHome);
        editor.commit();
    }
    public long getPubnubIntervalForHome()
    {
        return pref.getLong(PUBNUB_INTERVAL_HOME, (long) 5.0);
    }
    public void setGoogleMatrixData(String googleMatrixData)
    {
        editor.putString(GOOGLE_MATRIX_KEYS, googleMatrixData);
        editor.commit();
    }
    public String getGoogleMatrixData()
    {
        return pref.getString(GOOGLE_MATRIX_KEYS,  "");
    }
    public void setEtaIntervalForTracking(long etaIntervalForTracking)
    {
        editor.putLong(ETA_INTERVAL_FOR_TRACKING, etaIntervalForTracking);
        editor.commit();
    }
    public long getEtaIntervalForTracking()
    {
        return pref.getLong(ETA_INTERVAL_FOR_TRACKING, (long) 0.0);
    }

    public void setPaymentType(String paymentType)
    {
        editor.putString(PAYMENT_TYPE, paymentType);
        editor.commit();
    }
    public String getPaymentType()
    {
        return pref.getString(PAYMENT_TYPE, "2");
    }
    public void setDriverCancelledApt(boolean value2)
    {
        editor.putBoolean(IS_Driver_Cancelled_apt, value2);
        editor.commit();
    }
    public void setAllBusyPushMessage(String allBusyPushMessage)
    {
        editor.putString(ALL_BUSY_PUSH, allBusyPushMessage);
        editor.commit();
    }
    public String getAllBusyPushMessage()
    {
        return pref.getString(ALL_BUSY_PUSH, "");
    }
    public void setEmailId(String emailId)
    {
        editor.putString(EMAIL_ID, emailId);
        editor.commit();
    }
    public String getEmailId()
    {
        return pref.getString(EMAIL_ID,  "");
    }
}
