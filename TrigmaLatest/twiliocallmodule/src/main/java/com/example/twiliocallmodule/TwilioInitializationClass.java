package com.example.twiliocallmodule;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.twilio.client.Twilio;
/**
 * Created by embed on 6/6/17.
 */
public class TwilioInitializationClass {
    public void initializeTwilioClientSDK(final Activity activity) {

        if (!Twilio.isInitialized()) {
            Twilio.initialize(activity, new Twilio.InitListener() {

                /*
                 * Now that the SDK is initialized we can register using a Capability Token.
                 * A Capability Token is a JSON Web Token (JWT) that specifies how an associated Device
                 * can interact with Twilio services.
                 */
                @Override
                public void onInitialized() {
                    Twilio.setLogLevel(Log.DEBUG);
                }

                @Override
                public void onError(Exception e) {
                    Log.d("","Twilio Activity exception "+e);
//					Toast.makeText(MainActivity.this, "Failed to initialize the Twilio Client SDK", Toast.LENGTH_LONG).show();
//					Toast.makeText(MainActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
